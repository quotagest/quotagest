<?php
	#if (!isset($_SESSION))
	if (session_id() === "")
	{
	session_start();
	}

	/* FOLDER VARS */
	require_once($_SERVER['DOCUMENT_ROOT']."/configs/dir-vars.php");

	if(isset($_SESSION['id_user']))
	{
		$id_user 	= $_SESSION['id_user'];
		$id_assoc 	= $_SESSION['id_assoc'];
		$nome_assoc = $_SESSION['nome_assoc'];
		$nome_user 	= $_SESSION['nome_user'];
		$user_login = $_SESSION['user_login'];
		if(isset($_SESSION['img_assoc']))
		{
			$img_assoc = substr($_SESSION['img_assoc'], strripos($_SESSION['img_assoc'],'/'));
		}
		else
		{
			$img_assoc = "quotagest-logo.png";
		}
		
		$DO_LOGIN 	 = false;
		$INLOGINPAGE = false;
		$SHOWMENU 	 = true;
		$NEW_ACCOUNT = false;
	}
	else
	{
		$DO_LOGIN	= (!isset($_SESSION['id_user']));
		$INLOGINPAGE= (stripos($_SERVER['PHP_SELF'],'login.php') !== false);
		$SHOWMENU	= true;
		$NEW_ACCOUNT= (stripos($_SERVER['PHP_SELF'],'create-beta.php') !== false);

		$id_user	= 0;
		$id_assoc   = 0;
		$nome_assoc = '';
		$nome_user  = '';
		$user_login = '';
		$img_assoc  = 'quotagest-logo.png';
	}
?>