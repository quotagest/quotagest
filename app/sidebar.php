<ul class="left-menu">
<?php
	if(isset($arrLeftMenu))
	{
		foreach($arrLeftMenu as $item)
		{
			$item['href'] = (isset($item['javascript'])) ? "javascript:".$item['javascript'] : $item['href'];

			echo '<li><a class="item" href="'.$item['href'].'" >'.$item['titulo'].'</a></li>';
		}
	}
?>
</ul>
