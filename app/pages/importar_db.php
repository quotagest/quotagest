<html>
	<head>
		<title></title>
		<script src="../neo/js/ajax.js"></script>
		<script src="../neo/js/main.js"></script>
		<script type="text/javascript">
			//http://www.htmlgoodies.com/beyond/javascript/read-text-files-using-the-javascript-filereader.html#fbid=6VHQ1urPdtl
			var ObjAjax = new TObjAjax();
			
			function readSingleFile(evt)
			{
				var f = evt.target.files[0]; 
				if (f)
				{
					var r = new FileReader();
					
					r.onload = function(e)
					{ 
						var contents = e.target.result;
						var Result   = getDataValues(contents);
						
						var dateNow 					= getCurrentTime('Y-m-d h:m:s');
						var Socio_FieldsNewOrder 		= ['NULL',2,3,4,5,6,false,false,7,9,11,14,13,12,false,false];
						var AssocSocio_FieldsNewOrder 	= ['NULL','ID_ASSOC','ID_INSERT',false,false,1,false,16,17,false,'1','0','ID_USER','ID_USER',dateNow,dateNow,10];
						var Entidade_FNO 				= ['NULL',1,2,10,false,3,4,5,6,false,false,false,8,9,'ID_USER','ID_USER',dateNow,dateNow,'0'];
						var AssocEntidade_FNO 			= ['NULL','ID_ASSOC','ID_INSERT','0',false,'ID_USER','ID_USER',dateNow,dateNow,'0'];
						var PagForm_FNO 				= ['NULL','ID_ASSOC',1,'ID_USER','ID_USER',dateNow,dateNow,'0'];
						var Modalidade_FNO 				= ['NULL','ID_ASSOC',2,'ID_USER','ID_USER',dateNow,dateNow,'0'];
						var TipoSocio_FNO 				= ['NULL','ID_ASSOC',1,'0.00','ID_USER','ID_USER',dateNow,dateNow,'0'];
						var objData = {};
							objData.socio 		= processTable_NewValuesOrder(Result.socio,Socio_FieldsNewOrder);
							objData.assocsocio 	= processTable_NewValuesOrder(Result.socio,AssocSocio_FieldsNewOrder);
							objData.entidade 	= processTable_NewValuesOrder(Result.entidade,Entidade_FNO);
							objData.pagopcao 	= processTable_NewValuesOrder(Result.pagopcao,PagForm_FNO);
							objData.pagforma 	= processTable_NewValuesOrder(Result.pagforma,PagForm_FNO);
							objData.modalidade 	= processTable_NewValuesOrder(Result.modalidade,Modalidade_FNO);
							objData.tiposocio 	= processTable_NewValuesOrder(Result.tiposocio,TipoSocio_FNO);
						var jsonData = JSON.stringify(objData);
						console.log(objData);
						
						var fCallback = function(sResult)
						{
							jsonObj = JSON.parse(sResult);
							jsonData = jsonData.replace(/ID_ASSOC/gi,jsonObj.id_assoc);
							jsonData = jsonData.replace(/ID_USER/gi,jsonObj.id_user);
							
							console.log(JSON.parse(jsonData));
						}
						//var params = 'jsondata='+jsonData;
						var params = 'email='+document.forms['uploadtodb'].elements['email'].value;
						ObjAjax.Ajax({type:'POST', url:'/pages/getRoot.php', data:params, done:fCallback});
					}
					r.readAsText(f);
				}
				else
				{
					alert("Failed to load file");
				}
			}
			function getDataValues(AsText)
			{
				var tblTipoSocio 	= getValuesArray_TABLE('socio_tipo',AsText);
				var tblSocio 		= getValuesArray_TABLE('socio',AsText);
				var tblEntidade 	= getValuesArray_TABLE('entidade',AsText);
				var tblModalidade 	= getValuesArray_TABLE('modalidade',AsText);
				var tblAssocCargos 	= getValuesArray_TABLE('associacao_cargos',AsText);
				var tblPagOpcao 	= getValuesArray_TABLE('pagamento_opcao',AsText);
				var tblPagForma 	= getValuesArray_TABLE('pagamento_tipo',AsText);
				
				return {socio: 		tblSocio,
						tiposocio: 	tblTipoSocio,
						entidade: 	tblEntidade,
						modalidade: tblModalidade,
						assoccargos:tblAssocCargos,
						pagopcao: 	tblPagOpcao,
						pagforma: 	tblPagForma};
			}
			function getValuesArray_TABLE(AsTable,AsText)
			{
				//var sIni 	= "/*!40000 ALTER TABLE `"+AsTable+"` DISABLE KEYS */;";
				var sIni = "INSERT INTO `"+AsTable+"` VALUES (";
				var sEnd = "/*!40000 ALTER TABLE `"+AsTable+"` ENABLE KEYS */;";
				
				var iTblLen 	= AsTable.length;
				var iIniLen 	= 23+iTblLen; //sIni.length;
				var iEndLen 	= 39+iTblLen; //sEnd.length;
				var pos1 		= AsText.indexOf(sIni);
				var pos2 		= AsText.indexOf(sEnd);
				
				if (pos1 != false)
				{
					var sSubText = AsText.substr(pos1+iIniLen,(pos2-4)-(pos1+iIniLen));
			 			sSubText = sSubText.replace(/, /gi,'#');
					return splitTableFields(sSubText.split("),("));
				}
				return new Array();
			}
			function splitTableFields(AarrTable)
			{
				for (var i=0, ccI=AarrTable.length; i<ccI; ++i)
				{
					AarrTable[i] = AarrTable[i].split(',');
				}
				return AarrTable;
			}
			
			function processTable_NewValuesOrder(AarrTable,AarrNewOrder)
			{
				if (AarrTable.length > 0)
				{
					var arrAux = {};
					var ccI = AarrTable.length;
					var ccJ = AarrNewOrder.length;
					var sValue = '';
					var indx = '';
					var i=0, j=0;
					
					for (i=0; i<ccI; ++i)
					{
						arrAux[i] = {};
						for (j=0; j<ccJ; ++j)
						{
							indx = AarrNewOrder[j];
							sValue = ((indx === false) || (indx === '')) ? '' : indx;
							if (typeof sValue === 'number')
							{
								sValue = AarrTable[i][indx];
							}
							if ((sValue !== '') && (sValue !== 'NULL'))
							{
								sValue = sValue.replace(/#/gi, ", ");
								sValue = sValue.replace(/\\'/gi, "#");
								sValue = sValue.replace(/'/gi, "");
								sValue = sValue.replace(/#/gi, "\\'");
								sValue = Trim(sValue);
							}
							arrAux[i][j] = sValue;
						}
					}
					return arrAux;
				}
				return false;
			}
		</script>
	</head>
	<body>
		<form name="uploadtodb" action="" method="POST">
			<label>Email</label>
			<input type="text" name="email" placeholder="Email/Login Utilizador" />
			<br/>
			<label>Ficheiro</label>
			<input type="file" name="file" onchange="readSingleFile(event);"/>
			<br/>
			<button type="submit" value="Importar Dados">Importar Dados</button>
		</form>
	</body>
</html>