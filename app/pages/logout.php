<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/configs/dir-vars.php");

	session_start();
		unset($_SESSION['id_user']);
		unset($_SESSION['id_assoc']);
		unset($_SESSION['nome_assoc']);
		unset($_SESSION['nome_user']);
		unset($_SESSION['user_login']);	
		unset($_SESSION['img_assoc']);	
	session_unset();
	session_destroy();

	header('location: '.SETPATH('URL','PATH_APP'));
	exit();
?>