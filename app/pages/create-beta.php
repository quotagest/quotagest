<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	$titulo = 'Criar Conta | QuotaGest';
	$DO_LOGIN 	 = false;
	$INLOGINPAGE = true;
	$SHOWMENU	 = false;
	$NEW_ACCOUNT = true;
	require_once($_SERVER['DOCUMENT_ROOT'].'/header.php');

	$EMAIL_TESTER = (isset($_GET['email'])) ? $_GET['email'] : '';
	if (isset($_POST['email-teste']) &&
		#isset($_POST['login-teste']) &&
		isset($_POST['password-teste']) &&
		isset($_POST['password-confirm']) &&
		isset($_POST['nome-teste']) &&
		isset($_POST['apelido-teste'])
		)
	{
		if (($_POST['password-teste'] === $_POST['password-confirm']) && 
			($_POST['password-teste'] !== '') && ($_POST['password-confirm'] !== ''))
		{
			require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblEmailsTeste.class.php');
			$tblEmails = new dbTblEmailsTeste();
			$ID = $tblEmails->getIDbyLoginInfo($_POST['email-teste'],$_POST['password-teste']);
			if ($ID !== false)
			{
				$Result = $tblEmails->UPDATE($_POST);
			}
			else
			{
				$Result = $tblEmails->AddUserEmailsTeste($_POST);
				$ID 	= $tblEmails->decryptVar($Result['NEW_ID']);
			}
			unset($tblEmails);

			if ($Result)
			{
				$_SESSION['user_login']	= $_POST['email-teste'];
				$_SESSION['id_user']	= $ID;
				$_SESSION['id_assoc']	= '0';
				$_SESSION['nome_assoc']	= '';
				$_SESSION['nome_user']	= $_POST['nome-teste'].' '.$_POST['apelido-teste'];
				$_SESSION['img_assoc']	= 'quotagest-logo.png';
				echo "<script id='ctrlzscript'>CREATE_POP_MSG('O ".'"Registo de Conta de Teste"'." foi guardado com sucesso!',1500,'/ui/registo/passo1.php');</script>";
			}
			else
			{
				echo "<script id='ctrlzscript'>CREATE_POP_MSG('Erro ao guardar!');</script>";
			}
		}
		else
		{
			echo "<script id='ctrlzscript'>CREATE_POP_MSG('Erro: <b>Password</b> e <b>Password (confirmação)<b>, não são iguais!');</script>";
		}
	}
?>
<div id="corpo">
	<h2 class="titulo">Registo de Conta de Teste</h2>
	<p class="ajuda">Indique os seus dados para se registar.</p>

	<form id="login" name="login-registo" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
		<ul class="coluna no-margin">
			<!--
			<li>
				<h1>O registo de contas foi temporáriamente suspenso.</h1>
			</li>
			-->

			<li>
				<input name="email-teste" value="<?php echo $EMAIL_TESTER; ?>" placeholder="Email" maxlength="100" class="username" type="email" required>
			</li>
		<!--
			<li>
				<input name="login-teste" placeholder="Utilizador (login que vai utilizar)" maxlength="20" class="username" type="text" required>
			</li>
		-->
			<li>
				<input name="password-teste" placeholder="Password" maxlength="20" class="username" type="password" required>
			</li>
			<li>
				<input name="password-confirm" placeholder="Password (confirmação)" maxlength="20" class="username" type="password" required>
			</li>
			<li>&nbsp</li>
			<li>
				<input name="nome-teste" placeholder="o seu primeiro nome" maxlength="80" class="username" type="text" required>
			</li>
			
			<li>
				<input name="apelido-teste" placeholder="o seu apelido" maxlength="80" class="username" type="text" required>
			</li>

			<li><input type="button" value="Criar Conta" class="botao guardar" onClick="ValidateForm('login-registo');"></li>

			<li class="ajuda">
				Já tem conta? <a href="/pages/login" class="link">Inicie sessão</a>
				<br />
				Não consegue aceder? <a href="/pages/recover-password" class="link">Recuperar Password</a>
				<br />
				Pretende <a href="/pages/create-beta" class="link">registar uma nova conta</a>?
			</li>
		</ul>
		
		<div class="col">
			<br>
			<h3>Informações sobre a Conta Gratuita</h3>
			Ao criar a sua conta concorda com os seguintes pontos:
			<ol>
				<li>A sua conta será sempre gratuita, sem limites de tempo.</li>
				<li>Poderá utilizar a conta gratuitamente durante todo o tempo que o QuotaGest se encontrar online. </li>
				<li>As limitações das contas gratuitas podem ser alteradas sem aviso préviso. </li>
				<li>Compreenda que o QuotaGest ainda está em desenvolvimento, pelo que algumas funcionalidades não estão disponí­veis.</li>
				<li>Esta lista é provisória e contempla apenas as condições básicas, vamos sempre comunicar por email qualquer alteração a estas condições.</li>
			</ol>
		</div>
	</form>
</div>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>