<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');

	$fldsSocio 		= array('id','nome','telefone','telemovel','fax','email','outro1','outro2','morada','codigo_postal','data_nascimento','sexo','bicc','nif','nib','enabled');
	$fldsSocioRequi = array(false,true,false,false,false,false,false,false,false,true,false,false,false,false,false,false);
	$fldsSocioType 	= array('INTEGER','STRING','STRINGINT','STRINGINT','STRINGINT','STRING','STRING','STRING','MEMO','STRING','DATE','STRING','STRINGINT','STRINGINT','STRINGNEW','INTEGER');
	
	$fldsAssocSocio 	= array('id','id_assoc','id_socio','id_tipo','id_estado','codigo','codigo_sufixo','data_entrada','data_saida','divida','enabled','activo','id_user_created','id_user_edited','data_criacao','data_alterado','observacoes');
	$fldsAssocSocioType = array('INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','STRING','DATE','DATE','FLOAT','INTEGER','INTEGER','INTEGER','INTEGER','DATETIME','DATETIME','MEMO');
	$fldsAssocSocioRequi= array(true,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false);

	$fldsIndexSocio 	= array('NULL',2,3,4,5,6,false,false,7,9,11,14,13,12,false,false);
	$fldsIndexAssocSocio= array('NULL',false,false,false,false,1,false,16,17,false,false,false,false,false,false,false,10);

	function getColumns($AValue)
	{
		$arrAux = explode('  `', $AValue);
		$ccI = count($arrAux);
		for ($i=0; $i<$ccI; $i++)
		{
			$arrAux[$i] = substr($arrAux[$i], 0, stripos($arrAux[$i], '` '));
		}
		unset($arrAux[0]);
		unset($arrAux[$ccI-1]);

		return $arrAux;
	}
	function getValues($AValue)
	{
		$pos 	= stripos($AValue, 'VALUES (');
		$AValue = substr($AValue, $pos+8);

		$pos 	= stripos($AValue, ');');
		$AValue = substr($AValue, 0, $pos);
		

		$arrAux = explode('),(', $AValue);
		$ccI = count($arrAux);
		for ($i=0; $i<$ccI; $i++)
		{
			 $strAux = $arrAux[$i];
			 $strAux = str_ireplace(', ', '#', $strAux);
			 $arrAux[$i] = explode(',', $strAux);
		}
		#unset($arrAux[$ccI-1]);

		return $arrAux;
	}
	function constructSocio($ANewOrderFields,$AValues)
	{
		$arrAux = array();

		$ccI = count($AValues);
		for ($i=0; $i<$ccI; $i++)
		{
			$ccJ = count($ANewOrderFields);
			$arrAux[$i] = array();
			for ($j=0; $j<$ccJ; $j++)
			{
				$indx = $ANewOrderFields[$j];
				if (($indx === false) || ($indx === ''))
					$sValue = '';
				else
				{
					if (is_integer($indx))
					{
						$sValue = $AValues[$i][$indx];
					}
					else
						$sValue = $indx;

					$sValue = str_ireplace("\\'", "", $sValue);
					$sValue = str_ireplace("#", ", ", $sValue);
				}

				$arrAux[$i][$j]	= $sValue;
			}		
		}

		return $arrAux;		
	}
	function constructSQLINSERT($ATableName,$ANewArray)
	{
		$arrAux = array();

		$ccI = count($ANewArray);
		for ($i=0; $i<$ccI; $i++)
		{
			$arrAux[$i] = "INSERT INTO `".$ATableName."` VALUES  ('".implode("','",$ANewArray[$i])."');";
		}

		return $arrAux;			
	}
/*
	function constructSQLINSERT($ATableName,$ANewArray)
	{
		$ccI = count($ANewArray);
		$SQL = 'INSERT INTO `".$ATableName."` VALUES ';
		for ($i=0; $i<$ccI; $i++)
		{
			$SQL .= "('".implode("','",$ANewArray[$i])."'),";
		}
		$SQL = substr($SQL,0,strlen($SQL)-1);
		return $SQL;
	}
*/
	function INSERT_Socios($ATableName,$AarrValues,$AFieldsName,$AFieldsType,$AFieldsRequi)
	{
		$arrINSID = array();

		$tbl1 = new dbTable();
		$tbl1->setTableName($ATableName);
		$tbl1->setFieldsName($AFieldsName);
		$tbl1->setFieldsType($AFieldsType);
		$tbl1->setFieldsRequired($AFieldsRequi);

		$ccI = count($AarrValues);
		for ($i=0; $i<$ccI; $i++)
		{
			$tbl1->setFieldsValue($AarrValues[$i]);
			$Result	= false;
			$NEW_ID	= false;
			
			if ($tbl1->checkInformation())
			{
				$SQL 	= $tbl1->BuildSQL('INSERT');
				#$arrINSID[$i] = '9999';
				echo $SQL.'<br/>';
				$Result = $tbl1->ExecSQL($SQL);
				$NEW_ID = $tbl1->getInsertedID();
				$arrINSID[$i] = $NEW_ID;
				#$NEW_ID = $tbl1->encryptVar($NEW_ID);
			}
		}
		unset($tbl1);

		return $arrINSID;
	}
	function INSERT_AssocSocios($ATableName,$AINSERTED, $AarrValues,$AFieldsName,$AFieldsType,$AFieldsRequi)
	{
		$arrINSID = array();

		$tbl1 = new dbTable();
		$tbl1->setTableName($ATableName);
		$tbl1->setFieldsName($AFieldsName);
		$tbl1->setFieldsType($AFieldsType);
		$tbl1->setFieldsRequired($AFieldsRequi);

		$ccI = count($AINSERTED);
		for ($i=0; $i<$ccI; $i++)
		{	
			$AarrValues[$i][2] = $AINSERTED[$i];

			$tbl1->setFieldsValue($AarrValues[$i]);
			$Result	= false;
			$NEW_ID	= false;
			
			if ($tbl1->checkInformation())
			{
				$SQL 	= $tbl1->BuildSQL('INSERT');
				echo $SQL.'<br/>';
				$Result = $tbl1->ExecSQL($SQL);
				$NEW_ID = $tbl1->getInsertedID();
				$arrINSID[$i] = $NEW_ID;
				#$NEW_ID = $tbl1->encryptVar($NEW_ID);
			}
		}
		unset($tbl1);

		return $arrINSID;
	}

	if (isset($_POST['columns']) && isset($_POST['values'])
		&& isset($_POST['id_assoc']) && isset($_POST['id_user']))
	{
		$strCols = getColumns($_POST['columns']);
		$strVals = getValues($_POST['values']);

		$arrSocio = constructSocio($fldsIndexSocio,$strVals);

		$fldsIndexAssocSocio[0] = 'NULL';
		$fldsIndexAssocSocio[1] = $_POST['id_assoc'];
		$fldsIndexAssocSocio[2] = 'ID_INSERT';
		$fldsIndexAssocSocio[10] = '1';
		$fldsIndexAssocSocio[11] = '0';
		$fldsIndexAssocSocio[12] = $_POST['id_user'];
		$fldsIndexAssocSocio[13] = $_POST['id_user'];
		$fldsIndexAssocSocio[14] = date('Y-m-d h:m:s');
		$fldsIndexAssocSocio[15] = date('Y-m-d h:m:s');

		$arrAssocSocio = constructSocio($fldsIndexAssocSocio,$strVals);

		$SQLSocio 			= constructSQLINSERT('socio',$arrSocio);
		$SQLAssocSocio 		= constructSQLINSERT('assoc_socios',$arrAssocSocio);

		$arrInserted 	  = INSERT_Socios('socio',$arrSocio,$fldsSocio,$fldsSocioType,$fldsSocioRequi);
		$arrInsertedAssoc = INSERT_AssocSocios('assoc_socios',$arrInserted, $arrAssocSocio,$fldsAssocSocio,$fldsAssocSocioType,$fldsAssocSocioRequi);

		#var_dump($SQLSocio);
		#echo '<br/><br/>';
		#var_dump($SQLAssocSocio);
		#echo '<br/><br/>';
		#var_dump($arrSocio);
		#echo '<br/><br/>';
		#var_dump($strCols);
		#echo '<br/><br/>';
		#var_dump($strVals);
	}

?>

<html>
	<head>
		<title></title>
	</head>
	<body>
		<form action="" method="POST">
			<input type="text" name="id_assoc" placeholder="ID_ASSOC" />
			<input type="text" name="id_user" placeholder="ID_USER" />
			<input type="text" name="columns" placeholder="SQl columns (string)" />
			<input type="text" name="values" placeholder="SQl values INSERT (string)" />
			<button type="submit" value="Importar Dados"></button>
		</form>
	</body>
</html>