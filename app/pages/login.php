<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	$titulo = 'Login | QuotaGest';
	include($_SERVER['DOCUMENT_ROOT'].'/header.php');

	if ( (isset($_POST['login'])) && (isset($_POST['password'])) )
	{
		#include(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'logincheck.php');
		include(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'logincheck-testers.php');
	}
?>
<div id="corpo">
	<h2 class="titulo">Login</h2>
	<p class="ajuda">Bem vindo, introduza o seu nome de utilizador e palavra-passe para iniciar sessão na sua conta.</p>

	<form id="login" name="login" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
		<ul class="coluna no-margin">
			<li>
				<!-- //FIXME: Melhorar o formulário de Login -->
				<!-- <label class="titulo-3" for="login">Utilizador</label> -->
				<input name="login" placeholder="Utilizador" maxlength="80" class="username" type="text" autofocus="autofocus" required>
			</li>
			<li>
				<!-- <label class="titulo-3" for="password">Password</label> -->
				<input name="password" placeholder="Password" maxlength="20" class="username" type="password" required>
			</li>
			<li><input type="button" value="Entrar" class="botao guardar" onClick="ValidateForm('login');"></li>
			<li class="ajuda">
				Não consegue aceder? <a href="/pages/recover-password" class="link">Recuperar Password</a>
				<br />
				Pretende <a href="/pages/create-beta" class="link">registar uma nova conta</a>?
			</li>
		</ul>
	</form>
	
</div>
<script>
	document.body.onkeydown = function(event){ KeysUpDownSubmitForm(event,'login'); }
</script>

<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>