<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	
	$fldsSocio 		= array('id','nome','telefone','telemovel','fax','email','outro1','outro2','morada','codigo_postal','data_nascimento','sexo','bicc','nif','nib','enabled');
	$fldsSocioRequi = array(false,true,false,false,false,false,false,false,false,true,false,false,false,false,false,false);
	$fldsSocioType 	= array('INTEGER','STRING','STRINGINT','STRINGINT','STRINGINT','STRING','STRING','STRING','MEMO','STRING','DATE','STRING','STRINGINT','STRINGINT','STRINGNEW','INTEGER');
	
	$fldsAssocSocio 	= array('id','id_assoc','id_socio','id_tipo','id_estado','codigo','codigo_sufixo','data_entrada','data_saida','divida','enabled','activo','id_user_created','id_user_edited','data_criacao','data_alterado','observacoes');
	$fldsAssocSocioType = array('INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','STRING','DATE','DATE','FLOAT','INTEGER','INTEGER','INTEGER','INTEGER','DATETIME','DATETIME','MEMO');
	$fldsAssocSocioRequi= array(true,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false);
	
	
	function INSERT_VALUES($ATableName,$AarrValues,$AFieldsName,$AFieldsType,$AFieldsRequi)
	{
		$arrINSID = array();

		$tbl1 = new dbTable();
		$tbl1->setTableName($ATableName);
		$tbl1->setFieldsName($AFieldsName);
		$tbl1->setFieldsType($AFieldsType);
		$tbl1->setFieldsRequired($AFieldsRequi);

		$ccI = count($AarrValues);
		for ($i=0; $i<$ccI; $i++)
		{
			$tbl1->setFieldsValue($AarrValues[$i]);
			$Result	= false;
			$NEW_ID	= false;
			
			if ($tbl1->checkInformation())
			{
				$SQL 	= $tbl1->BuildSQL('INSERT');
				#echo $SQL.'<br/>';
				$Result = $tbl1->ExecSQL($SQL);
				$NEW_ID = $tbl1->getInsertedID();
				$arrINSID[$i] = $NEW_ID;
			}
		}
		unset($tbl1);

		return $arrINSID;
	}
	
	if (isset($_POST['jsondata']))
	{
		$arrJSON 		= json_decode($_POST['jsondata']);
		$arrSocios 		= $arrJSON['socio'];
		$arrAssocSocios = $arrJSON['assocsocio'];
		
		$AINSERTED = INSERT_VALUES('socio',$arrSocios, $fldsSocio,$fldsSocioType,$fldsSocioRequi);
		
		$ccI = count($arrSocios);
		for ($i=0; $i<$ccI; $i++)
		{
			$arrAssocSocios[$i][2] = $AINSERTED[$i];
		}
	}
?>