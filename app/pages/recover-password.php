<?
	# VERIFICACAO DE EMAIL
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	$titulo 	 = 'Recuperar Password | QuotaGest';
	$DO_LOGIN 	 = false;
	$INLOGINPAGE = true;
	$SHOWMENU	 = false;
	$NEW_ACCOUNT = true;
	require_once($_SERVER['DOCUMENT_ROOT'].'/header.php');

	if (isset($_POST['email']) && ($_POST['email'] !== ''))
	{
		require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjSendEmail.class.php');
		require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlUser.class.php');

		$objTplMdlUser = new tplmdlUser();
		$Result = $objTplMdlUser->tblEmailsTeste->resetPassword($_POST['email']);
		unset($objTplMdlUser);

		$mail = new TobjSendEmail();
		$mail->oMail->addAddress($_POST['email']);
		$mail->oMail->Subject = utf8_decode('QuotaGest - Recuperação de Password');
		$mail->oMail->Body    = utf8_decode('A sua nova password é: '.$Result['PASSWORD'].
											"<br>".
											'Poderá alterá-la depois de efectuar o <a href="'.SETPATH('URL','PATH_APP_PAGES').'login">Login</a>.');
		$mail->oMail->AltBody = utf8_decode('A sua nova password é: '.$Result['PASSWORD'].
											"\r\n\r\n".
											'Poderá alterá-la depois de efectuar o Login.'.
											"\r\n\r\n".
											SETPATH('URL','PATH_APP_PAGES').'login'
											);
		$Result = ($mail->oMail->send()) ? true : false;
		unset($mail);

		if ($Result)
			echo "<script id='ctrlzscript'>CREATE_POP_MSG('O ".'"Registo de Conta de Teste"'." foi guardado com sucesso!',1500,'/pages/login.php');</script>";
		else
			echo "<script id='ctrlzscript'>CREATE_POP_MSG('Erro ao guardar!');</script>";
	}
?>
<div id="corpo">
	<h2 class="titulo">Recuperar Password</h2>
	<p class="ajuda">Indique o email que usou para se registar.</p>
	<form id="login" name="password-recover" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
		<ul class="coluna no-margin">
			<li>
				<input name="email" value="" placeholder="Email" maxlength="100" class="username" type="email" required>
			</li>
			<li><input type="button" value="Recuperar Password" class="botao guardar" onClick="ValidateForm('password-recover');"></li>
			<li class="ajuda">
				Não consegue aceder? <a href="/pages/recover-password" class="link">Recuperar Password</a>
				<br />
				Pretende <a href="/pages/create-beta" class="link">registar uma nova conta</a>?
			</li>
		</ul>
	</form>
</div>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>