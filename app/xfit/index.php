<?php
	error_reporting(E_ALL|E_STRICT);
	ini_set('display_errors', 1);

	$path = '/Applications/MAMP/bin/mamp/ginasio/';

	require_once($path.'core/scripts/TobjAutoTemplate.class.php');
	$tblAutoTpl = new TobjAutoTemplate();
	$_SESSION['path'] 		= $path;
	$_SESSION['TplObject'] 	= $tblAutoTpl;
	
	require_once($path.'core/db/dbTblTreinador.class.php');
	$tblTreinador = new dbTblTreinador();
	$tblTreinador->set_IDMAIN('');
	$tblTreinador->set_IDUSER('');
	$tblTreinador->set_IDASSOC('');
	$tblAutoTpl->addObject('treinador',$tblTreinador);
	$tblAutoTpl->setMarkupType('pico');
	$tblAutoTpl->setCallback('pico','treinador','testeNew2',array('"Callback Object PICO"'));
	$tblAutoTpl->setCallback('pico',$tblTreinador,'testeNew2',array('"Callback Class PICO"'));
	$tblAutoTpl->setVar('{% pico2 %}','Sou uma variável!');

	function FunccaoNormal($parametro)
	{
		return '<br/>FunccaoNormal('.$parametro.')';
	}

	#replace pico Callback function
	#$tblAutoTpl->setCallback('pico',$tblTreinador,'testeNew2',array('"Callback PICO3"'),true);
	#var_dump($tblAutoTpl::$listMarkupType);

?>
<html>
	<head>
		<title>Ginasio Teste</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="/xfit/css/main.css">

		<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.js"></script>
		<script src="/xfit/js/touchSwipe.js"></script>
		<script src="/xfit/js/Chart.min.js"></script>
		<script src="/xfit/js/main.js"></script>
		<script>
			//http://www.lukew.com/ff/entry.asp?1569=#menu
			//http://www.w3schools.com/jquerymobile/event_swipe.asp
			//http://www.w3schools.com/jquerymobile/tryit.asp?filename=tryjqmob_event_swipeleft
			//http://css-tricks.com/controlling-css-animations-transitions-javascript/
			//$(document).on("pagecreate","#maindiv",function(){});
			$(document).on("ready",function()
			{
				console.log('complete');
				var eMain = $("#main");
				var eLeftMenu = $("#leftmenu");
				//var eButton = $("[data-id='btnOffCanvas']");
				
				var eRightMenuShadow = $("#rightmenu-shadow")[0];
				//$(eMain).on("swipeleft",function(){mobile_SwipeLeft();});
				//$(eRightMenuShadow).on("swiperight",function(){mobile_SwipeRight();});
				//$(eMain).on("swipeup",function(){mobile_SwipeLeft();});
				//$(eMain).on("swipedown",function(){mobile_SwipeRight();});
				$(eRightMenuShadow).on("tap",function(){mobile_SwipeRight();});
				
				//$(eButton).on("vclick",function(){mobile_SwipeRight();});
				//$(eButton).on("click",function(){mobile_SwipeRight();}); /* recomended! */
				//--$(eButton).on("tap",function(){mobile_SwipeRight();});
				
				//Enable swiping...
				var funcSwipe = function(event, direction, distance, duration, fingerCount)
				{
					switch(direction)
					{
						case 'right':
							{
								mobile_SwipeRight();
							} break;
						case 'left':
							{
								mobile_SwipeLeft();
							} break;
						case 'up':
							{
								
							} break;
						case 'down':
							{
								
							} break;
						case 'null':
							{
								
							} break;
					}
					//eMain.insertAdjacentHTML('beforeend',"You swiped " + direction );
				}
				
				$(eLeftMenu).swipe({swipe:funcSwipe, threshold:0});
				$(eMain).swipe({swipe:funcSwipe, threshold:0});
				
				$("body").mousemove(function(e)
				{
					document.Form1.posx.value = e.pageX;
					document.Form1.posy.value = e.pageY;
				});
				DesenharCanvas();
				DrawGraphic();
			});
			/*
				https://github.com/cowgp/xui-touchSwipe
				http://www.awwwards.com/touchswipe-a-jquery-plugin-for-touch-and-gesture-based-interaction.html
				https://github.com/mattbryson/TouchSwipe-Jquery-Plugin
				http://labs.rampinteractive.co.uk/touchSwipe/docs/
				http://labs.rampinteractive.co.uk/touchSwipe/demos/Swipe_status.html
				http://labs.rampinteractive.co.uk/touchSwipe/demos/Single_swipe.html
				http://labs.rampinteractive.co.uk/touchSwipe/demos/Basic_swipe.html
				http://padilicious.com/code/touchevents/index.html
			*/
			/*
				http://www.w3schools.com/html/html5_canvas.asp
				http://raphaeljs.com/
				http://stackoverflow.com/questions/19231603/draw-a-line-between-two-elements-with-hover
				http://jsfiddle.net/m1erickson/86f4C/
				http://stackoverflow.com/questions/21864989/draw-lines-between-2-elements-in-html-page
				http://jsfiddle.net/kDs2Q/45/
				
				http://jsfiddle.net/bnwpS/15/
				http://diveintohtml5.info/canvas.html
				https://developer.mozilla.org/en-US/docs/HTML/CORS_Enabled_Image
				
				http://webdesign.about.com/od/html5tutorials/a/draw-circles-on-html5-canvas.htm
			*/
			function DesenharCaixa(eContext,eElem,OffsetPos)
			{
				console.log(OffsetPos);
				eContext.beginPath();
				eContext.lineWidth="6";
				eContext.strokeStyle="red";
				eContext.rect(OffsetPos.top,OffsetPos.pos,OffsetPos.width,OffsetPos.height);
				eContext.font = "30px Arial";
				eContext.fillText("Hello World",10,50);
				eContext.stroke();
			}
			function getOffset( el )
			{
				var _x = 0;
				var _y = 0;
				var _w = el.offsetWidth|0;
				var _h = el.offsetHeight|0;
				while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) )
				{
					_x += el.offsetLeft - el.scrollLeft;
					_y += el.offsetTop - el.scrollTop;
					el = el.offsetParent;
				}
				return { top: _y, left: _x, width: _w, height: _h };
			}
			function DesenharCanvas()
			{
			
				var bodyMain = $("[data-id='bodystatistic']")[0];
				var arrElems1 = $("[data-id='values-ul2']")[0].children;
				var arrElems2 = $("[data-id='values-ul1']")[0].children;
				var arrPos1 = {0:[230,84],1:[240,125],2:[250,165]};
				var arrPos2 = {0:[160,84],1:[130,208],2:[178,246]};
				var objPos 	= false;
				var iPI 	= (2*Math.PI);
				
				var bodyImgLeft = 97;
				var bodyimg = document.createElement('img');
					bodyimg.height = 392;
					bodyimg.width = 200;
				
				// POSICAO DO BODY NA JANELA
				var off1 = getOffset(bodyMain);
				var cavPosY = (off1.top);
				var cavPosX = (off1.left);
				
				// CANVAS ELEMENT
				var eCanvas = document.createElement('canvas');
					eCanvas.setAttribute("height",bodyMain.clientHeight);
					eCanvas.setAttribute("width",bodyMain.clientWidth);
					eCanvas.height 	= bodyMain.clientHeight;
					eCanvas.width 	= bodyMain.clientWidth;
				var ctx = eCanvas.getContext("2d");
					ctx.lineWidth = 3;
					ctx.fillStyle 	= "rgba(0,0,0,0)"; //FUNDO
					ctx.strokeStyle = "rgba(17,82,148,1)"; //LINHA
					ctx.beginPath();
					ctx.fillRect(0, 0, bodyMain.clientWidth, eCanvas.height);
					
				bodyimg.onload = function()
				{
					// DESENHA IMAGEM DO CORPO
					ctx.drawImage(bodyimg,bodyImgLeft,0, bodyimg.width,bodyimg.height);
						
					// PARSE ELEMENTS
					for(var i=0, ccI=arrElems1.length; i<ccI; ++i)
					{
						objPos = getOffset(arrElems1[i]);
						
						ctx.moveTo(arrPos1[i][0],arrPos1[i][1]);
						ctx.lineTo((objPos.left-cavPosX),(objPos.top-cavPosY)+(arrElems1[i].offsetHeight/2));

						ctx.fillStyle = "rgba(17,82,148,1)";
						ctx.moveTo(arrPos1[i][0],arrPos1[i][1]);
						ctx.arc(arrPos1[i][0],arrPos1[i][1],4,0,iPI);
						ctx.fill();
						//console.log(((objPos.left-cavPosX))+','+((objPos.top-cavPosY)+(arrElems1[i].offsetHeight/2)));
					}
					
					/* ************************************************************ */
					/* ************************************************************ */
					for(var i=0, ccI=arrElems2.length; i<ccI; ++i)
					{
						objPos = getOffset(arrElems2[i]);
						
						ctx.moveTo(arrPos2[i][0],arrPos2[i][1]);
						ctx.lineTo((objPos.left-cavPosX)+arrElems2[i].offsetWidth,(objPos.top-cavPosY)+(arrElems2[i].offsetHeight/2));
						
						ctx.fillStyle = "rgba(17,82,148,1)";
						ctx.moveTo(arrPos2[i][0],arrPos2[i][1]);
						ctx.arc(arrPos2[i][0],arrPos2[i][1],4,0,iPI);
						ctx.fill();
						//console.log(((objPos.left-cavPosX)+arrElems2[i].offsetWidth)+','+((objPos.top-cavPosY)+(arrElems2[i].offsetHeight/2)));
					}
						//ctx.rect(objPos.top,objPos.pos,objPos.width,objPos.height);
					ctx.stroke();
					ctx.closePath();
					console.log(ctx);
					
					bodyMain.setAttribute('style',"background: url("+eCanvas.toDataURL()+");");
				}
				bodyimg.src = "/xfit/imgs/corpohomem.png";
			}
			/* ***** CANVAS GRAPHICS ***** */
			function resetCanvas(AsIDCanvas)
			{
				var canvas = document.getElementById(AsIDCanvas);
				var ctx = canvas.getContext("2d");
					ctx.beginPath();
					ctx.fillStyle = "rgba(255,255,255,1)";
					//ctx.rect(0, 0, canvas.width, canvas.height);
					ctx.fillRect(0, 0, canvas.width, canvas.height);
					ctx.fill();
				return ctx;
			}
			function DrawGraphic(AsRgbaColor,Aindex)
			{
				bAnimate = false;
				if (AsRgbaColor == undefined)
				{
					bAnimate = true;
					AsRgbaColor =  "#F7464A";
				}
				
				var rgb1 = "#F7464A";
				var rgb2 = "#E2EAE9";
				var rgb3 = "#D4CCC5";
				if (Aindex != undefined)
				{
					switch(Aindex)
					{
						case 0: rgb1 = AsRgbaColor; break;
						case 1: rgb2 = AsRgbaColor; break;
						case 2: rgb3 = AsRgbaColor; break;
					}
				}

				var canvas = document.getElementById("myChart");
				var ctx = canvas.getContext("2d");
				/*
				var data = {
					labels : ["January","February","March","April","May","June","July"],
					datasets : [
						{
							fillColor : "rgba(220,220,220,0.5)",
							strokeColor : "rgba(220,220,220,1)",
							pointColor : "rgba(220,220,220,1)",
							pointStrokeColor : "#fff",
							data : [65,59,90,81,56,55,40]
						},
						{
							fillColor : AsRgbaColor,
							strokeColor : "rgba(151,187,205,1)",
							pointColor : "rgba(151,187,205,1)",
							pointStrokeColor : "#fff",
							data : [28,48,40,19,96,27,100]
						}
					]
				}
				*/
				var data = [{
								value: 30,
								color: rgb1
							},
							{
								value : 50,
								color : rgb2
							},
							{
								value : 100,
								color : rgb3
							}]
				var myNewChar = new Chart(ctx).Doughnut(data,{animation : bAnimate});
				//var myNewChart = new Chart(ctx).Line(data,{animation : bAnimate});
			}

		</script>
	</head>
	<body>
		<?php echo include('/home/qgest/public_html/app/xfit/ui/teste_exemplo.php'); ?>
		<?php #echo $tblAutoTpl->tpl_page($path.'ui/teste_exemplo.php',true,true); ?>
		<?php #echo $tblAutoTpl->tpl_page($path.'ui/tpl_maindiv.php',true,true); ?>
	</body>
</html>
<?
	unset($tblAutoTpl);
?>