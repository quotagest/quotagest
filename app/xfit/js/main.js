	
	SCROLL_POSY 		= 0;
	SCROLL_POSX 		= 0;

	/* BROWSER MANIPULATION*/
	function getPageScrollPos()
	{
		var doc = document.body; 
		SCROLL_POSY = doc.scrollTop;
		SCROLL_POSX = doc.scrollLeft;
		//alert(doc.scrollHeight+' - '+doc.clientHeight);
		//percentageScrolled = Math.floor((scrollPosition / pageSize) * 100); 
	}
	function setScrollPos()
	{
		window.scrollTo(SCROLL_POSX,SCROLL_POSY); 
	}

	/* HANDLE MANIPULATION */
	function addEventHandler(AeElem,eventType,ACallback)
	{
		if (AeElem.addEventListener)
			AeElem.addEventListener(eventType,ACallback,false);
		else
		if (AeElem.attachEvent)
			AeElem.attachEvent('on'+eventType,ACallback); 
	}

	/* OBJECT MANIPULATION */
	function addEventHandler(AeElem,eventType,ACallback)
	{
		if (AeElem.addEventListener)
			AeElem.addEventListener(eventType,ACallback,false);
		else
		if (AeElem.attachEvent)
			AeElem.attachEvent('on'+eventType,ACallback); 
	}
	function ChangeImgSrc(vElemID,vPath)
	{
		SetAttElementByID(vElemID,'src',vPath);
	}
	function GetObjectByID(AName)
	{
		return document.getElementById(AName);
	}
	function GetObjectsByName(AName)
	{
		return document.getElementsByName(AName);
	}
	function GetObjectByName(AName,AIndex)
	{
		return document.getElementsByName(AName)[AIndex];
	}
	function GetObjectByClassName(AName,AIndex)
	{
		return document.getElementsByClassName(AName)[AIndex];
	}
	function GetObjectsByTagName(aTagName)
	{
		return document.getElementsByTagName(aTagName);
	}
	function FindParentFORMName(eObject)
	{
		// LINK: http://www.randomsnippets.com/2008/06/26/how-to-find-and-access-parent-nodes-via-javascript/
		if (eObject)
		{
			var testObj = eObject.parentNode;
			while (testObj != undefined)
			{
				if (testObj.tagName != 'FORM')
					testObj = testObj.parentNode;
				else
					return testObj.getAttribute('name');
			}
		}
		
		return '';
	}
	function GetObjectsByTagNameData(aTagName, aDataName, aName)
	{
		var arrObjs = GetObjectsByTagName(aTagName);
		for (i=0, ilen=arrObjs.length; i<ilen; i++)
		{
			if (arrObjs[i].getAttribute(aDataName) === aName)
			{
				return arrObjs[i];
			}
		}
		return false;
	}
	function SetAttElementForm(form_name,input_name,att,attvalue)
	{
		var Field = document.forms[form_name].elements[input_name];
			Field.setAttribute(att,attvalue);
	}
	function SetAttElementByID(id_element,att,attvalue)
	{
		var Elem = GetObjectByID(id_element);
			Elem.setAttribute(att,attvalue);
	}
	function SetAttElementObject(eObject,att,attvalue)
	{
		eObject.setAttribute(att,attvalue);
	}
	function RemoveAttElementObject(eObject,att)
	{
		eObject.removeAttribute(att);
	}
	function createObjElement(sType,arrAttName,arrAttValue,oParentElem)
	{
		if (oParentElem == undefined)
			oParentElem = document;
		
		var oElem = oParentElem.createElement(sType);
		for (var i=0, ccI=arrAttName.length; i<ccI; i++)
		{
			if (arrAttName[i] !== "")
				oElem.setAttribute(arrAttName[i],arrAttValue[i]);
		}
		
		return oElem;
	}
	function CleanElement(objElement)
	{
		while(objElement.firstChild)
		{
			objElement.removeChild(objElement.firstChild);
		}
	}
	function ReplaceInnerHTML(AObjElement,AnewText,APosition)
	{
		APosition = (APosition != undefined) ? APosition : 'afterbegin';
		CleanElement(AObjElement);
		AObjElement.insertAdjacentHTML(APosition,AnewText);
	}
	function ParseHTMLToDOM(AsHTML)
	{
		var parentNode = document.createElement('span');
			ReplaceInnerHTML(parentNode,AsHTML);
		var eNode = parentNode.childNodes[0].cloneNode(true);
		CleanElement(parentNode);
		parentNode = undefined;
		
		return eNode;
	}

	/* CLASS OBJECT MANIPULATION */
	function AddToClass(eElem,sString)
	{
		if (eElem.className.indexOf(sString) == -1)
		{
			eElem.className = (eElem.className+" "+sString);
		}
	}
	function RemoveFromClass(eElem,sString)
	{
		if (eElem.className.indexOf(sString) != -1)
		{
			ReplaceFromClass(eElem,sString,'');
		}
	}
	function ReplaceFromClass(eElem,AsClassOld,AsClassNew)
	{
		if (eElem.className.indexOf(AsClassOld) != -1)
		{
			eElem.className = Trim(eElem.className.replace(new RegExp(AsClassOld,"gi"),AsClassNew));
		}
	}
	function ToggleElemClass(AElem,AClassOne,AClassTwo)
	{
		if ((typeof AElem) !== 'object')
		{
			AElem = GetObjectByClassName(AElem,0);
			if (AElem == undefined)
				AElem = GetObjectByID(AElem);
		}
		
		if (AElem.className.indexOf(AClassOne) != -1)
			ReplaceFromClass(AElem,AClassOne,AClassTwo);
		else
		{
			ReplaceFromClass(AElem,AClassTwo,AClassOne);
			AddToClass(AElem,AClassOne);
		}
			//ReplaceFromClass(AElem,AClassTwo,AClassOne);
	}	

	/* STRING MANIPULATION */
	/* http://stackoverflow.com/questions/498970/how-do-i-trim-a-string-in-javascript?answertab=votes#tab-top */
	String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};
	String.prototype.ltrim=function(){return this.replace(/^\s+/,'');};
	String.prototype.rtrim=function(){return this.replace(/\s+$/,'');};
	String.prototype.fulltrim=function(){return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');};
	
	function Trim(str)
	{
		return str.replace(/^\s+|\s+$/g,'');
	}
	function isEmpty(str)
	{
		return (!str || 0 === str.length);
	}
	function isBlank(str)
	{
		return (!str || /^\s*$/.test(str));
	}
	function isNumeric(string)
	{
		return (string.match(/^[0-9]+$/)) ? true : false;
	}
	function isFloat(nValue)
	{
		nValue = parseFloat(nValue);
		if (nValue == 0)
			return true;
		else
			return (nValue != "") && (!isNaN(nValue)) && ((Math.round(nValue) == nValue) || (Math.round(nValue) != nValue));
	}
	function getCurrentTime(AsParams)
	{
		var currentTime = new Date();
		var seconds = currentTime.getSeconds();
		var minutes = currentTime.getMinutes();
		var hour 	= currentTime.getHours();
		var day 	= currentTime.getDate();
		var month 	= currentTime.getMonth()+1;
		var year 	= currentTime.getFullYear();

		if (month<10)
			month 	= '0'+month;
		if (day<10)
			day 	= '0'+day;
		if (hour<10)
			hour 	= '0'+hour;
		if (minutes<10)
			minutes = '0'+minutes;
		if (seconds<10)
			seconds = '0'+seconds;

		AsParams = AsParams.replace(/Y/,year);
		AsParams = AsParams.replace(/m/,month);
		AsParams = AsParams.replace(/d/,day);
		AsParams = AsParams.replace(/h/,hour);
		AsParams = AsParams.replace(/i/,minutes);
		AsParams = AsParams.replace(/s/,seconds);
		return AsParams;
	}
	function FloatToString(sString)
	{
		sString = sString.replace(/€/gi,"");
		sString = sString.replace(",",".");
		sString = sString.replace(" ","");
		return Trim(sString);		
	}
	function FloatToCurrency(fNum)
	{
		return "€"+FloatToCurrencyExtra(fNum,2);
	}
	function FloatToCurrencyExtra(fNum,iFixedNum)
	{
		fNum = parseFloat(fNum);
		fNum += 0.00;
		fNum = fNum.toFixed(iFixedNum);
		return numberWithSpaces(fNum);
	}
	function isArray(object)
	{
		return (object.constructor === Array) ? true : false;
	}
	function numberWithSpaces(x)
	{
		var parts 	 = x.toString().split(".");
			parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		return parts.join(".");
	}
	function FormatFloatDecimal(fNumber, aCasasDecimais)
	{
		return fNumber.toFixed(aCasasDecimais);	
	}
	function setEmptyIfNull(AValue)
	{
		return (AValue != undefined) ? AValue : '';
	}

	if(!Array.prototype.indexOf)
	{
		Array.prototype.indexOf = function(needle)
		{
			var ilen = this.length;
			for(var i=0; i<ilen; i++)
			{
				if(this[i] === needle)
				{
					return i;
				}
			}
			return -1;
		};
	}

	String.prototype.replaceArray = function(find, replace)
	{
		// LINK: http://stackoverflow.com/questions/5069464/replace-multiple-strings-at-once
		var repStr = this;
		var ilen = find.length;
		for (var i = 0; i < ilen; i++)
		{
			repStr = repStr.replace(find[i], replace[i]);
		}
		return repStr;
	}
	function randomstring(AiSize)
	{
		var Result = '';
		var randomchar = function()
		{
			var n = Math.floor(Math.random()*62);
			if (n<10)
				return n; //1-10
			if (n<36)
				return String.fromCharCode(n+55); //A-Z
			
			return String.fromCharCode(n+61); //a-z
		}
		
		while(Result.length < AiSize)
		{
			Result += randomchar();
		}
		return Result;
	}
	function ParseJSON(AString,ADecode)
	{
		AString = ((ADecode != undefined) && (ADecode == true)) ? decode64(AString) : AString;
		var Result = '';
		try
		{
			Result = JSON.parse(AString);
		}
		catch(e)
		{
			Result = false;
			console.log(e);
			console.log(AString);
		}
		return Result;
	}
	/* http://www.toao.net/32-my-htmlspecialchars-function-for-javascript */
	function htmlspecialchars(str)
	{
		if (typeof(str) == "string")
		{
			str = str.replace(/&/g, "&amp;"); /* must do &amp; first */
			str = str.replace(/"/g, "&quot;");
			str = str.replace(/'/g, "&#039;");
			str = str.replace(/</g, "&lt;");
			str = str.replace(/>/g, "&gt;");
		}
		return str;
	}
	function htmlspecialchars_decode(str)
	{
		if (typeof(str) == "string")
		{
			str = str.replace(/&gt;/ig, ">");
			str = str.replace(/&lt;/ig, "<");
			str = str.replace(/&#039;/g, "'");
			str = str.replace(/&quot;/ig, '"');
			str = str.replace(/&amp;/ig, '&'); /* must do &amp; last */
		}
		return str;
	}

	/* URL/HREF MANIPULATIONS */
	function getParamValue(AsParam,AsHref)
	{
		var regex 	= new RegExp("[\\?&]"+AsParam+"=([^&#]*)");
		var results = regex.exec(AsHref);
		return (results == null) ? '' : results[1];
	}
	function toggleUrlParam(AsHref,AsParam,AParamText,AbRetriveHOST)
	{
			AsHref = (AsHref === '') ? window.location.href : AsHref;
		var ipos   = AsHref.indexOf('?');
		if (ipos != -1)
		{
			var arrAux 	= AsHref.split('?');
			var sHOST 	= arrAux[0];
			var Result 	= arrAux[1];
			var ResVal 	= getParamValue(AsParam,Result);
				Result 	= Result.replace(AsParam+'='+ResVal,AsParam+'='+AParamText);

			sHOST  = (AbRetriveHOST) ? sHOST : '';
			Result = sHOST+'?'+Result;
			return Result;
		}
		return AsHref;
	}
	function getUrlHost(AsHref)
	{
		AsHref = (AsHref === '') ? window.location.href : AsHref;
		AsHref = AsHref.replace('://','#').split('/');
		return arrAux[0].replace('#','://');
	}

	/* FORM MANIPULATIONS */
	function Autofocus(AsFormName,AsElemName)
	{
		var eElem = document.forms[AsFormName].elements[AsElemName];
			eElem.focus();
	}
	function AutoSelectIndex(AsFormName,AsElemName,AiIndex)
	{
		var eElem = document.forms[AsFormName].elements[AsElemName];
			eElem.selectedIndex = AiIndex;
	}

	/* IMAGE ELEMENT MANIPULATION */
	function checkImage(AsSRC,AsFolder,fCallBack)
	{
		var img 	= new Image();
		var sURL 	= getUrlHost()+'/img/'; /* ALTERAR LINK!! */
		var imgSrc 	= sURL+AsFolder+AsSRC;

		img.onload = function()
			{
				if (fCallBack && typeof(fCallBack) === "function")
					fCallBack(true,imgSrc);
			};
		img.onerror = function()
			{
				if (fCallBack && typeof(fCallBack) === "function")
					fCallBack(false,sURL);
			};
	
		img.src = imgSrc;
	}
	




	// http://como-emagrecer.com/calculo-imc.html
	
	function mobile_SwipeLeft()
	{
		Animacao(true);
	}
	function mobile_SwipeRight()
	{
		Animacao(false);
	}

	var entered = false;
	var swipping = false;
	var tTap = false;
	/*
	function mouse_position()
	{
		var e = window.event;

		var posX = e.clientX;
		var posY = e.clientY;
		//console.log(posX+' -- '+posY);
		//document.Form1.posx.value = posX;
		//document.Form1.posy.value = posY;
		if ((posX <= 0) && (!entered))
		{
			Animacao();
			entered = true;
		}
		if ((posX >= 332) && (entered))
		{
			Animacao();
			entered = false;
		}
		else
		{
			//var t = setTimeout("mouse_position()",100);
		}
	}
	*/

	function Animacao(AbShow)
	{
		console.log('animacao');
		var eRightMenu 		 = $("#rightmenu")[0];
		var eRightMenuShadow = $("#rightmenu-shadow")[0];
		
		if (eRightMenu.className.indexOf('animacao-right') != -1)
		{
			ToggleElemClass(eRightMenu,'hidden','animacao-right');
			ToggleElemClass(eRightMenuShadow,'hidden','show');
		}
		else
		if ((eRightMenu.className.indexOf('hidden') != -1) && (AbShow))
		{
			ToggleElemClass(eRightMenu,'hidden','animacao-right');
			ToggleElemClass(eRightMenuShadow,'hidden','show');
		}
	}


	function CalcularIMC()
	{
		var iAltura = document.getElementsByName('altura')[0].value;
		var iPeso 	= document.getElementsByName('peso')[0].value;
		var iIdade 	= document.getElementsByName('idade')[0].value;
		var edtIMC 	= document.getElementsByName('imc')[0];
		var eBodyMass 	= document.getElementsByName('bodymass')[0];
		var eBloodPress = document.getElementsByName('bloodpressure')[0];
		var imc 	= 0;
		var iNAltura= 0;

		if (iAltura >= 100)
		{
			iNAltura = (iAltura/100);
		}
		imc = iPeso / (iNAltura * iNAltura);
		imc = imc.toFixed(2);
		edtIMC.value = imc;

		if (imc < 18.5)
		{
			eBodyMass.value = 'Underweight';
		}
		else
		if ((imc >= 18.5) && (imc <= 24.9))
		{
			eBodyMass.value = 'Normal';
		}
		else
		if ((imc >= 25) && (imc <= 29.9))
		{
			eBodyMass.value = 'Overweight';
		}
		else
		if ((imc >= 30) && (imc <= 34.9))
		{
			eBodyMass.value = 'Obesity I';
		}
		else
		if ((imc >= 35) && (imc <= 39.9))
		{
			eBodyMass.value = 'Obesity II';
		}
		else
		if (imc >= 40)
		{
			eBodyMass.value = 'Obesity III';
		}
		
		// Blodd Pressure
		if ((iAltura<120) && (iPeso<80))
		{
			eBloodPress.value = 'Normal - Encourage';
		}
		else
		if (((iAltura>=120) && (iAltura<=139)) && ((iPeso>=80) && (iPeso<=89)))
		{
			eBloodPress.value = 'Prehipertensão - Yes';
		}
		else
		if (((iAltura>=140) && (iAltura<=159)) && ((iPeso>=90) && (iPeso<=99)))
		{
			eBloodPress.value = 'Hipertensão (estádio 1) - Yes';
		}
		else
		if ((iAltura>=160) && (iPeso>=100))
		{
			eBloodPress.value = 'Hipertensão (estádio 2) - Yes';
		}
		else
		{
			eBloodPress.value = 'Normal';
		}
	}



/* ***************************************************** */
/* ***************************************************** */
/*
	http://stackoverflow.com/questions/17131815/how-to-swipe-top-down-jquery-mobile
*/
(function() {
	var supportTouch = $.support.touch,
			scrollEvent = "touchmove scroll",
			touchStartEvent = supportTouch ? "touchstart" : "mousedown",
			touchStopEvent = supportTouch ? "touchend" : "mouseup",
			touchMoveEvent = supportTouch ? "touchmove" : "mousemove";
	$.event.special.swipeupdown = {
		setup: function() {
			var thisObject = this;
			var $this = $(thisObject);
			$this.bind(touchStartEvent, function(event) {
				var data = event.originalEvent.touches ?
						event.originalEvent.touches[ 0 ] :
						event,
						start = {
							time: (new Date).getTime(),
							coords: [ data.pageX, data.pageY ],
							origin: $(event.target)
						},
						stop;

				function moveHandler(event) {
					if (!start) {
						return;
					}
					var data = event.originalEvent.touches ?
							event.originalEvent.touches[ 0 ] :
							event;
					stop = {
						time: (new Date).getTime(),
						coords: [ data.pageX, data.pageY ]
					};

					// prevent scrolling
					if (Math.abs(start.coords[1] - stop.coords[1]) > 10) {
						event.preventDefault();
					}
				}
				$this
						.bind(touchMoveEvent, moveHandler)
						.one(touchStopEvent, function(event) {
					$this.unbind(touchMoveEvent, moveHandler);
					if (start && stop) {
						if (stop.time - start.time < 1000 &&
								Math.abs(start.coords[1] - stop.coords[1]) > 30 &&
								Math.abs(start.coords[0] - stop.coords[0]) < 75) {
							start.origin
									.trigger("swipeupdown")
									.trigger(start.coords[1] > stop.coords[1] ? "swipeup" : "swipedown");
						}
					}
					start = stop = undefined;
				});
			});
		}
	};
	$.each({
		swipedown: "swipeupdown",
		swipeup: "swipeupdown"
	}, function(event, sourceEvent){
		$.event.special[event] = {
			setup: function(){
				$(this).bind(sourceEvent, $.noop);
			}
		};
	});

})();
