		<div class="maindiv noanimation" id="maindiv">
			<div class="leftmenu" id="leftmenu">
				<form action="index.php" method="POST" name="globalform">
					<label for="altura">Altura (cm)</label>
					<input type="text" name="altura" id="altura">
				<br/>
					<label for="peso">Peso (kg)</label>
					<input type="text" name="peso" id="peso">
				<br/>
					<label for="idade">Idade</label>
					<input type="text" name="idade" id="idade">
				<br/>
					<label for="imc">IMC</label>
					<input type="text" name="imc" id="imc">
				<br/>
					<input type="button" value="Guardar" onclick="CalcularIMC();"/>
				<br/>
					<label for="bodymass">Massa Muscular</label>
					<input type="text" name="bodymass" id="bodymass">
				<br/>
					<label for="bloodpressure">Pressão Sanguinea</label>
					<input type="text" name="bloodpressure" id="bloodpressure">
				<br/>
				</form>
			</div>
			<div class="menu" id="main">
				<table>
					<thead>
						<tr>
							<th>Nº</th>
							<th>ID</th>
							<th>Nome</th>
							{pico obj="treinador" func="testeNew2" params="['Markup novo PICO']"}{/pico}

							{manuel obj="treinador" func="testeNew4" jsonparams="%7B%220%22%3A%22%3Ctd%3E%7Bi%7D%3C%2Ftd%3E%22%7D" jsonencode="urlencode" postaction="true"}{/manuel}
						</tr>
					</thead>
					<tbody>
						{loop obj="treinador" func="getListSearch" params="[array;array;'nome';'ASC';1;1]"}
						<tr>
							<td>{i}</td>
							{% manuel %}
							{% pico2 %}
							<td>{random}</td>
							<td>{id_treinador}</td>
							<td>{nome}</td>
						</tr>
						{/loop}
					</tbody>
				</table>
				<?php
						$path 		= $_SESSION['path'];
						$tblAutoTpl = $_SESSION['TplObject'];
						echo $tblAutoTpl->tpl_page($path.'ui/tpl_maindiv2.php',true,false);
				?>
			</div>
		</div>