<?php
# Template - HTML / PHP
# @author: 	Paulo Mota
# @version: 0.0.0.1
# @date: 	24-08-2013
# LINKS:
# 

	#require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjRawDOM.class.php');
	require_once('/Applications/MAMP/bin/mamp/ginasio/'.'core/scripts/dbTable.class.php');
	
	class TobjTemplate
	{
		public $htmlDOM;
		public $html;
		public $filename;
		public $values = array();

		public function __construct($Afilename)
		{
			$this->filename = $Afilename;
			$this->html 	= file_get_contents($this->filename);
			$this->htmlDOM 	= new TobjRawDOM();
		}
		public function __destruct()
		{
			unset($htmlDOM);
			unset($html);
			unset($filename);
			unset($values);
		}
		public function setVar($AKey, $AValue)
		{
			$this->values[$AKey] = $AValue;
		}
		private function getLoopInnerHTML($AFind)
		{
			$Result = '';
			$iFindLen = strlen('{loop '.$AFind.'}');
			$pos = stripos($this->html,'{loop '.$AFind.'}');
			if ($pos != false)
			{
				$sAux = substr($this->html,$pos+$iFindLen);
				$pos = stripos($sAux,'{/loop}');
				if ($pos != false)
				{
					$Result = substr($sAux,0,$pos);
				}
			}
			return $Result;
		}
		public function BuildRows($AFind,$AiniKey,$AendKey,$AArray)
		{
			$loopHTML 	 = $this->getLoopInnerHTML($AFind);
			$auxLoopHTML = array();
			foreach ($AArray as $keyI => &$arrValue)
			{
				$DestHtml = $loopHTML;
				foreach($arrValue as $key => &$value)
				{
					$DestHtml = str_ireplace($AiniKey.$key.$AendKey, $value, $DestHtml);
				}
				$auxLoopHTML[] = $DestHtml;
			}

			$this->html = str_ireplace('{loop '.$AFind.'}'.$loopHTML.'{/loop}', implode("\t\n",$auxLoopHTML), $this->html);
		}
		public function echohtml()
		{
			foreach ($this->values as $key => &$value)
			{
				$this->html = str_ireplace($key,$value,$this->html);
			}
			return $this->html;
		}
		public function SetVarsBASH()
		{
			foreach ($this->values as $key => &$value)
			{
				$this->html = str_ireplace($key,$value,$this->html);
			}
			$this->values = array();
		}
		public function replaceDOM($AFind,$AWhereRep,$ANewContent)
		{
			$this->html = $this->htmlDOM->replaceFind($AFind,$ANewContent,$this->html,$AWhereRep);
		}
		public function setVarArray($AiniKey,$AendKey,$AArray)
		{
			foreach ($AArray as $key => &$value)
			{
				#if (empty($value)) $value = '';
				$this->setVar($AiniKey.$key.$AendKey, $value);
			}
		}
		public function setVarArray_Section($AFind,$AiniKey,$AendKey,$AArray)
		{
			$Result = $this->htmlDOM->getTagHTML($AFind,$this->html);
			$ANewContent = $Result["text"];
			
			#$ccI = count($AArray);
			#for ($i=0; $i<$ccI; $i++)
			foreach ($AArray as $keyI => &$arrValue)
			{
				foreach ($arrValue as $key => &$value)
				{
					$ANewContent = str_ireplace($AiniKey.$key.$AendKey, $value, $ANewContent);
				}
			}
			$this->html = $this->htmlDOM->replaceFind($AFind,$ANewContent,$this->html,'outertext');
		}
	}
?>