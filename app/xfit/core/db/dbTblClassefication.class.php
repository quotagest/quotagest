<?php
	require_once('/Applications/MAMP/bin/mamp/ginasio/'.'core/db/dbTable.class.php');
	
	class dbTblClassefication extends dbTable
	{
		public $ID_MAIN_ENCRYPTED = '';
		public $ID_MAIN_DECRYPTED = '';
		public $ID_USER = '';
		public $ID_ASSOC= '';

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName("classefications");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_USER);
			unset($this->ID_SSOC);
			unset($this->ID_MAIN_ENCRYPTED);
			unset($this->ID_MAIN_DECRYPTED);
		}

		public function setFieldsInformation()
		{
			$fieldsinfo = array('id' 			=> array('type'=>'INTEGER', 'required'=>true),
								'codgroup' 		=> array('type'=>'INTEGER', 'required'=>true),
								'class' 		=> array('type'=>'STRING', 'required'=>true),
								'sexo' 			=> array('type'=>'STRING', 'required'=>true),
								'percentagem' 	=> array('type'=>'INTEGER', 'required'=>true),
								'age_min' 		=> array('type'=>'INTEGER', 'required'=>true),
								'age_max' 		=> array('type'=>'INTEGER', 'required'=>true),
								'min_index' 	=> array('type'=>'FLOAT', 'required'=>true),
								'max_index' 	=> array('type'=>'FLOAT', 'required'=>true),
								'enabled' 		=> array('type'=>'INTEGER', 'required'=>true)
								);
			parent::setFieldsInfo($fieldsinfo);
		}

		public function set_IDMAIN($AsVar)
		{
			$this->ID_MAIN_ENCRYPTED = $AsVar;
			$this->ID_MAIN_DECRYPTED = ($AsVar !== '') ? parent::decryptVar($AsVar) : '';
		}
		public function set_IDUSER($AsVar)  { $this->ID_USER = $AsVar; }
		public function set_IDASSOC($AsVar) { $this->ID_ASSOC = $AsVar; }

		public function INSERT($A_POST)
		{
			$Result = false;
			$NEW_ID = false;

			#if ($this->ID_ASSOC !== '')
			#{
				parent::SetFieldsValue(array('id'			=>'NULL',
											'codgroup'		=>$A_POST['codgroup'],
											'class'			=>$A_POST['class'],
											'sexo'			=>$A_POST['sexo'],
											'percentagem'	=>$A_POST['percentagem'],
											'age_min'		=>$A_POST['age_min'],
											'age_max'		=>$A_POST['age_max'],
											'min_index'		=>$A_POST['min_index'],
											'max_index'		=>$A_POST['max_index'],
											'enabled'		=>'0')
										);
				$Result = parent::Build2RunQuery('INSERT');

				if ($Result)
				{
					$NEW_ID = parent::getInsertedID();
					$NEW_ID = parent::encryptVar($NEW_ID);
				}
			#}
			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ID);
		}
		public function UPDATE($A_POST)
		{
			parent::SetFieldsValue(array('id'			=>false,
										'codgroup'		=>$A_POST['codgroup'],
										'class'			=>$A_POST['class'],
										'sexo'			=>$A_POST['sexo'],
										'percentagem'	=>$A_POST['percentagem'],
										'age_min'		=>$A_POST['age_min'],
										'age_max'		=>$A_POST['age_max'],
										'min_index'		=>$A_POST['min_index'],
										'max_index'		=>$A_POST['max_index'],
										'enabled'		=>false)
									);
			$Result = parent::Build2RunQuery('UPDATE');

			return $Result;
		}
	}
?>