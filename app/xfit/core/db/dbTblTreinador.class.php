<?php
	require_once('/Applications/MAMP/bin/mamp/ginasio/'.'core/db/dbTable.class.php');
	
	class dbTblTreinador extends dbTable
	{
		public $ID_MAIN_ENCRYPTED = '';
		public $ID_MAIN_DECRYPTED = '';
		public $ID_USER = '';
		public $ID_ASSOC= '';

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName("classefications");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_USER);
			unset($this->ID_SSOC);
			unset($this->ID_MAIN_ENCRYPTED);
			unset($this->ID_MAIN_DECRYPTED);
		}

		public function setFieldsInformation()
		{
			$fieldsinfo = array('id' 				=> array('type'=>'INTEGER', 'required'=>true),
								'nome' 				=> array('type'=>'STRING', 'required'=>true),
								'id_user_criacao' 	=> array('type'=>'INTEGER', 'required'=>true),
								'id_user_alterado' 	=> array('type'=>'INTEGER', 'required'=>true),
								'data_criacao' 		=> array('type'=>'DATETIME', 'required'=>true),
								'data_alteracao' 	=> array('type'=>'DATETIME', 'required'=>true),
								'enabled' 			=> array('type'=>'INTEGER', 'required'=>true)
								);
			parent::setFieldsInfo($fieldsinfo);
		}

		public function set_IDMAIN($AsVar)
		{
			$this->ID_MAIN_ENCRYPTED = $AsVar;
			$this->ID_MAIN_DECRYPTED = ($AsVar !== '') ? parent::decryptVar($AsVar) : '';
		}
		public function set_IDUSER($AsVar)  { $this->ID_USER = $AsVar; }
		public function set_IDASSOC($AsVar) { $this->ID_ASSOC = $AsVar; }

		public function INSERT($A_POST)
		{
			$Result = false;
			$NEW_ID = false;

			parent::SetFieldsValue(array('id'				=>'NULL',
										'nome'				=>$A_POST['nome'],
										'id_user_criacao'	=>'1',
										'id_user_alterado'	=>'1',
										'data_criacao'		=>date('Y-m-d h:i:s'),
										'data_alteracao'	=>date('Y-m-d h:i:s'),
										'enabled'			=>'0')
									);
			$Result = parent::Build2RunQuery('INSERT');

			if ($Result)
			{
				$NEW_ID = parent::getInsertedID();
				$NEW_ID = parent::encryptVar($NEW_ID);
			}

			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ID);
		}
		public function UPDATE($A_POST)
		{
			parent::SetFieldsValue(array('id'				=>false,
										'nome'				=>$A_POST['nome'],
										'id_user_criacao'	=>false,
										'id_user_alterado'	=>'1',
										'data_criacao'		=>false,
										'data_alteracao'	=>date('Y-m-d h:i:s'),
										'enabled'			=>false)
									);
			$Result = parent::Build2RunQuery('UPDATE');

			return $Result;
		}

		public function getListSearch($AarrFieldName,$AarrValue, $FieldToOrder,$OrderBy, $iPage=0, $iLimPerPage=10)
		{
			$ROW 	= array();
			$EXIST 	= false;
			$TotalPages = 0;
			$AarrValue  = parent::processArraySearch($AarrFieldName,$AarrValue);
			
			$OrderBy = ($OrderBy === '') ? ' ASC ' : $OrderBy;
			switch ($FieldToOrder)
			{
				case 'nome'			: $FieldToOrder = 'treinador.nome '.$OrderBy; break;
				default 			: $FieldToOrder = 'treinador.nome '.$OrderBy; break;
			}
			$FieldToOrder = ' ORDER BY '.$FieldToOrder;
	
			$sSQLLIKE = ($AarrValue[0] === '*') ? '' : ' AND ('.parent::WHEREUPDATE_PARSER($AarrFieldName,$AarrValue,' OR ','LIKE',false).') ';
			$sLIMIT = parent::build_LIMITStatement($iPage,$iLimPerPage);
			
			$SQL  = "SELECT treinador.id AS 'id_treinador',
							treinador.nome AS 'nome'
					";
			$SQL .= ' FROM treinador ';
			#$SQL .= ' WHERE treinador.id_assoc="'.$this->ID_ASSOC.'" ';
			$SQL .= ' WHERE treinador.enabled="0" ';
			$SQL .= $sSQLLIKE;
			$SQL .= $FieldToOrder;
			$SQL .= $sLIMIT;

			$Result = parent::getResultsOut($SQL,array('ENCRYPT','STRING'));
			
			return $Result;
		}
		public function testeNew()
		{
			return '<br/>Eu sou o Return de uma função (ECHO)';
			#return array('echo'=>true, 'value'=>'testeNew');
		}
		public static function testeNew2($AParam)
		{
			if (is_array($AParam))
				$AParam = json_encode($AParam);
			return '<br/>Eu venho de um objecto ('.$AParam.')!';
		}
		public static function testeNew3($AParam)
		{
			if (is_array($AParam))
				$AParam = json_encode($AParam);
			return '<br/>Eu venho de uma Class ('.$AParam.')!';
		}
		public static function testeNew4($AParam)
		{
			if (is_array($AParam))
				$AParam = json_encode($AParam);
			return $AParam;
		}
	}
?>