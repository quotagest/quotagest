<?php

	function SETPATH($sTYPEPARENT,$sPATH)
	{
		$ARRAYPATH	= array('PATH_APP'					=> '',
							'PATH_APP_CONFIGS'			=> 'configs/',
							'PATH_APP_CORE'				=> 'core/',
							'PATH_APP_CORE_DB'			=> 'core/db/',
							'PATH_APP_CORE_FUNCTIONS'	=> 'core/functions/',
							'PATH_APP_CORE_SCRIPTS'		=> 'core/scripts/',
							'PATH_APP_CORE_MODULES'		=> 'core/modules/',
							'PATH_APP_CORE_TPL'			=> 'core/tpl/',
							'PATH_APP_CSS'				=> 'css/',
							'PATH_APP_CSS_TYPE'			=> 'css/type/',
							'PATH_APP_EXTERNAL'			=> 'external/',
							'PATH_APP_EXTERNAL_JQUERY'	=> 'external/jquery/',
							'PATH_APP_IMG'				=> 'img/',
							'PATH_APP_IMG_ASSOCS'		=> 'img/assocs/',
							'PATH_APP_IMG_ICONS'		=> 'img/icons/',
							'PATH_APP_IMG_ICONS_USERS'	=> 'img/icons/users/',
							'PATH_APP_IMG_SOCIOS'		=> 'img/socios/',
							'PATH_APP_IMG_USERS'		=> 'img/users/',
							'PATH_APP_JS'				=> 'js/',
							'PATH_APP_PAGES'			=> 'pages/',
							'PATH_APP_PLUGINS'			=> 'plugins/',
							'PATH_APP_TMP'				=> 'tmp/',
							'PATH_APP_UI'				=> 'ui/',
							'PATH_APP_UI_ASSOCIACAO'	=> 'ui/associacao/',
							'PATH_APP_UI_CFG'			=> 'ui/cfg/',
							'PATH_APP_UI_CFG_INSERT'	=> 'ui/cfg/insert/',
							'PATH_APP_UI_CONTABILIDADE'	=> 'ui/contabilidade/',
							'PATH_APP_UI_ENTIDADES'		=> 'ui/entidades/', 
							'PATH_APP_UI_FERRAMENTAS'	=> 'ui/ferramentas/', 
							'PATH_APP_UI_MODALIDADES'	=> 'ui/modalidades/', 
							'PATH_APP_UI_MODULES'		=> 'ui/modules/', 
							'PATH_APP_UI_QUOTAS'		=> 'ui/quotas/',
							'PATH_APP_UI_REGISTO'		=> 'ui/registo/',
							'PATH_APP_UI_RELATORIOS'	=> 'ui/relatorios/',
							'PATH_APP_UI_SOCIOS'		=> 'ui/socios/',
							'PATH_APP_UI_USER'			=> 'ui/user/'
						);

		$PATH_ROOT	= $_SERVER['DOCUMENT_ROOT'].'/';
		$PATH_URL	= 'http://app.quotagest.com/';

		if ($sTYPEPARENT === 'ROOT')	// PARA FICHEIROS PHP
		{
			return $PATH_ROOT.$ARRAYPATH[$sPATH];
		}
		else
		if ($sTYPEPARENT === 'URL')		// PARA IMGS, CSS, JS
		{
			return $PATH_URL.$ARRAYPATH[$sPATH];
		}
	}
?>