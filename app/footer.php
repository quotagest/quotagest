<?php
	#ob_start();	#Output Buffering - BEGIN
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);

	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	$objTempHeader = new TobjTemplate($_SERVER['DOCUMENT_ROOT'].'/footer.html');
	$objTempHeader->setVar('{$PATH_APP_JS}', SETPATH('URL','PATH_APP_JS'));
	$objTempHeader->setVar('{$PATH_APP}', SETPATH('URL','PATH_APP'));
	$objTempHeader->setVar('{$PATH_APP_PAGES}', SETPATH('URL','PATH_APP_PAGES'));
	$objTempHeader->setVar('{$PATH_APP_UI_USER}', SETPATH('URL','PATH_APP_UI_USER'));

	if ($DO_LOGIN)
		$objTempHeader->replaceDOM('[class=account]','outertext','');

	echo $objTempHeader->echohtml();
	unset($objTempHeader);

	ob_end_flush(); #Output Buffering - END
?>