<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	
	ob_start("ob_gzhandler");
	
	
	#http://stackoverflow.com/questions/189113/how-do-i-get-current-page-full-url-in-php-on-a-windows-iis-server/189167#189167
	$FULL_URL = $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

	#echo '<h1>'.htmlentities($_SERVER['PHP_SELF']).'</h1>';
	#echo '<h1>'.$FULL_URL.'</h1>';
	function getOption2($AURL,$AOptionName)
	{
		$Result = '';
		$pos = stripos($AURL, $AOptionName);
		if ($pos != false)
		{
			$Result = substr($AURL, $pos+strlen($AOptionName)+1);
			$pos = stripos($Result, '/');
			if ($pos != false)
			{
				$Result = substr($Result, 0, $pos);
			}
		}
		return $Result;
	}

	$OP = (isset($_GET['op'])) ? $_GET['op'] : '';
	#$OP = getOption2($FULL_URL,'op');
	#var_dump($OP);
	$sClassActivo = 'class="activo"';
	
	$objTempHeader = new TobjTemplate(SETPATH('ROOT','PATH_APP').'neo/header.php');
	$objTempHeader->setVar('{$caSocios}',		($OP === 'socios') ? $sClassActivo : '');
	$objTempHeader->setVar('{$caQuotas}',		($OP === 'quotas') ? $sClassActivo : '');
	$objTempHeader->setVar('{$caModalidades}',	($OP === 'modalidades') ? $sClassActivo : '');
	$objTempHeader->setVar('{$caEntidades}',	($OP === 'entidades') ? $sClassActivo : '');
	$objTempHeader->setVar('{$caConfiguracoes}',($OP === 'configs') ? $sClassActivo : '');
	$objTempHeader->setVar('{$caHistorico}',	($OP === 'historico') ? $sClassActivo : '');
	echo $objTempHeader->echohtml();
	unset($objTempHeader);

	if ($OP !== '')
	{
		switch ($OP)
		{
			case 'socios'	: include('ui/socios/index.php'); 	 break;
			case 'quotas'	: include('ui/quotas/index.php'); 	 break;
			case 'entidades': include('ui/entidades/index.php'); break;
			case 'configs'	: include('ui/cfg/index.php'); 		 break;
			case 'historico': include('ui/historico/index.php'); break;
			
			default: break;
		}
	}
?>
<script>
	ObjListagem.setVariables('<?php echo $OP; ?>');
	ObjListagem.LoadMore('begin','list');
	ObjTabs.setObjData('<?php echo $OP; ?>');
</script>
<?php
	$GET_TAB = (isset($_GET['tab'])) ? $_GET['tab'] : '';
	if ($GET_TAB != '')
		echo "<script>ObjTabs.activeTabArea(undefined,".$GET_TAB.");</script>";
?>

<?php
	include 'footer.php';
	ob_flush();
?>