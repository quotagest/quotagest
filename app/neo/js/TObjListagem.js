/*
	var h = document.getElementById('someDiv').clientHeight;
	var h = document.getElementById('someDiv').offsetHeight;
	var h = document.getElementById('someDiv').scrollHeight;
	clientHeight includes the height and vertical padding.
	
	offsetHeight includes the height, vertical padding, and vertical borders.
	
	scrollHeight includes the height of the contained document
	(would be greater than just height in case of scrolling),
	vertical padding, and vertical borders.

	console.log("scrollHeight: "+event.target.scrollHeight+"\n"+
				"scrollLeft: "+event.target.scrollLeft+"\n"+
				"scrollTop: "+event.target.scrollTop+"\n"+
				"scrollWidth: "+event.target.scrollWidth
				);
*/
		function TObjModal()
		{
			var Self 					= this;
			this.eTargetElement 		= false;
			this.sLastAreaCall 			= '';
			this.UpdateElement 			= UpdateElement;
			this.CloseWindow 			= CloseWindow;
			this.ShowWindow 			= ShowWindow;
			this.OpenWindow 			= OpenWindow;
			this.ValidateForm 			= ValidateForm;
			
			function UpdateElement(AObjJSON)
			{
				switch(this.eTargetElement.tagName)
				{
					case 'INPUT':
						{
							switch(this.eTargetElement.type)
							{
								case 'text':
									{
										
									} break;
								case 'radio':
									{
										
									} break;
								case 'checkbox':
									{
										
									} break;
							}
						} break;
					case 'SELECT':
						{
							this.eTargetElement.insertAdjacentHTML('beforeend','<option value="'+AObjJSON.ID+'" selected="">'+AObjJSON.Nome+'</option>');
							this.eTargetElement.selectedIndex = this.eTargetElement.options.length-1;
						} break;
					case 'TEXTAREA':
						{
						
						} break;
				}
				
				this.eTargetElement = false;
			}
			function CloseWindow()
			{
				var eModalArea = GetObjectsByTagNameData('SPAN','data-name','ShowModal');
				if (eModalArea !== false)
				{
					AddToClass(eModalArea,'hidden');
				}
			}
			function ShowWindow(AsNewHTML)
			{
				var eModalArea = GetObjectsByTagNameData('SPAN','data-name','ShowModal');
				if (eModalArea !== false)
				{
					if (AsNewHTML !== false)
					{
						ReplaceInnerHTML(eModalArea,AsNewHTML);
					}
					RemoveFromClass(eModalArea,'hidden');
					
					/* http://stackoverflow.com/a/16278107 */
					var arr = eModalArea.getElementsByTagName('script');
					for (var i=0, ccI=arr.length; i<ccI; i++)
						eval(arr[i].innerHTML);
				}
			}
			function OpenWindow(AsElem,AsArea)
			{
				this.eTargetElement = GetObjectByName(AsElem,0);
				if (this.sLastAreaCall !== AsArea)
				{
					this.sLastAreaCall = AsArea;
					
					var url 	= '';
					var params  = toggleUrlParam('','tab','0',false);
					switch(AsArea)
					{
						case 'socio-tipo':
							{
								url = '/neo/ui/socios/novo-sociotipo.php';
							} break;
						case 'socio-estado':
							{
								url = '/neo/ui/socios/novo-socioestado.php';
							} break;
					}
					
					var func = function(sResult)
					{
						Self.ShowWindow(sResult);
					}
					
					ObjAjax.Ajax({type:'POST', url:url, data:params, done:func});
				}
				else
				{
					this.ShowWindow(false);
				}
			}
			
			function ValidateForm(AsFormName)
			{
				ObjFormValid.onNotValid = function(AeField) {
																AeField.focus();
																CREATE_POP_MSG("O campo '<b>"+AeField.getAttribute("placeholder")+"</b>' encontra-se vazio.",3000);
															};
				ObjFormValid.onRecordSaved = function(AobjJSON)
														{
															CREATE_POP_MSG('Registo salvo com sucesso! '+getCloseLink(),3000);
															
															var objJSON = {};
																objJSON.Nome = document.forms[AsFormName].elements['Nome'].value;
																objJSON.ID   = AobjJSON.idinserted;
															
															Self.UpdateElement(objJSON);
															Self.CloseWindow();
															ObjFormValid.CleanForm(AsFormName,['Joia'],['0.00 €']);
														};
				ObjFormValid.onRecordSavedError = function() {
																CREATE_POP_MSG('Ocorreu um erro. <br/> Porfavor tente novamente.'+getCloseLink(),3000);
															};
				ObjFormValid.ValidateForm(AsFormName,'append');
			}
		}
		
		
		function TObjFormQuotas()
		{
			var Self 					= this;
			this.eTBody 				= false;
			this.eForm 					= false;
			this.eDEmes 				= false;
			this.eAOmes 				= false;
			this.eDEano 				= false;
			this.eAOano 				= false;
			this.eMontante 				= false;
			this.eMensalidade 			= false;
			this.eAnos 					= false;
			this.eDistribuicao 			= false;
			this.Meses 					= ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
			this.MesesShort 			= ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'];
			this.iDEmes 				= 1;
			this.iAOmes 				= 12;
			this.iDEano 				= 0;
			this.iAOano 				= 0;
			
			this.setVariables 			= setVariables;
			this.getMontante 			= getMontante;
			this.getMontanteFloat 		= getMontanteFloat;
			this.getMesesByMensalidade 	= getMesesByMensalidade;
			this.updateMontante 		= updateMontante;
			this.updateTabela 			= updateTabela;
			this.TipoMensalidade 		= TipoMensalidade;
			this.CriarQuotas 			= CriarQuotas;
			this.getSociosFiltro 		= getSociosFiltro;
			
			function setVariables()
			{
				this.eTBody = GetObjectsByTagNameData('TABLE','data-id','table-prev');
				this.eTBody = this.eTBody.children[1];
				
				this.eForm  		= document.forms['form-quotas'];
				this.eDEmes 		= this.eForm.elements['DEmes'];
				this.eAOmes 		= this.eForm.elements['AOmes'];
				this.eDEano 		= this.eForm.elements['DEano'];
				this.eAOano 		= this.eForm.elements['AOano'];
				this.eMontante 		= this.eForm.elements['montante'];
				this.eMensalidade 	= this.eForm.elements['TipoMensalidade'];
				this.eAnos 			= this.eForm.elements['anos'];
				this.eDistribuicao 	= this.eForm.elements['Distribuicao'];
				
				this.iDEano 		= this.eDEano.options[this.eDEano.selectedIndex].value;
				this.iAOano 		= this.eAOano.options[this.eAOano.selectedIndex].value;
				
				this.TipoMensalidade();
			}
			function setMeses(AeSelect)
			{
				
			}
			function getMontante()
			{
				return FloatToCurrency(this.getMontanteFloat());
			}
			function getMontanteFloat()
			{
				var sMontante = Trim(this.eMontante.value);
					sMontante = (sMontante !== '') ? FloatToString(sMontante) : '0.00';
				return sMontante;
			}
			function getMesesByMensalidade(AIndexMensal,AFirstChars)
			{
				AIndexMensal += 1;
				AFirstChars = (AFirstChars != undefined) ? AFirstChars : false;
				
				var objOptions = {};
				var k = 0;
				for(var i=0; i<12; i+=AIndexMensal)
				{
					var arrValues = new Array();
					var arrMeses = new Array();
					for(var j=0; j<AIndexMensal; j++)
					{
						arrValues[j] = (i+1)+j;
						if (AFirstChars)
						{
							arrMeses[j] = this.MesesShort[i+j];
						}
						else
						{
							arrMeses[j] = this.Meses[i+j];
						}
					}
					objOptions[k] = {};
					objOptions[k].values = arrValues.join("/");
					objOptions[k].months = arrMeses.join("/");
					objOptions[k].lastm  = i+AIndexMensal;
					objOptions[k].html = '<option data-lastm="'+objOptions[k].lastm+'" value="'+objOptions[k].values+'">'+objOptions[k].months+'</option>'+"\n";
					k++;
				}
				objOptions.length = k;
				
				return objOptions;
			}
			function updateMontante(event)
			{
				var ccI = this.eTBody.children.length;
				var sMontante = this.getMontanteFloat();
				switch(this.eDistribuicao.selectedIndex)
				{
					case 0:
						{
							
						} break;
					case 1:
						{
							sMontante = (sMontante/ccI);
						} break;
					case 2:
						{
							
						} break;
				}
				sMontante = FloatToCurrency(sMontante);
				
				for(var i=0; i<ccI; i++)
				{
					this.eTBody.children[i].children[2].children[0].value = sMontante;
				}
			}
			function updateTabela(ASelf)
			{
				this.iDEano = this.eDEano.options[this.eDEano.selectedIndex].value;
				this.iAOano = this.eAOano.options[this.eAOano.selectedIndex].value;
				var ccDE = this.eDEmes.options.length;
				var ccAO = (this.eDEmes.options.length - this.eDEmes.selectedIndex);
				var arrTR = new Array();
				var k 	= 0;
				
				var sMeses 		= '';
				var sAno 		= this.iDEano;
				var sMontante 	= this.getMontante();
				var lastmonth 	= '';
				
				// DE MES/ANO
				for(var i=this.eDEmes.selectedIndex; i<ccDE; i++,k++)
				{
					sMeses = this.eDEmes.options[i].text;
					lastmonth = this.eDEmes.options[i].getAttribute("data-lastm");
					
					day = 30;
					if (lastmonth === '2')
						day = 28;
					if (lastmonth < 10)
						lastmonth = '0'+lastmonth;
						
					arrTR[k] = new Array();
					arrTR[k][0] = '<tr>';
					arrTR[k][1] = 	'<td>'+'<input type="text" name="descricao" value="'+sMeses+'" >'+'</td>';
					arrTR[k][2] = 	'<td>'+'<input type="text" name="ano" value="'+sAno+'" size="8" >'+'</td>';
					arrTR[k][3] = 	'<td>'+'<input type="text" name="montante" value="'+sMontante+'" size="8" >'+'</td>';
					arrTR[k][4] = 	'<td>'+'<input type="date" name="prazopag" value="'+sAno+'-'+lastmonth+'-'+day+'">'+'</td>';
					arrTR[k][5] = 	'<td class="hidden">'+'<input type="text" name="lastm" value="'+lastmonth+'">'+'</td>';
					arrTR[k][6] = 	'<td class="hidden">'+'<input type="text" name="isquota" value="1">'+'</td>';
					arrTR[k][7] = '</tr>';
					
					arrTR[k] = arrTR[k].join("\n");
				}
				if (this.iAOano !== this.iDEano)
				{
					// AO MES/ANO
					var sAno = this.iAOano;
					for(var i=0; i<this.eAOmes.selectedIndex+1; i++,k++)
					{
						sMeses = this.eAOmes.options[i].text;
						lastmonth = this.eAOmes.options[i].getAttribute("data-lastm");
						
						day = 30;
						if (lastmonth === '2')
							day = 28;
						if (lastmonth < 10)
							lastmonth = '0'+lastmonth;
							
						arrTR[k] = new Array();
						arrTR[k][0] = '<tr>';
						arrTR[k][1] = 	'<td>'+'<input type="text" name="descricao" value="'+sMeses+'" >'+'</td>';
						arrTR[k][2] = 	'<td>'+'<input type="text" name="ano" value="'+sAno+'" size="8" >'+'</td>';
						arrTR[k][3] = 	'<td>'+'<input type="text" name="montante" value="'+sMontante+'" size="8" >'+'</td>';
						arrTR[k][4] = 	'<td>'+'<input type="date" name="prazopag" value="'+sAno+'-'+lastmonth+'-'+day+'">'+'</td>';
						arrTR[k][5] = 	'<td class="hidden">'+'<input type="text" name="lastm" value="'+lastmonth+'">'+'</td>';
						arrTR[k][6] = 	'<td class="hidden">'+'<input type="text" name="isquota" value="1">'+'</td>';
						arrTR[k][7] = '</tr>';
						
						arrTR[k] = arrTR[k].join("\n");
					}
				}
				else
				{
					if (this.eAOmes.selectedIndex !== 11)
					{
						var ccI = arrTR.length-((this.eDEmes.length)-this.eAOmes.selectedIndex);
						for(var i=arrTR.length-1; i>ccI; i--)
						{
							arrTR[i] = '';
						}
					}
				}
				sTRQuotas = arrTR.join("\n");
				delete arrTR;
				ReplaceInnerHTML(this.eTBody,sTRQuotas);
				this.updateMontante();
			}
			function TipoMensalidade(ASelf)
			{
				var arrOptions = new Array();
				var Options = false;
				
				switch(this.eMensalidade.selectedIndex)
				{
					case 1:
						{
							Options = this.getMesesByMensalidade(0,false);
						} break;
					case 2:
						{
							Options = this.getMesesByMensalidade(1,true);
						} break;
					case 3:
						{
							Options = this.getMesesByMensalidade(2,true);
						} break;
					case 4:
						{
							Options = this.getMesesByMensalidade(3,true);
						} break;
					case 5:
						{
							Options = this.getMesesByMensalidade(5,true);
						} break;
					case 6:
						{
							Options = { length : 1,
										0: {
											values : 12,
											months : 'Anual',
											lastm  : 12,
											html : '<option data-lastm="12" value="Anual">Anual</option>'+"\n"
										}
									};
						} break;
				}
				
				var arrDE = new Array();
				var arrAO = new Array();
				for(var i=0, ccI=Options.length; i<ccI; i++)
				{
					arrDE[i] = Options[i].html;
					arrAO[i] = Options[i].html;
				}
				ReplaceInnerHTML(this.eDEmes,arrDE.join("\n"));
				ReplaceInnerHTML(this.eAOmes,arrAO.join("\n"));
				delete arrDE;
				delete arrAO;
				delete Options;
				
				this.iDEano = this.eDEano.options[this.eDEano.selectedIndex].value;
				this.iAOano = this.eAOano.options[this.eAOano.selectedIndex].value;
				if (this.eAOmes.selectedIndex === 0)
					this.eAOmes.selectedIndex = this.eAOmes.options.length-1;
					
				this.updateTabela();
			}
			function getSociosFiltro()
			{
				var eForm 	 		= document.forms['form-socios-filter'];
				var eEstadoSocio 	= GetObjectsByName('EstadoSocio[]');
				var eTipoSocio 		= GetObjectsByName('TipoSocio[]');
				var eSexoSocio 		= GetObjectsByName('SexoSocio');
				var sLabel 			= '';
				var i=0;
				var k=0;
				
				var arrParams = {};
					arrParams.estado_socio = {};
				for(i=0, ccI=eEstadoSocio.length; i<ccI; i++)
				{
					if (eEstadoSocio[i].checked === true)
					{
						sLabel = eEstadoSocio[i].labels[0].innerHTML;
						
						arrParams.estado_socio[k] = eEstadoSocio[i].value;
						k++;
					}
				}
				k=0;
				arrParams.tipo_socio = {};
				for(i=0, ccI=eTipoSocio.length; i<ccI; i++)
				{
					if (eTipoSocio[i].checked === true)
					{
						sLabel = eTipoSocio[i].labels[0].innerHTML;
						
						arrParams.tipo_socio[k] = eTipoSocio[i].value;
						k++;
					}
				}
				k=0;
				arrParams.sexo = {};
				for(i=0, ccI=eSexoSocio.length; i<ccI; i++)
				{
					if (eSexoSocio[i].checked === true)
					{
						sLabel = eSexoSocio[i].labels[0].innerHTML;
						
						arrParams.sexo[k] = eSexoSocio[i].value;
						k++;
					}
				}
				
				return arrParams;
			}
			function CriarQuotas(ASelf)
			{
				objJSON = {};
				objJSON.length = this.eTBody.children.length;
				objJSON.ano_inicio 	= this.iDEano;
				objJSON.ano_fim 	= this.iAOano;
				objJSON.total 		= 0.0;
				for(var i=0; i<objJSON.length; i++)
				{
					objJSON[i] = {};
					objJSON[i].descricao 	= this.eTBody.children[i].children[0].children[0].value;
					objJSON[i].ano 			= this.eTBody.children[i].children[1].children[0].value;
					objJSON[i].montante 	= FloatToString(this.eTBody.children[i].children[2].children[0].value);
					objJSON[i].prazpag 		= this.eTBody.children[i].children[3].children[0].value;
					objJSON[i].lastmonth 	= this.eTBody.children[i].children[4].children[0].value;
					objJSON[i].isquota 		= this.eTBody.children[i].children[5].children[0].value;
					objJSON.total += parseFloat(objJSON[i].montante);
				}
				var objFiltros = this.getSociosFiltro();
				
				var func = function(sResult)
				{
					console.log(sResult);
					/*
					var jsonData = ParseJSON(sResult);
					if (jsonData)
					{
						var bResult= jsonData.result;
						var sNewID = (jsonData.idinserted) ? jsonData.idinserted : '';
						
						if (bResult == true)
						{
							
						}
						else
						if (bResult == false)
						{
							
						}
					}
					*/
				}
				var sJSON = JSON.stringify({_POST : objJSON, _FILTROS : objFiltros});
				console.log(sJSON);
				/*
				var url 	= '/core/scripts/';
				var params 	= '';
				var extraparams = '';
				var objParams = {};
					objParams.section 	= 'quotas-teste';
					objParams.opera 	= 'append';
				var sJSON = JSON.stringify({_POST : objJSON, _FILTROS : objFiltros});
					objParams.jsonPOST 	= encode64(sJSON);
					
				ToggleRecordAjax(objParams,extraparams,func);
				*/
			}
		}
		
		function TObjTabs(AAction)
		{
			var Self 			= this;
			this.sAction 		= '';
			this.eTabsParent 	= undefined;
			this.eTabs			= undefined;
			this.eTabsAreas 	= undefined;
			this.eFocusedTab 	= undefined;
			this.iIdxFocusTab 	= 0;
			
			this.setObjData 	= setObjData;
			this.setTabsAreas 	= setTabsAreas;
			this.activeTabArea 	= activeTabArea;

			function setObjData(AAction)
			{
				this.sAction 		= AAction;
				this.eTabsParent 	= GetObjectsByTagNameData('div','data-id','tabs');
				this.eTabs			= this.eTabsParent.children;
				this.eTabsAreas 	= undefined;
				this.eFocusedTab 	= undefined;
				this.iIdxFocusTab 	= 0;
				this.setTabsAreas();
			}
			function setTabsAreas()
			{
				var sTabName = '';
				this.eTabsAreas = new Array();
				if (this.eTabs)
				{
					for (var i=0, ccI=this.eTabs.length; i<ccI; i++)
					{
						sTabName = this.eTabs[i].getAttribute('data-name');
						this.eTabsAreas[i] = GetObjectsByTagNameData('span','data-name',sTabName);
					}
				}
			}
			function activeTabArea(ASelf,AidxToSelect)
			{
				var ADataName = (ASelf != undefined) ? ASelf.getAttribute('data-name') : '';
				var sTabName  = '';
				if (this.eTabs == undefined)
				{
					this.setObjData('');
				}
					
				for (var i=0, ccI=this.eTabs.length; i<ccI; i++)
				{
					sTabName = this.eTabs[i].getAttribute('data-name');
					
					if ((this.eTabs[i].className.indexOf('active') != -1) && (AidxToSelect == undefined))
						this.iIdxFocusTab = i;
					
					if (((sTabName === ADataName) && (AidxToSelect == undefined)) ||
						(i === AidxToSelect))
					{
						RemoveFromClass(this.eTabsAreas[i],'hidden');
						AddToClass(this.eTabs[i],'active');
						AidxToSelect = i;
					}
					else
					{
						AddToClass(this.eTabsAreas[i],'hidden');
						RemoveFromClass(this.eTabs[i],'active');
					}
				}
				window.history.pushState('', 'Tab Activa',toggleUrlParam('','tab',AidxToSelect));
			}
		}
		function TObjConfiguracoes()
		{
			var Self 					= this;
			this.eItem					= undefined;
			this.iIdxItem 				= 0;

			this.getItemIndex 			= getItemIndex;
			this.editItem 				= editItem;
			this.createPagForm 			= createPagForm;
			this.keyEvent 				= keyEvent;
			this.ajaxToggleRecord 		= ajaxToggleRecord;
			
			function keyEvent(event)
			{
				if (event.target.parentNode.tagName === 'SPAN')
				{
					var sAction = 'inserir';
					var target  = event.target.parentNode.getElementsByTagName('SPAN')[0].children[0];
				}
				else
				{
					var sAction = 'guardar';
					var target = event.target;
				}
				switch(KeysUpDown(event))
				{
					case 'ENTER':
					case 'SHIFT-ENTER':
						{
							this.editItem(target,sAction);
						} break;
					case 'ESC':
						{
							this.editItem(event.target,'anular');
						} break;
				}
			}
			function getItemIndex(AeElem)
			{
				var childs = AeElem.parentNode.getElementsByTagName('LI');
				var index  = 0;
				var object = undefined;
				for (var i=0, ccI=childs.length; i<ccI; i++)
				{
					if (AeElem == childs[i])
					{
						index  = i;
						object = childs[i];
						break;
					}
				}
				return {'index':index, 'object':object};
			}
			function createPagForm(AsIDRow,AarrInputs,AsFormName)
			{
				//AsFormName = 'pagform';
				var sform = AsFormName+'-perfil_'+randomstring(5);
				var arrElem = new Array();
					arrElem[0] = '<li class="editavel" data-idrow="'+AsIDRow+'" data-elemhide="true">';
					arrElem[1] = '<form name="'+sform+'" method="post" action="#">';
					arrElem[2] = '<input class="item input fluid" value="'+AarrInputs[0].value+'" data-value="'+AarrInputs[0].value+'" name="Nome" plceholder="Nome" onkeypress="ObjConfiguracoes.keyEvent(event);" disabled>';
					arrElem[3] = '<input type="hidden" name="idRow" value="'+AsIDRow+'" data-isid="true" >';
					arrElem[4] = '</form>';
					arrElem[5] = '<span class="botoes">';
					arrElem[6] = '<button class="botao ico fi-check hidden" 	data-action="guardar" title="Guardar" onclick="ObjConfiguracoes.editItem(this);"></button>';
					arrElem[7] = '<button class="botao ico fi-arrow-left hidden" data-action="anular" title="Anular" onclick="ObjConfiguracoes.editItem(this);"></button>';
					arrElem[8] = '<button class="botao ico fi-trash" 			data-action="apagar" title="Apagar" onclick="ObjConfiguracoes.editItem(this);"></button>';
					arrElem[9] = '<button class="botao ico fi-page-edit" 		data-action="editar" title="Editar" onclick="ObjConfiguracoes.editItem(this);"></button>';
					arrElem[10] = '</span>';
					arrElem[11] = '</li>';
				return arrElem;
			}

			function ajaxToggleRecord(AsFormName,AarrInputs,AsSection,AsAction,AsIDRow)
			{
				var func = function(sResult)
				{
					console.log(sResult);
					var jsonData = ParseJSON(sResult);
					if (jsonData)
					{
						var bResult= jsonData.result;
						var sNewID = (jsonData.idinserted) ? jsonData.idinserted : '';
						
						if (bResult == true)
						{
							CREATE_POP_MSG('Registo salvo com sucesso! '+getCloseLink(),3000);
							switch(AsAction)
							{
								case 'inserir':
									{
										var eUL = GetObjectsByTagNameData('UL','data-table',AsSection);
										if (eUL != false)
										{
											var sFormName 	= AsFormName.split('-perfil');
												sFormName 	= sFormName[0];
											var sHTML = Self.createPagForm(sNewID,AarrInputs,sFormName).join("\n");
											eUL.insertAdjacentHTML('afterbegin',sHTML);
										}
										for (var i=0, ccI=AarrInputs.length; i<ccI; i++)
										{
											AarrInputs[i].value = '';
										}
									} break;
								case 'append':
									{
										var eUL   = AarrInputs[0].parentNode.parentNode;
										var sHTML = Self.createPagForm(sNewID,AarrInputs).join("\n");
										eUL.insertAdjacentHTML('beforeend',sHTML);
										for (var i=0, ccI=AarrInputs.length; i<ccI; i++)
										{
											AarrInputs[i].value = '';
										}
									} break;
								case 'update':
									{
										Self.editItem(AarrInputs[0].parentNode,'anular');
									} break;
							}
						}
						else
						{
							CREATE_POP_MSG('Erro ao guardar! <br> Por favor tente de novo.'+getCloseLink(),3000);
						}
					}
				}
				console.log('USING --> ObjFormValid \n');
				var sAction = (AsAction === 'inserir') ? 'append' : AsAction;
				ObjFormValid.ValidateForm(AsFormName,sAction,func);
				//ValidateForm(AsFormName,func);
			}
			function editItem(ASelf,AsAction)
			{
				if (AsAction != undefined)
				{
					var sAction  = AsAction;
					var eBtnArea = ASelf.parentNode.getElementsByTagName('SPAN')[0];
				}
				else
				{
					var sAction  = ASelf.getAttribute("data-action");
					var eBtnArea = ASelf.parentNode;
				}
				if (sAction !== 'inserir')
				{
					var eLI 		= eBtnArea.parentNode;
					var eUL 		= eLI.parentNode;
					var sTable		= eUL.getAttribute("data-table");
					var sIDRow 		= eLI.getAttribute("data-idrow");
					var eForm 		= eLI.children[0];
					var arrInput 	= eForm.elements;
					var eBtnGuardar = eBtnArea.children[0];
					var eBtnAnular  = eBtnArea.children[1];
					var eBtnApagar  = eBtnArea.children[2];
					var eBtnEditar  = eBtnArea.children[3];
					var bDisable 	= true;
					var bDeleted 	= false;
				}

				switch(sAction)
				{
					case 'inserir':
						{
							var eSPAN 	 = ASelf.parentNode;
							var eSpanP 	 = eSPAN.parentNode;
							var eForm 	 = eSpanP.children[0];
							var arrInput = eForm.elements;
							var sTable 	 = eSpanP.getAttribute("data-table");
							this.ajaxToggleRecord(eForm.name,arrInput,sTable,'inserir','');
							bDeleted = true;
						} break;
					case 'guardar':
						{
							if ((sIDRow != null) && (sIDRow != ''))
							{
								AddToClass(eBtnGuardar,'hidden');
								AddToClass(eBtnAnular,'hidden');
								RemoveFromClass(eBtnApagar,'hidden');
								RemoveFromClass(eBtnEditar,'hidden');
								this.ajaxToggleRecord(eForm.name,arrInput,sTable,'update',sIDRow);
							}
							else
							{
								this.ajaxToggleRecord(eForm.name,arrInput,sTable,'append','');
								bDeleted = true;
							}
						} break;
					case 'editar':
						{
							RemoveFromClass(eBtnGuardar,'hidden');
							RemoveFromClass(eBtnAnular,'hidden');
							AddToClass(eBtnApagar,'hidden');
							AddToClass(eBtnEditar,'hidden');
							bDisable = false;
						} break;
					case 'apagar':
						{
							bDeleted = true;
							if ((sIDRow != null) && (sIDRow != ''))
							{
								console.log('USING --> ObjFormValid \n');
								ObjFormValid.RemoveRecord(eForm.name);
								//ToggleRemoveFormRecord('SUB-FORM',eForm.name);
							}
						} break;
					case 'anular':
						{
							AddToClass(eBtnGuardar,'hidden');
							AddToClass(eBtnAnular,'hidden');
							RemoveFromClass(eBtnApagar,'hidden');
							RemoveFromClass(eBtnEditar,'hidden');
							for (var i=0, ccI=arrInput.length; i<ccI; i++)
							{
								arrInput[i].value = arrInput[i].getAttribute("data-value");
							}
						} break;
				}
				if (!bDeleted)
				{
					if (bDisable)
					{
						for (var i=0, ccI=arrInput.length; i<ccI; i++)
						{
							arrInput[i].setAttribute("disabled","");
							arrInput[i].setAttribute("data-value",arrInput[i].value);
						}
					}
					else
					{
						for (var i=0, ccI=arrInput.length; i<ccI; i++)
						{
							arrInput[i].removeAttribute("disabled");
						}
						arrInput[0].focus();
					}
				}
			}
		}
		function TObjTabSocioQuotas()
		{
			var Self 					= this;
			this.arrHistoryObjects 		= {total:0};
			this.arrHistoryQuotas 		= {total:0};
			
			this.ResetHistory 				= ResetHistory;
			this.selectModByID 				= selectModByID;
			//this.findItemByID 				= findItemByID;
			//this.closeSelect 				= closeSelect;
			//this.setSelectItem 			= setSelectItem;
			//this.selectItemByID 			= selectItemByID;
			//this.createItemActividades 	= createItemActividades;
			//this.createItemActividadesAnos = createItemActividadesAnos;
			this.getQuotasByAno_AjaxJSON 	= getQuotasByAno_AjaxJSON;
			this.createAnos 				= createAnos;
			this.createModQuotas 			= createModQuotas;
			this.setQuotasInfoFromJSON 		= setQuotasInfoFromJSON;
			this.selectModalidade 			= selectModalidade;
			this.selectModAno 				= selectModAno;
			this.selectTabAno 				= selectTabAno;
			
			this.getTabAnoByValue 			= getTabAnoByValue;
			this.getElementsListByCheckbox 	= getElementsListByCheckbox;
			this.RegistarPagamento 			= RegistarPagamento;
			this.RemoverQuotas 				= RemoverQuotas;
			
			function ResetHistory()
			{
				this.arrHistoryObjects = {total:0};
				this.arrHistoryQuotas  = {total:0};
			}
			/*
			function findItemByID(AULName,AIDRow,AIndex)
			{
				var eElem 	 = undefined;
				var bFound 	 = false;
				var iIndex 	 = -1;

				if ((typeof AULName) !== 'object')
				{
					AULName = GetObjectsByTagNameData('UL','data-id',AULName);
				}
				if (AULName != false)
				{
					var arrElems = AULName.children;
						AIDRow 	 = AIDRow.toLowerCase();
					
					if (AIndex == undefined)
					{
						for(var i=0, ccI=arrElems.length; i<ccI; i++)
						{
							eElem = arrElems[i];
							if (eElem.children[0].getAttribute("onclick").toLowerCase().indexOf(AIDRow) != -1)
							{
								iIndex = i;
								bFound = true;
								break;
							}
						}
					}
					else
					{
						eElem = arrElems[AIndex];
						bFound = (eElem != undefined) ? true : false;
					}
				}

				return {'found':bFound, 'object':eElem, 'index':iIndex};
			}
			*/
			function getTabAnoByValue(AsAno)
			{
				var eButton = undefined;
				var eOption = undefined;
				var eListaQuotas = GetObjectsByTagNameData('DIV','data-id','lista-quotas');
				if (eListaQuotas)
				{
					var eListAnos = eListaQuotas.children[0].getElementsByTagName('BUTTON');
					for(var i=0, ccI=eListAnos.length; i<ccI; i++)
					{
						if(AsAno == eListAnos[i].getAttribute('data-ano'))
						{
							eButton = eListAnos[i];
							break;
						}
					}
					
					var eListAnos = eListaQuotas.children[0].getElementsByTagName('SELECT')[0];
					if (eListAnos)
					{
						for(var i=0, ccI=eListAnos.options.length; i<ccI; i++)
						{
							if(AsAno == eListAnos.options[i].value)
							{
								eOption = eListAnos.options[i];
								break;
							}
						}
					}
				}
				return {button: eButton, option: eOption};
			}
			function selectModByID(AsID)
			{
				var eSelect = document.getElementsByName('select-actividades')[0];
				for(var i=0, ccI=eSelect.options.length; i<ccI; i++)
				{
					if (eSelect.options[i].value === AsID)
					{
						eSelect.selectedIndex = i;
						break;
					}
				}
			}
			function selectModalidade(ASelf)
			{
				var iIndex 	= ASelf.selectedIndex;
				var sText 	= ASelf.options[iIndex].text;
				var sID 	= ASelf.options[iIndex].value;
				
				var params = toggleUrlParam('','idm',sID,false);
				window.history.pushState('', 'Sócio - Lista de Quotas', params);
				
				this.setQuotasInfoFromJSON();
			}
			function selectModAno(ASelf,AsAno)
			{
				if (ASelf != undefined)
				{
					if (ASelf.tagName === 'SELECT')
					{
						AsAno = ASelf.options[ASelf.selectedIndex].value;
					}
				}
				
				var eListaQuotas = GetObjectsByTagNameData('DIV','data-id','lista-quotas');
				if (eListaQuotas)
				{
					this.getQuotasByAno_AjaxJSON(AsAno,eListaQuotas);
				}
				console.log(AsAno);
			}
			function selectTabAno(AsAno,AeListAnos)
			{
				if (AeListAnos == undefined)
				{
					var eListAnos =  GetObjectsByTagNameData('DIV','data-id','lista-quotas');
						eListAnos = eListAnos.children[0];
				}
				else
				{
					var eListAnos = AeListAnos;
				}
					
				if (eListAnos)
				{
					var paramg = toggleUrlParam('','ano',AsAno,false);
					window.history.pushState('', 'Sócio - Lista de Quotas', paramg);
					
					var eBtnAnos = eListAnos.getElementsByTagName('BUTTON');
					for(var i=0, ccI=eBtnAnos.length; i<ccI; i++)
					{
						if (eBtnAnos[i].getAttribute('data-ano') === AsAno)
						{
							AddToClass(eBtnAnos[i],'active');
						}
						else
						{
							RemoveFromClass(eBtnAnos[i],'active');
						}
					}
					
					// VERIFICAR SE EXIST "selectbox"
					// SELECIONAR OPCAO NO "selectbox"
					var eSelectAno = eListAnos.getElementsByTagName('SELECT')[0];
					if (eSelectAno != undefined)
					{
						for(var i=0, ccI=eSelectAno.options.length; i<ccI; i++)
						{
							if (eSelectAno.options[i].value === AsAno)
							{
								eSelectAno.options[i].selected = true;
							}
						}
					}
				}
			}
			function getQuotasByAno_AjaxJSON(AsAno,AeListaQuotas)
			{
				this.ResetHistory();
				
				var url 	= 'ui/getList.php';
				var paramg  = toggleUrlParam('','ano',AsAno,false);
				var params  = 'action='+'socios-quotas';
					params += '&subaction='+'list-search';
					params += '&pagenext='+'0';
					params += '&q='+AsAno;
					params += '&filter=mes;ASC';

				var func = function(sResult)
				{
					var jsonData = ParseJSON(sResult);
					if (jsonData)
					{
						var bResult 	= jsonData.result;		//SUCESSO
						var ResTPage 	= jsonData.totalpages;	//TotalPages
						var ResNextPage = jsonData.nextpage;	//NextPage?
						var ResJson 	= jsonData.jsondata;	//JSON DATA
						
						if (bResult == false)
						{
							console.log('[getQuotasByAjaxJSON] '+sResult);
						}
						else
						if (bResult == true)
						{
							//var eListAnos 	= AeListaQuotas.children[0];
							//var eBtnAnos 	= eListAnos.getElementsByTagName('BUTTON');
							var eListTR = AeListaQuotas.children[1].children[0].children[1];

							ResJson = ParseJSON(ResJson);
							
							var arrLista = new Array();
							for(var i=0, ccQ=ResJson.length; i<ccQ; i++)
							{
								arrLista[i] = Self.createModQuotas(ResJson[i]);
							}
							ReplaceInnerHTML(eListTR,arrLista.join("\n"));
							
							Self.selectTabAno(AsAno,AeListaQuotas.children[0]);
						}
					}
				}
				//AJAX_REQUEST_CALLBACK("POST",url+paramg,params,func);
				ObjAjax.Ajax({type:'POST', url:url+paramg, data:params, done:func});
			}
			
			function createModQuotas(AObjJson)
			{
				var sData = (AObjJson.total_divida > 0.0) ? AObjJson.prazo_pagamento : AObjJson.data_pagamento;
				var sLabel = '';
				if (AObjJson.total_divida == 0.0)
				{
					sValue = 'Pago';
					sLabel = 'verde';
				}
				if (AObjJson.total_divida > 0.0)
				{
					sValue = '€ -'+AObjJson.total_divida;
					sLabel = 'dark-red';
					
					// VERIFICA se o PRAZO da quota é menor/igual que a data ACTUAL
					var today = new Date();
					var arrData = sData.split('-');
					if ((today.getFullYear() <= arrData[2]) && 
						((today.getMonth()+1) <= arrData[1]) &&
						(today.getDate() <= arrData[0]))
					{
						sLabel = '';
					}
				}
				var sIDRand = randomstring(5);
				
				var arrModQuotas = new Array();
					arrModQuotas[0] = '<tr data-ano="'+AObjJson.ano+'">';
					arrModQuotas[1] = '<td><input class="checkbox" id="checkbox-'+sIDRand+'" type="checkbox" name="checkbox">';
					arrModQuotas[2] = '<label for="checkbox-'+sIDRand+'"></label></td>';
					arrModQuotas[3] = '<td>'+AObjJson.descricao+'</td>';
					arrModQuotas[4] = '<td><span class="label">€ '+AObjJson.total_total+'</span></td>';
					arrModQuotas[5] = '<td><span class="label '+sLabel+'">'+sValue+'</span></td>';
					arrModQuotas[6] = '<td>'+sData+'</td>';
					arrModQuotas[7] = '<td class="hidden">'+JSON.stringify(AObjJson)+'</td>';
					arrModQuotas[8] = '</tr>';
				return arrModQuotas.join("\n");
			}
			function createAnos(AObjJson)
			{
				var arrModAnos = new Array();
				var sActive = 'active';
				var i = 0;
				var k = 0;
				var ccI = AObjJson.length;
				var iMax = (ccI<3) ? AObjJson.length : 3;
				for(i=0; i<iMax; i++)
				{
					arrModAnos[k] = '<button class="botao tab '+sActive+'" data-ano="'+AObjJson[i].ano+'" onClick="ObjTabSocioQuotas.selectModAno(this,'+"'"+AObjJson[i].ano+"'"+')">'+AObjJson[i].ano+' (€ '+AObjJson[i].total_divida+')</button>';
					sActive = '';
					k++;
				}
				
				if (ccI>=3)
				{
					arrModAnos[k] = '<select name="" class="select tab" onChange="ObjTabSocioQuotas.selectModAno(this)">';
					k++;
					for(i=0; i<ccI; i++)
					{
						arrModAnos[k] = '<option value="'+AObjJson[i].ano+'">'+AObjJson[i].ano+'</option>';
						k++;
					}
					arrModAnos[k] = '</select>';
				}
				
				return arrModAnos.join("\n");
			}
			
			function setQuotasInfoFromJSON()
			{
				this.ResetHistory();
				
				var url 	= '/core/scripts/ajax-actividades-anos-quotas.php';
				var params 	= toggleUrlParam('','tab','1',false);
				
				var func = function(sResult)
				{
					var jsonData = ParseJSON(sResult);
					if (jsonData)
					{
						var bResult = jsonData.result;
						var aQuotas = jsonData.json.quotas;
						var aAnos 	= jsonData.json.anos;
					}
					
					switch(bResult)
					{
						case false:
							{
								
							}
						case true:
							{
								if ((aQuotas) && (aAnos))
								{
									var eListaQuotas = GetObjectsByTagNameData('DIV','data-id','lista-quotas');
									if (eListaQuotas)
									{
										var eListAnos 	= eListaQuotas.children[0];
										var eListTR 	= eListaQuotas.children[1].children[0].children[1];
										
										var arrLista = new Array();
										var ccQ = aQuotas.length;
										
										for(var i=0; i<ccQ; i++)
										{
											arrLista[i] = Self.createModQuotas(aQuotas[i]);
										}
										ReplaceInnerHTML(eListTR,arrLista.join("\n"));
										ReplaceInnerHTML(eListAnos,Self.createAnos(aAnos));
										
										// SELECIONA O ANO CORRECTO!!
										if (aAnos.length > 0)
										{
											var sAno = extractParamGET('ano',params);
											var bFound = false;
											var iIndex = 0;
											for(var i=0, ccI=aAnos.length; i<ccI; i++)
											{
												if (sAno === aAnos[i].ano)
												{
													bFound = true;
													iIndex = i;
													break;
												}
											}
											
											Self.selectTabAno(aAnos[iIndex].ano);
										}
									}
								}
							}
					}
				}

				var eJSON = GetObjectsByTagNameData('SPAN','data-id','quotasJSON');
				if (eJSON != false)
				{
					var sJSONData = eJSON.innerHTML;
					eJSON.parentNode.removeChild(eJSON);
					func(sJSONData);
				}
				else
				{
					ObjAjax.Ajax({type:'POST', url:url, data:params, done:func});
				}
			}
			function getElementsListByCheckbox()
			{
				var Result 	= new Array();
				var k 		= 0;
				var arrTR = document.getElementsByClassName('tabela');
				if (arrTR)
				{
					var fDivida = 0.0;
					var fValue 	= 0;
					
					arrTR = arrTR[0].children[1].getElementsByTagName('TR');
					for(var i=0, ccI=arrTR.length; i<ccI; i++)
					{
						// VERIFICA SE CHECKBOX ESTA SELECIONADA!
						if (arrTR[i].children[0].children[0].checked)
						{
							Result[k++] = {element:arrTR[i], jsondata:ParseJSON(arrTR[i].children[5].innerHTML)};
						
							fValue  = arrTR[i].children[3].children[0].innerHTML;
							if (fValue !== 'Pago')
							{
								fValue = fValue.replace('-','');
								fValue = FloatToString(fValue);
							}
							else
							{
								fValue = 0.0;
							}
							fDivida += parseFloat(fValue);
						}
					}
				}
				return {elements:Result, divida:fDivida};
			}
			function RegistarPagamento(AbNew,AsAction)
			{
					AsAction = (AsAction == undefined) ? 'paycheck' : AsAction;
				var bRecover = false;
				
				if ((AbNew) || (this.arrHistoryQuotas.objects == undefined))
				{
					var objResult = this.getElementsListByCheckbox();
					var arrItems  = objResult.elements;
					var fDivida   = objResult.divida;
					
					var arrIDs 		= {};
					var addRow 		= false;
					var ccI 		= arrItems.length;
					var k 			= 0;
					var FeDivida 	= false;
					var FeData 		= false;
					var FeJson 		= false;
					var objJson 	= false;
					
					this.arrHistoryQuotas.objects = {};
					
					for(var i=0; i<ccI; i++)
					{
						FeDivida 	= arrItems[i].element.children[3].children[0];
						FeData 		= arrItems[i].element.children[4];
						FeJson 		= arrItems[i].element.children[5];
						objJson 	= ParseJSON(FeJson.innerHTML);
						
						if (AsAction === 'paycheck')
						{
							addRow = (FeDivida.innerHTML !== 'Pago') ? true : false;
							objJson.floatvalue 		= 0.0;
							objJson.new_data 		= getCurrentTime('d-m-Y');
						}
						else
						if (AsAction === 'unpaycheck')
						{
							addRow = (FeDivida.innerHTML === 'Pago') ? true : false;
							objJson.floatvalue 		= objJson.total_total;
							objJson.new_data 		= objJson.prazo_pagamento;
						}
						
						if (addRow)
						{
							this.arrHistoryQuotas.objects[k] = {eDivida: FeDivida, eData: FeData, eJson: FeJson,
																	json: objJson};
							arrIDs[k] = encode64(FeJson.innerHTML);
							k++;
						}
						objJson.backup = objJson;
					}
					
					this.arrHistoryObjects.opera 		= AsAction;
					this.arrHistoryObjects.section 		= 'quotas-socios';
					this.arrHistoryObjects.encodedJSON 	= encode64(JSON.stringify(arrIDs));
					
					this.arrHistoryQuotas.last_action = AsAction;
					this.arrHistoryQuotas.total = k;
					if (k==0)
					{
						this.arrHistoryQuotas.objects = undefined;
					}
				}
				else
				{
					bRecover = true;
				}
				
				if (this.arrHistoryQuotas.total != 0)
				{
					var func = function(sResult)
					{
						console.log(sResult);
						var jsonData = ParseJSON(sResult);
						if (jsonData)
						{
							var bResult= jsonData.result;
							var sNewID = jsonData.idhistory;
							
							if (bResult == true)
							{
								var ccI 	= Self.arrHistoryQuotas.total;
								var eElem 	= false;
								var jsonBackup = false;
								for(var i=0; i<ccI; i++)
								{
									eElem = Self.arrHistoryQuotas.objects[i];
									jsonBackup = eElem.json.backup;
									if (AsAction === 'paycheck')
									{
										if (bRecover)
										{
											ReplaceFromClass(eElem.eDivida,'dark-red','verde');
											ReplaceInnerHTML(eElem.eDivida,'Pago');
											ReplaceInnerHTML(eElem.eData,jsonBackup.data_pagamento);
										}
										else
										{
											ReplaceFromClass(eElem.eDivida,'dark-red','verde');
											ReplaceInnerHTML(eElem.eDivida,'Pago');
											ReplaceInnerHTML(eElem.eData,eElem.json.new_data);
										}
									}
									else
									if (AsAction === 'unpaycheck')
									{
										if (bRecover)
										{
											sTextDivida = '€ -'+FloatToCurrencyExtra(jsonBackup.total_divida,2);
											
											ReplaceFromClass(eElem.eDivida,'verde','dark-red');
											ReplaceInnerHTML(eElem.eDivida,sTextDivida);
											ReplaceInnerHTML(eElem.eData,eElem.json.backup.prazo_pagamento);
										}
										else
										{
											sTextDivida = '€ -'+FloatToCurrencyExtra(eElem.json.floatvalue,2);
											
											ReplaceFromClass(eElem.eDivida,'verde','dark-red');
											ReplaceInnerHTML(eElem.eDivida,sTextDivida);
											ReplaceInnerHTML(eElem.eData,eElem.json.new_data);
										}
									}
								}
								
								if (AsAction === 'paycheck')
								{
									if (bRecover)
									{
										CREATE_POP_MSG('Operação Anulada com sucesso! '+getCloseLink(),3000);
										Self.ResetHistory();
									}
									else
									{
										Self.arrHistoryObjects.opera = 'unpaycheck';
										CREATE_POP_MSG('Registo(s) Eliminado(s)! '+getCloseLink('A','#',"ObjTabSocioQuotas.RegistarPagamento(false,'unpaycheck')",'Anular')+' | '+getCloseLink(),20000);
									}
								}
								else
								if (AsAction === 'unpaycheck')
								{
									if (bRecover)
									{
										CREATE_POP_MSG('Operação Anulada com sucesso! '+getCloseLink(),3000);
										Self.ResetHistory();
									}
									else
									{
										Self.arrHistoryObjects.opera = 'paycheck';
										CREATE_POP_MSG('Registo(s) Eliminado(s)! '+getCloseLink('A','#',"ObjTabSocioQuotas.RegistarPagamento(false,'paycheck')",'Anular')+' | '+getCloseLink(),20000);
									}
								}
							}
							else
							if (bResult == false)
							{
								
							}
						}
					}
					
					var sUrl 		= '/core/scripts/ajax-toggle-quotas.php';
					var extraparams = '';
						extraparams += '&ids='+extractParamGET('ids');
						extraparams += '&idm='+extractParamGET('idm');
					var params 		= '';
						params 		+= 'opera=' 		+ this.arrHistoryObjects.opera;
						params 		+= '&section=' 		+ this.arrHistoryObjects.section;
						params 		+= '&encodedJSON=' 	+ this.arrHistoryObjects.encodedJSON;
						params 		+= extraparams;
					
					ObjAjax.Ajax({type:'POST', url:sUrl, data:params, done:func});
				}
				else
				{
					console.log('Nao existem Quotas a alterar para a Operação pertendida!');
				}
			}
			function RemoverQuotas(AbNew,AsToggleAction,AsHistoryID)
			{
				AbNew 			= (AbNew == undefined) ? true : AbNew;
				AsToggleAction 	= (AsToggleAction == undefined) ? 'bunk' : AsToggleAction;
				AsHistoryID 	= (AsHistoryID == undefined) ? '' : AsHistoryID;
				
				if ((AbNew) || (this.arrHistoryObjects.total == 0))
				{
					var objResult = this.getElementsListByCheckbox();
					var arrItems  = objResult.elements;
					var fDivida   = objResult.divida;
					
					var extraparams = toggleUrlParam('','tab','1',false);
					var objExtraParams = {
						ids:extractParamGET('ids',extraparams),
						idm:extractParamGET('idm',extraparams)
					};
					
					var objects = {};
					var arrIDs = new Array();
					var ccI = arrItems.length;
					for(var i=0; i<ccI; i++)
					{
						objects[i] 	= arrItems[i].element;
						arrIDs[i] 	= JSON.stringify({"value":arrItems[i].jsondata.id_quota, "title":arrItems[i].jsondata.descricao});
					}
					var objJSON = {"objects": "["+arrIDs.toString()+"]", "extraparams":JSON.stringify(objExtraParams)};
					
					this.arrHistoryObjects.extraparams 	= extraparams;
					this.arrHistoryObjects.opera 		= AsToggleAction;
					this.arrHistoryObjects.encodedJSON 	= encode64(JSON.stringify(objJSON));
					this.arrHistoryObjects.section 		= 'quotas-socio';
					this.arrHistoryObjects.objects 		= objects;
					this.arrHistoryObjects.total 		= ccI;
					this.arrHistoryObjects.idhistory 	= AsHistoryID;
					this.arrHistoryObjects.QuotasDivida = fDivida;
				}
				
				var extraparams = '';
				var objParams = {};
					objParams.opera 	= this.arrHistoryObjects.opera;
					objParams.section 	= this.arrHistoryObjects.section;
					objParams.jsonPOST 	= this.arrHistoryObjects.encodedJSON;
					objParams.idhistory = this.arrHistoryObjects.idhistory;
				
				var func = function(sResult)
				{
					var jsonData = ParseJSON(sResult);
					if (jsonData)
					{
						var bResult= jsonData.result;
						var sNewID = jsonData.idhistory;
						
						Self.arrHistoryObjects.idhistory 	= sNewID;
						Self.arrHistoryObjects.encodedJSON 	= '';
						
						if (bResult == true)
						{
							var sAno 		= extractParamGET('ano',Self.arrHistoryObjects.extraparams);
							var objResult 	= Self.getTabAnoByValue(sAno);
							var fBtnDivida 	= parseFloat(objResult.button.innerHTML.replace(sAno+' (€ ','').replace(')',''));
							var fDivida 	= 0.0;
							if (AsToggleAction === 'bunk')
							{
								fDivida 	= fBtnDivida-(Self.arrHistoryObjects.QuotasDivida+0.0);
							}
							else
							if (AsToggleAction === 'debunk')
							{
								fDivida 	= fBtnDivida+(Self.arrHistoryObjects.QuotasDivida+0.0);
							}
							var sHTMLDivida = sAno+' (€ '+FloatToCurrencyExtra(fDivida,2)+')';
							ReplaceInnerHTML(objResult.button,sHTMLDivida);
							
							switch(AsToggleAction)
							{
								case 'bunk':
									{
										Self.arrHistoryObjects.opera = 'debunk';
										
										for(var i=0; i<Self.arrHistoryObjects.total; i++)
										{
											AddToClass(Self.arrHistoryObjects.objects[i],'hidden');
										}
										
										CREATE_POP_MSG('Registo(s) Eliminado(s)! '+getCloseLink('A','#',"ObjTabSocioQuotas.RemoverQuotas(false,'debunk','"+sNewID+"')",'Anular')+' | '+getCloseLink(),20000);
									} break;
								case 'debunk':
									{
										for(var i=0; i<Self.arrHistoryObjects.total; i++)
										{
											RemoveFromClass(Self.arrHistoryObjects.objects[i],'hidden');
										}
										
										Self.ResetHistory();
										
										CREATE_POP_MSG('Registo(s) recuparado(s) com sucesso! '+getCloseLink(),3000);
									} break;
							}
							console.log('Actualizar valores em divida/pagos da Modalidade');
						}
						else
						if (bResult == false)
						{
							CREATE_POP_MSG('Erro ao guardar! <br> Por favor tente de novo.'+getCloseLink(),3000);
						}
					}
				}
				ToggleRecordAjax(objParams,extraparams,func);
			}
		}
		function TObjTouchSocioFamiliares()
		{
			var Self 					= this;
			this.bDroped 				= false;
			this.bCreatedElems 			= false;
			this.eAreaElements 			= undefined;

			this.inEditMode 			= inEditMode;
			this.enableFamiliar 		= enableFamiliar;
			this.deleteFamiliar 		= deleteFamiliar;
			this.saveFamiliar 			= saveFamiliar;
			this.insertFamiliar 		= insertFamiliar;
			this.editFamiliar 			= editFamiliar;
			this.findFamiliar 			= findFamiliar;
			this.dumpFamiliar 			= dumpFamiliar;
			this.createFamAfinidade 	= createFamAfinidade;
			this.createFamiliar 		= createFamiliar;
			this.setFamiliaresFromJSON 	= setFamiliaresFromJSON;
			
			this.setDragDropElements	= setDragDropElements;
			this.dragEnterZone 			= dragEnterZone;
			this.dragOverZone 			= dragOverZone;
			this.dragLeaveZone 			= dragLeaveZone;
			this.dropItem 				= dropItem;
			
			function inEditMode(AFormName,ABoolean)
			{
				var eForm = document.forms[AFormName];

				var eBotaoEditar	= eForm.elements['editar'];
				var eBotaoGuardar	= eForm.elements['guardar'];
				var eBotaoAnular	= eForm.elements['anular'];
				
				if (!ABoolean)
				{
					eBotaoEditar.removeAttribute("style");
					eBotaoGuardar.setAttribute("style","display:none;");
					eBotaoAnular.setAttribute("style","display:none;");
				}
				else
				{
					eBotaoEditar.setAttribute("style","display:none;");
					eBotaoGuardar.removeAttribute("style");
					eBotaoAnular.removeAttribute("style");
				}
				// SE FOI INSERIDO E SALVO COM SUCESSO, TORNA-O NORMAL
				if (eBotaoGuardar.getAttribute("onclick").indexOf('dumpFamiliar') != -1)
				{
					eBotaoGuardar.setAttribute("onclick","ObjTouchSocioFamiliares.saveFamiliar(this,'"+AFormName+"',false);");
					eBotaoApagar.setAttribute("onclick","ObjTouchSocioFamiliares.deleteFamiliar(this,'"+AFormName+"');");
				}
			}
			function enableFamiliar(ASelf,AFormName,bForceEnable)
			{
					AFormName 		= (AFormName != undefined) ? AFormName : ASelf.children[0].getAttribute("name");
				var eForm 			= document.forms[AFormName];
				var eElems 			= eForm.elements;
				var eDivParentForm 	= eForm.parentNode;
				var eClassElem 		= (!bForceEnable) ? eDivParentForm.getAttribute("class") : 'act-normal';

				if (eClassElem.indexOf("act-editar") != -1)
				{
					for (var i=0, ccI=eElems.length; i<ccI; i++)
					{
						if ((eElems[i].tagName === "INPUT") ||
							(eElems[i].tagName === "TEXTAREA") ||
							(eElems[i].tagName === "SELECT") )
						{
							eElems[i].setAttribute("disabled","disabled");
						}
					}
					ReplaceFromClass(eDivParentForm,'act-editar','act-normal');
					this.inEditMode(AFormName,false);
				}
				else
				if (eClassElem.indexOf("act-normal") != -1)
				{
					for (var i=0, ccI=eElems.length; i<ccI; i++)
					{
						if ((eElems[i].tagName === "INPUT") ||
							(eElems[i].tagName === "TEXTAREA") ||
							(eElems[i].tagName === "SELECT") )
						{
							eElems[i].removeAttribute("disabled");
						}
					}
					ReplaceFromClass(eDivParentForm,'act-normal','act-editar');
					this.inEditMode(AFormName,true);
				}
			}
			function saveFamiliar(ASelf,AFormName,AbAppend)
			{
				if (ValidateForm(AFormName,false))
				{
					var AsToggleAction 	= (AbAppend == false) ? 'append' : 'update';
					var jsonData		= serializeFORM(AFormName);
					var Result 			= getURLParamsFORM(AFormName,'familiares','');

					var objParams = {};
						objParams.section = 'familiares';
						objParams.opera   = AsToggleAction;
						objParams.jsonPOST= encode64(jsonData);
					var extraparams = Result['params'];
						
					var func = function(sResult)
					{
						var jsonData = ParseJSON(sResult);
						if (jsonData)
						{
							var bResult= jsonData.result;
							var sNewID = (jsonData.idinserted) ? jsonData.idinserted : '';
							
							console.log(sResult);
							if (bResult == true)
							{
								CREATE_POP_MSG('Registo salvo com sucesso! '+getCloseLink(),3000);
								//Self.inEditMode(AFormName,false);
								Self.enableFamiliar(undefined,AFormName);
							}
							else
							{
								CREATE_POP_MSG('Erro ao guardar! <br> Por favor tente de novo.'+getCloseLink(),3000);
							}
						}
					}
					ToggleRecordAjax(objParams,extraparams,func);
				}
			}
			function insertFamiliar()
			{
				var eAreaFamiliar = GetObjectsByTagNameData('span','data-name','tab-family');
				if (eAreaFamiliar != false)
				{
					var arrJson = new Array();
					var arrFamiliar = this.createFamiliar(arrJson,false,true);
					eAreaFamiliar.insertAdjacentHTML('afterbegin',arrFamiliar.join(''));
				}
			}
			function deleteFamiliar(ASelf,AFormName)
			{
				//DeleteRecordAjax(this,'familiares','SUB-FORM',AFormName,'bunk');
				ToggleRemoveFormRecord('SUB-FORM',AFormName);
			}
			function editFamiliar(ASelf)
			{
				var sFormName = FindParentFORMName(ASelf);
				//var eForm 	  = document.forms[sFormName];
				console.log(sFormName);
			}
			function findFamiliar(AId)
			{
				var bFind 	= false;
				var eObject = undefined;
				var eAreaFamiliar = GetObjectsByTagNameData('span','data-name','tab-family');
				if (eAreaFamiliar != false)
				{
					var eElems 		 = eAreaFamiliar.getElementsByClassName('familiar');
					var sCodSocio 	 = '';
					var sCodSocioFam = '';
					var eForm 		 = undefined;
					for (var i=0, ccI=eElems.length; i<ccI; i++)
					{
						eForm 		 = eElems[i].children[0];
						sCodSocio 	 = eForm.elements['CodSocio'].value;
						sCodSocioFam = eForm.elements['CodSocioFamiliar'].value;
						if (AId === sCodSocioFam)
						{
							bFind 	= true;
							eObject = eElems[i];
							break;
						}
					}
				}
				return {"find":bFind, "object":eObject };
			}
			function dumpFamiliar(ASelf,AFormName)
			{
				ASelf = ASelf.parentNode; 		// BUTTON --> DIV
				ASelf = ASelf.parentNode; 		// DIV --> FORM
				ASelf = ASelf.parentNode; 		// FORM --> DIV#.familiar
				ASelf.parentNode.removeChild(ASelf);
			}
			
			function createFamAfinidade(AindxName,AbDisable)
			{
				AindxName = (AindxName != undefined) ? AindxName : '';
				var PaiSelect 	= (AindxName === 'Pai') ? 'selected' : '';
				var MaeSelect 	= (AindxName === 'Mãe') ? 'selected' : '';
				var IrmaoSelect = (AindxName === 'Irmão') ? 'selected' : '';
				var IrmaSelect 	= (AindxName === 'Irmã') ? 'selected' : '';
				var PrimoSelect = (AindxName === 'Primo') ? 'selected' : '';
				var PrimaSelect = (AindxName === 'Prima') ? 'selected' : '';
				var TioPSelect 	= (AindxName === 'Tio Paterno') ? 'selected' : '';
				var TiaPSelect 	= (AindxName === 'Tia Paterna') ? 'selected' : '';
				var TioMSelect 	= (AindxName === 'Tio Materno') ? 'selected' : '';
				var TiaMSelect 	= (AindxName === 'Tia Materna') ? 'selected' : '';
				var AvoMPSelect = (AindxName === 'Avô Paterno') ? 'selected' : '';
				var AvoFPSelect = (AindxName === 'Avó Paterna') ? 'selected' : '';
				var AvoMMSelect = (AindxName === 'Avô Materno') ? 'selected' : '';
				var AvoFMSelect = (AindxName === 'Avó Materna') ? 'selected' : '';
				
				var arrAux = new Array();
					arrAux[0] = '<select name="Afinidade" placeholder="Grau de Parentesco" '+((AbDisable)?'disabled':'')+' required>';
					arrAux[1] = '<option value="0" disabled selected>Grau de parentesco...</option>';
					arrAux[2] = '<option value="Pai" '+PaiSelect+'>Pai</option>';
					arrAux[3] = '<option value="Mãe" '+MaeSelect+'>Mãe</option>';
					arrAux[4] = '<option value="Irmão" '+IrmaoSelect+'>Irmão</option>';
					arrAux[5] = '<option value="Irmã" '+IrmaSelect+'>Irmã</option>';
					arrAux[6] = '<option value="Primo" '+PrimoSelect+'>Primo</option>';
					arrAux[7] = '<option value="Prima" '+PrimaSelect+'>Prima</option>';
					arrAux[8] = '<option value="Tio Paterno" '+TioPSelect+'>Tio Paterno</option>';
					arrAux[9] = '<option value="Tia Paterna" '+TiaPSelect+'>Tia Paterna</option>';
					arrAux[10] = '<option value="Tio Materno" '+TioMSelect+'>Tio Materno</option>';
					arrAux[11] = '<option value="Tia Materna" '+TiaMSelect+'>Tia Materna</option>';
					arrAux[12] = '<option value="Avô Paterno" '+AvoMPSelect+'>Avô Paterno</option>';
					arrAux[13] = '<option value="Avó Paterna" '+AvoMPSelect+'>Avó Paterna</option>';
					arrAux[14] = '<option value="Avô Materno" '+AvoMMSelect+'>Avô Materno</option>';
					arrAux[15] = '<option value="Avó Materna" '+AvoMMSelect+'>Avó Materna</option>';
					arrAux[16] = '</select>';
				return arrAux;
			}
			function createFamiliar(AarrJSON,AbDisable,bInserting)
			{
					bInserting 		 = (bInserting != undefined) ? bInserting : false;
				var sClassDragDrop 	 = (bInserting) ? 'dragdrop' : '';
				var sGuardarDisabled = (!AbDisable) ? '' : ' style="display:none;" ';
				var sEditarDisabled  = (AbDisable) ? '' : ' style="display:none;" ';
				var sClassDisabled 	 = (!AbDisable) ? 'act-editar' : 'act-normal';
				var sform 			 = 'socio-familiar-perfil_'+randomstring(10);
				var formName 		 = "'"+sform+"'";
				var sDragDrop 		 = ' ondrop="ObjTouchSocioFamiliares.dropItem(event);" '; 
					sDragDrop 		+= ' ondragenter="ObjTouchSocioFamiliares.dragEnterZone(event);" ';
					sDragDrop 		+= ' ondragover="ObjTouchSocioFamiliares.dragOverZone(event);" ';
					sDragDrop 		+= ' ondragleave="ObjTouchSocioFamiliares.dragLeaveZone(event);" ';
				var sOnDelete 		 = (bInserting == false) ? "deleteFamiliar(this,"+formName+");" : "dumpFamiliar(this,"+formName+");";

				AarrJSON['afinidade'] 		 = setEmptyIfNull(AarrJSON['afinidade']);
				AarrJSON['nome'] 			 = setEmptyIfNull(AarrJSON['nome']);
				AarrJSON['id_tblfamiliar']   = setEmptyIfNull(AarrJSON['id_tblfamiliar']);
				AarrJSON['id_socio_parent']  = setEmptyIfNull(AarrJSON['id_socio_parent']);
				AarrJSON['id_socio_familia'] = setEmptyIfNull(AarrJSON['id_socio_familia']);
				
				if ((AarrJSON['nome'] === '') && (bInserting == true))
					AarrJSON['nome'] = 'Arrastar Sócio para aqui.';

				var arrAux = new Array();
					arrAux[0] = '<div class="familiar '+sClassDisabled+'">';
					arrAux[1] = '<form name="'+sform+'" action="" method="POST">';
					arrAux[2] = '<div class="fam-afinidade">'+(this.createFamAfinidade(AarrJSON['afinidade'],AbDisable).join("\n"))+'</div>';
					arrAux[3] = '<div class="fam-socio-nome '+sClassDragDrop+'" '+sDragDrop+' data-name="item-family" >'+AarrJSON['nome']+'</div>';
					arrAux[4] = '<div class="fam-savebtn botoes-tab">';
					arrAux[5] = '<button data-name="guardar" name="guardar" class="botao ico fi-check" '+sGuardarDisabled+' title="Guardar" type="button" onclick="ObjTouchSocioFamiliares.saveFamiliar(this,'+formName+','+bInserting+');"></button>';
					arrAux[6] = '<button data-name="editar" name="editar" class="botao ico fi-page-edit" '+sEditarDisabled+' title="Editar" type="button" onClick="ObjTouchSocioFamiliares.enableFamiliar(this,'+formName+');"></button>';
					arrAux[7] = '<button data-name="anular" name="anular" class="botao ico fi-arrow-left" style="display:none;" title="Anular" type="button" onclick="ObjTouchSocioFamiliares.enableFamiliar(this,'+formName+');"></button>';
					arrAux[8] = '<button data-name="apagar" name="apagar" class="botao ico fi-trash" title="Apagar" type="button" onClick="ObjTouchSocioFamiliares.'+sOnDelete+'"></button>';
					arrAux[9] = '</div>';
					arrAux[10] = '<input type="hidden" name="CodFamilia" value="'+AarrJSON['id_tblfamiliar']+'" required>';
					arrAux[11] = '<input type="hidden" name="CodSocio" value="'+AarrJSON['id_socio_parent']+'" required>';
					arrAux[12] = '<input type="hidden" name="CodSocioFamiliar" value="'+AarrJSON['id_socio_familia']+'" required>';
					arrAux[13] = '<input type="hidden" name="jsonData" value="'+(encode64(JSON.stringify(AarrJSON)))+'" required>';
					arrAux[14] = '</form>';
					arrAux[15] = '</div>';
				return arrAux;
			}
			function setFamiliaresFromJSON()
			{
				var eJsonData = GetObjectsByTagNameData('span','data-id','tab-family-json');
				if (eJsonData != false)
				{
					var jsonData = eJsonData.innerHTML;
					if (jsonData !== '[]')
					{
						//jsonData = decode64(jsonData);
						var arrJson = JSON.parse(jsonData);
						var ccI = arrJson.length;
						
						if (ccI > 0)
						{
							var eAreaFamiliar = GetObjectsByTagNameData('span','data-name','tab-family');
							if (eAreaFamiliar != false)
							{
								var arrFamiliar;
								for (var i=0; i<ccI; i++)
								{
									arrFamiliar = this.createFamiliar(arrJson[i],true);
									eAreaFamiliar.insertAdjacentHTML('beforeend',arrFamiliar.join(''));
								}
							}
						}
						eJsonData.parentNode.removeChild(eJsonData);
					}
					else
						CleanElement(eJsonData);
				}
			}
			function setDragDropElements()
			{
				//this.eTabDragDrop = GetObjectsByTagNameData('button','data-name','tab-dragdrop');
				//this.eDragDropZone = GetObjectsByTagNameData('span','data-id','dragdropZone');
			}
			function dragEnterZone(event)
			{
			}
			function dragOverZone(event)
			{
				event.preventDefault();
				//this.bInDropZone = true;
			}
			function dragLeaveZone(event)
			{
				//this.bInDropZone = false;
			}

			function dropItem(event)
			{
				event.preventDefault();
				var eTarget = event.target;
				var data 	= event.dataTransfer.getData("json");
				var sIDSocio= document.forms['socio-perfil-edit'].elements['CodSocio'].value;
				var arrJson = JSON.parse(data);
					arrJson['id_socio_parent']  = sIDSocio;
					arrJson['id_socio_familia'] = arrJson['id_socio'];

				if (sIDSocio !== arrJson['id_socio'])
				{
					var Result    = this.findFamiliar(arrJson['id_socio']);
					var bFind 	  = Result['find'];
					var eObject   = Result['object'];
					
					var sDataName = eTarget.getAttribute("data-name");
					if (sDataName === 'item-family')
					{
						// ENCONTROU FAMILIAR JA INSERIDO??
						if (!bFind)
						{
							eTarget.className = 'fam-socio-nome';
							ReplaceInnerHTML(eTarget,arrJson['nome']);
						}
						else
						{
							this.enableFamiliar(eObject,undefined,true);
						}
					}
					else
					if (sDataName === 'tab-family')
					{
						// ENCONTROU FAMILIAR JA INSERIDO??
						if (!bFind)
						{
							var arrFamiliar = this.createFamiliar(arrJson,false,true);
							eTarget.insertAdjacentHTML('afterbegin',arrFamiliar.join(''));
						}
						else
							this.enableFamiliar(eObject,undefined,true);
					}
					//this.bDroped = false;
				}
			}
		}
		function TObjDragDropSocio()
		{
			var Self 			= this;
			
			this.dragStartItem 	= dragStartItem;
			this.dragEndItem 	= dragEndItem;
			
			function dragStartItem(event)
			{
				//if ((this.eTabDragDrop != false) && (this.eDragDropZone != false))
				//{
					//ObjTabs.activeTabArea(this.eTabDragDrop);
					event.dataTransfer.setData("json",event.target.childNodes[2].innerHTML);
				//}
			}
			function dragEndItem(event)
			{
				//if ((this.eTabDragDrop != false) && (this.eDragDropZone != false))
				//{
				//	if ((this.bInDropZone == false) && (this.iDropedItems == 0))
				//	{
				//		ObjTabs.activeTabArea(this.eTabDragDrop,this.indxTabActive);
				//	}
				//}
			}
		}
		function TObjTouchSocioControlo()
		{
			var Self 			= this;
			this.eDragDropZone 	= undefined;
			this.eTabDragDrop 	= undefined;
			this.bInDropZone 	= false;
			this.iDropedItems 	= 0;

			this.setDragDropElements	= setDragDropElements;
			this.dragEnterZone 			= dragEnterZone;
			this.dragOverZone 			= dragOverZone;
			this.dragLeaveZone 			= dragLeaveZone;
			//this.dragStartItem 			= dragStartItem;
			//this.dragEndItem 			= dragEndItem;
			this.dropItem 				= dropItem;
			this.createItemSocioDragDrop= createItemSocioDragDrop;

			function setDragDropElements()
			{
				this.eTabDragDrop = GetObjectsByTagNameData('button','data-name','tab-dragdrop');
				this.eDragDropZone = GetObjectsByTagNameData('span','data-id','dragdropZone');
			}
			function dragEnterZone(event)
			{
			}
			function dragOverZone(event)
			{
				event.preventDefault();
				this.bInDropZone = true;
			}
			function dragLeaveZone(event)
			{
				this.bInDropZone = false;
			}

			function dropItem(event)
			{
				event.preventDefault();
				this.iDropedItems++;
				var data = event.dataTransfer.getData("json");
				this.createItemSocioDragDrop(this.eDragDropZone,data);
			}
			function createItemSocioDragDrop(AeTargetZone,AsJSON)
			{
				var arrJSON = ParseJSON(AsJSON);
				var sID 	= arrJSON['id_socio'];
				var sName 	= arrJSON['nome'];
				var sSexo 	= arrJSON['sexo'];

				var func = function(AbResult,AsURL)
				{
					var sSRC = '';
					if (AbResult)
					{
						sSRC = AsURL;
					}
					else
					{
						sSRC = AsURL+'icons/users/';
						sSRC += (sSexo === 'Masculino') ? 'user-male-a-01.jpg' : 'user-female-a-01.jpg';
					}
					
					var arrAux = new Array();
						arrAux[0] = '<div class="item-dragdrop">';
						arrAux[1] = '<img class="img-preview" width="100px" height="100px" src="'+sSRC+'" id="imgUserTumb" />';
						arrAux[2] = '<h5 class="nome">'+sName+'</h5>';
						arrAux[3] = '</div>';
					AeTargetZone.insertAdjacentHTML('afterbegin',arrAux.join(''));
				}

				checkImage((sID+'.jpg'),'socios/',func);
			}
		}
		function TObjListagem(AAction)
		{
			var Self 			= this;
			this.eAllParent 	= document.getElementById('sub-nav');
			this.eMainSubNav 	= document.getElementById('main');
			this.eListagem 		= document.getElementsByClassName('listagem')[0];
			this.Action 		= AAction;
			this.ePage 			= document.getElementById("PageNum");
			this.ePageNext 		= document.getElementById("PageNumNext");
			this.eActiveElem 	= undefined;
			this.iNumItems 		= 0;

			this.setAction 		= setAction;
			this.setVariables 	= setVariables;
			this.cleanLista 	= cleanLista;
			this.createLoadMore = createLoadMore;
			this.getAllHeight 	= getAllHeight;
			this.getItemByDataID= getItemByDataID;
			this.LoadMore 		= LoadMore;

			this.createItemSocio		= createItemSocio;
			this.createItemEntidade		= createItemEntidade;
			this.createItemHistorico	= createItemHistorico;
			this.createLoadingHeader	= createLoadingHeader;
			
			this.addItemsToList = addItemsToList;
			this.getPage 		= getPage;
			this.getPageAjax 	= getPageAjax;
			this.LoadContent 	= LoadContent;
			this.setFocusItem 	= setFocusItem;
			this.resetItemsClass= resetItemsClass;
			
			/* ************************************************************** */
			/* ************************************************************** */

			/* ************************************************************** */
			/* ************************************************************** */

			function setAction()
			{
				if (this.Action === '')
					this.Action = extractParamGET('op',window.location.href);
			}
			function setVariables(AsAction)
			{
				this.eAllParent 	= document.getElementById('sub-nav');
				this.eMainSubNav 	= document.getElementById('main');
				this.eListagem 		= document.getElementsByClassName('listagem')[0];
				this.ePage 			= document.getElementById("PageNum");
				this.ePageNext 		= document.getElementById("PageNumNext");
				this.iNumItems 		= 0;
				this.setAction();
			}
			function cleanLista()
			{
				var arrItems = this.eListagem.getElementsByClassName("item");
				var ccI = arrItems.length;
	
				for (var i=(ccI-1); i>(-1); i--)
				{
					this.eListagem.removeChild(arrItems[i]);
				}
			}
			function createLoadMore()
			{
				/*
				var eDivItem = document.createElement("DIV");
					eDivItem.setAttribute("class","item");
					eDivItem.setAttribute("onclick","ObjListagem.LoadMore(this,'list');");
				var eH5 = document.createElement("H5");
					eH5.setAttribute("class","nome");
					eH5.setAttribute("style","text-align:center;");
					eH5.setAttribute("id","LoadMore");
					eH5.innerHTML = "carregar mais";
				eDivItem.appendChild(eH5);
				*/
				var eDivItem = new Array();
					eDivItem[0] = '<div class="item" onclick="ObjListagem.LoadMore(this,'+"'list'"+');">';
					eDivItem[1] = '<h5 class="nome" id="LoadMore" style="text-align:center;">carregar mais</h5>';
					eDivItem[2] = '</div>';
				
				return eDivItem;
			}
			function getAllHeight()
			{
				var arrItems = document.getElementsByClassName('item');
				var numElem  = arrItems.length;

				if (numElem == 10)
				{
					var iscrollHeight = arrItems[0].scrollHeight;
					var ioffsetHeight = arrItems[0].offsetHeight;
					var iclientHeight = arrItems[0].clientHeight;
					var iParentHeight = this.eAllParent.scrollHeight;
					var iItemsHeight  = (iclientHeight*numElem);
					/*
						Verifica se a altura do elemento pai "sub-nav" é maior ou igual
						à soma do "carregar mais" ++ a soma das alturas dos elmentos "item".
					*/
					//if ((iItemsHeight) <= (iParentHeight))
					//{
						this.eListagem.insertAdjacentHTML('beforeend',this.createLoadMore().join("\n"));
						//this.eListagem.appendChild(this.createLoadMore());
	
						this.eAllParent.addEventListener("scroll",
								function (event)
								{
									event.preventDefault();
									//console.log(event.target.scrollHeight);
									//console.log(event.target.scrollTop);
									//console.log(iParentHeight);
									if ((event.target.scrollHeight-event.target.scrollTop) === Self.eAllParent.clientHeight)
									{
										Self.LoadMore(this,'list');
									}
								});
					//}
				}
			}
			function getItemByDataID(AId)
			{
				var arrItems = new Array();
					arrItems = this.eListagem.getElementsByClassName("sub-item");
				var eTarget = undefined;
				var iIndex  = -1;
					AId 	= AId.toLowerCase();

				for (var i=0, ccI=arrItems.length; i<ccI; i++)
				{
					if (arrItems[i].getAttribute("onclick").toLowerCase().indexOf(AId) != -1)
					{
						eTarget = arrItems[i];
						eTarget = eTarget.parentNode;
						//console.log(eTarget);
						iIndex  = i;
						break;
					}
				}
				return {"element":eTarget, "index":iIndex};
			}
			function resetItemsClass()
			{
				var arrItems = this.eListagem.getElementsByClassName("item ativo");
				for (var i=0, ccI=arrItems.length; i<ccI; i++)
				{
					arrItems[i].className = "item";
				}
			}
			function setFocusItem(AObject,AId)
			{
				var iIndex  = -1;
				if (AObject == undefined)
				{
					if (AId !== '')
					{
						var Result = this.getItemByDataID(AId);
						AObject= Result['element'];
						iIndex = Result['index'];
					}
				}
				if (this.eActiveElem)
					this.eActiveElem.className = 'item';
				else
					this.resetItemsClass();
				
				if (AObject)
				{
					if (AObject.className === 'sub-item')
						AObject = AObject.parentNode;

					if (AObject.tagName === 'DIV')
					{
						AObject.className = 'item ativo';
						this.eActiveElem  = AObject;
					}
				}
				
				return AObject;
			}
			// *****************************************************************
			function createItemSocio(AJsonArray,AiPage)
			{
				this.iNumItems = (this.iNumItems != undefined) ? this.iNumItems : 0;
				this.iNumItems++;
				var aUrl = "ObjListagem.LoadContent(this,'', 'Ficha de Sócio', '/neo/?op=socios&ids="+AJsonArray["id_socio"]+"&page="+AiPage+"');";
				
				var hideDivida = (AJsonArray["divida"] === '0.00') ? '' : '';
				var eDivItem = new Array();
					eDivItem[1] = '<div class="item" draggable="true" ondragstart="ObjDragDropSocio.dragStartItem(event)" ondragend="ObjDragDropSocio.dragEndItem(event)">';
					eDivItem[2] = '<div class="checkbox-wrap">';
					eDivItem[3] = '<input class="checkbox" id="checkbox-'+this.iNumItems+'" type="checkbox" name="checkbox">';
					eDivItem[4] = '<label for="checkbox-'+this.iNumItems+'"></label>';
					eDivItem[5] = '</div>';
					eDivItem[6] = '<div class="sub-item" onclick="'+aUrl+'">';
					eDivItem[7] = '<H5 class="nome">'+AJsonArray["nome"]+'</H5>';
					eDivItem[8] = '<span class="label soc-num">nº '+AJsonArray["codigo"]+'</span>';
					eDivItem[9] = '<span class="label grey-blue">'+AJsonArray["socio_nome_tipo"]+'</span>';
					eDivItem[10] = '<span class="label dark-red">'+((AJsonArray["divida"] === '0.00') ? '' : '<img src="/neo/img/exclamation_y.png" style="width:20px; height:20px;"/>')+'</span>';
					eDivItem[11] = '</div>';
					eDivItem[12] = '<span class="hidden" data-id="jsonData">'+JSON.stringify(AJsonArray)+'</span>';
					eDivItem[13] = '</div>';
				return 	eDivItem;
			}
			function createItemEntidade(AJsonArray,AiPage)
			{
				this.iNumItems = (this.iNumItems != undefined) ? this.iNumItems : 0;
				this.iNumItems++;
				var aUrl = "ObjListagem.LoadContent(this,'', 'Ficha de Entidade', '/neo/?op=entidades&ide="+AJsonArray["id_entidade"]+"&page="+AiPage+"');";
	
				var eDivItem = new Array();
					eDivItem[1] = '<div class="item" draggable="true">';
					eDivItem[2] = '<div class="checkbox-wrap">';
					eDivItem[3] = '<input class="checkbox" id="checkbox-'+this.iNumItems+'" type="checkbox" name="checkbox">';
					eDivItem[4] = '<label for="checkbox-'+this.iNumItems+'"></label>';
					eDivItem[5] = '</div>';
					eDivItem[6] = '<div class="sub-item" onclick="'+aUrl+'">';
					eDivItem[7] = '<H5 class="nome">'+AJsonArray["nome"]+'</H5>';
					eDivItem[8] = '<span class="label verde">'+AJsonArray["abreviatura"]+'</span>';
					eDivItem[9] = '<span class="label grey-blue">'+AJsonArray["tipo"]+'</span>';
					eDivItem[10] = '</div>';
					eDivItem[11] = '<span class="hidden" data-id="jsonData">'+JSON.stringify(AJsonArray)+'</span>';
					eDivItem[12] = '</div>';
				return 	eDivItem;
			}
			function createItemHistorico(AJsonArray,AiPage)
			{
				var aUrl = "ObjListagem.LoadContent(this,'', 'Ficha de Histórico', '/neo/?op=historico&idh="+AJsonArray["id_history"]+"&page="+AiPage+"');";
	
				var eDivItem = new Array();
					eDivItem[1] = '<div class="item" onclick="'+aUrl+'">';
					eDivItem[2] = '<H5 class="nome">'+AJsonArray["titulo"]+'</H5>';
					eDivItem[3] = '<span class="nome">'+AJsonArray["descricao"]+'</span>';
					eDivItem[4] = '<span class="label verde">'+AJsonArray["action"]+'</span>';
					eDivItem[5] = '<span class="label grey-blue">'+AJsonArray["data_criacao"]+'</span>';
					eDivItem[6] = '</div>';
				return 	eDivItem;
			}
			function createLoadingHeader()
			{
				var arrAux = new Array();
					arrAux[0] = '<div id="corpo">';
					arrAux[1] = '<header class="content-header">';
					arrAux[2] = '<h2 class="titulo"></h2>';
					arrAux[3] = '<div class="botoes"></div>';
					arrAux[4] = '</header>';
					arrAux[5] = '<img src="img/loading.gif" class="loading-gif" />';
					arrAux[6] = '</div>';
					
				return arrAux.join('');
			}
			function addItemsToList(AResJson,ASubAction,AiPage,AeLoadMore,AExistLoadMore)
			{
				var arrJSON = ParseJSON(AResJson);
				var ccI = arrJSON.length;

				switch(ASubAction)
				{
					case 'list':
					{
					
					} break;
					case 'list-search':
					{
						ReplaceInnerHTML(this.ePageNext,(AiPage+1));
						this.cleanLista();
						AeLoadMore = undefined;
					} break;
				}
				
				var item = new Array();
				var paramGET = '';
				switch (this.Action)
				{
					case 'socios':
						{
							for (var i=0; i<ccI; i++)
							{
								item[i] = this.createItemSocio(arrJSON[i],AiPage).join("");
							}
							paramGET = extractParamGET("ids",window.location.href);
						} break;
					case 'entidades':
						{
							for (var i=0; i<ccI; i++)
							{
								item[i] = this.createItemEntidade(arrJSON[i],AiPage).join("");
							}
							paramGET = extractParamGET("ide",window.location.href);
						} break;
					case 'historico':
						{
							for (var i=0; i<ccI; i++)
							{
								item[i] = this.createItemHistorico(arrJSON[i],AiPage).join("");
							}
							paramGET = extractParamGET("idh",window.location.href);
						} break;
					default:{
					
							} break;
				}
				var strHTML = item.join("\n");
				if ((AExistLoadMore) && (AeLoadMore))
					AeLoadMore.insertAdjacentHTML("beforebegin",strHTML);
				else
					this.eListagem.insertAdjacentHTML("beforeend",strHTML);
				
				this.setFocusItem(undefined,paramGET);
				
				// NUMERO DE SOCIOS
				var eMenu = GetObjectByClassName('menu-principal',0);
				var eMenu_A = eMenu.children[0].children[0].children[0];
				var eMenu_A_Span = eMenu_A.children[0];
				if (eMenu_A_Span != undefined)
				{
					ReplaceInnerHTML(eMenu_A_Span,'('+item.length+')');
				}
				else
				{
					eMenu_A.insertAdjacentHTML('beforeend',' <span class="aviso">('+item.length+')</span>');
				}
			}
			function getPageAjax(AUrl)
			{
				var ipos 	= AUrl.indexOf('?');
				var auxUrl  = AUrl.substr(ipos+1);
				var AAction = extractParamGET('op',auxUrl);
				var url 	= '';
				var params  = '';

				switch (AAction)
				{
					case 'socios': 	{
										var sIDS = extractParamGET('ids',auxUrl);
										if (sIDS !== '')
										{
											url 	+= 'socios/perfil-edit.php?';
											params  += 'ids='+sIDS;
										}
										else
										{
											url 	+= 'socios/novo.php';
											params  = '';
										}
									} break;
					case 'entidades':{
										var sIDE = extractParamGET('ide',auxUrl);
										if (sIDE !== '')
										{
											url 	+= 'entidades/perfil-edit.php?';
											params  += 'ide='+sIDE;
										}
										else
										{
											url 	+= 'entidades/novo.php';
											params  = '';
										}
									} break;
					case 'historico':{
										var sIDH = extractParamGET('idh',auxUrl);
										if (sIDH !== '')
										{
											url 	+= 'historico/perfil-edit.php?';
											params  += 'idh='+sIDH;
										}
										else
										{
											url 	+= 'historico/novo.php';
											params  = '';
										}
									} break;
					default:{
					
							} break;
				}
				return {"url":url,"params":params};
			}
			// *****************************************************************
			function LoadMore(ASelf,ASubAction)
			{
				if (this.ePage)
				{
					this.setAction();
					var eLoadMore	 	= document.getElementById("LoadMore");
					var eFormFilter  	= document.forms["orderbyfield"];
					var eSelectFilter	= eFormFilter.elements["listfilter"];
					var eCheckboxFilter = eFormFilter.elements["chkOrderBy"];
					
					var iPage		 	= parseInt(this.ePage.innerHTML);
					var iPageNext 		= parseInt(this.ePageNext.innerHTML);
					var eField 		 	= (ASelf.tagName === "INPUT") ? ASelf : undefined;
					var ExistLoadMore	= (eLoadMore != undefined);
					if (ExistLoadMore)
						eLoadMore = eLoadMore.parentNode;
		
					if ((ExistLoadMore) || (eField != undefined) || (ASelf === 'begin'))
					{
						var sUrl 	 = 'ui/getList.php';
						var params 	 = 'action='+this.Action;
							params 	+= '&subaction='+ASubAction;
							params 	+= '&pagenext='+iPageNext;
							params 	+= (eField != undefined) ? ('&q='+eField.value) : '';
						if (eSelectFilter != undefined)
						{
							if (eSelectFilter.selectedIndex != 0)
							{
								params += '&filter='+eSelectFilter.options[eSelectFilter.selectedIndex].value;
								params += (eCheckboxFilter.checked) ? ';DESC' : ';ASC';
							}
						}

						var func = function(sResult)
						{
							//console.log('[LoadMore] '+sResult);
							
							var jsonData = ParseJSON(sResult);
							if (jsonData)
							{
								var ResBool 	= jsonData.result;
								var ResTPage 	= jsonData.totalpages;
								var ResNextPage = jsonData.nextpage;
								var ResJson 	= jsonData.jsondata;
								var boolRemove 	= false;
								
								if (ResBool === false)
								{
									boolRemove = true;
									if (ASubAction === 'list-search')
									{
										Self.cleanLista();
										eLoadMore = undefined;
									}
								}
								else
								if (ResBool === true)
								{
									Self.addItemsToList(ResJson,ASubAction,iPageNext,eLoadMore,ExistLoadMore);
		
									if (ResNextPage)
									{
										ReplaceInnerHTML(Self.ePageNext,(iPageNext+1));
										//Self.ePageNext.innerHTML = (iPageNext+1);
									}
									else
									{
										boolRemove = true;
									}
								}
								
								switch(ASubAction)
								{
									case 'list':
									{
										if ((ExistLoadMore) && (boolRemove) && (ASelf !== 'begin'))
										{
											Self.eListagem.removeChild(eLoadMore);
										}
										else
										if (ASelf === 'begin')
										{
											Self.getAllHeight();
										}
									} break;
									case 'list-search':
									{
										if ((eField != undefined) && (eField.value === ""))
											Self.getAllHeight();
									} break;
								}
							}
						}
						ObjAjax.Ajax({type:'POST', url:sUrl, data:params, done:func});
						//AJAX_REQUEST_CALLBACK("POST",url,params,func);
					}
				}
			}
			function LoadContent(ASelf, AData,ATitle,AUrl)
			{
				window.history.pushState(AData, ATitle,AUrl);
				//window.history.go(0);
				this.getPage(ASelf, AUrl);
			}
			function getPage(ASelf,AUrl)
			{
				var url 	 = 'ui/';
				var params   = '';
				
				Self.setFocusItem(ASelf,AUrl);

				ReplaceInnerHTML(this.eMainSubNav, this.createLoadingHeader());
	
				var Result = this.getPageAjax(AUrl);
				url 	+= Result['url'];
				params  += Result['params'];

				var func = function(sResult)
				{
			//console.log(sResult);
					ReplaceInnerHTML(Self.eMainSubNav,sResult);
					// Se HTML mudou, aponta ponteiros para os elementos respectivos
					ObjTouchSocioControlo.setDragDropElements();

					/* http://stackoverflow.com/a/16278107 */
					var arr = Self.eMainSubNav.getElementsByTagName('script');
					for (var n=0; n<arr.length; n++)
						eval(arr[n].innerHTML); //run script inside div
				}
				ObjAjax.Ajax({type:'GET', url:url, data:params, done:func});
				//AJAX_REQUEST_CALLBACK("GET",url,params,func);
			}
		}