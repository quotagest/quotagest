function ZeroClipboardAPI(ATargetObject,fCacllbackRequest,fCallbackComplete)
{
	var Self 		= this;
	var Zclip 		= undefined;
	this.AmoviePath = "js/ZeroClipboard/ZeroClipboard.swf";
	this.TargetObj 	= ATargetObject;
	

	Zclip = new ZeroClipboard(this.TargetObj, {moviePath: this.AmoviePath});

	//this event happens upon initiating the copy
	if (fCacllbackRequest !== undefined)
	{
		if (fCacllbackRequest && typeof(fCacllbackRequest) === "function")
			Zclip.on('dataRequested', function (client, args)
			{
				console.log("[Zclip] Listening input...");
				fCacllbackRequest(client, args);
			});
		else
			console.log('[Zclip] fCacllbackRequest Error: '+fCacllbackRequest);
	}
	//this event happens upon the copy finishing
	if (fCallbackComplete !== undefined)
	{
		if (fCallbackComplete && typeof(fCallbackComplete) === "function")
			Zclip.on('complete', function (client, args)
			{
				console.log("[Zclip] Listening input... Ended.");
				fCallbackComplete(client, args);
			});
		else
			console.log('[Zclip] fCallbackComplete Error: '+fCallbackComplete);
	}
}