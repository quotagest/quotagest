	/*
	* https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest?redirectlocale=en-US
	* https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
	*/
	function TObjAjax()
	{
		var Self = this;
		
		this.create = function()
		{
			var xmlhttp = false;
			
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				var sxmlhttp = new Array(
					'Microsoft.XMLHTTP',
					'Msxml2.XMLHTTP.5.0',
					'Msxml2.XMLHTTP.4.0',
					'Msxml2.XMLHTTP.3.0',
					'Msxml2.XMLHTTP');
				for (var i=0, ccI=sxmlhttp.length; i<ccI; i++)
				{
					try
					{	//Internet Explorer
						xmlhttp = new ActiveXObject(sxmlhttp[i]);
					}
					catch (e)
					{
						xmlhttp = false;
						console.log('[ERROR] XMLHTTPRequest could not be created! ('+sxmlhttp[i]+')');
					}
				}
			}
			
			return xmlhttp;
		};
		this.Trim = function(Str)
		{
			return Str.replace(/^\s+|\s+$/g,'');
		};
		this.Ajax = function(AoData)
		{
			var XMLHTTP = this.create();
			if (XMLHTTP != false)
			{
				//{type:POST, url:URL, data: [field1:value1], done: function()}
				AoData.url += (AoData.url.indexOf('?') === -1) ? '?' : '';
				if (typeof(AoData.data) === "object")
					AoData.data = AoData.data.join("&");

				if (!AoData.hasOwnProperty('async'))
					AoData.async = true;
				var fCallback = AoData.done;
				
				XMLHTTP.onreadystatechange = function()
				{
					switch(this.readyState)
					{
						case 0:
							{
								// UNSENT -- open() has not been called yet.
							} break;
						case 1:
							{
								// OPENED -- send() has not been called yet.
							} break;
						case 2:
							{
								// HEADERS_RECEIVED -- send() has been called, and headers and status are available.
								//console.log(this.getAllResponseHeaders());
							} break;
						case 3:
							{
								// LOADING -- Downloading; responseText holds partial data.
							} break;
						case 4:
							{
								// DONE -- The operation is complete.
								if (this.status==200)
								{
									if (fCallback && typeof(fCallback) === "function")
										fCallback(Self.Trim(this.responseText));
								}
							} break;
					}
				};

				switch(AoData.type)
				{
					case 'GET' :
						{
							XMLHTTP.open('GET',AoData.url+AoData.data,AoData.async);
							XMLHTTP.send(null);
						} break;
					case 'POST':
						{
							XMLHTTP.open("POST",AoData.url,AoData.async);
							XMLHTTP.setRequestHeader("Content-type","application/x-www-form-urlencoded");
							XMLHTTP.send(AoData.data);
						} break;
				}
			}
		};
	}