	/* 535ln */
	
	var ObjListagem 			= new TObjListagem('');
	var ObjTabs 				= new TObjTabs('');
	var ObjDragDropSocio 		= new TObjDragDropSocio();
	var ObjTouchSocioControlo 	= new TObjTouchSocioControlo();
	var ObjTouchSocioFamiliares = new TObjTouchSocioFamiliares();
	var ObjTabSocioQuotas 		= new TObjTabSocioQuotas();
	var ObjConfiguracoes 		= new TObjConfiguracoes();
	var SCROLL_POSY = 0;
	var SCROLL_POSX = 0;
	var arrHistoryObjects 		= {'total':0};
	var ObjAjax 				= new TObjAjax();
	
	var objNote 				= new Notefication();
	
	var ObjFormQuotas 			= new TObjFormQuotas();
	var ObjModal 				= new TObjModal();
	var ObjFormValid 			= new TObjFormsValidation();
		ObjFormValid.onNotValid = function(AeElement) { console.log(AeElement); };
		ObjFormValid.onDebunk 	= function() { CloseAnularMessage(); };
		ObjFormValid.onBunk 	= function(AsFormName) { CREATE_POP_MSG('Registo(s) Eliminado(s)! '+getCloseLink('A','#',"ObjFormValid.RemoveRecord('"+AsFormName+"')",'Anular')+' | '+getCloseLink(),20000); };

/* ************************************************************************** */
/* ************************************************************************** */
	function GET_SCROLL_POS()
	{
		var doc = document.body; 
		SCROLL_POSY = doc.scrollTop;
		SCROLL_POSX = doc.scrollLeft;
		//alert(doc.scrollHeight+' - '+doc.clientHeight);
		//percentageScrolled = Math.floor((scrollPosition / pageSize) * 100); 
	}
	function DONTSCROLL()
	{
		window.scrollTo(SCROLL_POSX,SCROLL_POSY); 
	}

	function addEventHandler(elem,eventType,handler)
	{
		if (elem.addEventListener)
			elem.addEventListener(eventType,handler,false);
		else
		if (elem.attachEvent)
			elem.attachEvent('on'+eventType,handler); 
	}	

	function Trim(str)
	{
		return str.replace(/^\s+|\s+$/g,'');
	}
	function extractParamGET(AIdName,AUrl)
	{
		AUrl = (AUrl != undefined) ? AUrl : document.location.href;
			
		var Result = '';
		var ipos   = AUrl.indexOf(AIdName);
		if (ipos != -1)
		{
			Result  = AUrl.substr(ipos+AIdName.length+1);
			ipos	= Result.indexOf('&');
			if (ipos != -1)
			{
				Result = Result.substr(0,ipos);
			}
			Result = Result.replace(/#/gi,'');
		}
		return Result;
	}
	function isEmpty(str)
	{
		return (!str || 0 === str.length);
	}
	function isBlank(str)
	{
		return (!str || /^\s*$/.test(str));
	}
	function isNumeric(string)
	{
		var numExpr = /^[0-9]+$/;
		if (string.match(numExpr))
			return true;
		else
			return false;
	}
	function isFloat(nValue)
	{
		nValue = parseFloat(nValue);
		if (nValue == 0)
			return true;
		else
			return (nValue != "") && (!isNaN(nValue)) && ((Math.round(nValue) == nValue) || (Math.round(nValue) != nValue));
	}
	function getCurrentTime(AsParams)
	{
		var currentTime = new Date();
		var seconds = currentTime.getSeconds();
		var minutes = currentTime.getMinutes();
		var hour 	= currentTime.getHours();
		var day 	= currentTime.getDate();
		var month 	= currentTime.getMonth()+1;
		var year 	= currentTime.getFullYear();

		if (month<10)
			month 	= '0'+month;
		if (day<10)
			day 	= '0'+day;
		if (hour<10)
			hour 	= '0'+hour;
		if (minutes<10)
			minutes = '0'+minutes;
		if (seconds<10)
			seconds = '0'+seconds;

		AsParams = AsParams.replace(/Y/,year);
		AsParams = AsParams.replace(/m/,month);
		AsParams = AsParams.replace(/d/,day);
		AsParams = AsParams.replace(/h/,hour);
		AsParams = AsParams.replace(/m/,minutes);
		AsParams = AsParams.replace(/s/,seconds);
		return AsParams;
	}
	function FloatToString(sString)
	{
		sString = sString.replace(/€/gi,"");
		sString = sString.replace(",",".");
		sString = sString.replace(" ","");
		sString = Trim(sString);
		return sString;		
	}
	function FloatToCurrency(fNum)
	{
		fNum = parseFloat(fNum);
		fNum += 0.00;
		fNum = fNum.toFixed(2);
		fNum = "€"+numberWithSpaces(fNum);
		return fNum;
	}
	function FloatToCurrencyExtra(fNum,iFixedNum)
	{
		fNum = parseFloat(fNum);
		fNum = fNum.toFixed(iFixedNum);
		fNum = numberWithSpaces(fNum);
		return fNum;
	}
	function isArray(object)
	{
		if (object.constructor === Array) return true;
		else return false;
	}
	function numberWithSpaces(x)
	{
		var parts = x.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		return parts.join(".");
	}
	function FormatFloatDecimal(fNumber, aCasasDecimais)
	{
		return fNumber.toFixed(aCasasDecimais);	
	}
	function setEmptyIfNull(AValue)
	{
		return (AValue != undefined) ? AValue : '';
	}

	if(!Array.prototype.indexOf)
	{
		Array.prototype.indexOf = function(needle)
		{
			var ilen = this.length;
			for(var i=0; i<ilen; i++)
			{
				if(this[i] === needle)
				{
					return i;
				}
			}
			return -1;
		};
	}

	String.prototype.replaceArray = function(find, replace)
	{
		// LINK: http://stackoverflow.com/questions/5069464/replace-multiple-strings-at-once
		var repStr = this;
		var ilen = find.length;
		for (var i = 0; i < ilen; i++)
		{
			repStr = repStr.replace(find[i], replace[i]);
		}
		return repStr;
	}
	function randomstring(L)
	{
		var s = '';
		var randomchar=function()
		{
			var n = Math.floor(Math.random()*62);
			if (n<10)
				return n; //1-10
			if (n<36)
				return String.fromCharCode(n+55); //A-Z
			
			return String.fromCharCode(n+61); //a-z
		}
		
		while(s.length < L)
			s += randomchar();
		return s;
	}
	function ParseJSON(AString,ADecode)
	{
		AString = ((ADecode != undefined) && (ADecode == true)) ? decode64(AString) : AString;
		var Result = '';
		try
		{
			Result = JSON.parse(AString);
		}
		catch(e)
		{
			Result = false;
			console.log(e);
			console.log(AString);
		}
		return Result;
	}
	/* http://www.toao.net/32-my-htmlspecialchars-function-for-javascript */
	function htmlspecialchars(str)
	{
		if (typeof(str) == "string")
		{
			str = str.replace(/&/g, "&amp;"); /* must do &amp; first */
			str = str.replace(/"/g, "&quot;");
			str = str.replace(/'/g, "&#039;");
			str = str.replace(/</g, "&lt;");
			str = str.replace(/>/g, "&gt;");
		}
		return str;
	}
	function htmlspecialchars_decode(str)
	{
		if (typeof(str) == "string")
		{
			str = str.replace(/&gt;/ig, ">");
			str = str.replace(/&lt;/ig, "<");
			str = str.replace(/&#039;/g, "'");
			str = str.replace(/&quot;/ig, '"');
			str = str.replace(/&amp;/ig, '&'); /* must do &amp; last */
		}
		return str;
	}
	
	function Autofocus(AsFormName,AsElemName)
	{
		var eElem = document.forms[AsFormName].elements[AsElemName];
			eElem.focus();
	}
	function AutoSelectIndex(AsFormName,AsElemName,AiIndex)
	{
		var eElem = document.forms[AsFormName].elements[AsElemName];
			eElem.selectedIndex = AiIndex;
	}
/* ************************************************************************** */
/* ************************************************************************** */
/* ******************* FUNÇÕES PARA TRABALHAR COM IMAGENS ******************** */
			function ChangeImgSrc(vElemID,vPath)
			{
				SetAttElementByID(vElemID,'src',vPath);
			}
			function ChangeUserTumb(vElemID,vOption)
			{
				var eElem = GetObjectByID(vElemID);
				var srcAux = eElem.getAttribute('src');
				var eMasc = GetObjectByID('socio-sexo-1');
				var eFeme = GetObjectByID('socio-sexo-2');
				var strAux = '';

				eMasc.removeAttribute('checked');
				eFeme.removeAttribute('checked');
				if (vOption === 'Masculino')
				{
					eMasc.setAttribute('checked','');
					strAux = 'user-male-a-01.jpg';
				}
				else
				if (vOption === 'Feminino')
				{
					eFeme.setAttribute('checked','');
					strAux = 'user-female-a-01.jpg';
				}
				if ((srcAux.indexOf('user-female-a-01.jpg') != -1) ||
					(srcAux.indexOf('user-male-a-01.jpg') != -1) )
				{
					ChangeImgSrc(vElemID,"/img/icons/users/"+strAux);
				}
			}
/* *********************** FUNÇÕES PROCURAR ELEMENTOS ************************ */
			function GetObjectByID(AName)
			{
				return document.getElementById(AName);
			}
			function GetObjectsByName(AName)
			{
				return document.getElementsByName(AName);
			}
			function GetObjectByName(AName,AIndex)
			{
				return document.getElementsByName(AName)[AIndex];
			}
			function GetObjectByClassName(AName,AIndex)
			{
				return document.getElementsByClassName(AName)[AIndex];
			}
			function GetObjectsByTagName(aTagName)
			{
				var arrAux = document.getElementsByTagName(aTagName);
				return arrAux;
			}
			function FindParentFORMName(eObject)
			{
				// LINK: http://www.randomsnippets.com/2008/06/26/how-to-find-and-access-parent-nodes-via-javascript/
				if (eObject)
				{
					var testObj = eObject.parentNode;
					while (testObj != undefined)
					{
						if (testObj.tagName != 'FORM')
							testObj = testObj.parentNode;
						else
							return testObj.getAttribute('name');
					}
				}
				
				return '';
			}
			function GetObjectsByTagNameData(aTagName, aDataName, aName)
			{
				var arrObjs = document.getElementsByTagName(aTagName);
				for (i=0, ilen=arrObjs.length; i<ilen; i++)
				{
					if (arrObjs[i].getAttribute(aDataName) === aName)
					{
						return arrObjs[i];
					}
				}
				return false;
			}
			function SetAttElementForm(form_name,input_name,att,attvalue)
			{
				var Field = document.forms[form_name].elements[input_name];
				Field.setAttribute(att,attvalue);
			}
			function SetAttElementByID(id_element,att,attvalue)
			{
				var Elem = GetObjectByID(id_element);
				Elem.setAttribute(att,attvalue);
			}
			function SetAttElementObject(eObject,att,attvalue)
			{
				eObject.setAttribute(att,attvalue);
			}
			function RemoveAttElementObject(eObject,att)
			{
				eObject.removeAttribute(att);
			}
			function createObjElement(sType,arrAttName,arrAttValue,oParentElem)
			{
				if (oParentElem == undefined) oParentElem = document;
				
				var oElem = oParentElem.createElement(sType);
				for (var i=0, ccI=arrAttName.length; i<ccI; i++)
				{
					if (arrAttName[i] !== "")
						oElem.setAttribute(arrAttName[i],arrAttValue[i]);
				}
				
				return oElem;
			}
			function CleanElement(objElement)
			{
				while(objElement.firstChild)
				{
					objElement.removeChild(objElement.firstChild);
				}
			}
			function ReplaceInnerHTML(AObjElement,AnewText,APosition)
			{
				APosition = (APosition != undefined) ? APosition : 'afterbegin';
				CleanElement(AObjElement);
				AObjElement.insertAdjacentHTML(APosition,AnewText);
			}
			function ParseHTMLToDOM(AsHTML)
			{
				var parentNode = document.createElement('span');
					ReplaceInnerHTML(parentNode,AsHTML);
				var eNode = parentNode.childNodes[0].cloneNode(true);
				CleanElement(parentNode);
				parentNode = undefined;
				
				return eNode;
			}
			
			function toggleUrlParam(AUrl,AParamName,AParamText,AbRetriveHOST)
			{
					AUrl   = (AUrl === '') ? window.location.href : AUrl;
				var ipos   = AUrl.indexOf('?');
				if (ipos != -1)
				{
					var sHOST 		= AUrl.substr(0,ipos);
					var Result 		= AUrl.substr(ipos+1);
					var arrParams 	= Result.split('&');
					var arrAux 		= new Array();
					var k 			= 0;
					var bFound 		= false;
					for (var i=0, ccI=arrParams.length; i<ccI; i++)
					{
						if (arrParams[i] !== '')
						{
							if (arrParams[i].indexOf(AParamName+'=') != -1)
							{
								arrParams[i] = AParamName+'='+AParamText;
								bFound = true;
							}
							arrAux[k] = arrParams[i];
							k++;
						}
					}
					if (!bFound)
					{
						arrAux.push(AParamName+'='+AParamText);
					}
					sHOST  = (AbRetriveHOST) ? sHOST : '';
					Result = sHOST+'?'+arrAux.join('&');
					return Result;
				}
				return AUrl;
			}
/* ****************** FUNÇÕES MOSTRAR-ESCONDER ELEMENTOS ****************** */
			function ToogleElemClassFromObject(AeElem,AsClass)
			{
				if (AeElem.className.indexOf(AsClass) != -1)
					ReplaceFromClass(AeElem,AsClass,'');
				else
					AddToClass(AeElem,AsClass);
			}
			function ToggleElemClass(AElemName,AClassOne,AClassTwo)
			{
				if ((typeof AElemName) === 'object')
				{
					ToogleElemClassFromObject(AElemName,AClassOne);
				}
				else
				{
					var eElem = GetObjectByClassName(AElemName,0);
					if (eElem == undefined)
						eElem = GetObjectByID(AElemName);
	
					if (eElem.className.indexOf(AClassOne) != -1)
						ReplaceFromClass(eElem,AClassOne,AClassTwo);
					else
						ReplaceFromClass(eElem,AClassTwo,AClassOne);
				}
			}
			function AddToClass(eElem,sString)
			{
				RemoveFromClass(eElem,sString);
				eElem.className = Trim(eElem.className+" "+sString);
			}
			function RemoveFromClass(eElem,sString)
			{
				ReplaceFromClass(eElem,sString,"");
			}
			function ReplaceFromClass(eElem,sString,sStrToReplace)
			{
				var sClassName = eElem.className;
				if (sClassName !== '')
				{
					var arrClass   = sClassName.split(" ");
					var sNewClass  = '';
					for (var i=0, ccI=arrClass.length; i<ccI; i++)
					{
						if (arrClass[i] === sString)
							arrClass[i] = sStrToReplace;
					}
					
					eElem.className = Trim(arrClass.join(" "));
				}
			}	
			//LINK: http://stackoverflow.com/questions/8834126/how-to-efficiently-check-if-variable-is-array-or-object-in-nodejs-v8
			function ChangeAttByTagData(aPEelem,aElemName,aElemDataName,aAttLocation,aAttDestName,aAttDestValue, aMsgYes,aMsgNo)
			{
				var sAtt = aPEelem.getAttribute(aAttLocation);
				var eObj = GetObjectsByTagNameData(aElemName,aElemDataName,sAtt);
				var sMsg = '';
				if (eObj.getAttribute(aAttDestName) !== aAttDestValue)
				{
					SetAttElementObject(eObj,aAttDestName,aAttDestValue);
					sMsg = aMsgYes;
				}
				else
				{
					SetAttElementObject(eObj,aAttDestName,'');
					sMsg = aMsgNo;
				}
				ReplaceInnerHTML(aPEelem,sMsg);
			}
			function ShowMoreFields(ASelf, aTargetTagName, aTargetDataAtt, aTextClose, aTextOpen)
			{
				if (ASelf.tagName == 'A')
				{
					var AHref   = ASelf.href.substr(ASelf.href.lastIndexOf('#'));
					var eTarget = GetObjectsByTagNameData(aTargetTagName,aTargetDataAtt,AHref);
					var aText 	= '';
					if (eTarget.className === 'hidden')
					{
						RemoveFromClass(eTarget,'hidden');
						aText = aTextOpen;
					}
					else
					{
						ToggleElemClass(eTarget,'hidden');
						aText = aTextClose;
					}
					ReplaceInnerHTML(ASelf,aText);
				}
			}
			/* =============================================================== */
			/* =============================================================== */
			
			function InitInputsForm(aFormName)
			{
				if (document.forms[aFormName])
				{
					var arrObjs = GetFormElementsArray_Objects(aFormName);
					var ccI = arrObjs.length;
					for (var i=0; i<ccI; i++)
					{
						if ( (arrObjs[i].getAttribute("data-magicinput") == undefined) ||
							 (arrObjs[i].getAttribute("data-magicinput")) === "true")
						{
							addEventHandler(arrObjs[i],"blur",function(event){ MagicInput_Blur(event); });
							if (arrObjs[i].tagName === "SELECT")
								addEventHandler(arrObjs[i],"change",function(event){ MagicInput_SelectChange(event); });
							else
								addEventHandler(arrObjs[i],"keyup",function(event){ MagicInput_KeyPress(event); });
						}
					}
				}
			}
			function getMagicInputLabel(aElemParent,aInputName)
			{
				var aList = aElemParent.getElementsByTagName("label");
				for (var i=0, ccI=aList.length; i<ccI; i++)
				{
					if (aList[i].getAttribute("for") === aInputName)
					{
						return aList[i];
					}
				}
				return undefined;
			}
			function findMagicInput_Label(event)
			{
				var eInput  = event.target;
				var eParent = eInput.parentNode;
				var bExist	= true;
				var eLabel  = getMagicInputLabel(eParent,eInput.name);
	
				if (eLabel == undefined)
				{
					eLabel = document.createElement("label");
					eLabel.setAttribute("for",eInput.name);
					eLabel.setAttribute("class","input-label");
					ReplaceInnerHTML(eLabel,eInput.getAttribute("placeholder"));
					bExist = false;
				}
	
				return {"parent":eParent, "input":eInput, "label":eLabel, "exist":bExist};			
			}
			function MagicInput_KeyPress(event,ASelf)
			{
				var result = findMagicInput_Label(event);
	
				if (result["input"].value.length > 0)
				{
					if (!result["exist"])
						result["parent"].insertBefore(result["label"],result["input"]);
				}
				else
				{
					if (result["exist"])
						result["parent"].removeChild(result["label"]);
				}
			}
			function MagicInput_SelectChange(event)
			{
				var result = findMagicInput_Label(event);
	
				if (result["input"].selectedIndex > 0)
				{
					if (!result["exist"])
						result["parent"].insertBefore(result["label"],result["input"]);
				}
				else
				{
					if (result["exist"])
						result["parent"].removeChild(result["label"]);
				}
			}
			function MagicInput_Blur(event)
			{
	
			}
			/* =============================================================== */
			/* =============================================================== */

/* ************************************************************************** */
/* ************************************************************************** */
		//var objNote = null;
		function getCloseLink(AType,AHref,AonClick,ATitle)
		{
			var FType 	 = (AType != undefined) 	? AType : 'BUTTON';
			var FHref 	 = (AHref != undefined) 	? AHref : '#';
			var FonClick = (AonClick != undefined) 	? AonClick.replace(/"/gi,"'") : 'CloseAnularMessage();';
			var FTitle 	 = (ATitle != undefined) 	? ATitle : 'fechar';
			if (FType === 'BUTTON')
			{
				return ' <button class="botao close" onclick="'+FonClick+'">'+FTitle+'</button> ';
			}
			else
			{
				return ' <a class="anular" href="'+FHref+'" onclick="'+FonClick+'">'+FTitle+'</a> ';
			}
		}
		function CREATE_POP_MSG(AText,ATimer)
		{
			objNote.Close();
			objNote.Create(ATimer);
			objNote.addInfo(AText,'string');
			objNote.append();
		}
		function CloseAnularMessage(AbForceClose)
		{
			AbForceClose = (AbForceClose != undefined) ? AbForceClose : false;
			objNote.Close(AbForceClose);
		}
		

/* ************************************************************************** */
/* *************** FUNÇÕES TRABALHAR COM FORM-INPUT ELEMENTOS *************** */
/* ************************************************************************** */
		function GetFormElementsArray_Objects(aFormName)
		{
			var aObjInput = new Array(); /* CRIA ARRAY DE INPUTS*/
			var iIndex	  = 0;
			var aForm	  = document.forms[aFormName];
			var aElements = aForm.elements;
			var ilen 	  = aElements.length;

			for (var i=0; i<ilen; i++)
			{
				var eObj = aElements[i];
				if ((eObj.tagName === 'INPUT') ||
					(eObj.tagName === 'TEXTAREA') ||
					(eObj.tagName === 'SELECT'))
				{
					aObjInput[iIndex] = eObj;
					iIndex++;
				}
			}
			return aObjInput;
		}
		//######################################################################
		
		function ValidateForm(AFormName,fCallback)
		{
			ObjFormValid.onValidateFields = function(AeForm, AeJsonFields, AeJsonRequired)
						{
							var bValid 		= true;
							var eFirstField = false;
							
							for(var key in AeJsonRequired)
							{
								if (eFirstField === false)
								{
									eFirstField = AeForm.elements[key];
								}
								var eWarnEmpty = GetObjectsByTagNameData('SPAN','data-id',key);
								
								if ( (AeJsonRequired[key] === true) && ((AeJsonFields._POST[key] === '') || (AeJsonFields._POST[key] === 'false')) )
								{
									bValid = false;
			
									if (eWarnEmpty === false)
									{
										var iposLeft = 415;
										var iposTop  = AeForm.elements[key].clientTop+AeForm.elements[key].offsetTop+3;
										
										var strWarnEmpty = '<span class="warn-empty" data-id="'+key+'" style="top:'+(iposTop)+'px; left:'+(iposLeft)+'px;">Preecha o campo!</span>';
										AeForm.elements[key].insertAdjacentHTML('beforebegin',strWarnEmpty);
									}
								}
								else
								if ( (AeJsonRequired[key] === true) && (AeJsonFields._POST[key] !== '') )
								{
									if (eWarnEmpty !== false)
									{
										AeForm.elements[key].parentNode.removeChild(eWarnEmpty);
									}
								}
							}
							if (eFirstField !== false)
							{
								eFirstField.focus();
							}
							
							ObjFormValid.onValidateFields = false;
							
							return bValid;
						};
			ObjFormValid.onRecordSaved = function() {
														var objResult = ObjFormValid.serializeFORM(AFormName);
														var jsonPOST  = objResult.fields;
														
														var sURL = document.location.href;
														appendRowToListagem(jsonPOST,extractParamGET('op',sURL),sURL);
														CREATE_POP_MSG('Registo salvo com sucesso! '+getCloseLink(),3000);
														EnableForm(AFormName);
													};
			ObjFormValid.onRecordSavedError = function() {
															CREATE_POP_MSG('Ocorreu um erro. <br/> Porfavor tente novamente.'+getCloseLink(),3000);
														};
			var objResult = getParamsFORM(AFormName);
			ObjFormValid.ValidateForm(AFormName,objResult.opera);
		}
		//######################################################################
		//######################################################################
		
		function CleanForm(aFormName,AarrResetField,AarrResetTo) /* ResetFormElements(aFormName); */
		{
			var arrElem = document.forms[aFormName].elements;
			var ilen	= arrElem.length;
			var bRad	= false;
			var eObj 	= false;
			var ccJ 	= AarrResetField.length;
			var bChange = false;
			var sResetValue = '';
			
			for (var i=0; i<ilen; i++)
			{
				eObj 		= arrElem[i];
				bChange 	= false;
				sResetValue = '';
				
				/* PROCURA CAMPO PARA MODIFICAR */
				for(var j=0; j<ccJ; j++)
				{
					if (eObj.name === AarrResetField[j])
					{
						bChange = true;
						sResetValue = AarrResetTo[j];
						break;
					}
				}
				
				if (eObj.getAttribute("type") === "radio")
				{
					if (!bRad)
					{
						eObj.checked = true;
						var eField = GetObjectsByTagNameData("LABEL","for",eObj.id);
						if (eField.onclick !== undefined)
							eField.click();
					}
					else
						eObj.checked = false;
				}
				else
				if ((eObj.tagName === "INPUT") || (eObj.tagName === "TEXTAREA"))
				{
					eObj.value = (bChange !== false) ? sResetValue : '';
				}
				else
				if (eObj.tagName === "SELECT")
					eObj.selectedIndex = (bChange !== false) ? sResetValue : 0;
			}
		}

		function EnableForm(AFormName)
		{
			var eForm = document.forms[AFormName];
			if (eForm != undefined)
			{
				var eTitulo 		= GetObjectByClassName("titulo",0);
				var eBotaoEditar	= GetObjectsByTagNameData('BUTTON','data-name','editar');
				var eBotaoGuardar	= GetObjectsByTagNameData('BUTTON','data-name','guardar');
				var eBotaoAnular	= GetObjectsByTagNameData('BUTTON','data-name','anular');
				var eCorpo 			= GetObjectsByTagNameData('SPAN','data-id','corpo');
				var sTitulo 	= eTitulo.innerHTML.replace("Edição | ","").replace("Visualização | ","");
				var eClassElem 	= eCorpo.getAttribute("class");
				var eElems 		= eForm.elements;
				console.log('[Acção Actual] '+eClassElem);
	
				if (eClassElem === "act-editar")
				{
					for (var i=0, ccI=eElems.length; i<ccI; i++)
					{
						if ((eElems[i].tagName === "INPUT") ||
							(eElems[i].tagName === "TEXTAREA") ||
							(eElems[i].tagName === "SELECT") )
						{
							eElems[i].setAttribute("disabled","disabled");
						}
					}
					eCorpo.setAttribute("class","act-normal");
					eBotaoEditar.removeAttribute("style");
					eBotaoGuardar.setAttribute("style","display:none;");
					eBotaoAnular.setAttribute("style","display:none;");
					ReplaceInnerHTML(eTitulo,"Visualização | "+sTitulo);
				}
				else
				if (eClassElem === "act-normal")
				{
					for (var i=0, ccI=eElems.length; i<ccI; i++)
					{
						if ((eElems[i].tagName === "INPUT") ||
							(eElems[i].tagName === "TEXTAREA") ||
							(eElems[i].tagName === "SELECT") )
						{
							eElems[i].removeAttribute("disabled");
						}
					}
					eCorpo.setAttribute("class","act-editar");
					eBotaoEditar.setAttribute("style","display:none;");
					eBotaoGuardar.removeAttribute("style");
					eBotaoAnular.removeAttribute("style");
					ReplaceInnerHTML(eTitulo,"Edição | "+sTitulo);
				}
			}
		}
		
		function RemoveRecord(AFormName)
		{
			ToggleRemoveFormRecord('',AFormName);
		}
		/*
		function RemoveRecord(AFormName,AFormType)
		{
			AFormType = (AFormType != undefined) ? AFormType : 'FORM';
			ToggleRemoveFormRecord(AFormType,AFormName);
		}
		*/
		function ResetFormValues(AFormName)
		{
			var eForm = document.forms[AFormName];
			if (eForm)
			{
				var sJSON = eForm.elements['jsondata'].value;
					//sJSON = decode64(sJSON);
					sJSON = htmlspecialchars_decode(sJSON);
				var arrJson = ParseJSON(sJSON);
	
				var eElem 	= null;
				var objROW 	= null;
				var sName	= '';
				var sType	= '';
				var sValue 	= '';
				for (var i=0, ccI=arrJson.length; i<ccI; i++)
				{
					objROW	= arrJson[i];
					sName	= objROW['name'];
					sType	= objROW['type'];
					sValue 	= objROW['value'];
					eElem 	= eForm.elements[sName];;
	
					if ((sType === 'input') || (sType === 'textarea'))
					{
						eElem.value = sValue;
					}
					else
					if (sType === 'radio')
					{
						for (var j=0, ccJ=eElem.length; j<ccJ; j++)
						{
							if (eElem[j].value === sValue)
							{
								eElem[j].setAttribute('checked','true');
							}
							else
							{
								eElem[j].removeAttribute('checked');
							}
						}
					}
					else
					if (sType === 'select')
					{
						for (var j=0, ccJ=eElem.options.length; j<ccJ; j++)
						{
							if (eElem.options[j].value === sValue)
							{
								eElem.selectedIndex = j;
								break;
							}
						}
					}
				}
				
				EnableForm(AFormName);
			}
		}

		function ToggleRecordAjax(AobjPars,AsExtraParams,fCallback)
		{
			var url 	= '/core/scripts/ajax-toggle-record.php';
			var params 	= '';
		//AobjPars.jsonPOST = decode64(AobjPars.jsonPOST);
			for(var key in AobjPars)
			{ 
				if (AobjPars.hasOwnProperty(key))
				{
					params 	+= '&'+key+'='+AobjPars[key];
				}
			}
			if (AsExtraParams[0] !== '&')
				AsExtraParams = '&'+AsExtraParams;
			params += AsExtraParams;
		//console.log(params);
			//AJAX_REQUEST_CALLBACK("POST",url,params,fCallback);
			ObjAjax.Ajax({type:'POST', url:url, data:params, done:fCallback});
		}

/* ******************************************************************************
*							NOTIFICATION OBJECT									*
******************************************************************************* */
		function Notefication()
		{
			var Self 			= this;
			this.tTimeOut 		= 300;
			this.eNote 			= undefined;
			this.eNoteInner 	= undefined;
			this.eNoteTimer 	= undefined;
			this.eNoteT
			this.tInterval 		= undefined;
			this.tTimeoutClose 	= undefined;
			this.iInterval 		= 5000;
			this.iTimeoutClose 	= 5100;
			this.ClassDown 		= 'slideDown';
			this.ClassUp 		= 'slideUp';
			
			this.cleanENote			= cleanENote;
			this.Close				= Close;
			this.Create				= Create;
			this.addInfo			= addInfo;
			this.append				= append;
			this.clearTimers		= clearTimers;
			this.execNodeInterval	= execNodeInterval;
			
			function cleanENote()
			{
				document.body.removeChild(Self.eNote);
				Self.eNote 		= undefined;
				Self.eNoteInner = undefined;
				Self.eNoteTimer = undefined;
				Self.clearTimers();
				
				ObjFormValid.ResetHistory();
				ObjTabSocioQuotas.ResetHistory();
			}
			function Close(AbActionImediate)
			{
				this.clearTimers();
				
				if (Self.eNote != undefined)
				{
					Self.eNote.className = "note "+this.ClassUp;
					
					if (AbActionImediate == true)
					{
						this.cleanENote();
					}
					else
					{
						this.tTimeoutClose = setTimeout(function()
								{
									Self.cleanENote();
								}, Self.tTimeOut);
					}
				}
			}
			function Create(ATimeOut)
			{
				this.iInterval 		= (ATimeOut == undefined) ? this.iInterval : ATimeOut;
				this.iTimeoutClose 	= (this.iInterval + 10);

				this.eNote = document.createElement("DIV");
				this.eNote.className = "note "+this.ClassDown;
				this.eNote.setAttribute("id","note_"+randomstring(5));

				this.eNoteInner = document.createElement("SPAN");
				this.eNoteInner.setAttribute("id","NoteInner");
				this.eNoteInner.innerHTML = "";

				this.eNoteTimer = document.createElement("SPAN");
				this.eNoteTimer.setAttribute("id","NoteTimer");
				this.eNoteTimer.setAttribute("class","temporizador");
				this.eNoteTimer.setAttribute("style","width:100%;");
				//this.eNoteTimer.innerHTML = "("+(this.iInterval/1000)+"s)";

				this.eNote.appendChild(this.eNoteInner);
				this.eNote.appendChild(this.eNoteTimer);

				return Self;
			}
			function addInfo(AInfor,AType)
			{
				if (this.eNoteInner != undefined)
				{
					switch(AType)
					{
						case "object" : this.eNoteInner.appendChild(AInfor); break;
						case "string" : ReplaceInnerHTML(this.eNoteInner,AInfor,'beforeend'); break;
					}
				}
			}
			function append()
			{
				if (this.eNote)
					setTimeout(function(){ document.body.appendChild(Self.eNote); Self.execNodeInterval(); },100);
			}
			function execNodeInterval()
			{
				this.clearTimers();
				this.tInterval = setInterval(function()
					{
						Self.iInterval -= 1000;
						
						var iPercent = ((Self.iInterval-1000)/(Self.iTimeoutClose-510) * 100);
						if (iPercent < 0)
							iPercent = 0;
						//console.log(iPercent+"% --- "+Self.iInterval+" --- "+Self.iTimeoutClose);
						
						//Self.eNoteTimer.innerHTML   = "("+(Self.iInterval/1000)+"s)";
						Self.eNoteTimer.style.width = iPercent+"%";
					}, 1000);
				this.tTimeoutClose = setTimeout(function(){ Self.Close(); }, Self.iTimeoutClose);
			}
			function clearTimers()
			{
				clearInterval(this.tInterval);
				clearTimeout(this.tTimeoutClose);
			}
		}
		
/* ************************************************************************** */
/* ************************************************************************** */
		function KeysUpDown(event)
		{
			event = event || window.event;
			var keycode = event.charCode || event.keyCode;
			var Result = '';
			//console.log(event.which);

			if ((event.which == 13) && (event.shiftKey))
			{
				Result = 'SHIFT-ENTER';
				//console.log('SHIFT+ENTER');
			}
			else
			if (event.which == 13)
			{
				Result = 'ENTER';
			}
			else
			if (event.which == 27)
			{
				Result = 'ESC';
			}
			else
			if (event.which == 8)
			{
				Result = 'BACKDELETE';
			}
			return Result;
		}
/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
/* ************************************************************************** */
	function checkImage(AsSRC,AsFolder,fCallBack)
	{
		var img 	= new Image();
		var sURL 	= 'http://app.quotagest.com/img/';
		var imgSrc 	= sURL+AsFolder+AsSRC;

		img.onload = function()
			{
				if (fCallBack && typeof(fCallBack) === "function")
					fCallBack(true,imgSrc);
			};
		img.onerror = function()
			{
				if (fCallBack && typeof(fCallBack) === "function")
					fCallBack(false,sURL);
			};
	
		img.src = imgSrc;
	}
/* ************************************************************************************ */
/* ****************************** SEARCH CTT INFORMATION ****************************** */
/* ************************************************************************************ */
	//js_xmlhttp.js
	//main.js
	function CTTError()
	{
		var objInfoBox = GetObjectByClassName('info-box',0);
		SetAttElementObject(objInfoBox,"class","info-box erro");
		ReplaceInnerHTML(objInfoBox,"Código Postal incorrecto!");
		
		var objMorada = GetObjectByName("Morada",0);
		objMorada.value = "";	
	}
	function CTTINFO(event)
	{
		event.preventDefault();
		
		var COD = GetObjectByName("Codigo-Postal",0);
		var value = COD.value;
		var auxcod = value.replace(/-/gi,"");

		var objInfoBox 	= GetObjectByClassName('info-box',0);
		var objMorada 	= GetObjectByName("Morada",0);
		
		switch(auxcod.length)
		{
			case 0: {
						SetAttElementObject(objInfoBox,"class","info-box");
						ReplaceInnerHTML(objInfoBox,"Introduza um código postal (ex: 3000-123)");
						objMorada.value = "";
						return false;
					} break;
			case 4: {
						if (event.which != 8) //backspace-delete
						{
							if ((auxcod+'-') !== value)
								COD.value = auxcod+'-';
							return false;
						}
					} break;
			case 7: {
						value = (auxcod.substr(0,4))+'-'+(auxcod.substr(4,3));
						COD.value = value;
					} break;
			default : {
						return false;
					} break;
		}

		if (isNumeric(auxcod))
		{
			auxcod = value.split("-");
			var objCOD1 = auxcod[0];
			var objCOD2 = auxcod[1];
			var url 	= "../core/scripts/get_cttinfo.php";
			var params 	= "cod1="+objCOD1;
				params += "&cod2="+objCOD2;
				params += "&up=1";
			
			var func = function(sResult)
			{  
				if (sResult.indexOf("false") !== -1)
				{
					CTTError();
				}
				else
				{
					var arrJSON	= JSON.parse(sResult);
					ReplaceInnerHTML(objInfoBox,arrJSON['concelho']+', '+arrJSON['distrito']);
					SetAttElementObject(objInfoBox,"class","info-box");

					objMorada.value = arrJSON['rua'];
					objMorada.focus();
				}
			}

			//AJAX_REQUEST_CALLBACK("GET",url,params,func);
			ObjAjax.Ajax({type:'GET', url:url, data:params, done:func});
		}
		else
			CTTError();
	}
/* ************************************************************************************ */
/* ************************************************************************************ */
		function appendRowToListagem(AjsonData,AAction,AExtraURLID)
		{
			var arrJSON = ParseJSON(AjsonData);
				arrJSON = arrJSON['_POST'];
			var htmlData = '';
			var eItemParent = focusRecoveredItem(AAction,AExtraURLID);
			var eItem = undefined;
			var eJSON = undefined;

			if (eItemParent != undefined)
			{
				var eItem = eItemParent.children[1];
				var eJSON = eItemParent.children[2];
			}
			switch (AAction)
			{
				case 'socios':
					{
						var jsonData = ParseJSON(eJSON.innerHTML);
							jsonData.id_socio 		= extractParamGET('ids',AExtraURLID);
							jsonData.nome 			= arrJSON['Nome'];
							jsonData.codigo 		= arrJSON['NumSocio'];
							jsonData.socio_nome_tipo= arrJSON['TipoSocio-Title'];
							jsonData.sexo 			= arrJSON['socio-sexo-Title'];
							jsonData.divida 		= (eItem == undefined) ? '' : eItem.children[3].innerHTML;
						
						if (eItem == undefined)
						{
							htmlData = ObjListagem.createItemSocio(jsonData,'');
						}
						else
						{
							ReplaceInnerHTML(eItem.children[0],arrJSON['Nome']);
							ReplaceInnerHTML(eItem.children[1],arrJSON['NumSocio']);
							ReplaceInnerHTML(eItem.children[2],arrJSON['TipoSocio-Title']);
							ReplaceInnerHTML(eJSON,JSON.stringify(jsonData));
							//ReplaceInnerHTML(eItem.children[3],arrJSON['EstadoSocio-Title']);
						}
					} break;
				case 'entidades':
					{
						var jsonData = ParseJSON(eJSON.innerHTML);
							jsonData.id_entidade 	= extractParamGET('ide',AExtraURLID);
							jsonData.nome 			= arrJSON['Nome'];
							jsonData.abreviatura 	= arrJSON['Abreviatura'];
							jsonData.tipo 			= arrJSON['Tipo-Entidade-Title'];
						
						if (eItem == undefined)
						{
							htmlData = ObjListagem.createItemEntidade(jsonData,'');
						}
						else
						{
							ReplaceInnerHTML(eItem.children[0],arrJSON['Nome']);
							ReplaceInnerHTML(eItem.children[1],arrJSON['Abreviatura']);
							ReplaceInnerHTML(eItem.children[2],arrJSON['Tipo-Entidade-Title']);
							ReplaceInnerHTML(eJSON,JSON.stringify(jsonData));
						}
					} break;
				case 'historico':{} break;
				default: {} break;
			}
			if (eItem == undefined)
			{
				ObjListagem.eListagem.insertAdjacentHTML('afterbegin',htmlData.join(''));
			}
		}
		function focusRecoveredItem(APlace,AHREF)
		{
			var sID = '';
			switch (APlace)
			{
				case 'socios': 	 { sID = extractParamGET('ids',AHREF); } break;
				case 'entidades':{ sID = extractParamGET('ide',AHREF); } break;
				case 'historico':{ sID = extractParamGET('idh',AHREF); } break;
				default: {} break;
			}
			return ObjListagem.setFocusItem(undefined,sID);
		}

		function getParamsFORM(AFormName)
		{
			var eForm 	= document.forms[AFormName]

			var objParams = {};
				objParams.url 	 = '';
				objParams.params = '';
				objParams.action = extractParamGET('op',document.location.href);
				objParams.opera  = '';
			switch (objParams.action)
			{
				case 'socios': 
					{
						if (eForm.elements['CodSocio'])
						{
							objParams.url 	 += 'socios/perfil-edit.php?';
							objParams.params += '&ids='+eForm.elements['CodSocio'].value;
							objParams.params += '&extraparam=editar';
							objParams.opera   = 'update';
						}
						else
						{
							objParams.url 	 += 'socios/novo.php?';
							objParams.params  = '&extraparam=novo';
							objParams.opera   = 'append';
						}
					} break;
				case 'familiares': 
					{
						if (eForm.elements['CodSocioFamiliar'])
						{
							objParams.url 	 += '';
							objParams.params += '&ids='+eForm.elements['CodSocio'].value;
							objParams.params += '&idsf='+eForm.elements['CodSocioFamiliar'].value;
							objParams.params += '&extraparam=editar';
							objParams.opera   = 'update';
						}
						else
						{
							objParams.url 	 += '';
							objParams.params += '&ids='+eForm.elements['CodSocio'].value;
							objParams.params += '&extraparam=novo';
							objParams.opera   = 'append';
						}
					} break;
				case 'entidades':{
									//var sIDE = extractParamGET('ide',AUrl);
									if (eForm.elements['CodEntidade'])
									{
										objParams.url 	 += 'entidades/perfil-edit.php?';
										objParams.params += '&ide='+eForm.elements['CodEntidade'].value;
										objParams.params += '&extraparam=editar';
										objParams.opera   = 'update';
									}
									else
									{
										objParams.url 	 += 'entidades/novo.php?';
										objParams.params  = '&extraparam=novo';
										objParams.opera   = 'append';
									}
								} break;
				case 'historico':{
									var sIDH = extractParamGET('idh',AUrl);
									if (sIDH !== '')
									{
										objParams.url 	 += 'historico/perfil-edit.php?';
										objParams.params += '&idh='+sIDH;
										objParams.params += '&extraparam=editar';
										objParams.opera   = 'update';
									}
									else
									{
										objParams.url 	 += 'historico/novo.php?';
										objParams.params  = '&extraparam=novo';
										objParams.opera   = 'append';
									}
								} break;
				case 'configs':
					{
						AFormName = AFormName.substr(0,AFormName.indexOf('_'));
						var ssAction 	= '';
						var esInput 	= false;
						
						switch(AFormName)
						{
							case 'pagforma-perfil':
								{
									ssAction 	= 'pagforma';
									esInput 	= eForm.elements['CodPagForm'];
								} break;
							case 'pagopcao-perfil':
								{
									ssAction 	= 'pagopcao';
									esInput 	= eForm.elements['CodPagOpcao'];
								} break;
							case 'socioestado-perfil':
								{
									ssAction 	= 'socioestado';
									esInput 	= eForm.elements['CodSocioEstado'];
								} break;
							case 'sociotipo-perfil':
								{
									ssAction 	= 'sociotipo';
									esInput 	= eForm.elements['CodSocioTipo'];
								} break;
							default:
								{
									console.log(AFormName);
								} break;
						}
						
						objParams.action = ssAction;
						if (esInput)
						{
							objParams.url 	 += '';
							objParams.params += '&idrow='+esInput.value;
							objParams.params += '&extraparam=editar';
							objParams.opera   = 'update';
						}
						else
						{
							objParams.url 	 += '';
							objParams.params += '&extraparam=novo';
							objParams.opera   = 'append';
						}
						console.log(objParams);
					} break;
				default:{
				
						} break;
			}
			
			return objParams;
		}

/* ************************************************************************************ */
/* ************************************************************************************ */
/*
	INI_FILEUPLOAD("upload-image","divUploadImage","spanProgressBar");
	document.body.onkeydown = function(event){ KeysUpDownSubmitForm(event,'socio-perfil-edit'); }
*/
	function ToggleRemoveFormRecord(AFormType,AFormName)
	{
		AFormType = (AFormName.indexOf('_') == -1) ? 'FORM' : 'SUB-FORM'
		switch(AFormType)
		{
			case 'FORM':
				{
					var eForm = document.forms[AFormName];
					switch(AFormName)
					{
						case 'socio-perfil-edit':
							{
								var sSection	= 'socios';
								var strIDValue 	= eForm.elements['CodSocio'].value;
								var strTitle 	= eForm.elements['Nome'].value;
								var extraparams = '&ids='+strIDValue;
							} break;
						case 'entidade-perfil-edit':
							{
								var sSection	= 'entidades';
								var strIDValue 	= eForm.elements['CodEntidade'].value;
								var strTitle 	= eForm.elements['Nome'].value;
								var extraparams = '&ide='+strIDValue;
							} break;
						default:
							{
								
							} break;
					}
					if (arrHistoryObjects.total == 0)
					{
						var Result = ObjListagem.getItemByDataID(strIDValue);
						ObjFormValid.eObjectToHide = Result['element'];
					}
					else
					{
						ObjListagem.setFocusItem(arrHistoryObjects['objects']['0']);
					}
				} break;
			case 'SUB-FORM':
				{
					var eForm 	  = document.forms[AFormName];
					var arrsForm  = AFormName.split('_');
					var sFormName = arrsForm[0];
					
					ObjFormValid.eObjectToHide = eForm.parentNode;
					
					switch(sFormName)
					{
						case 'socio-familiar-perfil':
							{
								var CodSocio 	= document.forms['socio-perfil-edit'].elements['CodSocio'].value;
								
								var sSection	= 'familiares';
								var arrJSON 	= ParseJSON(eForm.elements['jsonData'].value);
								var strIDValue 	= arrJSON['id_tblfamiliar'];
								var strTitle 	= arrJSON['nome'];
								var extraparams = '&ids='+CodSocio;
									extraparams += '&idf='+strIDValue;
							} break;
						case 'pagforma-perfil':
							{
								var sSection	= 'pagforma';
								var strIDValue 	= eObject.getAttribute('data-idrow');
								var strTitle 	= eForm.elements['Nome'].value;
								var extraparams = '';
							} break;
						case 'pagopcao-perfil':
							{
								var sSection	= 'pagopcao';
								var strIDValue 	= eObject.getAttribute('data-idrow');
								var strTitle 	= eForm.elements['Nome'].value;
								var extraparams = '';
							} break;
						case 'sociotipo-perfil':
							{
								var sSection	= 'sociotipo';
								var strIDValue 	= eObject.getAttribute('data-idrow');
								var strTitle 	= eForm.elements['Nome'].value;
								var extraparams = '';
							} break;
						case 'socioestado-perfil':
							{
								var sSection	= 'socioestado';
								var strIDValue 	= eObject.getAttribute('data-idrow');
								var strTitle 	= eForm.elements['Nome'].value;
								var extraparams = '';
							} break;
						default:
							{
								
							} break;
					}
					
					if (arrHistoryObjects.total == 0)
					{

					}
				} break;
		}
		
		ObjFormValid.onNotValid = function(AeElement) 	{
															CREATE_POP_MSG('Ocorreu um erro. <br/> Porfavor tente novamente.'+getCloseLink(),3000);
														};
		ObjFormValid.onDebunk 	= function() 			{
															for(var i=0; i<arrHistoryObjects.total; i++)
															{
																RemoveFromClass(arrHistoryObjects.objects[i],'hidden');
															}
															
															CREATE_POP_MSG('Registo(s) recuparado(s) com sucesso! '+getCloseLink(),3000);
														};
		ObjFormValid.onBunk 	= function(AsFormName) 	{
															for(var i=0; i<arrHistoryObjects.total; i++)
															{
																AddToClass(arrHistoryObjects.objects[i],'hidden');
															}
															
															CREATE_POP_MSG('Registo(s) Eliminado(s)! '+getCloseLink('A','#',"ToggleRemoveFormRecord('"+AFormType+"','"+AFormName+"')",'Anular')+' | '+getCloseLink(),20000);
														};
		
		ObjFormValid.RemoveRecord(AFormName);
	}
	function ValidateForm_SocioPerfil(AFormName)
	{
		var funcPOST = function(sResult)
		{
			var jsonData = ParseJSON(sResult);
			if (jsonData)
			{
				var bResult= jsonData.result;
				var sNewID = (jsonData.idinserted) ? jsonData.idinserted : '';
				
				if (bResult)
				{
					var objResult = ObjFormValid.serializeFORM(AFormName);
					var jsonPOST  = objResult.fields;
					
					appendRowToListagem(jsonPOST,'socios',document.location.href);
					CREATE_POP_MSG('Registo salvo com sucesso! '+getCloseLink(),3000);
					EnableForm(AFormName);
				}
				else
				{
					CREATE_POP_MSG('Ocorreu um erro. <br/> Porfavor tente novamente.'+getCloseLink(),3000);
				}
			}
		}
		
		var eForm 		= document.forms[AFormName];
		var eNumSocio 	= eForm.elements['NumSocio'];
			eNumSocio.value = Trim(eNumSocio.value);
		if (eNumSocio.value !== "")
		{
			var url			= "/core/scripts/find_sociocodigo.php";
			var params		= "pesquisa="+eNumSocio.value;
			if (eForm.elements['CodSocio'])
				params		+= "&ids="+eForm.elements['CodSocio'].value;
			
			var func = function(sResult)
			{
				if (sResult.indexOf("false") !== -1) // IF DIDN'T FIND RECORD, GOOD
				{
					ValidateForm(AFormName,funcPOST);
					return true;
				}
				else
				if (sResult.indexOf("true") !== -1)
				{
					eNumSocio.focus();
					CREATE_POP_MSG("O código: <b>'"+eNumSocio.value+"'</b> já se encontra atribuido."+getCloseLink(),5000);
					return false;
				}
			}

			//AJAX_REQUEST_CALLBACK("POST",url,params,func);
			ObjAjax.Ajax({type:'POST', url:url, data:params, done:func});
		}
		else
		{
			ValidateForm(AFormName,funcPOST);
			return true;
		}
	}
/* ************************************************************************************ */
/* ************************************************************************************ */