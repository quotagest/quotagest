/*
	* FILES *
	## REQUIRED: ajax.js
	## REQUIRED: encode64.js

	* FUNCTIONS *
	## REQUIRED: GetObjectsByTagNameData
	## REQUIRED: Trim
	## REQUIRED: ParseJSON
	## REQUIRED: AddToClass
	## REQUIRED: RemoveFromClass
	## REQUIRED: 
	
	# data-isid 	--> indica que o campo é de ID principal
	# -> O campo deve ser "required"
	# data-elemhide --> indica que o elemento deve ser ocultado ao "apagar"
	# data-setpost 	--> indica se o campo deve ser adicionado ao "_POST" para enviar por Ajax
	# isotherid 	--> indica se o campo é outro ID principal
	
*/

	function TObjFormsValidation()
	{
		var Self 				= this;
		this.arrItems 			= new Array();
		this.countItems 		= 0;
		this.eObjectToHide 		= false;
		this.arrHistoryObjects 	= {'total':0}

		this.ResetHistory 			= ResetHistory;
		this.getRadioInputField 	= getRadioInputField;
		this.serializeFORM 			= serializeFORM;
		this.CleanForm 				= CleanForm;
		this.RemoveRecord 			= RemoveRecord;
		this.ValidateForm 			= ValidateForm;
		this.ToggleRecordAjax 		= ToggleRecordAjax;
		this.addInfoRecordToList 	= addInfoRecordToList;
		this.getRecordsJSON 		= getRecordsJSON;
		this.resetRecordList 		= resetRecordList;
		this.extractFormRealName 	= extractFormRealName;
		this.getElementToHide 		= getElementToHide;
		this.getIDsList 			= getIDsList;
		this.onNotValid 			= false;
		this.onBunk 				= false;
		this.onDebunk 				= false;
		this.onRecordSavedError 	= false;
		this.onRecordSaved 			= false;
		this.onRemoveError 			= false;
		this.onValidateFields 		= false;
		
		function ResetHistory()
		{
			// LIMPA Objecto REMOVERRECORD
			this.arrHistoryObjects.total 		= 0;
			this.arrHistoryObjects.idhistory	= '';
			this.arrHistoryObjects.opera 		= '';
			this.arrHistoryObjects.encodedJSON 	= '';
			this.arrHistoryObjects.extraparams 	= '';
			this.arrHistoryObjects.section 		= '';
			this.arrHistoryObjects.objects 		= {};
			
			ObjFormValid.resetRecordList();
		}
		
		function addInfoRecordToList(AsID,AsTitle)
		{
			this.arrItems[this.countItems] = JSON.stringify({"value":AsID, "title":AsTitle});
			this.countItems++;
		}
		function getRecordsJSON(AsExtraParams)
		{
			var strItems = this.arrItems.toString();
			var objJSON = {"objects": "["+strItems+"]", "extraparams":AsExtraParams};

			return JSON.stringify(objJSON);
		}
		function resetRecordList()
		{
			delete this.arrItems;
			this.arrItems = {};
			this.countItems = 0;
		}
		function extractFormRealName(AsFormName)
		{
			var arrString = AsFormName.split('_');
				AsFormName = arrString[0].replace(/novo/gi,'');
				AsFormName = AsFormName.replace(/edit/gi,'');
				AsFormName = AsFormName.replace(/perfil/gi,'');
				AsFormName = AsFormName.replace(/-/gi,'');
				AsFormName = AsFormName.replace(/^\s+|\s+$/g,'-');
			return AsFormName;
		}
		function getElementToHide(AElemFormName)
		{
			// LINK: http://www.randomsnippets.com/2008/06/26/how-to-find-and-access-parent-nodes-via-javascript/
			var eParent = ((typeof AElemFormName) === 'object') ? AElemFormName : document.forms[AElemFormName];
			var testObj = eParent.parentNode;
			while (testObj != undefined)
			{
				if (testObj.getAttribute("data-elemhide") === 'true')
					return testObj;
				else
					testObj = testObj.parentNode;
			}
			
			return undefined;
		}
		function getIDsList(AFormName,ArrJsonFields,ArrJsonRequired)
		{
			var jsonFields 		= ArrJsonFields;
			var jsonRequired 	= ArrJsonRequired;
			if ((ArrJsonFields == undefined) && (ArrJsonRequired == undefined))
			{
				var jsonData = this.serializeFORM(AFormName);
				jsonFields 	 = ParseJSON(jsonData.fields);
				jsonRequired = ParseJSON(jsonData.required);
			}
			
			var eForm  = document.forms[AFormName];
			var eField = false;
			var Result = new Array();
			var idx = 0;
			for (var key in jsonRequired)
			{
				eField = this.getRadioInputField(eForm.elements[key]);
				
				if (eField.getAttribute("data-isotherid") === "true") 
				{
					Result[idx] = key+'='+jsonFields._POST[key];
					idx++;
				}
				else
				if (eField.getAttribute("data-isid") === "true")
				{
					Result[idx] = 'idrow='+jsonFields._POST[key];
					idx++;
				}
			}
			return Result;
		}
		function getRadioInputField(AeField)
		{
			if (AeField[0] != undefined)
			{
				for(var i=0, ccI=AeField.length; i<ccI; i++)
				{
					if ( (AeField[i].getAttribute("checked") != undefined) ||
						 (AeField[i].getAttribute("selected") != undefined) )
					{
						AeField = AeField[i];
						break;
					}
				}
			}
			return AeField;
		}
		function serializeFORM(AFormName)
		{
			var eForm  = document.forms[AFormName];
			var eElems = eForm.elements;
			var vField = null;
			var isize  = eElems.length;
			
			var jsonRequired 	= {};
			var arrJson 		= {};
				arrJson.FORM 	= AFormName;
				arrJson._POST 	= {};
			
			var sName 		= '';
			var sValue 		= '';
			var sTitle 		= '';
			var bRequired 	= false;
			for (var i=0; i<isize; i++)
			{
				vField = eElems[i];
				
				if ((vField.tagName === "INPUT") || (vField.tagName === "TEXTAREA") || (vField.tagName === "SELECT"))
				{
					sName 		= vField.name;
					bRequired 	= (vField.getAttribute("required") != undefined) ? true : false;
					sValue 		= '';
					sTitle 		= '';
					jsonRequired[sName] = bRequired;
				
					if (vField.tagName === "INPUT")
					{
						switch(vField.type)
						{
							case 'radio': 
								{
									var radios = eElems[sName];
									var eLabel = null;
										sValue = 'false';
									for (var j=0, jlen=radios.length; j<jlen; j++)
									{
										if (radios[j].checked === true)
										{
											sValue = radios[j].value;
											eLabel = GetObjectsByTagNameData('LABEL','for',radios[j].getAttribute('id'));
											sTitle = eLabel.innerHTML;
											break;
										}
									}
								} break;
							default: 
								{
									if ((vField.type === "hidden") && (vField.getAttribute("data-setpost") === "false"))
										sValue = '';
									else
										sValue = Trim(vField.value);
								} break;
						}
					}
					else
					if (vField.tagName === "TEXTAREA")
					{
						sValue = Trim(vField.value);
					}
					else
					if (vField.tagName === "SELECT")
					{
						if (vField.selectedIndex === 0)
						{
							sValue = 0;
						}
						else
						{
							sValue = vField.options[vField.selectedIndex].value;
							sTitle = vField.options[vField.selectedIndex].text;
						}
					}
					if (sName !== '')
					{
						arrJson._POST[sName] = sValue;
						if (sTitle !== '')
							arrJson._POST[sName+'-Title'] = sTitle;
					}
				}
			}
			
			return {'required':JSON.stringify(jsonRequired), 'fields':JSON.stringify(arrJson)};
		}

		function CleanForm(AFormName,AarrResetField,AarrResetTo)
		{
			var eForm 	= document.forms[AFormName];
			var arrElem = eForm.elements;
			var ilen	= arrElem.length;
			var sRadio 	= '';
			var eObj 	= false;
			var ccJ 	= (AarrResetField != undefined) ? AarrResetField.length : 0;
			var bChange = false;
			var sResetValue = '';
			
			for (var i=0; i<ilen; i++)
			{
				eObj 		= arrElem[i];
				bChange 	= false;
				sResetValue = '';
				
				/* PROCURA CAMPO PARA MODIFICAR */
				for(var j=0; j<ccJ; j++)
				{
					if (eObj.name === AarrResetField[j])
					{
						bChange 	= true;
						sResetValue = AarrResetTo[j];
						break;
					}
				}
				
				if (eObj.getAttribute("type") === "radio")
				{
					if (sRadio !== eObj.id)
					{
						sRadio = eObj.id;
						eObj.checked = true;
						var eField = GetObjectsByTagNameData("LABEL","for",eObj.id);
						if (eField.onclick != undefined)
							eField.click();
					}
					else
						eObj.checked = false;
				}
				else
				if ((eObj.tagName === "INPUT") || (eObj.tagName === "TEXTAREA"))
				{
					eObj.value = (bChange !== false) ? sResetValue : '';
				}
				else
				if (eObj.tagName === "SELECT")
					eObj.selectedIndex = (bChange !== false) ? sResetValue : 0;
			}
		}

		function ValidateForm(AFormName,AOperation,fCallback)
		{
			if (fCallback == undefined)
			{
				fCallback = function(sResult)
				{
					console.log(sResult);
					var jsonData = ParseJSON(sResult);
					if (jsonData)
					{
						//var bResult= jsonData.result;
						//var sNewID = (jsonData.idinserted) ? jsonData.idinserted : '';
						
						if (jsonData.result)
						{
							if (Self.onRecordSaved && typeof(Self.onRecordSaved) === "function")
								Self.onRecordSaved(jsonData);
							else
								alert('Registo salvo com sucesso!');
							
							this.onRecordSaved = false;
						}
						else
						{
							if (Self.onRecordSavedError && typeof(Self.onRecordSavedError) === "function")
								Self.onRecordSavedError(jsonData);
							else
								alert('Ocorreu um erro. <br/> Porfavor tente novamente.');
							
							this.onRecordSavedError = false;
						}
					}
					Self.ResetHistory();
				};
			}
			
			AOperation = ((AOperation == undefined) || (AOperation === '')) ? 'update' : AOperation;

			var jsonData 	 = this.serializeFORM(AFormName);
			var jsonFields 	 = ParseJSON(jsonData.fields);
			var jsonRequired = ParseJSON(jsonData.required);
			
			var bValid = true;
			var eForm  = document.forms[AFormName];
			var eField = false;
			
			if (this.onValidateFields && typeof(this.onValidateFields) === "function")
				bValid = this.onValidateFields(eForm,jsonFields,jsonRequired);
			else
			{
				for (var key in jsonRequired)
				{
					if ( (jsonRequired[key] === true) && ((jsonFields._POST[key] === '') || (jsonFields._POST[key] === 'false')) )
					{
						bValid = false;
						
						eField = this.getRadioInputField(eForm.elements[key]);
						eField.focus();
						
						if (this.onNotValid && typeof(this.onNotValid) === "function")
							this.onNotValid(eField);
						else
							alert("O campo '"+eField.getAttribute("placeholder")+"' encontra-se vazio.");
						
						break;
					}
				}
			}

			if (bValid)
			{
				this.onNotValid 		= false;
				
				var extraparams 		= this.getIDsList(AFormName,jsonFields,jsonRequired);
				var objParams = {};
					objParams.section 	= this.extractFormRealName(AFormName);
					objParams.opera 	= AOperation;
					objParams.jsonPOST 	= encode64(jsonData.fields);
				
				this.ToggleRecordAjax(objParams,extraparams,fCallback);
			}
			else
				return false;
		}


		function RemoveRecord(AFormName)
		{
			var func = function(sResult)
			{
				console.log(sResult);
				var jsonData = ParseJSON(sResult);
				if (jsonData)
				{
					var bResult 	= jsonData.result;
					var sIDHistory 	= (jsonData.idhistory) ? jsonData.idhistory : '';

					if (bResult)
					{
						switch(Self.arrHistoryObjects.opera)
						{
							case 'bunk':
								{
									for(var i=0; i<Self.arrHistoryObjects.total; i++)
									{
										AddToClass(Self.arrHistoryObjects.objects[i],'hidden');
									}
									Self.arrHistoryObjects.idhistory = sIDHistory;
									Self.arrHistoryObjects.opera 	 = 'debunk';

									if (Self.onBunk && typeof(Self.onBunk) === "function")
										Self.onBunk(AFormName,jsonData);
									else
										alert('Registo removido com sucesso!');
										
									Self.onBunk = false;
								} break;
							case 'debunk':
								{
									for(var i=0; i<Self.arrHistoryObjects.total; i++)
									{
										RemoveFromClass(Self.arrHistoryObjects.objects[i],'hidden');
									}

									if (Self.onDebunk && typeof(Self.onDebunk) === "function")
										Self.onDebunk(jsonData);
									else
										alert('Registo recuperado com sucesso!');
										
									Self.onDebunk = false;
									Self.ResetHistory();
								} break;
						}
					}
					else
					{
						if (Self.onRemoveError && typeof(Self.onRemoveError) === "function")
							Self.onRemoveError(jsonData);
						else
							alert('Ocorreu um erro.\n\nPorfavor tente novamente.');
							
						Self.onRemoveError = false;
						Self.ResetHistory();
					}
				}
			};

			var jsonData 	 = this.serializeFORM(AFormName);
			var jsonFields 	 = ParseJSON(jsonData.fields);
			var jsonRequired = ParseJSON(jsonData.required);
			
			var eForm  = document.forms[AFormName];
			var eField = false;
			for (var key in jsonRequired)
			{
				eField = this.getRadioInputField(eForm.elements[key]);

				if (eField.getAttribute("data-isid") === "true")
				{
					this.addInfoRecordToList(jsonFields._POST[key],jsonFields._POST['Nome']);
				}
			}
			
			if (this.arrHistoryObjects.total === 0)
			{
				if (this.eObjectToHide === false)
				{
					this.eObjectToHide = this.getElementToHide(AFormName);
				}
				var extraparams = this.getIDsList(AFormName,jsonFields,jsonRequired);
				
				this.arrHistoryObjects.extraparams 	= extraparams.join("&");
				this.arrHistoryObjects.opera 		= 'bunk';
				this.arrHistoryObjects.encodedJSON 	= encode64(this.getRecordsJSON(extraparams));
				this.arrHistoryObjects.section 		= extractFormRealName(AFormName);
				this.arrHistoryObjects.objects 		= {};
				this.arrHistoryObjects.objects['0'] = this.eObjectToHide;
				this.arrHistoryObjects.total 		= 1;
			}
			
			var sIDHist   = this.arrHistoryObjects.idhistory;
			var objParams = {};
				objParams.opera 	= this.arrHistoryObjects.opera;
				objParams.section 	= this.arrHistoryObjects.section;
				objParams.jsonPOST 	= this.arrHistoryObjects.encodedJSON;
				objParams.idhistory = (sIDHist != undefined) ? sIDHist : '';
			
			this.ToggleRecordAjax(objParams,this.arrHistoryObjects.extraparams,func);
		}

		function ToggleRecordAjax(AobjPars,AsExtraParams,fCallback)
		{
			var url 	= '/core/scripts/ajax-toggle-record.php';
			var params 	= '';
			for(var key in AobjPars)
			{ 
				if (AobjPars.hasOwnProperty(key))
				{
					params 	+= '&'+key+'='+AobjPars[key];
				}
			}
			if (AsExtraParams[0] !== '&')
				AsExtraParams = '&'+AsExtraParams;
			params += AsExtraParams;
			
			ObjAjax.Ajax({type:'POST', url:url, data:params, done:fCallback});
		}
	}