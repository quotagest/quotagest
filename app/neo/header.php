<!DOCTYPE html>
<!--[if lt IE 7]	  <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
	<!--[if IE 7]>  	 <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
	<!--[if IE 8]>  	 <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>QuotaGest NeoUI</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- Folhas de Estilo CSS -->
		<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/neo-ui.css">
		<link rel="stylesheet" href="css/type/webicons/foundation-icons.css">
		
		<!-- CSS Paulo -->
		<link rel="stylesheet" href="css/paulo-magic.css">
		
		<!-- Google Web Fonts "OpenSans" -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet' type='text/css'>


				<!-- Script do Modernizer -->
		<script src="js/vendor/modernizr-2.6.2.min.js"></script>
		
		<script src="js/vendor/jquery-1.10.2.min.js"></script>
		<script src="../js/js.min.js"></script>
		<script src="../js/upload_images.js"></script>
		<script src="js/TObjListagem.js"></script>
		<script src="js/ajax.js"></script>
		<script src="js/TObjFormsValidation.class.js"></script>
		<script src="js/main.js"></script>
		<!-- FICHEIROS PARA "clipboard" -->
		<!--
		<script src="js/ZeroClipboard/ZeroClipboard.min.js"></script>
		<script src="js/ZeroClipboard/ZeroClipboardAPI.js"></script>
		-->
		<!--
		<script type="text/javascript" src="plugins/scrollbar/perfect-scrollbar-0.4.5.min.js"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		-->
	</head>
	<body id="layout">
	<!--[if lt IE 9]>
		<p class="chromeframe">Está a utilizar um <strong>navegador desatualizado</strong>. 
		Por favor <a href="http://browsehappy.com/">actualize o seu browser</a> 
		para melhorar a sua segurança e experiência.</p>
	<![endif]-->
	
	<nav id="nav">
		<div class="sidebar">
			<input id="global-search" type="text" placeholder="pesquisar...">
			<div class="menu menu-principal">
				<ul>
					<li><a {$caSocios} 			href="?op=socios">Sócios</a></li>
					<li><a {$caQuotas} 			href="?op=quotas">Quotas <span class="aviso">(92)</span></a></li>
					<li><a {$caModalidades} 	href="#">Modalidades</a></li>
					<li><a {$caEntidades} 		href="?op=entidades">Entidades</a></li>
					<li><a {$caHistorico} 		href="?op=historico">Histórico</a></li>
					<li><a {$caConfiguracoes} 	href="?op=configs">Configurações</a></li>
				</ul>
			</div>
		</div>
	</nav>
		