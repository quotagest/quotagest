<?php
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');
	
	$Page 			= (isset($_GET['page'])) ? $_GET['page'] : '1';
	$Page 			= ($Page != '') ? $Page : '1';
	$ID_HISTORY 	= (isset($_GET['idh'])) ? $_GET['idh'] : '';
	$ID_HISTORY 	= ($ID_HISTORY != '') ? $ID_HISTORY : false;
	$INPUT_PAGE 	= $Page;
	$INPUT_PAGENEXT = $Page;
	$EXISTList 		= false;
	$TPages 		= $Page;

	if ($ID_HISTORY)
	{
		$main_include = processPage(SETPATH('ROOT','PATH_APP').'neo/ui/historico/perfil-edit.php');
	}
	else
	{
		$main_include = processPage(SETPATH('ROOT','PATH_APP').'neo/ui/historico/novo.php');
	}

	$InsertTitle	= "Adicionar Sócio";
	$InsertOnClick  = "ObjListagem.LoadContent(this,'', 'Ficha de Sócio', '/neo/?op=historico');";
	$ViewOnClick 	= "ObjListagem.LoadContent(this,'', 'Ficha de Sócio', '/neo/?op=historico&ids={field:id_history}&page=".$INPUT_PAGE."');";
	$SearchList 	= "ObjListagem.LoadMore(this,'list-search');";
	$SearchFilter 	= "ObjListagem.LoadMore(document.getElementsByName('searchlist')[0],'list-search');";
	
	$Options = array();
	$Options['nome;ASC'] 	= 'Nome ASC';
	$Options['nome;DESC'] 	= 'Nome DESC';
	$OptionsFilter = buildOptionsFromArray($Options);
	
	################################################
	############### CLASS - TEMPLATE ###############
	$ATPLFileName = SETPATH('ROOT','PATH_APP').'/neo/main-nav.html';

	$objTemp = new TobjTemplate($ATPLFileName);
	$objTemp->setVar('{$INPUT_PAGE}', 		$INPUT_PAGE);
	$objTemp->setVar('{$INPUT_PAGENEXT}',	$INPUT_PAGENEXT);
	$objTemp->setVar('{$InsertOnClick}', 	$InsertOnClick);
	$objTemp->setVar('{$InsertTitle}', 		$InsertTitle);
	$objTemp->setVar('{$ViewOnClick}', 		$ViewOnClick);
	$objTemp->setVar('{$SearchList}', 		$SearchList);
	$objTemp->setVar('{$OptionsFilter}', 	$OptionsFilter);
	$objTemp->setVar('{$SearchFilter}', 	$SearchFilter);
	$objTemp->setVar('{$main_include}', 	$main_include);
	$tplHTML = $objTemp->echohtml();
	unset($objTemp);
	################################################

	echo $tplHTML;
?>