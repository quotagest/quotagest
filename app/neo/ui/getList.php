<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	
	/* *********************************************************************** */
	/* *********************************************************************** */
	function getListSearchResults($ASubAction,$ATable,$ASearchPOST, $AarrFieldsSearch,$AarrSearchText, $AFilterField,$AFilterOrder, $APageNext)
	{
		$Result = false;
		switch($ASubAction)
		{
			case 'list' :
			{
				$Result = $ATable->getListSearch(array(),array(), $AFilterField,$AFilterOrder, $APageNext);
			} break;
			case 'list-search':
			{
				if (isset($ASearchPOST))
				{
					$Result = $ATable->getListSearch($AarrFieldsSearch,$AarrSearchText, $AFilterField,$AFilterOrder);
				}
			} break;
		}
		if ($Result)
		{
			$resNext = ($Result['TotalPages'] >= ($APageNext++)) ? true : false;
			$Result['resNext'] = ($ASubAction === 'list') ? $resNext : false;
		}
		return $Result;
	}
	/* *********************************************************************** */
	/* *********************************************************************** */
	
	$ROW 		= array();
	$EXIST 		= false;
	$Result 	= false;
	$TPage 		= 0;
	$resNext 	= false;
	$AQuery 	= (isset($_POST['q'])) ? $_POST['q'] : false;

	if ( (isset($_POST['action'])) && (isset($_POST['pagenext'])) && (isset($_POST['subaction'])))
	{
		$Action 	= $_POST['action'];
		$SubAction 	= $_POST['subaction'];
		$PageNext 	= $_POST['pagenext'];
		
		$filterField = '';
		$filterOrder = 'ASC';
		if (isset($_POST['filter']))
		{
			$Filter		 = explode(';',$_POST['filter']);
			$filterField = $Filter[0];
			$filterOrder = $Filter[1];
		}

		switch($Action)
		{
			case 'socios':
				{
					require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocios.class.php');

					$dbSocios = new dbTblSocios();
					$dbSocios->set_IDSOCIO(0);
					$dbSocios->set_IDMODALIDADE(0);
					$dbSocios->set_IDUSER($id_user);
					$dbSocios->set_IDASSOC($id_assoc);
					$Result = getListSearchResults($SubAction,$dbSocios,$AQuery, array('socio.nome','socio.bicc','socio.nif','socio.email','socio.telefone','socio.telemovel','assoc_socios.codigo','socio_tipo.nome','socio_estado.nome'),array($AQuery), $filterField,$filterOrder, $PageNext);
					unset($dbSocios);
					
					if ($Result)
					{
						$ROW 	= $Result['ROW'];
						$EXIST 	= $Result['EXIST'];
						$TPages = $Result['TotalPages'];
						$resNext= $Result['resNext'];
						#$resNext= ($PageNext <= $TPages) ? $resNext : $TPages;
	
						if ($EXIST)
						{
							/*
							$arrJson = array();
							$ccI = count($ROW);
							for ($i=0; $i<$ccI; $i++)
							{
								$arrJson[$i] = array();
								$arrJson[$i]['id_socio'] 		= $ROW[$i]['id_socio'];
								$arrJson[$i]['nome'] 			= $ROW[$i]['nome'];
								$arrJson[$i]['codigo'] 			= $ROW[$i]['codigo'];
								$arrJson[$i]['socio_nome_tipo'] = $ROW[$i]['socio_nome_tipo'];
								$arrJson[$i]['divida'] 			= $ROW[$i]['divida'];
								$arrJson[$i]['sexo'] 			= $ROW[$i]['sexo'];
							}
							*/
							#$AjaxResult = json_encode($ROW);
							#$AjaxResult = 'true#'.$TPages.'#'.$resNext.base64_encode($AjaxResult);
						}
					}
				} break;

			case 'entidades':
				{
					require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblEntidades.class.php');

					$dbEntidades = new dbTblEntidades();
					$dbEntidades->set_IDENTIDADE(0);
					$dbEntidades->set_IDUSER($id_user);
					$dbEntidades->set_IDASSOC($id_assoc);
					$Result = getListSearchResults($SubAction,$dbEntidades,$AQuery, array('entidades.nome','entidades.abreviatura','entidades.nif','entidades.email','entidades.telefone','entidades.telemovel','user_entidades.tipo'),array($AQuery), $filterField,$filterOrder, $PageNext);
					unset($dbEntidades);
					
					if ($Result)
					{
						$ROW 	= $Result['ROW'];
						$EXIST 	= $Result['EXIST'];
						$TPages = $Result['TotalPages'];
						$resNext= $Result['resNext'];
	
						if ($EXIST)
						{
							/*
							$arrJson = array();
							$ccI = count($ROW);
							for ($i=0; $i<$ccI; $i++)
							{
								$arrJson[$i] = array();
								$arrJson[$i]['id_entidade'] = $ROW[$i]['id_entidade'];
								$arrJson[$i]['nome'] 		= $ROW[$i]['nome'];
								$arrJson[$i]['abreviatura'] = $ROW[$i]['abreviatura'];
								$arrJson[$i]['tipo'] 		= $ROW[$i]['tipo'];
							}
							*/
							#$AjaxResult = json_encode($ROW);
							#$AjaxResult = 'true#'.$TPages.'#'.$resNext.base64_encode($AjaxResult);
						}
					}
				} break;
			case 'socios-quotas':
				{
					require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblQuotas.class.php');
				
					$GET_ANO 	= (isset($_GET['ano'])) ? $_GET['ano'] : '';
					$GET_IDM 	= (isset($_GET['idm'])) ? $_GET['idm'] : '';
					$GET_IDS 	= (isset($_GET['ids'])) ? $_GET['ids'] : '';
					$GET_PAGE 	= (isset($_GET['page'])) ? $_GET['page'] : '0';
				
					if (($GET_IDM != '') && ($GET_IDS != ''))
					{
						$dbQuotas = new dbTblQuotas();
						$dbQuotas->set_IDQUOTA('');
						$dbQuotas->set_IDMODALIDADE($GET_IDM);
						$dbQuotas->set_IDSOCIO($GET_IDS);
						$dbQuotas->set_IDUSER($id_user);
						$dbQuotas->set_IDASSOC($id_assoc);
						$Result = getListSearchResults($SubAction,$dbQuotas,$AQuery, array('quotas_socios.ano'),array($AQuery), $filterField,$filterOrder, $PageNext);
						#$Result = $dbQuotas->getListSearch(array('quotas_socios'),array($ANO), 'ano','ASC', $GET_PAGE);
						unset($dbQuotas);

						if ($Result)
						{
							$ROW 	= $Result['ROW'];
							$EXIST 	= $Result['EXIST'];
							$TPages = $Result['TotalPages'];
							$resNext= $Result['resNext'];
							
							if ($EXIST)
							{
								#$AjaxResult = json_encode($ROW);
								#$AjaxResult = 'true#'.$TPages.'#'.$resNext.base64_encode($AjaxResult);
							}
						}
					}
				} break;
			case 'historico':
				{
					require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblActionHistory.class.php');

					$dbActHistory = new dbTblActionHistory();
					$dbActHistory->set_IDHISTORY(0);
					$dbActHistory->set_IDUSER($id_user);
					$dbActHistory->set_IDASSOC($id_assoc);
					$Result = getListSearchResults($SubAction,$dbActHistory,$AQuery, array('action_history.section','action_history.titulo','action_history.descricao','action_history.action'),array($AQuery), $filterField,$filterOrder, $PageNext);
					unset($dbActHistory);

					if ($Result)
					{
						$ROW 	= $Result['ROW'];
						$EXIST 	= $Result['EXIST'];
						$TPages = $Result['TotalPages'];
						$resNext= $Result['resNext'];
	
						if ($EXIST)
						{
							$arrJson = array();
							$ccI = count($ROW);
							for ($i=0; $i<$ccI; $i++)
							{
								$arrJson[$i] = array();
								$arrJson[$i]['id_history'] 	= $ROW[$i]['id_history'];
								$arrJson[$i]['titulo'] 		= $ROW[$i]['titulo'];
								$arrJson[$i]['descricao'] 	= $ROW[$i]['descricao'];
								$arrJson[$i]['action'] 		= $ROW[$i]['action'];
								$arrJson[$i]['data_criacao']= $ROW[$i]['data_criacao'];
							}
							#$AjaxResult = json_encode($arrJson);
							#$AjaxResult = 'true#'.$TPages.'#'.$resNext.base64_encode($AjaxResult);
						}
					}
				} break;
			default:{
						
					} break;
		}
	}

	$ResultJSON = array('result'=>$EXIST, 'totalpages'=>$TPages, 'nextpage'=>$resNext, 'jsondata'=>json_encode($ROW));
	echo json_encode($ResultJSON);
?>