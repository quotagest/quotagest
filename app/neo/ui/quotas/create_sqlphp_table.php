<?php
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	
	$tableName 	= 'socio_tipo';
	$tbl2ID 	= strtoupper(str_ireplace('_','',$tableName));
	$tblClass 	= str_ireplace('_',' ',$tableName);
	$tblClass 	= str_ireplace(' ','',ucwords($tblClass));
	
	$tbl1 = new dbTable();
	$SQL = "DESCRIBE ".$tableName;
	$ROW = $tbl1->getRESULTS($SQL);
	
	$arrFieldsName = array();
	$arrFieldsType = array();
	$arrFieldsRequ = array();
	
	$isize = count($ROW);
	for($i=0; $i<$isize; $i++)
	{
		$arrFieldsName[] = $ROW[$i]['Field'];
		
		$ipos = stripos($ROW[$i]['Type'],'(');
		if ($ipos != false)
		{
			$fieldType = substr($ROW[$i]['Type'], 0, stripos($ROW[$i]['Type'],'('));
		}
		else
			$fieldType = $ROW[$i]['Type'];

		switch($fieldType)
		{
			case 'int':
			case 'tinyint':
				{
					$arrFieldsType[] = "INTEGER";
				} break;
			case 'varchar':
			case 'longtext':
				{
					$arrFieldsType[] = "STRING";
				} break;
			case 'datetime':
				{
					$arrFieldsType[] = "DATETIME";
				} break;
		}
		
		## REQUIRED
		switch($ROW[$i]['Null'])
		{
			case 'YES':
				{
					$arrFieldsRequ[] = "false";
				} break;
			case 'NO':
				{
					$arrFieldsRequ[] = "true";
				} break;
		}
	}

	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	
	$ATPLFileName = SETPATH('ROOT','PATH_APP').'neo/ui/quotas/template.php.html';
	
	$objTemp = new TobjTemplate($ATPLFileName);
	$objTemp->setVar('{$fieldsName}', 		"'".implode("','",$arrFieldsName)."'" );
	$objTemp->setVar('{$fieldsType}', 		"'".implode("','",$arrFieldsType)."'" );
	$objTemp->setVar('{$fieldsRequired}', 	implode("','",$arrFieldsRequ));
	$objTemp->setVar('{$ClassName}', 		$tblClass);
	$objTemp->setVar('{$IDMain}', 			$tbl2ID);
	$objTemp->setVar('{$TableName}', 		$tableName);
	$tplHTML = $objTemp->echohtml();
	unset($objTemp);
	
	echo '<pre>'.$tplHTML.'</pre>';
?>