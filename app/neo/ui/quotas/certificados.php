<?php
	# http://www.php.net/manual/pt_BR/book.openssl.php
	
	if (isset($_SERVER['HTTPS']) )
	{
	echo "SECURE: This page is being accessed through a secure connection.<br><br>";
	}
	else
	{
	echo "UNSECURE: This page is being access through an unsecure connection.<br><br>";
	}
	
	// Create the keypair
	$config = array(
	    "digest_alg" => "sha1", /* sha512*/
	    "private_key_bits" => 1024,
	    "private_key_type" => OPENSSL_KEYTYPE_RSA,
	    'x509_extensions' => 'v3_ca'
	);
	$res=openssl_pkey_new($config);
	
	// Get private key
	openssl_pkey_export($res, $privatekey);
	echo "Private Key:<BR>$privatekey<br><br>";
	
	
	// Get public key
	$publickey=openssl_pkey_get_details($res);
	$publickey=$publickey["key"];
	
	echo "Public Key:<BR>$publickey<BR><BR>";
	
	
	$cleartext = '2010-05-18;2010-05-18T11:22:19;FAC 001/14;3.12;';
	echo "Clear text:<br>$cleartext<BR><BR>";
	
	openssl_public_encrypt($cleartext, $crypttext, $publickey);
	
	echo strlen($crypttext)."<br>";
	echo "Crypt text:<br>$crypttext<BR><BR>";
	
	$base64_data = base64_encode($crypttext);
	echo strlen($base64_data)."<br>";
	echo "Base64 text:<br>$base64_data<BR><BR>";
	
	
	openssl_private_decrypt($crypttext, $decrypted, $privatekey);
	echo strlen($decrypted)."<br>";
	echo "Decrypted text:<BR>$decrypted<br><br>";
	
	echo '<br/>';	
	echo '<br/>';
	
	/*
		$pkeyid = openssl_get_privatekey($priv_key);
		openssl_sign($assinar,$signature,$pkeyid,"sha1");
		return base64_encode($signature);
	*/
	# http://www.portugal-a-programar.pt/topic/58028-saft-openssl/page__hl__openssl
	# http://www.portugal-a-programar.pt/topic/61541-at-questoes-legais/page__hl__openssl
	# http://www.portugal-a-programar.pt/topic/61541-at-questoes-legais/page__st__20#entry517850
	# http://www.portugal-a-programar.pt/topic/57734-utilizar-webservices-da-at/page__st__760__hl__openssl#entry503022
	
	# GETs PUBLIC KEY object, from PRIVATE KEY
	$pubkey = openssl_get_privatekey($privatekey);
	# CREATES signature
	openssl_sign($cleartext, $signature, $pubkey, "sha1");
	# GETs PUBLIC KEYID object from PUBLIC KEY
	$pubkeyid = openssl_get_publickey($publickey);
	
	# http://www.portugal-a-programar.pt/topic/63288-at-saft-facturacao-php/#entry532920
	$sig64 = base64_encode($signature);
	echo strlen($sig64).'<br>';
	echo $sig64.'<br>';
	# CHECK IF data is VALID
	$Result = openssl_verify($cleartext, $signature, $pubkeyid,"sha1");
	switch($Result)
	{
		case 0: 
			{
				echo "ERROR: bad (there's something wrong)\n";
			} break;
		case 1: 
			{
				echo "SUCESS: signature ok (as it should be)\n";
			} break;
		default:
			{
				echo "ERROR: ugly, error checking signature\n";
			} break;
	}
	
	echo '<br>----------------------------<br><br>';
	
	$dn = array("countryName" => 'XX', "stateOrProvinceName" => 'State', "localityName" => 'SomewhereCity', "organizationName" => 'MySelf', "organizationalUnitName" => 'Whatever', "commonName" => 'mySelf', "emailAddress" => 'user@domain.com');
	$privkeypass = '1234';
	$numberofdays = 365;
	
	$privkey = openssl_pkey_new($config);
	$csr = openssl_csr_new($dn, $privkey);
	$sscert = openssl_csr_sign($csr, null, $privkey, $numberofdays);
	openssl_x509_export($sscert, $publickey);
	openssl_pkey_export($privkey, $privatekey, $privkeypass);
	openssl_csr_export($csr, $csrStr);
	
	echo $privatekey.'<br><br>'; // Will hold the exported PriKey
	echo $publickey.'<br><br>';  // Will hold the exported PubKey
	echo $csrStr.'<br><br>';     // Will hold the exported Certificate
?>