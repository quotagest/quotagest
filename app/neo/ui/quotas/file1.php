<?php
	/*
		http://www.devmedia.com.br/utilizando-a-dupla-mongodb-e-php/27798
		http://docs.mongodb.org/manual/reference/operator/
		http://docs.mongodb.org/manual/reference/command/
		http://www.php.net/manual/en/class.mongoregex.php
		http://www.php.net/manual/en/class.mongodb.php

	*/

	error_reporting(E_ALL|E_STRICT);
	ini_set('display_errors', 1);

	class TObjMongoAPI
	{
		const DATABASE = 'mongodb';

		var $mongoObj 	= false;
		var $database 	= false;
		var $table 		= false;

		public function __construct($AobjMongo=false, $AsTblName='')
		{
			if ($AobjMongo == false)
			{
				$this->startMongoDB($this::DATABASE);
			}
			else
			{
				$this->mongoObj = $AobjMongo;
				$this->database = $this->mongoObj->selectDB($this::DATABASE);
			}
			$this->table = $this->selectTable($AsTblName);
		}
		public function __destruct()
		{
			$this->closeMongoDB();
			unset($this->mongoObj);
			unset($this->database);
			unset($this->table);
		}

		private function startMongoDB($AsDBName)
		{
			$this->mongoObj = new Mongo();
			$this->database = $this->mongoObj->selectDB($AsDBName);
		}
		private function selectTable($AsTblName)
		{
			return $this->mongoObj->selectCollection($AsTblName);
		}
		private function closeMongoDB()
		{
			$this->mongoObj->close();
		}
		
		/* 
		* FUNCTIONS GERAIS
		*/
		public function getObjectId($AsID)
		{
			return new MongoId($AsID);
		}
	}
	$objMongo = new TObjMongoAPI(false,'teste');


	#$arrServer = startMongoDB("mongodb");
	#$mongo 	= $arrServer['mongodb'];
	#$db 	= $arrServer['db'];

	#$tbl = $db->selectCollection('teste');
	#$tbl->insert(array('name'=>'João Martins', 'age'=>'24'));
	#var_dump($db);

	echo '<br/>'.$objMongo::table->count().'<br/>';
	echo '<br/>';
/*
	#$rows = $tbl->find(array(), array('_id'=>false)); #listar normal
	$rows = $tbl->find(); #listar normal
	#$rows = $tbl->find()->sort(array('_id' => -1)); #listar DESC
	#$rows = $tbl->find(array('name' => 'Paulo'));	#listar com pesquisa por campo
	#$rows = $tbl->find(array('name' => array('$all' => array(new MongoRegex('/mar/i')))));
	echo '<br/>'.$rows->count().'<br/>';
	foreach ($rows as $obj)
	{
		$nID = getObjectId($obj['_id']);
		echo '<br>'.utf8_decode(json_encode($obj, JSON_UNESCAPED_UNICODE ));
		echo '<br>'.$nID->getTimestamp().' - '.date('Y-m-d H:i:s',$nID->getTimestamp());
		echo "<br/>";
	}

	#ALTERANDO REGISTO por ID
	$dbid = '5318f3e6ccf0f68516000003';
	$tbl->update(array('_id'=> getObjectId($dbid)), array('$set' => array("name"=>"João Martins")));

	$dbid = '5318f3f5ccf0f68616000002';
	$tbl->remove(array('_id' => getObjectId($dbid)));

	$mongo->close();
*/
?>