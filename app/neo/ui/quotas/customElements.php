<!--
	http://coding.smashingmagazine.com/2014/03/04/introduction-to-custom-elements/
	http://www.html5rocks.com/en/tutorials/webcomponents/customelements/
	
-->
<script>

	var XFooProto = Object.create(HTMLElement.prototype);
	XFooProto.innitiate = function(AElement)
	{
		switch(AElement.getAttribute("type"))
		{
			case '1':
				{
					AElement.insertAdjacentHTML('beforeend'," - Sou do tipo '1'!");
				} break;
			case '2':
				{
					AElement.insertAdjacentHTML('beforeend'," - Sou do tipo '2'!");
				} break;
			case '3':
				{
					AElement.insertAdjacentHTML('beforeend'," - Faço um AjaxRequest!");
				} break;
			default:
				{
					AElement.insertAdjacentHTML('beforeend'," - Sou do tipo 'Default'!");
				} break;
		}
	};
	// An instance of the element is created
	XFooProto.createdCallback = function()
	{
		//this.xwidth = '200px';
		XFooProto.innitiate(this);
		this.addEventListener('click', function(e)
			{
				if (e.target.getAttribute("type") === "3")
				{
					var url = "/neo/ui/socios/index.php";
					var fCallback = function(sResult)
					{
						console.log(sResult);
						var eID = document.getElementById("results");
						eID.innerHTML = sResult;
					};
					ObjAjax.Ajax({type:'POST', url:url, data:"", done:fCallback});
				}
				console.log('Event: click'+
					"\nHTMLElement: "+e.target.tagName+
					"\n"+e.target.outerHTML+
					"\nAttInHTML: "+e.target.getAttribute("xwidth"));
			});
	};
	// An instance was inserted into the document
	XFooProto.attachedCallback = function()
	{
		console.log('attachedCallback: '+this);
	};
	// An instance was removed from the document
	XFooProto.detachedCallback = function()
	{
		console.log('detachedCallback: '+this);
	};
	// An attribute was added, removed, or updated
	XFooProto.attributeChangedCallback  = function(attrName, oldVal, newVal)
	{
		console.log('attributeChangedCallback: '+attrName+', '+oldVal+', '+newVal);
	};
	console.log(XFooProto);
	var XFoo = document.registerElement('x-foo', {prototype: XFooProto});
	
</script>


<x-foo type="1" xwidth="100px">X-FOO</x-foo>
<br/>
<x-foo type="2" xwidth="200px">X-FOO</x-foo>
<br/>
<x-foo type="3" >X-FOO</x-foo>
<br/>
<span id="results"></span>

<script>

</script>
