<?php
	# http://www.binpress.com/app/php-paypal-api-class/20
	# https://developer.paypal.com/docs/classic/training/
	
	# http://code.tutsplus.com/tutorials/how-to-process-credit-cards-with-paypal-payments-pro-using-php--net-25397
	
	#https://developer.paypal.com/docs/classic/lifecycle/sb_credentials/
	#https://developer.paypal.com/webapps/developer/docs/api/#authentication--headers
	
	# http://jdmweb.com/how-to-easily-integrate-a-paypal-checkout-with-php

	class TobjPaypalAPI
	{
		const business 		= 'paulomota2@gmail.com';	#paypal email address
		const currency 		= 'EUR';					#paypal currency
		const cursymbol 	= '&euro;';					#currency symbol
		#const cursymbol 	= '€';					#currency symbol
		const location 		= 'PT';						#location code (ex GB)
		const returnurl 	= 'http://app.quotagest.com/neo/?op=quotas&flag_return=1';  #where to go back, when the transaction is done.
		const returntxt 	= 'Return to QuotaGest'; 									#What is written on the return button in paypal
		const cancelurl 	= 'http://app.quotagest.com/neo/?op=quotas&flag_cancel=1';	#Where to go if the user cancels.
		var $shipping 		= 0;						#Shipping Cost
		var $custom 		= '';						#Custom attribute
		var $items 			= array();
		
		public function __construct()
		{
		
		}
		public function __destruct()
		{
			unset($this->items);
		}
		
		//=========================================//
		//==> Add an array of items to the cart <==//
		//=========================================//
		# Lopp through the items, and add them 1 by 1
		public function addMultipleItems($items)
		{
			if (!empty($items))
			{
				foreach ($items as $item)
				{
					$this->addSimpleItem($item);
				}
			}
		}
		 
		//=====================================//
		//==> Add a simple item to the cart <==//
		//=====================================//
		# Check the quantity and the name,
		# and add the item to the cart if it is correct
		public function addSimpleItem($item)
		{
			if ( (!empty($item['quantity'])) && 
				(is_numeric($item['quantity'])) && 
				($item['quantity'] > 0) && 
				(!empty($item['name'])) )
			{
				$this->items[] = $item;
			}
		}
		
		public function StringToFloat($sValue)
		{
			return ($this::cursymbol.' '.$sValue);
		}
		public function getCartContentAsHtml()
		{
			$tPrice = 0.0;
			$tItems = 0;
			$htmlLI = array();
			
			foreach($this->items as $item)
			{
				$total_price = $item['quantity']*$item['price'];
				$tPrice += $total_price;
				$tItems += $item['quantity'];
				
				$htmlLI[] = '<li>"'.$item['name'].'" '.$item['quantity'].' x '.$this->StringToFloat($item['price']).' = '.$this->StringToFloat($total_price).'</li>';
			}
			$htmlLI[] = '<li>Total: '.$tItems.' Items por '.$this->StringToFloat($tPrice).'</li>';
			$html = '<ul>'.implode("\n",$htmlLI).'</ul>';
			
			return $html;
		}
	}
	
?>