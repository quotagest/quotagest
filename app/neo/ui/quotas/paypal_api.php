<?php

	$items = array(
				1 => array(
					"name" 		=> "Gold Tickets",
					"price" 	=> 30,
					"quantity" 	=> 4,
					"shipping" 	=> 0
				),
				2 => array(
					"name" 		=> "Silver Tickets",
					"price" 	=> 20,
					"quantity" 	=> 2,
					"shipping" 	=> 0
				),
				3 => array(
					"name" 		=> "Bronze Tickets",
					"price" 	=> 15,
					"quantity" 	=> 2,
					"shipping" 	=> 0
				)
	);
	
	include("TObjPaypalAPI.class.php"); 	#Include the class
	$objPaypal = new TObjPaypalAPI(); 		#Create an instance of the class
	$objPaypal->addMultipleItems($items); 	#Add all the items to the cart in one go

	var_dump($objPaypal);
	echo $objPaypal->getCartContentAsHtml();
	
?>

<form id = "paypal_checkout" action = "https://www.paypal.com/cgi-bin/webscr" method = "post">
    <input name = "cmd" value = "_cart" />
    <input name = "upload" value = "1" />
    <input name = "no_note" value = "0" />
    <input name = "bn" value = "PP-BuyNowBF" />
    <input name = "tax" value = "0" />
    <input name = "rm" value = "2" />
<br/>
<br/>
    <input name = "business" value = "jeremy@jdmweb.com" />
    <input name = "handling_cart" value = "0" />
    <input name = "currency_code" value = "GBP" />
    <input name = "lc" value = "GB" />
    <input name = "return" value = "http://mysite/myreturnpage" />
    <input name = "cbt" value = "Return to My Site" />
    <input name = "cancel_return" value = "http://mysite/mycancelpage" />
    <input name = "custom" value = "" />
<br/>
<br/>
    <div id = "item_1" class = "itemwrap">
        <span>item_name_1</span> | 
        <span>quantity_1</span> | 
        <span>amount_1</span> | 
        <span>shipping_1</span>
    </div>
    <div id = "item_1" class = "itemwrap">
        <input name = "item_name_1" value = "Gold Tickets" />
        <input name = "quantity_1" value = "4" />
        <input name = "amount_1" value = "30" />
        <input name = "shipping_1" value = "0" />
    </div>
    <div id = "item_2" class = "itemwrap">
        <input name = "item_name_2" value = "Silver Tickets" />
        <input name = "quantity_2" value = "2" />
        <input name = "amount_2" value = "20" />
        <input name = "shipping_2" value = "0" />
    </div>
    <div id = "item_3" class = "itemwrap">
        <input name = "item_name_3" value = "Bronze Tickets" />
        <input name = "quantity_3" value = "2" />
        <input name = "amount_3" value = "15" />
        <input name = "shipping_3" value = "0" />
    </div>
 
    <input id = "ppcheckoutbtn" value = "Checkout" class = "button" type = "submit">
</form>