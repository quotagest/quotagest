<?php 
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	$GET_IDS 		= (isset($_GET['ids'])) ? $_GET['ids'] : '';
	$GET_IDM 		= (isset($_GET['idm'])) ? $_GET['idm'] : '';
	$GET_TAB 		= (isset($_GET['tab'])) ? $_GET['tab'] : '';
	$GET_ANO 		= (isset($_GET['ano'])) ? $_GET['ano'] : '';
	$id_user 		= (isset($id_user))  ? $id_user  : $_SESSION['id_user'];
	$id_assoc 		= (isset($id_assoc)) ? $id_assoc : $_SESSION['id_assoc'];
	#$URL 	 		= ($GET_IDS !== '') ? "?ids=".$GET_IDS : '';
	#$URL 	 	   .= ($GET_IDM !== '') ? "&idm=".$GET_IDM : '';
	#$URL 	 	   .= ($GET_ANO !== '') ? "&ano=".$GET_ANO : '';
	#$URL 	 	   .= ($GET_TAB !== '') ? "&tab=".$GET_TAB : '';
	$ADBConnection 	= (isset($ADBConnection)) ? $ADBConnection : false;
	$ATPLFileName 	= SETPATH('ROOT','PATH_APP').'/neo/ui/socios/perfil-edit.html';
	$outputHTML 	= '';

	if ($GET_IDS != '')
	{
		require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlSocios.class.php');
		$TplMdlSocios = new tplmdlSocios($ADBConnection);
		$TplMdlSocios->setIDValues($GET_IDS,$GET_IDM,$id_user,$id_assoc);
		$ADBConnection = $TplMdlSocios->tblSocios->getDBConnection();
		
		############################## GUARDAR REGISTO ##############################
		#$TplMdlSocios->UPDATE_SOCIO($_POST,$_GET);
		############################################################################

		$Result = $TplMdlSocios->getPerfilEditSocio();
		$ROW 	= $Result['ROW'];
		$EXIST 	= $Result['EXIST'];
		$jsondata = $Result['JSON'];

		$objTemp = new TobjTemplate($ATPLFileName);
		$objTemp->setVar('{$titulo}', 	'Perfil de Sócio');
		#$objTemp->setVar('{$ajuda}', 	'Edite as informações básicas do perfil do Sócio.');

		if ($EXIST)
		{
			#############################################################################
			############################ MENU QUOTAS EM FALTA ###########################
			$tpl_socio_quotas = processPage(SETPATH('ROOT','PATH_APP').'/neo/ui/socios/quotas-lista.php');
			#############################################################################
			$HTML_SELECT_ESTADO = $TplMdlSocios->buildSELECT_SocioEstado($ROW[0]['id_estado'],$id_assoc);
			$HTML_SELECT_TIPO 	= $TplMdlSocios->buildSELECT_SocioTipo($ROW[0]['id_tipo'],$id_assoc);

			$JSScript = array();
			$JSScript[] = "<script>EnableForm('socio-perfil-edit');</script>";
			$JSScript[] = "<script>INI_FILEUPLOAD('upload-image','divUploadImage','spanProgressBar');</script>";
			$JSScript[] = "<script>ObjTabs.setObjData('socios');</script>";
			#$JSScript[] = "<script>ObjTouchSocioControlo.setDragDropElements();</script>";
			if ($GET_TAB === '1')
				$JSScript[] = "<script>ObjTabSocioQuotas.setQuotasInfoFromJSON();</script>";
			#if ($GET_TAB != '')
			#	$JSScript[] = "<script>ObjTabs.activeTabArea(undefined,".$GET_TAB.");</script>";
			if ($GET_IDM != '')
				$JSScript[] = "<script>ObjTabSocioQuotas.selectModByID('".$GET_IDM."');</script>";
			if ($GET_ANO != '')
				$JSScript[] = "<script>ObjTabSocioQuotas.selectModAno(undefined,'".$GET_ANO."');</script>";
			$Msg = implode('',$JSScript);

			$objTemp->setVar('{$reg-update}', 			getLastDateEdit_WithUser($ROW[0]['data_alterado'], $ROW[0]['user_nome'],$ROW[0]['user_apelido']));
			$objTemp->setVar('{$foto-url}', 			$ROW[0]['imgSocioSrc']);
			$objTemp->setVar('{$imgsPath}', 			SETPATH('URL','PATH_APP_IMG_ICONS'));
			$objTemp->setVar('{$FormAction}', 			htmlentities($_SERVER['PHP_SELF']).$URL);
			$objTemp->setVar('{$HTML_SELECT_ESTADO}', 	$HTML_SELECT_ESTADO);
			$objTemp->setVar('{$HTML_SELECT_TIPO}', 	$HTML_SELECT_TIPO);
			$objTemp->setVar('{$LINK_ANULAR}', 			'perfil.php?id='.$GET_IDS);
			$objTemp->setVarArray_Section("[data-form=form-socio]","{field:","}",$ROW);
			$objTemp->setVar('{$jsondata}', 			$jsondata);
			
			# QUOTAS DO SOCIO
			$objTemp->setVar('{$template_socio_quotas}',$tpl_socio_quotas);


			require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSociosFamiliar.class.php');
			$dbSociosFamiliar = new dbTblSociosFamiliar($ADBConnection);
			$dbSociosFamiliar->set_IDSOCIO($GET_IDS);
			$dbSociosFamiliar->set_IDFAMILIAR('');
			$dbSociosFamiliar->set_IDUSER($id_user);
			$dbSociosFamiliar->set_IDASSOC($id_assoc);
					
			$Result = $dbSociosFamiliar->getFamiliaresLista();
			$JSONFamiliares = json_encode($Result['ROW']);
			$objTemp->setVar('{$family_jsondata}',$JSONFamiliares);
			unset($dbSociosFamiliar);
		}
		else
		{
			$sMessage = CREATE_NOTFOUND('O Registo não existe!','Voltar','listagem.php');
			$objTemp->replaceDOM('[data-id=corpo]','innertext',$sMessage);
		}
		$objTemp->setVar('{$MsgScript}', (isset($Msg)) ? $Msg : '');

		$outputHTML = $objTemp->echohtml();
		unset($TplMdlSocios);
		unset($objTemp);
	}
	if ($nogzip)
	{
		ob_start("ob_gzhandler");
		echo $outputHTML;
		ob_flush();
	}
	else
		echo $outputHTML;
?>