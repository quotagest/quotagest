<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	$GET_IDS 		= (isset($_GET['ids'])) ? $_GET['ids'] : '';
	$id_user 		= (isset($id_user))  ? $id_user  : $_SESSION['id_user'];
	$id_assoc 		= (isset($id_assoc)) ? $id_assoc : $_SESSION['id_assoc'];
	
	$ADBConnection 	= (isset($ADBConnection)) ? $ADBConnection : false;
	$ATPLFileName 	= SETPATH('ROOT','PATH_APP').'/neo/ui/socios/novo.html';
	$outputHTML 	= '';

	########## TEMPLATE PROCESSING ##########
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlSocios.class.php');

	$TplMdlSocios = new tplmdlSocios($ADBConnection);
	$ADBConnection= $TplMdlSocios->tblSocios->getDBConnection();
	$TplMdlSocios->setIDValues(0,0,$id_user,$id_assoc);
	# ##### GUARDAR REGISTO
	$Result = $TplMdlSocios->INSERT_SOCIO($_POST,$_GET);
	# ##### RECEBER Nº SOCIO ultimo socio inserido
	$NumSocio = $TplMdlSocios->getLastNumSocio($id_user,$id_assoc);
	############################################################################
	#$HTML_SELECT_ESTADO = $TplMdlSocios->buildSELECT_SocioEstado('0',$id_user);
	#$HTML_SELECT_TIPO   = $TplMdlSocios->buildSELECT_SocioTipo('0',$id_user);
	
	############################ SOCIO TIPO ####################################
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocioTipo.class.php');
	$dbSocioTipo = new dbTblSocioTipo($ADBConnection);
	$dbSocioTipo->set_IDSOCIOTIPO('');
	$dbSocioTipo->set_IDUSER($id_user);
	$dbSocioTipo->set_IDASSOC($id_assoc);
	$Result 				= $dbSocioTipo->getListSearch(array(),array(), 'nome','ASC');
	$ROW_SocTipo 			= $Result['ROW'];
	$EXIST_SocTipo 			= $Result['EXIST'];
	$TotalPages_SocTipo 	= $Result['TotalPages'];
	unset($dbSocioTipo);
	############################ SOCIO TIPO ####################################
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocioEstado.class.php');
	$dbSocioEstado = new dbTblSocioEstado($ADBConnection);
	$dbSocioEstado->set_IDSOCIOESTADO('');
	$dbSocioEstado->set_IDUSER($id_user);
	$dbSocioEstado->set_IDASSOC($id_assoc);
	$Result 				= $dbSocioEstado->getListSearch(array(),array(), 'nome','ASC');
	$ROW_SocEstado 			= $Result['ROW'];
	$EXIST_SocEstado 		= $Result['EXIST'];
	$TotalPages_SocEstado 	= $Result['TotalPages'];
	unset($dbSocioEstado);
	
	unset($TplMdlSocios);
	############################################################################

	$JSScript = array();
	$JSScript[] = "<script>AutoSelectIndex('novo-socio','TipoSocio',1);</script>";
	$JSScript[] = "<script>AutoSelectIndex('novo-socio','EstadoSocio',1);</script>";
	$JSScript[] = "<script>INI_FILEUPLOAD('upload-image','divUploadImage','spanProgressBar');</script>";
	$JSScript[] = "<script>InitInputsForm('novo-socio');</script>";
	$JSScript[] = "<script>ObjModal.sLastAreaCall='';</script>";
	$Msg = implode('',$JSScript);

	$objTemp = new TobjTemplate($ATPLFileName);
	$objTemp->setVar('{$MsgScript}', 			(isset($Msg)) ? $Msg : '');
	$objTemp->setVar('{$titulo}', 				'Adicionar Sócio');
	$objTemp->setVar('{$ajuda}', 				'Adicionar um novo registo de Sócio.');
	$objTemp->setVar('{$NumSocio}', 			($NumSocio+1));
	$objTemp->setVar('{$FORMAction}', 			htmlentities($_SERVER['PHP_SELF']));
	$objTemp->setVar('{$imgsPath}', 			SETPATH('URL','PATH_APP_IMG_ICONS'));
	$objTemp->BuildRows('id=sociotipo',	 '{field:','}',$ROW_SocTipo);
	$objTemp->BuildRows('id=socioestado','{field:','}',$ROW_SocEstado);
	
	#$objTemp->setVar('{$HTML_SELECT_ESTADO}', 	$HTML_SELECT_ESTADO);
	#$objTemp->setVar('{$HTML_SELECT_TIPO}', 	$HTML_SELECT_TIPO);

	$outputHTML = $objTemp->echohtml();
	unset($objTemp);

	echo $outputHTML;
?>