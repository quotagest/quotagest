<?php
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');
	
	$Page 			= (isset($_GET['page'])) ? $_GET['page'] : '1';
	$Page 			= ($Page != '') ? $Page : '1';
	$ID_SOCIO 		= (isset($_GET['ids'])) ? $_GET['ids'] : '';
	$ID_SOCIO 		= ($ID_SOCIO != '') ? $ID_SOCIO : false;
	$INPUT_PAGE 	= $Page;
	$INPUT_PAGENEXT = $Page;
	$EXISTList 		= false;
	$TPages 		= $Page;

	if ($ID_SOCIO)
	{
		$nogzip = true;
		$main_include = processPage(SETPATH('ROOT','PATH_APP').'neo/ui/socios/perfil-edit.php');
	}
	else
	{
		$main_include = processPage(SETPATH('ROOT','PATH_APP').'neo/ui/socios/novo.php');
	}
	

	$InsertTitle	= "Adicionar Sócio";
	$InsertOnClick  = "ObjListagem.LoadContent(this,'', 'Ficha de Sócio', '/neo/?op=socios');";
	$ViewOnClick 	= "ObjListagem.LoadContent(this,'', 'Ficha de Sócio', '/neo/?op=socios&ids={field:id_socio}&page=".$INPUT_PAGE."');";
	$SearchList 	= "ObjListagem.LoadMore(this,'list-search');";
	$SearchFilter 	= "ObjListagem.LoadMore(document.getElementsByName('searchlist')[0],'list-search');";
	
	$Options = array();
	$Options['nome'] 	= 'Nome';
	#$Options['nome;DESC'] 	= 'Nome DESC';
	$Options['codigo'] 	= 'Código';
	#$Options['codigo;DESC'] = 'Código DESC';
	$Options['tipo'] 	= 'Tipo Sócio';
	#$Options['tipo;DESC'] 	= 'Tipo Sócio DESC';
	$Options['estado'] 	= 'Estado Sócio';
	#$Options['estado;DESC'] = 'Estado Sócio DESC';
	$OptionsFilter = buildOptionsFromArray($Options);
	
	################################################
	############### CLASS - TEMPLATE ###############
	$ATPLFileName = SETPATH('ROOT','PATH_APP').'/neo/main-nav.html';

	$objTemp = new TobjTemplate($ATPLFileName);
	$objTemp->setVar('{$INPUT_PAGE}', 		$INPUT_PAGE);
	$objTemp->setVar('{$INPUT_PAGENEXT}',	$INPUT_PAGENEXT);
	$objTemp->setVar('{$InsertOnClick}', 	$InsertOnClick);
	$objTemp->setVar('{$InsertTitle}', 		$InsertTitle);
	$objTemp->setVar('{$ViewOnClick}', 		$ViewOnClick);
	$objTemp->setVar('{$SearchList}', 		$SearchList);
	$objTemp->setVar('{$OptionsFilter}', 	$OptionsFilter);
	$objTemp->setVar('{$SearchFilter}', 	$SearchFilter);
	$objTemp->setVar('{$main_include}', 	$main_include);
	$tplHTML = $objTemp->echohtml();
	unset($objTemp);
	################################################
	
	echo $tplHTML;
?>