<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	$GET_IDS 		= (isset($_GET['ids'])) ? $_GET['ids'] : '';
	$GET_IDM 		= (isset($_GET['idm'])) ? $_GET['idm'] : '';
	$GET_ANO 		= (isset($_GET['ano'])) ? $_GET['ano'] : '';
	$ADBConnection 	= (isset($ADBConnection)) ? $ADBConnection : false;
	$id_user 		= (isset($id_user))  ? $id_user  : $_SESSION['id_user'];
	$id_assoc 		= (isset($id_assoc)) ? $id_assoc : $_SESSION['id_assoc'];
	$ATPLFileName 	= SETPATH('ROOT','PATH_APP').'/neo/ui/socios/quotas-lista.html';
	
	$jsonResult 	= array('result'=>false, 'json'=>false);
	
	if ($GET_IDS != '')
	{
		require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblQuotas.class.php');

		$dbQuotas = new dbTblQuotas($ADBConnection);
		$dbQuotas->set_IDSOCIO($GET_IDS);
		$dbQuotas->set_IDMODALIDADE($GET_IDM);
		$dbQuotas->set_IDUSER($id_user);
		$dbQuotas->set_IDASSOC($id_assoc);
		$ResultActiv  = $dbQuotas->getModalidadesTotal_SOCIO();
		$ROWActivi    = $ResultActiv['ROW'];
		$EXISTActivi  = $ResultActiv['EXIST'];
		#var_dump($ROWActivi);
		#die('');
		# CRIAR LISTA DE QUOTAS DA ACTIVIDADE SELECIONADO
		
		$jsonResult = array('result'=>true, 'json'=>false);
		if ($EXISTActivi)
		{
			# SE NAO EXISTE MODALIDADE SELECIONADA, seleciona a primeira!
			if ($GET_IDM == '')
				$dbQuotas->set_IDMODALIDADE($ROWActivi[0]['id_modalidade']);
			
			# RECEBE LISTA DE ANOS DA -- modalidade-xpto
			$ResultAnos = $dbQuotas->getListaDeAnos();
			$ROWAnos = $ResultAnos['ROW'];
			$ROWExist= $ResultAnos['EXIST'];
			
			# SE NAO EXISTE ANO SELECIONADO, seleciona o primeiro!
			if ($GET_ANO == '')
				$GET_ANO = $ROWAnos[0]['ano'];
			
			# RECEBE LISTA DAS QUOTAS DO ANO xpto
			$ResultQuotas = $dbQuotas->getListNEW($GET_ANO);
			$ROW 	= $ResultQuotas['ROW'];
			$EXIST 	= $ResultQuotas['EXIST'];
			
			unset($dbQuotas);
			$jsonResult['json'] = array('quotas'=>$ROW, 'anos'=>$ROWAnos);
		}
	}
	#############################################################################
	$objTemp = new TobjTemplate($ATPLFileName);
	$objTemp->setVar('{$json}', json_encode($jsonResult) );
	$objTemp->BuildRows('id=listamodalidades','{field:','}',$ROWActivi);
	$outputHTML = $objTemp->echohtml();
	unset($objTemp);

	echo $outputHTML;
?>