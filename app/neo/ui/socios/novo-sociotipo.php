<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	$GET_IDS 		= (isset($_GET['ids'])) ? $_GET['ids'] : '';
	$id_user 		= (isset($id_user))  ? $id_user  : $_SESSION['id_user'];
	$id_assoc 		= (isset($id_assoc)) ? $id_assoc : $_SESSION['id_assoc'];
	
	$ADBConnection 	= (isset($ADBConnection)) ? $ADBConnection : false;
	$ATPLFileName 	= SETPATH('ROOT','PATH_APP').'/neo/ui/socios/novo-sociotipo.html';
	$outputHTML 	= '';

	########## TEMPLATE PROCESSING ##########
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');;
	############################################################################

	$JSScript = array();
	$JSScript[] = "<script>InitInputsForm('novo-sociotipo');</script>";
	$JSScript[] = "<script>Autofocus('novo-sociotipo','Nome');</script>";
	$Msg = implode('',$JSScript);

	$objTemp = new TobjTemplate($ATPLFileName);
	$objTemp->setVar('{$MsgScript}', 			(isset($Msg)) ? $Msg : '');
	$objTemp->setVar('{$titulo}', 				'Adicionar Tipo de Sócio');
	$objTemp->setVar('{$ajuda}', 				'Adicionar um novo registo de Tipo de Sócio.');
	$objTemp->setVar('{$formNome}', 			'novo-sociotipo');
	$objTemp->setVar('{$FORMAction}', 			htmlentities($_SERVER['PHP_SELF']));

	$outputHTML = $objTemp->echohtml();
	unset($objTemp);

	echo $outputHTML;
?>