/*
--------------- Clipboard === COPY-PASTE --------------
<!--
<span style="position:relative; left:350px; top:100px">
	<button id="copy-button" data-clipboard-target="zclip-socio_edit" title="Click to copy me.">Copy to Clipboard</button>
	<a href="javascript:void(0);" id="copy-button2" >Copy to Clipboard 22</a>
	
	<div id="textarea-zclip">OLA MUNDO <b>dsdsd</b></div>
</span>

<script>
	var funcRequest = function (client, args)
	{
		client.setText(document.getElementById("zclip-socio_edit").innerHTML);
	}
	var funcComplete = function (client, args)
	{
		console.log("Texto Copiado! = '"+args.text+"'");
	}
	var ObjTarget = document.getElementById("copy-button2");
	var ZClipAPI = new ZeroClipboardAPI(ObjTarget, funcRequest, funcComplete);
</script>
-->
*/
//##############################################################################
//##############################################################################

    //https://google-developers.appspot.com/chart/interactive/docs/gallery/columnchart
	//https://developers.google.com/chart/interactive/docs/reference#dataparam
	//https://developers.google.com/chart/interactive/docs/php_example
    //https://code.google.com/p/gchartphp/
/*
    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {'packages':['corechart']});
      
    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);
      
    function drawChart()
    {
      var jsonData = $.ajax({
          url: "teste/getData.php",
          dataType:"json",
          async: false
          }).responseText;
          
      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(jsonData);

      // Instantiate and draw our chart, passing in some options.
      var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
      chart.draw(data, {width: 400, height: 240});
      console.log(jsonData);
    }
<!-- Div that will hold the pie chart --> <-- ADICIONAR NO FIM -->
<!-- <div id="chart_div" style="float:right; width: 900px; height: 500px;"></div> -->
*/





		function getAllHeight(AAction)
		{
			var eAllParent 		= document.getElementById('sub-nav');
			var eItemsParent 	= document.getElementsByClassName('listagem')[0];
			var arrItems 		= document.getElementsByClassName('item');
			var numElem 		= arrItems.length;

			if (numElem == 10)
			{
				var iscrollHeight = arrItems[0].scrollHeight;
				var ioffsetHeight = arrItems[0].offsetHeight;
				var iclientHeight = arrItems[0].clientHeight;
				var iParentHeight = eAllParent.scrollHeight;
				var iItemsHeight  = (iclientHeight*numElem);
				//
				//	Verifica se a altura do elemento pai "sub-nav" é maior ou igual
				//	à soma do "carregar mais" ++ a soma das alturas dos elmentos "item".
				
				if ((iItemsHeight) <= (iParentHeight))
				{
					var eDivItem = document.createElement("DIV");
						eDivItem.setAttribute("class","item");
						eDivItem.setAttribute("onclick","LoadMore(this,'"+AAction+"','list');");
					//	eDivItem.setAttribute();
					var eH5 = document.createElement("H5");
						eH5.setAttribute("class","nome");
						eH5.setAttribute("style","text-align:center;");
						eH5.setAttribute("id","LoadMore");
						eH5.innerHTML = "carregar mais";
					eDivItem.appendChild(eH5);
					eItemsParent.appendChild(eDivItem);

					eAllParent.addEventListener("scroll",
							function (event)
							{
								if ((event.target.scrollHeight-event.target.scrollTop) === iParentHeight)
								{
									LoadMore(this,AAction,'list');
								}
							});
				}
			}
		}
		function createItemSocio(AJsonArray)
		{
			var eDivItem = document.createElement("DIV");
				eDivItem.setAttribute("class","item");
				eDivItem.setAttribute("onclick",AJsonArray["LoadContent"]);

			var eH5 = document.createElement("H5");
				eH5.setAttribute("class","nome");
				eH5.innerHTML = AJsonArray["nome"];
			eDivItem.appendChild(eH5);
			
			var eSpan = document.createElement("SPAN");
				eSpan.setAttribute("class","label verde");
				eSpan.innerHTML = AJsonArray["codigo"];
			eDivItem.appendChild(eSpan);
			
			var eSpan = document.createElement("SPAN");
				eSpan.setAttribute("class","label grey-blue");
				eSpan.innerHTML = AJsonArray["socio_nome_tipo"];
			eDivItem.appendChild(eSpan);

			var eSpan = document.createElement("SPAN");
				eSpan.setAttribute("class","saldo label");
				eSpan.setAttribute("style","background:#C2BE68; color:#FFF;");
				eSpan.innerHTML = AJsonArray["divida"];
			eDivItem.appendChild(eSpan);

			return 	eDivItem;
		}
		function cleanLista(AElemLista)
		{
			var arrItems = AElemLista.getElementsByClassName("item");
			var ccI = arrItems.length;

			for (var i=(ccI-1); i>(-1); i--)
			{
				AElemLista.removeChild(arrItems[i]);
			}
		}

		function LoadMore(ASelf,AAction,ASubAction)
		{
			var eLoadMore	 = document.getElementById("LoadMore");
			var ePage	  	 = document.getElementById("PageNum");
			var ePageNext 	 = document.getElementById("PageNumNext");
			var iPage		 = parseInt(ePage.innerHTML);
			var iPageNext 	 = parseInt(ePageNext.innerHTML);
			var eItemsParent = document.getElementsByClassName('listagem')[0];
			var boolRemove   = false;
			var eField 		 = (ASelf.tagName === "INPUT") ? ASelf : undefined;
			var ExistLoadMore= (eLoadMore != undefined);
			if (ExistLoadMore)
				eLoadMore = eLoadMore.parentNode;

			if ((ExistLoadMore) || (eField != undefined))
			{
				var url 	 = 'teste/getList.php';
				var params 	 = 'action='+AAction;
					params 	+= '&subaction='+ASubAction;
					params 	+= '&pagenext='+iPageNext;
					params 	+= (eField != undefined) ? ('&q='+eField.value) : '';
				console.log(params);
				var func = function(sResult)
				{
					arrResult = sResult.split("#");
					ResBool 	= arrResult[0];			//SUCESSO
					ResTPage 	= arrResult[1];			//TotalPages
					ResNextPage = arrResult[2];			//NextPage?
					ResJson 	= arrResult[3];			//JSON DATA
					console.log(ResBool+' - '+ResTPage+' - '+ResNextPage);
					if (ResBool === 'false')
					{
						boolRemove = true;
						if (ASubAction === 'list-search')
						{
							cleanLista(eItemsParent);
							eLoadMore = undefined;
						}
					}
					else
					if (ResBool === 'true')
					{
						var	AJSON 	= decode64(ResJson);
						var arrJSON = JSON.parse(AJSON);
						var ccI = arrJSON.length;

						switch (AAction)
						{
							case 'socios': 	{
												switch(ASubAction)
												{
													case 'list':
													{
													
													} break;
													case 'list-search':
													{
														ePageNext.innerHTML = (iPage+1);
														cleanLista(eItemsParent);
														eLoadMore = undefined;
													} break;
												}
												for (var i=0; i<ccI; i++)
												{
													var item = createItemSocio(arrJSON[i]);
													if (ExistLoadMore)
														eItemsParent.insertBefore(item,eLoadMore);
													else
														eItemsParent.appendChild(item);
												}
											} break;
							default:{
							
									} break;
						}
						if (ResNextPage)
						{
							ePageNext.innerHTML = (iPageNext+1);
						}
						else
						{
							boolRemove = true;
						}
					}
					
					switch(ASubAction)
					{
						case 'list':
						{
							if ((ExistLoadMore) && (boolRemove))
							{
								eItemsParent.removeChild(eLoadMore);
							}
						} break;
						case 'list-search':
						{
							if ((eField != undefined) && (eField.value === ""))
								getAllHeight(AAction);
						} break;
					}
				}
				AJAX_REQUEST_CALLBACK("POST",url,params,func);
			}
		}