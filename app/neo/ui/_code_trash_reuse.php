<?php
		$json_string = json_encode($json_array);
		#echo $json_string;
		
		if ($json_string != '')
		{
			$json_string = (array)json_decode($json_string);
			$ROW = $json_string['ROW'];

			$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP').'/neo/teste/socios-perfil-edit.html');
			foreach($json_string as $key => $value)
			{
				if (($key != 'ROW') && ($key != 'replaceDOM'))
				{
					$objTemp->setVar($key, $value);
				}
				else
				{
					if ($key === 'replaceDOM')
					{
						foreach($value as $keyReplaceDOM => $valueReplaceDOM)
						{
							$objTemp->replaceDOM($valueReplaceDOM[0],$valueReplaceDOM[1],$valueReplaceDOM[2]);
						}
					}
					else
					if ($key === 'ROW')
					{
						foreach($value as $keyROW => $valueROW)
						{
							switch($keyROW)
							{
								case 'setVarArray_Section'	: $objTemp->setVarArray_Section($valueROW[0],$valueROW[1],$valueROW[2],$valueROW[3]); break;
								case 'BuildRows'			: $objTemp->BuildRows($valueROW[0],$valueROW[1],$valueROW[2],$valueROW[3]); break;
							}
						}
					}
				}
			}
			$tplHTML = $objTemp->echohtml();
			unset($objTemp);
		}
		
?>