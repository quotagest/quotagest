<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	$GET_IDE 		= (isset($_GET['ide'])) ? $_GET['ide'] : '';
	$id_user 		= (isset($id_user))  ? $id_user  : $_SESSION['id_user'];
	$id_assoc 		= (isset($id_assoc)) ? $id_assoc : $_SESSION['id_assoc'];
	$ADBConnection 	= (isset($ADBConnection)) ? $ADBConnection : false;
	$ATPLFileName 	= SETPATH('ROOT','PATH_APP').'/neo/ui/entidades/perfil-edit.html';
	$outputHTML 	= '';

	if ($GET_IDE != '')
	{
		require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlEntidades.class.php');

		$TplMdlEntidades = new tplmdlEntidades($ADBConnection);
		$TplMdlEntidades->setIDValues($GET_IDE,$id_user,$id_assoc);
		$ADBConnection = $TplMdlEntidades->tblEntidades->getDBConnection();

		############################## GUARDAR REGISTO ##############################
		$TplMdlEntidades->UPDATE_ENTIDADE($_POST,$_GET);
		############################################################################

		$Result = $TplMdlEntidades->getPerfilEditEntidade();
		$ROW 	= $Result['ROW'];
		$EXIST 	= $Result['EXIST'];
		$jsondata = $Result['JSON'];

		$objTemp = new TobjTemplate($ATPLFileName);
		$objTemp->setVar('{$titulo}', 	'Perfil de Entidade');
		#$objTemp->setVar('{$ajuda}', 	'Edite as informações básicas do perfil do Sócio.');

		if ($EXIST)
		{
			$JSScript = array();
			$JSScript[] = "<script>EnableForm('entidade-perfil-edit');</script>";
			$JSScript[] = "<script>ObjTabs.setObjData('entidades');</script>";
			$Msg = implode('',$JSScript);

			$objTemp->setVar('{$reg-update}', 		getLastDateEdit_WithUser($ROW[0]['data_alterado'], $ROW[0]['user_nome'],$ROW[0]['user_apelido']));
			$objTemp->setVar('{$FormAction}', 		htmlentities($_SERVER['PHP_SELF']).$URL);
			$objTemp->setVar('{$jsondata}', 		$jsondata);
			$objTemp->setVarArray_Section("[data-id=form-entidade]","{field:","}",$ROW);
		}
		else
		{
			$sMessage = CREATE_NOTFOUND('O Registo não existe!','Voltar','listagem.php');
			$objTemp->replaceDOM('[data-id=corpo]','innertext',$sMessage);
		}
		$objTemp->setVar('{$MsgScript}', (isset($Msg)) ? $Msg : '');

		$outputHTML = $objTemp->echohtml();
		unset($TplMdlEntidades);
		unset($objTemp);
	}

	echo $outputHTML;
?>