<?php
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');
	
	$Page 			= (isset($_GET['page'])) ? $_GET['page'] : '1';
	$Page 			= ($Page != '') ? $Page : '1';
	$ID_ENTIDADE 	= (isset($_GET['ide'])) ? $_GET['ide'] : '';
	$ID_ENTIDADE 	= ($ID_ENTIDADE != '') ? $ID_ENTIDADE : false;
	$INPUT_PAGE 	= $Page;
	$INPUT_PAGENEXT = $Page;
	$EXISTList 		= false;
	$TPages 		= $Page;

	if ($ID_ENTIDADE)
	{
		$main_include = processPage(SETPATH('ROOT','PATH_APP').'neo/ui/entidades/perfil-edit.php');
	}
	else
	{
		$main_include = processPage(SETPATH('ROOT','PATH_APP').'neo/ui/entidades/novo.php');
	}

	$InsertTitle	= "Adicionar Entidade";
	$InsertOnClick  = "ObjListagem.LoadContent(this,'', 'Ficha de Entidade', '/neo/?op=entidades');";
	$ViewOnClick 	= "ObjListagem.LoadContent(this,'', 'Ficha de Entidade', '/neo/?op=entidades&ide={field:id_entidade}&page=".$INPUT_PAGE."');";
	$SearchList 	= "ObjListagem.LoadMore(this,'list-search');";
	$SearchFilter 	= "ObjListagem.LoadMore(document.getElementsByName('searchlist')[0],'list-search');";
	
	$Options = array();
	$Options['nome;ASC'] 			= 'Nome ASC';
	$Options['nome;DESC'] 			= 'Nome DESC';
	$Options['abreviatura;ASC'] 	= 'Abreviatura ASC';
	$Options['abreviatura;DESC'] 	= 'Abreviatura DESC';
	$Options['tipo;ASC'] 			= 'Tipo ASC';
	$Options['tipo;DESC'] 			= 'Tipo DESC';
	$OptionsFilter = buildOptionsFromArray($Options);
	
	################################################
	############### CLASS - TEMPLATE ###############
	$ATPLFileName = SETPATH('ROOT','PATH_APP').'/neo/main-nav.html';

	$objTemp = new TobjTemplate($ATPLFileName);
	$objTemp->setVar('{$INPUT_PAGE}', 		$INPUT_PAGE);
	$objTemp->setVar('{$INPUT_PAGENEXT}',	$INPUT_PAGENEXT);
	$objTemp->setVar('{$InsertOnClick}', 	$InsertOnClick);
	$objTemp->setVar('{$InsertTitle}', 		$InsertTitle);
	$objTemp->setVar('{$ViewOnClick}', 		$ViewOnClick);
	$objTemp->setVar('{$SearchList}', 		$SearchList);
	$objTemp->setVar('{$OptionsFilter}', 	$OptionsFilter);
	$objTemp->setVar('{$SearchFilter}', 	$SearchFilter);
	$objTemp->setVar('{$main_include}', 	$main_include);
	$tplHTML = $objTemp->echohtml();
	unset($objTemp);
	################################################

	echo $tplHTML;
?>