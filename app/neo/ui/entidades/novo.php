<?php 
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	$GET_IDE 		= (isset($_GET['ide'])) ? $_GET['ide'] : '';
	$URL 	 		= ($GET_IDE !== '') ? "?ide=".$GET_IDE : '';
	$ADBConnection 	= (isset($ADBConnection)) ? $ADBConnection : false;
	$ATPLFileName 	= SETPATH('ROOT','PATH_APP').'/neo/ui/entidades/novo.html';
	$outputHTML 	= '';

	########## TEMPLATE PROCESSING ##########
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlEntidades.class.php');
	
	$TplMdlEntidades = new tplmdlEntidades($ADBConnection);
	$TplMdlEntidades->setIDValues(0,$id_user,$id_assoc);
	# ##### GUARDAR REGISTO
	$Result = $TplMdlEntidades->INSERT_ENTIDADE($_POST,$_GET);
	unset($TplMdlEntidades);
	############################################################################

	$JSScript = array();
	$JSScript[] = "<script>InitInputsForm('novo-entidade');</script>";
	$Msg = implode('',$JSScript);

	$objTemp = new TobjTemplate($ATPLFileName);
	$objTemp->setVar('{$MsgScript}', 	(isset($Msg)) ? $Msg : '');
	$objTemp->setVar('{$titulo}', 		'Adicionar Entidade');
	$objTemp->setVar('{$ajuda}', 		'Adicionar um novo registo de Entidade.');
	$objTemp->setVar('{$FORMAction}', 	htmlentities($_SERVER['PHP_SELF']));

	$outputHTML = $objTemp->echohtml();
	unset($objTemp);

	echo $Msg;
	echo $outputHTML;
?>