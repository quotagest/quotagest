/*
		public function forkTemplate_SocioNovo($AID_USER,$AID_ASSOC)
		{
			$this->setTplFileName('novo.html');
			parent::setObjTemplate($this->tplFolderPath.$this->tplFileName);
			parent::setTitulo('Adicionar Sócio');
			parent::setAjuda('Adicionar um novo registo de Sócio.');

			#$this->tblSocios->set_IDSOCIO($AID_SOCIO);
			#$this->tblSocios->set_IDMODALIDADE($AID_MODALIDADE);
			$this->tblSocios->set_IDUSER($AID_USER);
			$this->tblSocios->set_IDASSOC($AID_ASSOC);

			parent::addLeftMenuItem(array("titulo" => "Guardar", "href" => "/", "javascript" => "ValidateForm('novo-socio');"));
			parent::addLeftMenuItem(array("titulo" => "Ver listagem de Sócios", "href" => "/ui/socios/listagem.php"));

			$resHTML_ESTADO = $this->buildSELECT_SocioEstado('0',$AID_USER);
			$resHTML_TIPO 	= $this->buildSELECT_SocioTipo('0',$AID_USER);
	
			$this->objTemplate->setVar('{$imgsPath}', SETPATH('URL','PATH_APP_IMG_ICONS'));
			$this->objTemplate->setVar('{$dateNow}', date('Y-m-d'));
			$this->objTemplate->setVar('{$resHTML_ESTADO}', $resHTML_ESTADO);
			$this->objTemplate->setVar('{$resHTML_TIPO}', $resHTML_TIPO);

			$this->sHTML = $this->objTemplate->echohtml();
		}
*/	
/*
		public function forkTemplate_SocioPerfil($AID_SOCIO,$AID_MODALIDADE,$AID_USER,$AID_ASSOC)
		{
			$this->setTplFileName('perfil.html');
			parent::setObjTemplate($this->tplFolderPath.$this->tplFileName);
			parent::setTitulo('Perfil de Sócio');
			parent::setAjuda('Utilize o botão "Editar" para alterar os dados do Sócio.');

			$this->tblSocios->set_IDSOCIO($AID_SOCIO);
			$this->tblSocios->set_IDMODALIDADE($AID_MODALIDADE);
			$this->tblSocios->set_IDUSER($AID_USER);
			$this->tblSocios->set_IDASSOC($AID_ASSOC);
			$Result = $this->getPerfilSocio();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];

			if ($EXIST)
			{
				$URL = ($AID_SOCIO !== '') ? "?ids=".$AID_SOCIO : '';
				parent::addLeftMenuItem(array("titulo" => "Inscrever em modalidade", "href" => "/ui/modalidades/novo-inscricao.php".$URL));
				parent::addLeftMenuItem(array("titulo" => "Ver Quotas", "href" => "/ui/quotas/listagem.php".$URL));
				parent::addLeftMenuItem(array("titulo" => "Ver listagem de Sócios", "href" => "/ui/socios/listagem.php"));

				$this->objTemplate->setVarArray('{field:','}',$ROW[0]);
				$this->objTemplate->setVar('{$reg-update}', getLastDateEdit($ROW[0]['data_alterado']));
				$this->objTemplate->setVar('{$edit-link}', 'perfil-edit.php?id='.$AID_SOCIO);
				$this->objTemplate->setVar('{$foto-url}',	$ROW[0]['imgSocioSrc']);

				#############################################################################
				############################ MENU QUOTAS EM FALTA ###########################
				$ResultQuotas = $this->tblSocios->getModalidadesTotal();
				$ROWQuotas    = $ResultQuotas['ROW'];
				$EXISTQuotas  = $ResultQuotas['EXIST'];

				$tDivida = 0.0;
				$tPago	 = 0.0;
				if ($EXISTQuotas)
				{
					$sPathQuotas = SETPATH('URL','PATH_APP_UI_QUOTAS');
					$ccI = count($ROWQuotas);
					for ($i=0; $i<$ccI; $i++)
					{
						$tDivida += $ROWQuotas[$i]['total_divida'];
						$tPago	 += $ROWQuotas[$i]['total_pago'];
						$ROWQuotas[$i]['goto-link'] = $sPathQuotas.'listagem.php?ids='.$AID_SOCIO.'&idm='.$ROWQuotas[$i]['id_modalidade'].'&ano='.date('Y');
					}
					$this->objTemplate->BuildRows('id=linhas','{field:','}',$ROWQuotas);
				}
				else
				{
					$this->objTemplate->setVar('{loop id=linhas}', '');
					$this->objTemplate->setVar('{/loop}', '');
					$this->objTemplate->setVar('por modalidade:', '');
					$this->objTemplate->replaceDOM('[class=modalidade]','outertext','');
				}
				$this->objTemplate->setVar('{$total_divida}',	number_format($tDivida, 2, '.', ''));
				#############################################################################
			}
			else
			{
				$sMessage = CREATE_NOTFOUND('O Registo não existe!','Voltar','listagem.php');
				$this->objTemplate->replaceDOM('[data-id=corpo]','innertext',$sMessage);
			}

			$this->sHTML = $this->objTemplate->echohtml();
		}
*/
/*
		public function forkTemplate_SocioPerfilEdit($AID_SOCIO,$AID_MODALIDADE,$AID_USER,$AID_ASSOC)
		{
			$this->setTplFileName('perfil-edit.html');
			parent::setObjTemplate($this->tplFolderPath.$this->tplFileName);
			parent::setTitulo('Editar perfil do Sócio');
			parent::setAjuda('Edite as informações básicas do perfil do Sócio.');

			$this->tblSocios->set_IDSOCIO($AID_SOCIO);
			$this->tblSocios->set_IDMODALIDADE($AID_MODALIDADE);
			$this->tblSocios->set_IDUSER($AID_USER);
			$this->tblSocios->set_IDASSOC($AID_ASSOC);
			$Result = $this->getPerfilEditSocio();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];

			if ($EXIST)
			{
				$URL  = ($AID_SOCIO !== '') ? '?id='.$AID_SOCIO : '';
				$sKey = $this->tblSocios->encryptVar(date('h:i:s').'*'.$AID_SOCIO);
				parent::addLeftMenuItem(array("titulo" => "Guardar", 		"href" => "/", "javascript" => "ValidateForm('socio-perfil-edit');"));
				parent::addLeftMenuItem(array("titulo" => "Remover Foto", 	"href" => "/", "javascript" => "REMOVE_IMAGEFILE('socio','".$sKey."','unset');"));
				parent::addLeftMenuItem(array("titulo" => "Voltar", 		"href" => "/ui/socios/perfil.php".$URL));

				$this->objTemplate->setVarArray('{field:','}',$ROW[0]);
				$this->objTemplate->setVar('{$reg-update}', getLastDateEdit($ROW[0]['data_alterado']));
				$this->objTemplate->setVar('{$foto-url}', $ROW[0]['imgSocioSrc']);
				$this->objTemplate->setVar('{$imgsPath}', SETPATH('URL','PATH_APP_IMG_ICONS'));
				$this->objTemplate->setVar('{$FormAction}', htmlentities($_SERVER['PHP_SELF']).$URL);
			}
			else
			{
				$sMessage = CREATE_NOTFOUND('O Registo não existe!','Voltar','listagem.php');
				$this->objTemplate->replaceDOM('[data-id=corpo]','innertext',$sMessage);
			}

			$this->sHTML = $this->objTemplate->echohtml();
		}
*/
/*
		public function forkTemplate_SociosListagem($AID_SOCIO,$AID_MODALIDADE,$AID_USER,$AID_ASSOC,$APAGE='0')
		{
			$this->setTplFileName('listagem.html');
			parent::setObjTemplate($this->tplFolderPath.$this->tplFileName);
			parent::setTitulo('Listagem de Sócios');
			parent::setAjuda('Utilize a pesquisa para encontrar rápidamente o(s) Sócio(s) que procura.');

			$this->tblSocios->set_IDSOCIO($AID_SOCIO);
			$this->tblSocios->set_IDMODALIDADE($AID_MODALIDADE);
			$this->tblSocios->set_IDUSER($AID_USER);
			$this->tblSocios->set_IDASSOC($AID_ASSOC);
			$Result = $this->tblSocios->getList($APAGE);
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];

			if ($EXIST)
			{
				parent::addLeftMenuItem(array("titulo" => "Novo Sócio", "href" => "/ui/socios/novo.php"));
				parent::addLeftMenuItem(array("titulo" => "Relatórios", "href" => "/ui/socios/relatorio"));

				$this->objTemplate->BuildRows('id=linhas','{field:','}',$ROW);
			}
			else
			{
				$sMessage = CREATE_NOTFOUND('Não existem Registos!','Novo Sócio','novo.php');
				$this->objTemplate->replaceDOM('[data-id=corpo]','innertext',$sMessage);
			}

			$this->sHTML = $this->objTemplate->echohtml();
		}
*/