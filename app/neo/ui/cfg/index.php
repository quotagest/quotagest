<?php require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php'); ?>
<div id="main">
	<div id="corpo" class="cfg" data-id="form-socio">
		<header class="content-header">
			<h2 class="titulo">Configurações</h2>
			<!-- Tabs -->
			<div class="botoes tabs" data-id="tabs">
				<a data-name="tab-pagforma" class="botao tab no-btn active" onClick="ObjTabs.activeTabArea(this);">Formas de Pagamento</a>
				<a data-name="tab-pagopcao" class="botao tab no-btn" onClick="ObjTabs.activeTabArea(this);">Opções de Pagamento</a>
				<a data-name="tab-sociotipo" class="botao tab no-btn" onClick="ObjTabs.activeTabArea(this);">Tipos de Sócio</a>
				<a data-name="tab-socioestado" class="botao tab no-btn" onClick="ObjTabs.activeTabArea(this);">Estados de Sócio</a>
			</div>
		</header>
		
		<span data-name="tab-pagforma" class="act-editar">
			<?php include(SETPATH('ROOT','PATH_APP').'/neo/ui/cfg/pagforma.php'); ?>
		</span>
		<span data-name="tab-pagopcao" class="act-editar hidden">
			<?php include(SETPATH('ROOT','PATH_APP').'/neo/ui/cfg/pagopcao.php'); ?>
		</span>
		<span data-name="tab-sociotipo" class="act-editar hidden">
			<?php include(SETPATH('ROOT','PATH_APP').'/neo/ui/cfg/sociotipo.php'); ?>
		</span>
		<span data-name="tab-socioestado" class="act-editar hidden">
			<?php include(SETPATH('ROOT','PATH_APP').'/neo/ui/cfg/socioestado.php'); ?>
		</span>
	</div>
	<br/>
	<script>
		ObjFormQuotas.setVariables();
	</script>
</div>