<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');

	$ADBConnection 	= (isset($ADBConnection)) ? $ADBConnection : false;
	$ATPLFileName 	= SETPATH('ROOT','PATH_APP').'/neo/ui/cfg/template.html';
	$outputHTML 	= '';
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocioEstado.class.php');
	$dbSocioEstado = new dbTblSocioEstado($ADBConnection);
	#$ADBConnection = $dbSocioEstado->getDBConnection();
	$dbSocioEstado->set_IDSOCIOESTADO('');
	$dbSocioEstado->set_IDUSER($id_user);
	$dbSocioEstado->set_IDASSOC($id_assoc);
	$Result 				= $dbSocioEstado->getListSearch(array(),array(), 'nome','ASC','','');
	$ROW_SocioEstado 		= $Result['ROW'];
	$EXIST_SocioEstado 		= $Result['EXIST'];
	#$TotalPages_SocTipo 	= $Result['TotalPages'];
	
	unset($dbSocioEstado);

	$objTemp = new TobjTemplate($ATPLFileName);
	$objTemp->setVar('{$form_name}',			'socioestado');
	$objTemp->setVar('{$Titulo}',				'Estados de Sócio');
	$objTemp->setVar('{$TituloPredefinido}',	'Estado de Sócio Predefinida');
	$objTemp->setVar('{field:id}',				'{field:id_socioestado}');
	$objTemp->setVar('{$CodIDField}',			'CodSocioEstado');
	$objTemp->SetVarsBASH();
	$objTemp->BuildRows('id=socioestado','{field:','}',$ROW_SocioEstado);
	$objTemp->BuildRows('id=socioestadopred','{field:','}',$ROW_SocioEstado);
	$outputHTML = $objTemp->echohtml();
	unset($objTemp);

	echo $outputHTML;
?>