<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');

	$ADBConnection 	= (isset($ADBConnection)) ? $ADBConnection : false;
	$ATPLFileName 	= SETPATH('ROOT','PATH_APP').'/neo/ui/cfg/template.html';
	$outputHTML 	= '';
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblPagForma.class.php');
	$dbPagForma = new dbTblPagForma($ADBConnection);
	#$ADBConnection = $dbPagForma->getDBConnection();
	$dbPagForma->set_IDPAGFORMA('');
	$dbPagForma->set_IDUSER($id_user);
	$dbPagForma->set_IDASSOC($id_assoc);
	$Result 				= $dbPagForma->getListSearch(array(),array(), 'nome','ASC','','');
	$ROW_PagForma 			= $Result['ROW'];
	$EXIST_PagForma 		= $Result['EXIST'];
	#$TotalPages_SocTipo 	= $Result['TotalPages'];
/*
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlSocios.class.php');
	$tplmdlSocios 		= new tplmdlSocios($ADBConnection);
	$HTML_ESTADOSOCIO	= $tplmdlSocios->buildLABLESSocioExta('checkbox','EstadoSocio[]','socio_estado','0',$id_assoc);
	$HTML_TIPOSOCIO		= $tplmdlSocios->buildLABLESSocioExta('checkbox','TipoSocio[]','socio_tipo','0',$id_assoc);
	unset($tplmdlSocios);
*/
	unset($dbPagForma);

	$objTemp = new TobjTemplate($ATPLFileName);
	$objTemp->setVar('{$HTML_TIPOSOCIO}', 	$HTML_TIPOSOCIO);
	$objTemp->setVar('{$HTML_ESTADOSOCIO}', $HTML_ESTADOSOCIO);
	
	$objTemp->setVar('{$form_name}',			'pagforma');
	$objTemp->setVar('{$Titulo}',				'Métodos de Pagamento');
	$objTemp->setVar('{$TituloPredefinido}',	'Método de Pagamento Predefinida');
	$objTemp->setVar('{field:id}',				'{field:id_pagforma}');
	$objTemp->setVar('{$CodIDField}',			'CodPagForm');
	$objTemp->SetVarsBASH();
	$objTemp->BuildRows('id=pagforma','{field:','}',$ROW_PagForma);
	$objTemp->BuildRows('id=pagformapred','{field:','}',$ROW_PagForma);
	$outputHTML = $objTemp->echohtml();
	unset($objTemp);

	echo $outputHTML;
?>