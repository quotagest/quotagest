<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');

	$ADBConnection 	= (isset($ADBConnection)) ? $ADBConnection : false;
	$ATPLFileName 	= SETPATH('ROOT','PATH_APP').'/neo/ui/cfg/template.html';
	$outputHTML 	= '';
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblPagOpcao.class.php');
	$dbPagOpcao = new dbTblPagOpcao($ADBConnection);
	#$ADBConnection = $dbPagOpcao->getDBConnection();
	$dbPagOpcao->set_IDPAGOPCAO('');
	$dbPagOpcao->set_IDUSER($id_user);
	$dbPagOpcao->set_IDASSOC($id_assoc);
	$Result 				= $dbPagOpcao->getListSearch(array(),array(), 'nome','ASC','','');
	$ROW_PagOpcao 			= $Result['ROW'];
	$EXIST_PagOpcao 		= $Result['EXIST'];
	#$TotalPages_SocTipo 	= $Result['TotalPages'];
	
	unset($dbPagOpcao);

	$objTemp = new TobjTemplate($ATPLFileName);
	$objTemp->setVar('{$form_name}',			'pagopcao');
	$objTemp->setVar('{$Titulo}',				'Opções de Pagamento');
	$objTemp->setVar('{$TituloPredefinido}',	'Opção de Pagamento Predefinida');
	$objTemp->setVar('{field:id}',				'{field:id_pagopcao}');
	$objTemp->setVar('{$CodIDField}',			'CodPagOpcao');
	$objTemp->SetVarsBASH();
	$objTemp->BuildRows('id=pagopcao','{field:','}',$ROW_PagOpcao);
	$objTemp->BuildRows('id=pagopcaopred','{field:','}',$ROW_PagOpcao);
	$outputHTML = $objTemp->echohtml();
	unset($objTemp);

	echo $outputHTML;
?>