<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');

	$ADBConnection 	= (isset($ADBConnection)) ? $ADBConnection : false;
	$ATPLFileName 	= SETPATH('ROOT','PATH_APP').'/neo/ui/cfg/template.html';
	$outputHTML 	= '';
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocioTipo.class.php');
	$dbSocioTipo = new dbTblSocioTipo($ADBConnection);
	#$ADBConnection = $dbSocioTipo->getDBConnection();
	$dbSocioTipo->set_IDSOCIOTIPO('');
	$dbSocioTipo->set_IDUSER($id_user);
	$dbSocioTipo->set_IDASSOC($id_assoc);
	$Result 				= $dbSocioTipo->getListSearch(array(),array(), 'nome','ASC','','');
	$ROW_SocioTipo 			= $Result['ROW'];
	$EXIST_SocioTipo 		= $Result['EXIST'];
	#$TotalPages_SocTipo 	= $Result['TotalPages'];
	
	unset($dbSocioTipo);

	$objTemp = new TobjTemplate($ATPLFileName);
	$objTemp->setVar('{$form_name}',			'sociotipo');
	$objTemp->setVar('{$Titulo}',				'Tipos de Sócio');
	$objTemp->setVar('{$TituloPredefinido}',	'Tipo de Sócio Predefinido');
	$objTemp->setVar('{field:id}',				'{field:id_sociotipo}');
	$objTemp->setVar('{$CodIDField}',			'CodSocioTipo');
	$objTemp->SetVarsBASH();
	$objTemp->BuildRows('id=sociotipo','{field:','}',$ROW_SocioTipo);
	$objTemp->BuildRows('id=sociotipopred','{field:','}',$ROW_SocioTipo);
	$outputHTML = $objTemp->echohtml();
	unset($objTemp);

	echo $outputHTML;
?>