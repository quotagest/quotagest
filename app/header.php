<?php
	ob_start('ob_gzhandler');	#Output Buffering - BEGIN
	#ob_start();	#Output Buffering - BEGIN
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');

	$objTempHeader = new TobjTemplate($_SERVER['DOCUMENT_ROOT'].'/header.html');
	$objTempHeader->setVar('{$titulo}', ((!isset($titulo)) ? 'QuotaGest' : $titulo));
	$objTempHeader->setVar('{$PATH_APP_CSS}', SETPATH('URL','PATH_APP_CSS'));
	$objTempHeader->setVar('{$PATH_APP_JS}', SETPATH('URL','PATH_APP_JS'));
	$objTempHeader->setVar('{$PATH_APP}', SETPATH('URL','PATH_APP'));
	$objTempHeader->setVar('{$PATH_APP_IMG_ASSOCS}', SETPATH('URL','PATH_APP_IMG_ASSOCS'));
	$objTempHeader->setVar('{$PATH_APP_PAGES}', SETPATH('URL','PATH_APP_PAGES'));
	$objTempHeader->setVar('{$PATH_APP_IMG_ICONS}', SETPATH('URL','PATH_APP_IMG_ICONS'));
	$objTempHeader->setVar('{$PATH_APP_UI_ASSOCIACAO}', SETPATH('URL','PATH_APP_UI_ASSOCIACAO'));
	$objTempHeader->setVar('{$PATH_APP_UI_QUOTAS}', SETPATH('URL','PATH_APP_UI_QUOTAS'));
	$objTempHeader->setVar('{$PATH_APP_UI_SOCIOS}', SETPATH('URL','PATH_APP_UI_SOCIOS'));
	$objTempHeader->setVar('{$PATH_APP_UI_MODALIDADES}', SETPATH('URL','PATH_APP_UI_MODALIDADES'));
	#$objTempHeader->setVar('{$PATH_APP_UI_MODULES}', SETPATH('URL','PATH_APP_UI_MODULES'));
	$objTempHeader->setVar('{$PATH_APP_UI_ENTIDADES}', SETPATH('URL','PATH_APP_UI_ENTIDADES'));
	$objTempHeader->setVar('{$PATH_APP_UI_CFG}', SETPATH('URL','PATH_APP_UI_CFG'));
	$objTempHeader->setVar('{$PATH_APP_UI_RELATORIOS}', SETPATH('URL','PATH_APP_UI_RELATORIOS'));
	#$objTempHeader->setVar('{$DOCUMENT_ROOT/sidebar.php}', $_SERVER['DOCUMENT_ROOT'].'/sidebar.php');
	
	$objTempHeader->setVar('{$ASSOCIACAO_LOGOTIPO}', SETPATH('URL','PATH_APP_IMG_ASSOCS').$img_assoc);

	$relocatelogin = '';
	if ($NEW_ACCOUNT === false)
	{
		if (($INLOGINPAGE === false) && ($DO_LOGIN))
		{
			if ((stripos($_SERVER['PHP_SELF'],'recover-password') === false) &&
				(stripos($_SERVER['PHP_SELF'],'create-beta') === false))
			{
				$relocatelogin = '<script>window.location.href="'.SETPATH('URL','PATH_APP_PAGES').'login.php";</script>';				
			}
		}		
	}
	$objTempHeader->setVar('{$relocate-login}', $relocatelogin);

	if (!$SHOWMENU)
		$objTempHeader->replaceDOM('[data-id=header-menu]','outertext','');
	if (!$DO_LOGIN)
		$objTempHeader->replaceDOM('[data-id=header-menu-login]','outertext','');
	else
		$objTempHeader->replaceDOM('[data-id=header-menu-full]','outertext','');

	if ($NEW_ACCOUNT)
	{
		$objTempHeader->replaceDOM('[data-id=header-menu]','outertext','');
	}

	############################## TOP MENU ##############################
	$html_topmenu  		= '';
	$html_modules_css 	= '';
	$html_modules_jss 	= '';

	$dbTable1 = new dbTable();
	$SQL = 'SELECT modulo FROM modulos WHERE id_assoc="'.$id_assoc.'"';
	$ROW = $dbTable1->getRESULTS($SQL);
	if (isset($ROW[0]))
	{
		$topmenu_modules = array();
		$CSS_files 		 = array();
		$JS_files 		 = array();
		$path 			 = SETPATH('ROOT','PATH_APP_UI_MODULES');
		$ccI 			 = count($ROW);
		for ($i=0; $i<$ccI; $i++)
		{ 
			echo '<!-- '.$path.$ROW[$i]['modulo'].'.mdl.php'.' -->';
			if (file_exists($path.$ROW[$i]['modulo'].'.mdl.php'))
			{
				require_once($path.$ROW[$i]['modulo'].'.mdl.php');
			}
		}
		$html_topmenu  		= implode("\n",$topmenu_modules);
		$html_modules_css 	= implode("\n",$CSS_files);
		$html_modules_jss 	= implode("\n",$JS_files);
	}
	#var_dump($dbTable1->encryptVar(''));
	unset($dbTable1);

	$objTempHeader->setVar('{$topmenu-module}', $html_topmenu);
	$objTempHeader->setVar('{$MODULES_CSS}', $html_modules_css);
	$objTempHeader->setVar('{$MODULES_JS}', $html_modules_jss);
	#######################################################################
	##### LEFT MENU #####
	$menusidebar = '';
	if(isset($arrLeftMenu))
	{
		foreach($arrLeftMenu as $item)
		{
			if (isset($item['titulo']))
			{
				if (isset($item['javascript']))
				{
					$item['href'] 		= 'javascript:void(0);';
					$item['javascript'] = 'onClick="'.$item['javascript'].'"';
				}
				else
				{
					$item['javascript'] = '';
				}

				$menusidebar .= '<li><a class="item" href="'.$item['href'].'" '.$item['javascript'].'>'.$item['titulo'].'</a></li>';
			}
		}
	}
	$objTempHeader->setVar('{$menu-sidebar}', $menusidebar);

	if ((isset($id_assoc)) && ($id_assoc === ''))
	{
		$objTempHeader->setVar('{$LINovaAssoc}','<li><a href="'.SETPATH('URL','PATH_APP_UI_ASSOCIACAO').'novo.php">Nova Associação</a>');				
	}
	else
	{
		$objTempHeader->setVar('{$LINovaAssoc}','');
	}

	#echo '<!-- ';
	#var_dump($_SESSION);
	#echo ' -->';
	echo $objTempHeader->echohtml();
	unset($objTempHeader);
?>