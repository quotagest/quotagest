<?php
	
	class TobjModuleTopMenu
	{
		protected $topmenu_title	= '';
		protected $topmenu_href_path= '';
		protected $topmenu_href_file= '';
		protected $topmenu_src_path	= '';
		protected $topmenu_src_file	= '';
		protected $topmenu_options 	= array();

		public function __construct()
		{

		}
		public function __destruct()
		{
			unset($topmenu_title);
			unset($topmenu_href_path);
			unset($topmenu_href_file);
			unset($topmenu_src_path);
			unset($topmenu_src_file);
			unset($topmenu_options);
		}

		public function setTopMenuItem($ATitle,$AHrefPath,$AHrefFile,$ASrcPath='',$ASrcFile='')
		{
			$this->setTopMenu_Title($ATitle);
			$this->setTopMenu_HrefPath($AHrefPath);
			$this->setTopMenu_HrefFile($AHrefFile);
			$this->setTopMenu_SrcPath($ASrcPath);
			$this->setTopMenu_SrcFile($ASrcFile);
		}
		private function setTopMenu_Title($AValue) 	 { $this->topmenu_title 	= $AValue; }
		private function setTopMenu_HrefPath($AValue){ $this->topmenu_href_path = $AValue; }
		private function setTopMenu_HrefFile($AValue){ $this->topmenu_href_file = $AValue; }
		private function setTopMenu_SrcPath($AValue) { $this->topmenu_src_path 	= $AValue; }
		private function setTopMenu_SrcFile($AValue) { $this->topmenu_src_file 	= $AValue; }
		public function addItem($Atitle,$Ahref_path,$Ahref_file)
		{
			$this->topmenu_options[] = '<li><a href="'.$Ahref_path.$Ahref_file.'">'.$Atitle.'</a></li>';
		}

		private function getTitle()
		{
			$href = $this->topmenu_href_path.$this->topmenu_href_file;
			$src  = $this->topmenu_src_path.$this->topmenu_src_file;

			$html_title  = '<a title="'.$this->topmenu_title.'" href="'.$href.'">'."\n";
			$html_title .= '<span>'.$this->topmenu_title.'</span>'."\n";
			$html_title .= ($src !== '') ? '<img src="'.$src.'" class="alt-ico">'."\n" : '';
			$html_title .= '</a>'."\n";
			return $html_title;
		}
		private function getItems()
		{
			return implode("\n", $this->topmenu_options);
		}
		public function buildTopMenu()
		{
			$title = $this->getTitle();
			$items = $this->getItems();
			#$items 		= '';
			#foreach($this->topmenu_options as $item)
			#{
			#	$items .= '<li><a href="'.$item['href_path'].$item['href_file'].'">'.$item['title'].'</a></li>';
			#}
			$html = '<li>'.$title.'<ul>'.$items.'</ul></li>';

			return $html;
		}
	}
?>