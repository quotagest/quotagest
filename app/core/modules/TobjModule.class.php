<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_MODULES').'TobjModuleTopMenu.class.php');

	class TobjModule extends TobjModuleTopMenu
	{
		private $ModuleName = '';
		private $ModuleCSS 	= ''; #array
		private $ModuleJS 	= ''; #array
		private $ModuleFD 	= '';
		private $ModuleFP	= '';
		# adicionar folder CSS e JS ao "animais/"

		public function __construct()
		{
			parent::__construct();
		}
		public function __destruct()
		{
			parent::__destruct();
		}

		public function setModuleTopMenu($ATitle,$AHrefFile,$ASrcPath='',$ASrcFile='')
		{
			parent::setTopMenuItem($ATitle,$this->ModuleFP,$this->ModuleFD.$AHrefFile,$ASrcPath,$ASrcFile);
		}
		public function addModuleMenuItem($Atitle,$Ahref_file)
		{
			parent::addItem($Atitle,$this->ModuleFP,$this->ModuleFD.$Ahref_file);
		}

		public function setModuleName($AValue)	 		{ $this->ModuleName = $AValue; }
		public function setModulePath($APath)	 		{ $this->ModuleFP 	= SETPATH('URL',$APath); }
		public function setModuleFolder($AFolderName)	{ $this->ModuleFD 	= $AFolderName; }
		public function setModuleCSS($AFileName)
		{
			if (file_exists($this->ModuleFP.$this->ModuleFD.'css/'.$AFileName))
				$this->ModuleCSS = 'css/'.$AFileName;
		}
		public function setModuleJS($AFileName)
		{
			if (file_exists($this->ModuleFP.$this->ModuleFD.'js/'.$AFileName))
			$this->ModuleJS = 'js/'.$AFileName;
		}
		public function getModuleCSS() 		{ return ($this->ModuleCSS !== '') ? '<link rel="stylesheet" href="'.$this->ModuleFP.$this->ModuleFD.$this->ModuleCSS.'">' : ''; }
		public function getModuleJS() 		{ return ($this->ModuleJS !== '')  ? '<script type="text/javascript" src="'.$this->ModuleFP.$this->ModuleFD.$this->ModuleJS.'"></script>' : ''; }
		public function getModuleTopMenu() 	{ return parent::buildTopMenu(); }
	}
?>