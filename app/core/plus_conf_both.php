<?php
	/* ************************************************************************************************************ */
	/* 																												*/
	/* 	ACÇÃO		: SELECTION																						*/
	/* 	TABELA		: ALL															 								*/
	/* 																												*/
	/* 	CAMPOS_IN	:																								*/
	/* 		- $_SESSION['id_user']																					*/
	/* 		- $_SESSION['id_assoc']																					*/
	/* 		- $_POST['encrypt']																						*/
	/* 																												*/
	/* 	CAMPOS_OUT	:																								*/
	/* 		- $Result																								*/
	/* 																												*/
	/* ************************************************************************************************************ */
	
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
	
	/* FOLDER VARS */
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'db_tables.php');

	if (isset($_POST['encrypt']) && ($_POST['encrypt'] !== ''))
	{
		$aEncrypt 	= explode('#',$_POST['encrypt']);
		$ID_MASTER 	= decrypt($aEncrypt[0],'N5KyEXAnDdwGL4eV');
		$TABLE		= $aEncrypt[1];

		switch($TABLE)
		{
			case 'pagforma'		: {
									$tblName					= 'pag_forma';
									$arrWFNExist				= array('id','id_assoc');
									$arrWFVExist				= array($ID_MASTER,$id_assoc);
									
									$arrFieldsINSERT			= $arr_PAG_FORMA_FIELDS;
									$arrValuesINSERT			= array('NULL',$id_assoc,$_POST['Nome'],$id_user,$id_user,date('Y-m-d H:i:s'),date('Y-m-d H:i:s'),'0');
									$arrTypesINSERT				= $arr_PAG_FORMA_TYPES;
									$arrRequiINSERT				= array(false,true,true,true,true,true,true,true);

									$arrFieldsUPDATE			= REMOVE_ARRAY_FIELD_BYARRAY($arr_PAG_FORMA_FIELDS,array(0,1,3,5,7));
									$arrValuesUPDATE			= array($_POST['Nome'],$id_user,date('Y-m-d H:i:s'));
									$arrTypesUPDATE				= REMOVE_ARRAY_FIELD_BYARRAY($arr_PAG_FORMA_TYPES,array(0,1,3,5,7));
									$arrRequiUPDATE				= array(true,true,true);
								  } break;
			case 'pagopcao'		: {
									$tblName					= 'pag_opcao';
									$arrWFNExist				= array('id','id_assoc');
									$arrWFVExist				= array($ID_MASTER,$id_assoc);
									
									$arrFieldsINSERT			= $arr_PAG_OPCAO_FIELDS;
									$arrValuesINSERT			= array('NULL',$id_assoc,$_POST['Nome'],$id_user,$id_user,date('Y-m-d H:i:s'),date('Y-m-d H:i:s'),'0');
									$arrTypesINSERT				= $arr_PAG_OPCAO_TYPES;
									$arrRequiINSERT				= array(false,true,true,true,true,true,true,true);

									$arrFieldsUPDATE			= REMOVE_ARRAY_FIELD_BYARRAY($arr_PAG_OPCAO_FIELDS,array(0,1,3,5,7));
									$arrValuesUPDATE			= array($_POST['Nome'],$id_user,date('Y-m-d H:i:s'));
									$arrTypesUPDATE				= REMOVE_ARRAY_FIELD_BYARRAY($arr_PAG_OPCAO_TYPES,array(0,1,3,5,7));
									$arrRequiUPDATE				= array(true,true,true);
								  } break;
			case 'socioestado'	: {
									$tblName					= 'socio_estado';
									$arrWFNExist				= array('id','id_assoc');
									$arrWFVExist				= array($ID_MASTER,$id_assoc);
									
									$arrFieldsINSERT			= $arr_SOCIO_ESTADO_FIELDS;
									$arrValuesINSERT			= array('NULL',$id_assoc,$_POST['Nome'],$id_user,$id_user,date('Y-m-d H:i:s'),date('Y-m-d H:i:s'),'0');
									$arrTypesINSERT				= $arr_SOCIO_ESTADO_TYPES;
									$arrRequiINSERT				= array(false,true,true,true,true,true,true,true);
									
									$arrFieldsUPDATE			= REMOVE_ARRAY_FIELD_BYARRAY($arr_SOCIO_ESTADO_FIELDS,array(0,1,3,5,7));
									$arrValuesUPDATE			= array($_POST['Nome'],$id_user,date('Y-m-d H:i:s'));
									$arrTypesUPDATE				= REMOVE_ARRAY_FIELD_BYARRAY($arr_SOCIO_ESTADO_TYPES,array(0,1,3,5,7));
									$arrRequiUPDATE				= array(true,true,true);
								  } break;
			case 'sociotipo'	: {
									$_POST['Joia'] = str_replace(' ','',$_POST['Joia']);
									$_POST['Joia'] = str_replace('€','',$_POST['Joia']);
									$_POST['Joia'] = str_replace(',','.',$_POST['Joia']);
									
									$tblName					= 'socio_tipo';
									$arrWFNExist				= array('id','id_assoc');
									$arrWFVExist				= array($ID_MASTER,$id_assoc);
									
									$arrFieldsINSERT			= $arr_SOCIO_TIPO_FIELDS;
									$arrValuesINSERT			= array('NULL',$id_assoc,$_POST['Nome'],$_POST['Joia'],$id_user,$id_user,date('Y-m-d H:i:s'),date('Y-m-d H:i:s'),'0');
									$arrTypesINSERT				= $arr_SOCIO_TIPO_TYPES;
									$arrRequiINSERT				= array(false,true,true,true,true,true,true,true,true);

									$arrFieldsUPDATE			= REMOVE_ARRAY_FIELD_BYARRAY($arr_SOCIO_TIPO_FIELDS,array(0,1,4,6,8));
									$arrValuesUPDATE			= array($_POST['Nome'],$_POST['Joia'],$id_user,date('Y-m-d H:i:s'));
									$arrTypesUPDATE				= REMOVE_ARRAY_FIELD_BYARRAY($arr_SOCIO_TIPO_TYPES,array(0,1,4,6,8));
									$arrRequiUPDATE				= array(true,true,true,true);
								  } break;
			case 'modalidade-new': {									
									$tblName					= 'modalidade';
									$arrWFNExist				= array('id','id_assoc');
									$arrWFVExist				= array($ID_MASTER,$id_assoc);
									
									$arrFieldsINSERT			= $arr_MODALIDADES_FIELDS;
									$arrValuesINSERT			= array('NULL',$id_assoc,$_POST['Nome'],$id_user,$id_user,date('Y-m-d H:i:s'),date('Y-m-d H:i:s'),'0');
									$arrTypesINSERT				= $arr_MODALIDADES_TYPES;
									$arrRequiINSERT				= array(false,true,true,true,true,true,true,true,true);

									$arrFieldsUPDATE			= REMOVE_ARRAY_FIELD_BYARRAY($arr_MODALIDADES_FIELDS,array(0,1,3,5,7));
									$arrValuesUPDATE			= array($_POST['Nome'],$id_user,date('Y-m-d H:i:s'));
									$arrTypesUPDATE				= REMOVE_ARRAY_FIELD_BYARRAY($arr_MODALIDADES_TYPES,array(0,1,3,5,7));
									$arrRequiUPDATE				= array(true,true,true);
								  } break;
			case 'modalidade'	: {									
									$tblName					= 'modalidade';
									$arrWFNExist				= array('id','id_assoc');
									$arrWFVExist				= array($ID_MASTER,$id_assoc);
									
									$arrFieldsINSERT			= $arr_MODALIDADES_FIELDS;
									$arrValuesINSERT			= array('NULL',$id_assoc,$_POST['Nome'],$id_user,$id_user,date('Y-m-d H:i:s'),date('Y-m-d H:i:s'),'0');
									$arrTypesINSERT				= $arr_MODALIDADES_TYPES;
									$arrRequiINSERT				= array(false,true,true,true,true,true,true,true,true);

									$arrFieldsUPDATE			= REMOVE_ARRAY_FIELD_BYARRAY($arr_MODALIDADES_FIELDS,array(0,1,3,5,7));
									$arrValuesUPDATE			= array($_POST['Nome'],$id_user,date('Y-m-d H:i:s'));
									$arrTypesUPDATE				= REMOVE_ARRAY_FIELD_BYARRAY($arr_MODALIDADES_TYPES,array(0,1,3,5,7));
									$arrRequiUPDATE				= array(true,true,true);
								  } break;
			case 'modsocnivel'	: {									
									$ID_MODALIDADE = decrypt($_POST['modsocnivel'],'N5KyEXAnDdwGL4eV');
									
									$tblName					= 'mod_socio_nivel';
									$arrWFNExist				= array('id','id_assoc','id_modalidade');
									$arrWFVExist				= array($ID_MASTER,$id_assoc,$ID_MODALIDADE);
									
									$arrFieldsINSERT			= $arr_MOD_SOCIO_NIVEL_FIELDS;
									$arrValuesINSERT			= array('NULL',$id_assoc,$ID_MODALIDADE,$_POST['Nome'],$id_user,$id_user,date('Y-m-d H:i:s'),date('Y-m-d H:i:s'),'0');
									$arrTypesINSERT				= $arr_MOD_SOCIO_NIVEL_TYPES;
									$arrRequiINSERT				= array(false,true,true,true,true,true,true,true,true,true);

									$arrFieldsUPDATE			= REMOVE_ARRAY_FIELD_BYARRAY($arr_MOD_SOCIO_NIVEL_FIELDS,array(0,1,2,4,6,8));
									$arrValuesUPDATE			= array($_POST['Nome'],$id_user,date('Y-m-d H:i:s'));
									$arrTypesUPDATE				= REMOVE_ARRAY_FIELD_BYARRAY($arr_MOD_SOCIO_NIVEL_TYPES,array(0,1,2,4,6,8));
									$arrRequiUPDATE				= array(true,true,true);
								  } break;
			case 'modturma'		: {									
									$ID_MODALIDADE = decrypt($_POST['modturma'],'N5KyEXAnDdwGL4eV');
									
									$tblName					= 'mod_turma';
									$arrWFNExist				= array('id','id_assoc','id_modalidade');
									$arrWFVExist				= array($ID_MASTER,$id_assoc,$ID_MODALIDADE);
									
									$arrFieldsINSERT			= $arr_MOD_TURMA_FIELDS;
									$arrValuesINSERT			= array('NULL',$id_assoc,$ID_MODALIDADE,$_POST['Nome'],$id_user,$id_user,date('Y-m-d H:i:s'),date('Y-m-d H:i:s'),'0');
									$arrTypesINSERT				= $arr_MOD_TURMA_TYPES;
									$arrRequiINSERT				= array(false,true,true,true,true,true,true,true,true,true);
									
									$arrFieldsUPDATE			= REMOVE_ARRAY_FIELD_BYARRAY($arr_MOD_TURMA_FIELDS,array(0,1,2,4,6,8));
									$arrValuesUPDATE			= array($_POST['Nome'],$id_user,date('Y-m-d H:i:s'));
									$arrTypesUPDATE				= REMOVE_ARRAY_FIELD_BYARRAY($arr_MOD_TURMA_TYPES,array(0,1,2,4,6,8));
									$arrRequiUPDATE				= array(true,true,true);
								  } break;
		}
		include(SETPATH('ROOT','PATH_APP_CORE_DB').'db_actions.php');
	}
?>