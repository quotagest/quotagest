<?php
	/* *********************************************************************************** */
	/* ****************************** PROTECCAO DE STRINGS ******************************* */
	function ms_escape_stringFuntion($data)
	{
		$non_displayables = array(
		'/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
		'/%1[0-9a-f]/',             // url encoded 16-31
		'/[\x00-\x08]/',            // 00-08
		'/\x0b/',                   // 11
		'/\x0c/',                   // 12
		'/[\x0e-\x1f]/'             // 14-31
		);
		foreach ($non_displayables as $regex)
		$data = preg_replace($regex, '', $data);
		#$data = str_replace("'", "''", $data );
		
		return $data;
	}
	function escapeFunction($str)
	{
		$search  = array("\\","\0","\n","\r","\x1a","'",'"');
		$replace = array("\\\\","\\0","\\n","\\r","\Z","\'",'\"');
		return str_replace($search,$replace,$str);
	}
	function StringProtect($AValue)
	{
		$AValue	= trim($AValue);
		$AValue = ms_escape_stringFuntion($AValue);
		$AValue = escapeFunction($AValue);
		$AValue	= utf8_decode($AValue);
		return $AValue;
	}
	/* *********************************************************************************** */
	/* *********************************************************************************** */
	function ExistValue_inArray($sValue, $aArray)
	{
		foreach($aArray as $key => $array)
		{
			foreach($array as $key2 => $value2)
			{
				if ($value2 === $sValue)
					return true;
			}
		}
		return false;
	}
	/* *********************************************************************************** */
	/* *********************************************************************************** */
	function POST_SQLSAFE(&$aArrayPOST)
	{
		foreach ($aArrayPOST as $field => $value)
		{
			$aArrayPOST[$field] = StringProtect($value);
		}
	}
	
	function SpaceReplace($text)
	{
		$text = preg_replace("/\s*/","",$text);
		/*
		$ccI = strlen($text);
		for ($i=0; $i<$ccI; $i++)
		{
			if ($text[$i] === ' ')
				$text[$i] = '';
		}
		*/
		return $text;
	}

	function string_to_float(&$num)
	{
		$valid = false;
		
		$num = str_replace('€',' ',$num);
		$num = trim($num);
		$num = str_replace(',','.',$num);
		$num = (float)$num;
		//number_format($num, 2, '.', '');	
		if (is_float($num))
			$valid = true;
			
		return $valid;
	}
	function is_valid_date($str)
	{
		$stamp = strtotime( $str );
		$result = false;
		if (!is_numeric($stamp))
		{
			$result = false;
		}
		$month = date( 'm', $stamp );
		$day   = date( 'd', $stamp );
		$year  = date( 'Y', $stamp );
		 
		if (checkdate($month, $day, $year))
		{
			$result = true;
		 }
		 
		 return $result;
	}
	function IsSetIsNumeric(&$Var)
	{
		$result = false;
		if (isset($Var))
			if (is_numeric($Var))
				$result = true;
		return $result;
	}
	/* http://www.lateralcode.com/creating-a-random-string-with-php/ */
	/* http://www.totallyphp.co.uk/create-a-random-password */
	/* http://www.laughing-buddha.net/php/password */
	/* 
	 * The letter l (lowercase L) and the number 1 
	 * have been removed, as they can be mistaken 
	 * for each other. 
	 */ 
	function rand_string($length)
	{
		$chars = "abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ023456789";	
		$str   = '';
		$size = strlen($chars);
		for( $i = 0; $i < $length; $i++ )
		{
			$str .= $chars[ rand(0, $size - 1) ];
		}

		return $str;
	}
	
	function IndexOfArray($AValue, $AArray)
	{
		$key = -1;
		$size = count($AArray);

		for ($i=0; $i<$size; $i++)
		{
			if ($AValue === $AArray[$i])
			{
				$key = $i;
				break;
			}
		}
		return $key;
	}

	/**
	 * params[1]: $sType 		# GET or POST
	 * params[2]: $aFieldsName 	# Array of Fields Name to check
	 * return: 	  $Result 		# Boolean {False|True}
	 */
	function ValidarCampos($sType, $aFieldsName)
	{
		$result = false;
		$ccI  	= count($aFieldsName);
		for ($i=0; $i<$ccI; $i++)
		{
			$value  = '';
			$result = false;
			if ($sType === 'GET')
			{
				if (isset($_GET[$aFieldsName[$i]]))
				{
					$value = trim($_GET[$field]);
				}
			}
			else
			if ($sType === 'POST')
			{
				if (isset($_POST[$aFieldsName[$i]]))
				{
					$value = trim($_POST[$field]);
				}
			}

			if ($value != '')
				$result = true;
			else
				break;
		}
		return $result;
	}
?>