<?php
	/* *********************************************************************
	*  Author: Paulo Mota (paulomota2@gmail.com)
	*  Web...: N/A
	*  Name..: dbTable.class.php
	*  Desc..: MySQLi Class
	*  Date..: 05/09/2013
	*  Date-update: 26/02/2014
	*
	*********************************************************************** */
	
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);

	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	
	class dbTable
	{
		const HOST_NAME = '127.0.0.1';
		const HOST_USER = 'qgest_admin';
		const HOST_PASS = 'MepTMoWXPACT62TTWX3045';
		const DB_NAME   = 'qgest_quotages_db';
		const PASSCRYPT = 'N5KyEXAnDdwGL4eV';
		
		var $ConnectionOut 	= false;
		var $conLink		= null;
		var $TableName 		= '';

		var $FieldsInfo 	= array();
		var $FieldsValue 	= array();
		/* ********** SETFUNCTIONS ********** */
		/* ********************************** */
		public function setTableName($vVar)		{ $this->TableName 	 	= $vVar; }
		public function setFieldsInfo($vVar)	{ $this->FieldsInfo  	= $vVar; }
		public function setFieldsValue($vVar)	{ $this->FieldsValue 	= $vVar; }
		/* ********************************** */
		/* ********** GETFUNCTIONS ********** */
		public function getInsertedID() 	{ return $this->conLink->insert_id; }
		public function getDBConnection() 	{ return $this->conLink; }
		private function getFieldsName()
		{
			$Result = array();
			foreach($this->FieldsInfo as $key => $value)
			{
				$Result[] = $key;
			}
			return $Result;
		}
		/* ********************************** */
		private function ERROR_CONNECT()
		{
			die('<b>Erro Ligação MSG:</b> ' .mysqli_connect_error().'<br/>'.
				'<b>Erro Ligação NUM:</b> ' .mysqli_connect_errno().'<br/>');
		}
		private function ERROR_SQL()
		{
			die('<b>Erro SQL MSG:</b> ' .mysqli_error($this->conLink).'<br/>'.
				'<b>Erro SQL NUM:</b> ' .mysqli_errno($this->conLink).'<br/>');
		}
		public function __construct($ADBConnection=false)
		{
			$this->ConnectDB($ADBConnection);
		}
		public function __destruct()
		{
			$this->DisconnectDB();
			unset($this->FieldsInfo);
			unset($this->FieldsValue);
		}

		public function ConnectDB($ADBConnection)
		{
			if ($ADBConnection !== false)
			{
				$this->conLink = $ADBConnection;
				$this->ConnectionOut = true;
			}
			else
			{
				try
				{
					mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
					
					$this->conLink = new mysqli($this::HOST_NAME,$this::HOST_USER,$this::HOST_PASS,$this::DB_NAME);
					if (mysqli_connect_errno())
					{
						$this->ERROR_CONNECT();
					}
				}
				catch (Exception $e)
				{
					die($e->getMessage());
				}
			}
		}
		public function DisconnectDB()
		{
			if (!$this->ConnectionOut)
			{
				if (!$this->conLink)
					ERROR_CONNECT();
				else
					mysqli_close($this->conLink);
			}
		}
		
		/* *********************************************************************************** */
		/* ****************************** FUNCOES GERAIS - HTML ****************************** */
		public function DATA_EXTENSO($aData)
		{
			$meses = array('Janeiro','Fevereiro','Março','Abril','Maio','Junho',
							'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
			$dia = date('d',strtotime($aData));
			$mes = date('m',strtotime($aData));
			$ano = date('Y',strtotime($aData));
			return $dia.' de '.$meses[$mes-1].' de '.$ano;
		}
		public function decodeJSONtoArray($AJSON)
		{
			$jsonData = base64_decode($AJSON);
			$jsonData = urldecode($jsonData);
			$jsonData = utf8_encode($jsonData);
			$jsonData = json_decode($jsonData,true);
			return $jsonData;
		}
		/* *********************************************************************************** */
		/* ****************************** PROTECCAO DE STRINGS ******************************* */
		private function ms_escape_string($data)
		{
			$non_displayables = array(
			'/%0[0-8bcef]/', 			# url encoded 00-08, 11, 12, 14, 15
			'/%1[0-9a-f]/', 			# url encoded 16-31
			'/[\x00-\x08]/', 			# 00-08
			'/\x0b/', 					# 11
			'/\x0c/', 					# 12
			'/[\x0e-\x1f]/' 			# 14-31
			);

			foreach ($non_displayables as $regex)
			{
				$data = preg_replace($regex, '', $data);
			}

			return $data;
		}
		private function escape($str)
		{
			$search  = array("\\","\0","\n","\r","\x1a","'",'"');
			$replace = array("\\\\","\\0","\\n","\\r","\Z","\'",'\"');
			return str_replace($search,$replace,$str);
		}
		public function StringProtect($AValue,$AEncode=true)
		{
			$AValue = trim($AValue);
			$AValue = $this->ms_escape_string($AValue);
			$AValue = $this->escape($AValue);
			if ($AEncode) $AValue = utf8_decode($AValue);
			return $AValue;
		}
		public function protectVar($AValue)
		{
			return $this->StringProtect($AValue);
		}
		
		/* *********************************************************************************** */
		public function encryptVar($AValue)
		{
			return encrypt($AValue,$this::PASSCRYPT);
		}
		public function decryptVar($AValue)
		{
			return decrypt($AValue,$this::PASSCRYPT);
		}
		
		public function br2nl($text)
		{
			return preg_replace('/<br\\s*?\/??>/i', "", $text);
		}
		public function isStrFloat(&$num,$charReplace)
		{
			$num = str_replace($charReplace,' ',$num);
			$num = trim($num);
			$num = str_replace(',','.',$num);
			$num = (float)$num;
			return (is_float($num)) ? true : false;
		}
		public function isStrDate(&$str)
		{
			$stamp = strtotime( $str );
			$result = false;
			if (!is_numeric($stamp))
			{
				$result = false;
			}
			$month = date( 'm', $stamp );
			$day   = date( 'd', $stamp );
			$year  = date( 'Y', $stamp );

			if (checkdate($month, $day, $year))
			{
				$str    = $year.'-'.$month.'-'.$day;
				$result = true;
			}

			return $result;
		}
		public function setAsInteger($AValue)
		{
			preg_match_all('!\d+!', $AValue, $AValue);
			return implode('',$AValue[0]);
		}
		public function setAsFloat($AValue)
		{
			preg_match_all('!\d+\.*\d*!', $AValue, $AValue);
			return implode('',$AValue[0]);
		}
		public function setAsDate($AValue)
		{
			if (!$this->isStrDate($AValue))
			{
				$AValue = '';
			}
			return $AValue;
		}
		public function getAsString($AValue)
		{
			return utf8_encode($AValue);
		}
		public function getAsMemo($AValue)
		{
			return nl2br($this->getAsString($AValue));
		}
		public function getAsInteger()
		{
			preg_match_all('!\d+!', $AValue, $AValue);
			return implode('',$AValue[0]);
		}
		public function getAsFloat($AValue, $ADecimals=2)
		{
			return number_format($AValue, $ADecimals, '.', '');
		}
		public function getAsDate($AValue, $AOtherDate, $AInput=false)
		{
			$sType = ($AInput) ? 'Y-m-d' : 'd-m-Y';
			$AValue = str_replace('0000-00-00','',$AValue);
			$AValue = ($AValue !== '') ? date($sType,strtotime($AValue)) : $AOtherDate;
			return $AValue;
		}
		
		/* *********************************************************************************** */
		/* ************************* SQL QUERY SEARCH ************************* */
		public function processArraySearch($AarrFieldName,$AarrValue)
		{
			if ((isset($AarrFieldName[0])) && (isset($AarrValue[0])))
			{
				if ((!is_array($AarrValue)) || (count($AarrValue)==1))
				{
					$arrAux = array();
					$isArr  = is_array($AarrValue);
					$isStr	= (!$isArr);
		
					$ccI 	= count($AarrFieldName);
					for ($i=0; $i<$ccI; $i++)
					{
						$arrAux[] = ($isArr) ? $AarrValue[0] : $AarrValue;
					}
		
					$Result = $arrAux;
				}
			}
			else
			{
				$Result = array();
				$Result[0] = '*';
			}
			return $Result;
		}
		/* ************************* PAGINACAO ************************* */
		public function getTotalPages($ASQL,$AiLimPerPage)
		{
			$Result = 0;
			$pos 	= stripos($ASQL,'FROM');
			$AuxSQL = substr($ASQL,$pos);
			$pos 	= stripos($AuxSQL,'LIMIT');
			$AuxSQL = substr($AuxSQL,0,$pos);
			$AuxSQL = "SELECT COUNT(*) AS 'total' ".$AuxSQL;
			
			$ROW = $this->getRows($AuxSQL);
			if (isset($ROW[0]))
			{
				$total = $ROW[0]['total'];
				$AiLimPerPage = ($AiLimPerPage <= 0) ? 1 : $AiLimPerPage;
				$Result = ceil($total/$AiLimPerPage);
				#http://php.net/manual/en/function.round.php
			}
			return $Result;
		}

		public function TotalRows($sTableName)
		{
			$Result	= 0;

			$SQL = 'SELECT COUNT(*) AS "total_rows" FROM '.$sTableName;
			$ROW = $this->getRows($SQL);
			if (isset($ROW[0]))
			{
				$Result = $ROW[0]['total_rows'];
			}

			return $Result;
		}
		/* *********************************************************************************** */
		public function SQL_SELECT($strTable, $arrFieldName, $vFieldsNameWHERE, $vFieldsValueWHERE, $strLimit = '0,1', $strOther = '')
		{
			$WHERE	= $this->WHEREUPDATE_PARSER($vFieldsNameWHERE,$vFieldsValueWHERE,' AND ');
			$LIMIT	= ($strLimit !== '') ? 'LIMIT '.$strLimit : '';
			$SQL	= 'SELECT '.implode(", ",$arrFieldName).' FROM '.$strTable.' WHERE '.$WHERE.' '.$strOther.' '.$LIMIT.';';

			return $this->getRows($SQL);
		}
		/* *********************************************************************************** */
		#http://dadomingues.blogspot.pt/2008/10/select-ignorando-acento.html
		private function WHEREUPDATE_PARSER($vFieldsName,$vFieldsValue,$strGlue,$sSignal='=',$bProtect=true)
		{
			$sP 		= ($bProtect === true) ? '`' : '';
			$count 		= count($vFieldsName);
			$arrWHERE 	= array();
			$k 			= 0;

			if ($sSignal === '=')
			{
				$sBegin = "'";
				$sEnd 	= "' ";
			}
			else
			if ($sSignal === 'LIKE')
			{
				$sBegin = " _utf8 '%";
				$sEnd 	= "%' ";
			}
			for ($i=0; $i<$count; $i++)
			{
				if ($vFieldsValue[$i] !== false)
				{
					$arrWHERE[$k] = $sP.$vFieldsName[$i].$sP.' '.$sSignal.' '.$sBegin.$vFieldsValue[$i].$sEnd;
					$k++;
				}
			}
			return implode($strGlue,$arrWHERE);
		}
		/* *********************************************************************************** */
		private function outputValues(&$aValues, $arrFieldsType)
		{
			foreach ($aValues as $keyI => &$arrValue)
			{
				$k=0;
				foreach ($arrValue as $key => &$value)
				{
					if ($value !== '')
					{
						switch($arrFieldsType[$k])
						{
							case 'STRING'	: { $value = $this->getAsString($value); 					} break;
							case 'STRINGINT': {} break;
							case 'STRINGNEW': {} break;
							case 'INTEGER'	: { $value = wordwrap($value,3,' ',true); 					} break;
							case 'FLOAT'	: { $value = $this->getAsFloat($value); 					} break;
							case 'DATE'		: { $value = $this->getAsDate($value,'0000-00-00'); 		} break;
							case 'DATEINPUT': { $value = $this->getAsDate($value,'',true); 				} break;
							case 'DATEINPUTNOW': { $value = $this->getAsDate($value,date('Y-m-d'),true);} break;
							case 'DATETIME'	: {} break;
							case 'MEMO'		: { $value = $this->getAsMemo($value); 						} break;
							case 'ENCRYPT'	: { $value = $this->encryptVar($value); 					} break;
						}
					}
					$k++;
				}
			}
		}
		#se (_POST !== false) 
		#se (_fieldrequired === false)
		#se (_POST !== "")
		private function validateValues()
		{
			foreach($this->FieldsValue as $key => &$value)
			{
				if (($value !== false) && ($value !== ''))
				{
					$value = $this->StringProtect($value);
					switch($this->FieldsInfo[$key]['type'])
					{
						case 'STRING'	: {} break;
						case 'STRINGINT': { $value = $this->setAsInteger($value); 	} break;
						case 'STRINGNEW': { $value = str_replace(' ','',$value); 	} break;
						case 'INTEGER'	: { 
											if ($value !== "NULL") # IF field name == 'id'
											{
												$value = $this->setAsInteger($value);
											}
										  } break;
						case 'FLOAT'	: { $value = $this->setAsFloat($value); 	} break;
						case 'DATE'		: { $value = $this->setAsDate($value); 		} break;
						case 'DATETIME'	: {} break;
						case 'MEMO'		: {} break;
						case 'DECRYPT'	: { $value = $this->decryptVar($value); 	} break;
					}
				}
				else
				if (($value === '') && ($this->FieldsInfo[$key]['required'] === true))
				{
					return false;
				}
			}

			return true;
		}
		/* *********************************************************************************** */
		public function ExecSQL($vSQL)
		{
			$vSQL 	= $this->ms_escape_string($vSQL);
			$Result = $this->conLink->query($vSQL) or $this->ERROR_SQL();

			return $Result;
		}
		public function BuildSQL($sAction,$vFieldsNameWHERE='',$vFieldsValueWHERE='')
		{
			switch($sAction)
			{
				case 'INSERT': 	{
									$arrFields 	= $this->getFieldsName();
									$sqlFields 	= "`".implode("`,`",$arrFields)."`";
									$sqlValues 	= implode("','",$this->FieldsValue);
									$SQL 		= 'INSERT INTO '.$this->TableName.' ('.$sqlFields.") VALUES ('".$sqlValues."');";
								} break;
				case 'UPDATE': 	{
									$arrFields 	= $this->getFieldsName();
									$FValues 	= s$this->WHEREUPDATE_PARSER($arrFields, $this->FieldsValue,' , ');
									$WHERE		= $this->WHEREUPDATE_PARSER($vFieldsNameWHERE,$vFieldsValueWHERE,' AND ');
									$SQL 		= 'UPDATE '.$this->TableName.' SET '.$FValues.' WHERE '.$WHERE.';';
								} break;
				case 'DELETE': 	{
									$WHERE 		= $this->WHEREUPDATE_PARSER($vFieldsNameWHERE,$vFieldsValueWHERE,' AND ');
									$SQL 		= 'DELETE FROM '.$this->TableName.' WHERE '.$WHERE.';';
								} break;
			}

			return $SQL;
		}
		public function getRows($vSQL)
		{
			$aARRAY = array();

			$res = $this->ExecSQL($vSQL);
			if (($res) && ($this->conLink->affected_rows > 0))
			{
				while ($row = $res->fetch_assoc())
				{
					$aARRAY[] = $row;
				}
				mysqli_free_result($res);
			}

			return $aARRAY;
		}
		public function Build2RunQuery($sAction,$vFieldsNameWHERE='',$vFieldsValueWHERE='')
		{
			$Result = false;
			if ($this->validateValues())
			{
				$SQL 	= $this->BuildSQL($sAction,$vFieldsNameWHERE,$vFieldsValueWHERE);
				$Result = $this->ExecSQL($SQL);
			}
			return $Result;
		}
		public function getResultsOut($sSQL,$arrFieldsTypeOut)
		{
			$EXIST 	= false;
			$ROW 	= $this->getRows($sSQL);
			
			if ($EXIST = isset($ROW[0]))
			{
				$this->outputValues($ROW,$arrFieldsTypeOut);
			}
			
			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
	}
?>