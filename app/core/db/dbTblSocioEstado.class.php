<?php
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	
	class dbTblSocioEstado extends dbTable
	{
		public $ID_PAGFORMA_ENCRYPTED = '';
		public $ID_PAGFORMA_DECRYPTED = '';
		public $ID_USER = '';
		public $ID_ASSOC= '';

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName("socio_estado");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_USER);
			unset($this->ID_SSOC);
			unset($this->ID_SOCIOESTADO_ENCRYPTED);
			unset($this->ID_SOCIOESTADO_DECRYPTED);
		}

		public function setFieldsInformation()
		{
			parent::SetFieldsName(array('id','id_assoc','nome','id_user_created','id_user_edited','data_criacao','data_alterado','enabled'));
			parent::SetFieldsType(array('INTEGER','INTEGER','STRING','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));
			parent::setFieldsRequired(array(true,true,true,false,true,true,true,true,true));
		}

		public function set_IDSOCIOESTADO($AsVar)
		{
			$this->ID_SOCIOESTADO_ENCRYPTED = $AsVar;
			$this->ID_SOCIOESTADO_DECRYPTED = ($AsVar !== '') ? parent::decryptVar($AsVar) : '';;
		}
		public function set_IDUSER($AsVar)  { $this->ID_USER = $AsVar; }
		public function set_IDASSOC($AsVar) { $this->ID_ASSOC = $AsVar; }

		public function INSERT($A_POST)
		{
			$Result = false;
			$NEW_ID = false;

			if ($this->ID_ASSOC !== '')
			{
				$this->SetFieldsValue(array('NULL',
											$this->ID_ASSOC,
											$A_POST['Nome'],
											$this->ID_USER,
											$this->ID_USER,
											date('Y-m-d H:i:s'),
											date('Y-m-d H:i:s'),
											'0')
										);
				$Result = false;
				$NEW_ID = 0;

				if (parent::checkInformation())
				{
					$SQL 	= parent::BuildSQL('INSERT');
					$Result = parent::ExecSQL($SQL);
					$NEW_ID = parent::getInsertedID();
					$NEW_ID = parent::encryptVar($NEW_ID);
				}
			}
			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ID);
		}
		public function UPDATE($A_POST)
		{
			parent::setFieldsValue(array(false,
										false,
										$A_POST['Nome'],
										false,
										$this->ID_USER,
										false,
										date('Y-m-d H:i:s'),
										false)
										);
			$Result = false;

			if (($this->ID_SOCIOESTADO_DECRYPTED !== '') && (parent::checkInformation())) 
			{
				$SQL 	= $this->BuildSQL('UPDATE',array('id_assoc','id'),array($this->ID_ASSOC,$this->ID_SOCIOESTADO_DECRYPTED));
				$Result = parent::ExecSQL($SQL);
			}

			return $Result;
		}
		
		public function getListSearch($AarrFieldName,$AarrValue, $FieldToOrder,$OrderBy, $iPage=0, $iLimPerPage=10)
		{
			$ROW 	= array();
			$EXIST 	= false;
			$TotalPages = 0;
			$AarrValue  = parent::processArraySearch($AarrFieldName,$AarrValue);
			
			$OrderBy = ($OrderBy === '') ? ' ASC ' : $OrderBy;
			switch ($FieldToOrder)
			{
				case 'nome'			: $FieldToOrder = 'socio_estado.nome '.$OrderBy; break;
				default 			: $FieldToOrder = 'socio_estado.nome '.$OrderBy; break;
			}
			$FieldToOrder = ' ORDER BY '.$FieldToOrder;
	
			$sSQLLIKE = ($AarrValue[0] === '*') ? '' : ' AND ('.parent::WHEREUPDATE_PARSER($AarrFieldName,$AarrValue,' OR ','LIKE',false).') ';
			$sLIMIT = parent::build_LIMITStatement($iPage,$iLimPerPage);
			#$sLIMIT   = ((isset($iPage)) && ($iPage!=='') && ($iPage > 1)) ? ' LIMIT '.(($iPage-1)*$iLimPerPage).','.($iLimPerPage) : (' LIMIT 0,'.$iLimPerPage) ;

			$SQL  = "SELECT socio_estado.id AS 'id_socioestado',
							socio_estado.nome AS 'nome'
					";
			$SQL .= ' FROM socio_estado ';
			$SQL .= ' WHERE socio_estado.id_assoc="'.$this->ID_ASSOC.'" ';
			$SQL .= ' AND socio_estado.enabled="0" ';
			$SQL .= $sSQLLIKE;
			$SQL .= $FieldToOrder;
			$SQL .= $sLIMIT;

			$ROW = parent::getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				parent::setFieldsType(array('ENCRYPT','STRING'));
				parent::FieldsOutput($ROW);
				
				$EXIST 		= true;
				#$TotalPages = parent::getTotalPages($SQL,$iLimPerPage);
			}
			
			return array('ROW'=>$ROW, 'EXIST'=>$EXIST/*, 'TotalPages'=>$TotalPages*/);
		}
	}
?>