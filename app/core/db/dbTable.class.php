<?php
	/* *********************************************************************
	*  Author: Paulo Mota (paulomota2@gmail.com)
	*  Web...: N/A
	*  Name..: dbTable.class.php
	*  Desc..: MySQLi Class
	*  Date..: 05/09/2013
	*
	*********************************************************************** */
	
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);

	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblActionHistory.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'pagination.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'ctt.api.class.php');
	
	class dbTable
	{
		const HOST_NAME = '127.0.0.1';
		const HOST_USER = 'qgest_admin';
		const HOST_PASS = 'MepTMoWXPACT62TTWX3045';
		const DB_NAME   = 'qgest_quotages_db';

		#const HOST_USER = 'root';
		#const HOST_PASS = '';
		
		var $ConnectionOut 	= false;
		var $conLink		= null;
		var $TableName 		= '';

		var $FieldsName 	= array();
		var $FieldsValue 	= array();
		var $FieldsType		= array();
		var $FieldsRequired = array();

		/* ********** SETFUNCTIONS ********** */
		/* ********************************** */
		public function setTableName($vVar)		{ $this->TableName 	 	= $vVar; }
		public function setFieldsName($vVar)	{ $this->FieldsName  	= $vVar; }
		public function setFieldsValue($vVar)	{ $this->FieldsValue 	= $vVar; }
		public function setFieldsType($vVar)	{ $this->FieldsType  	= $vVar; }
		public function setFieldsRequired($vVar){ $this->FieldsRequired = $vVar; }
		/* ********************************** */
		/* ********** GETFUNCTIONS ********** */
		public function getInsertedID() 	{ return $this->conLink->insert_id; }
		public function getDBConnection() 	{ return $this->conLink; }
		/* ********************************** */
		private function ERROR_CONNECT()
		{
			die('<b>Erro Ligação MSG:</b> '.mysqli_connect_error().'<br/>'.
				'<b>Erro Ligação NUM:</b> '.mysqli_connect_errno().'<br/>');
		}
		private function ERROR_SQL()
		{
			die('<b>Erro SQL MSG:</b> '.mysqli_error($this->conLink).'<br/>'.
				'<b>Erro SQL NUM:</b> '.mysqli_errno($this->conLink).'<br/>');
		}
		public function __construct($ADBConnection=false)
		{
			$this->ConnectDB($ADBConnection);
		}
		public function __destruct()
		{
			$this->DisconnectDB();
			unset($this->FieldsName);
			unset($this->FieldsValue);
			unset($this->FieldsType);
			unset($this->FieldsRequired);
		}

		public function ConnectDB($ADBConnection)
		{
			if ($ADBConnection !== false)
			{
				$this->conLink = $ADBConnection;
				$this->ConnectionOut = true;
			}
			else
			{
				try
				{
					mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
					
					$this->conLink = new mysqli($this::HOST_NAME,$this::HOST_USER,$this::HOST_PASS,$this::DB_NAME);
					if (mysqli_connect_errno())
					{
						$this->ERROR_CONNECT();
					}
				}
				catch (Exception $e)
				{
					die($e->getMessage());
				}
			}
		}
		public function DisconnectDB()
		{
			if (!$this->ConnectionOut)
			{
				if (!$this->conLink)
					ERROR_CONNECT();
				else
					mysqli_close($this->conLink);
			}
		}
		
		/* *********************************************************************************** */
		/* ****************************** PROTECCAO DE STRINGS ******************************* */
		private function ms_escape_string($data)
		{
			$non_displayables = array(
			'/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
			'/%1[0-9a-f]/',             // url encoded 16-31
			'/[\x00-\x08]/',            // 00-08
			'/\x0b/',                   // 11
			'/\x0c/',                   // 12
			'/[\x0e-\x1f]/'             // 14-31
			);

			foreach ($non_displayables as $regex)
			{
				$data = preg_replace($regex, '', $data);
			}

			return $data;
		}
		private function escape($str)
		{
			$search  = array("\\","\0","\n","\r","\x1a","'",'"');
			$replace = array("\\\\","\\0","\\n","\\r","\Z","\'",'\"');
			return str_replace($search,$replace,$str);
		}
		public function StringProtect($AValue,$AEncode=true)
		{
			$AValue	= trim($AValue);
			$AValue = $this->ms_escape_string($AValue);
			$AValue = $this->escape($AValue);
			if ($AEncode) $AValue = utf8_decode($AValue);
			return $AValue;
		}
		public function protectVar($AValue)
		{
			return $this->StringProtect($AValue);
		}
		public function FieldsOutput(&$aValues)
		{
			#$ccI = count($aValues);
			#for ($i=0; $i<$ccI; $i++)
			foreach ($aValues as $keyI => &$arrValue)
			{
				$k=0;
				foreach ($arrValue as $key => &$value)
				{
					if ($value !== '')
					{
						switch($this->FieldsType[$k])
						{
							case 'STRING'	: { $value = $this->getAsString($value); 					} break;
							case 'STRINGINT': {} break;
							case 'STRINGNEW': {} break;
							case 'INTEGER'	: { $value = wordwrap($value,3,' ',true); 					} break;
							case 'FLOAT'	: { $value = $this->getAsFloat($value); 					} break;
							case 'DATE'		: { $value = $this->getAsDate($value,'0000-00-00'); 		} break;
							case 'DATEINPUT': { $value = $this->getAsDate($value,'',true); 				} break;
							case 'DATEINPUTNOW': { $value = $this->getAsDate($value,date('Y-m-d'),true);} break;
							case 'DATETIME'	: {} break;
							case 'MEMO'		: { $value = $this->getAsMemo($value); 						} break;
							case 'ENCRYPT'	: { $value = $this->encryptVar($value); 					} break;
						}
						#$aValues[$i][$key] = $value;
					}
					$k++;
				}
			}
		}
		/* *********************************************************************************** */
		public function br2nl($text)
		{
			return preg_replace('/<br\\s*?\/??>/i', "", $text);
		}
		public function isStrFloat(&$num,$charReplace)
		{
			$num = str_replace($charReplace,' ',$num);
			$num = trim($num);
			$num = str_replace(',','.',$num);
			$num = (float)$num;
			return (is_float($num)) ? true : false;
		}
		public function isStrDate(&$str)
		{
			$stamp = strtotime( $str );
			$result = false;
			if (!is_numeric($stamp))
			{
				$result = false;
			}
			$month = date( 'm', $stamp );
			$day   = date( 'd', $stamp );
			$year  = date( 'Y', $stamp );

			if (checkdate($month, $day, $year))
			{
				$str    = $year.'-'.$month.'-'.$day;
				$result = true;
			}

			return $result;
		}
		public function setAsInteger($AValue)
		{
			preg_match_all('!\d+!', $AValue, $AValue);
			return implode('',$AValue[0]);
		}
		public function setAsFloat($AValue)
		{
			preg_match_all('!\d+\.*\d*!', $AValue, $AValue);
			return implode('',$AValue[0]);
		}
		public function setAsDate($AValue)
		{
			if (!$this->isStrDate($AValue))
			{
				$AValue = '';
			}
			return $AValue;
		}
		public function getAsString($AValue)
		{
			return utf8_encode($AValue);
		}
		public function getAsMemo($AValue)
		{
			return nl2br($this->getAsString($AValue));
		}
		public function getAsInteger()
		{
			preg_match_all('!\d+!', $AValue, $AValue);
			return implode('',$AValue[0]);
		}
		public function getAsFloat($AValue, $ADecimals=2)
		{
			return number_format($AValue, $ADecimals, '.', '');
		}
		public function getAsDate($AValue, $AOtherDate, $AInput=false)
		{
			$sType = ($AInput) ? 'Y-m-d' : 'd-m-Y';
			$AValue = str_replace('0000-00-00','',$AValue);
			$AValue = ($AValue !== '') ? date($sType,strtotime($AValue)) : $AOtherDate;
			return $AValue;
		}
		/* *********************************************************************************** */
		
		##SELECT BUILD
		public function build_ORStatments($Asqlfield,$AArray)
		{
			$SQLAux = array();
			foreach ($AArray as $key => $value)
			{
				$SQLAux[] = $Asqlfield.'="'.$this->decryptVar($value).'"';
			}
			return ' AND ('.implode(' OR ',$SQLAux).')';
		}
		#$sLIMIT   = ((isset($iPage)) && ($iPage!=='') && ($iPage > 1)) ? ' LIMIT '.(($iPage-1)*$iLimPerPage).','.($iLimPerPage) : (' LIMIT 0,'.$iLimPerPage) ;
		public function build_LIMITStatement($AiPage=0,$AiLimPerPage=10)
		{
			$sLIMIT = '';
			if (($AiLimPerPage!=='') && ($AiLimPerPage > 1))
			{
				if (($AiPage!=='') && ($AiPage > 1))
				{
					$sLIMIT = (($AiPage-1)*$AiLimPerPage).','.($AiLimPerPage);
				}
				else
				{
					$sLIMIT = '0,'.$AiLimPerPage;
				}
			}
			
			$sLIMIT = ' LIMIT '.$sLIMIT;
			
			if (($AiPage === '') && ($AiLimPerPage === ''))
			{
				$sLIMIT = '';
			}
			return $sLIMIT;
		}
		/* *********************************************************************************** */
		/* ************************* SQL QUERY SEARCH ************************* */
		public function processArraySearch($AarrFieldName,$AarrValue)
		{
			if ((isset($AarrFieldName[0])) && (isset($AarrValue[0])))
			{
				if ((!is_array($AarrValue)) || (count($AarrValue)==1))
				{
					$arrAux = array();
					$isArr  = is_array($AarrValue);
					$isStr	= (!$isArr);
		
					$ccI 	= count($AarrFieldName);
					for ($i=0; $i<$ccI; $i++)
					{
						$arrAux[] = ($isArr) ? $AarrValue[0] : $AarrValue;
					}
		
					$Result = $arrAux;
				}
			}
			else
			{
				$Result = array();
				$Result[0] = '*';
			}
			return $Result;
		}
		/* ************************* PAGINACAO ************************* */
		public function getTotalPages($ASQL,$AiLimPerPage)
		{
			$Result = 0;
			$pos 	= stripos($ASQL,'FROM');
			$AuxSQL = substr($ASQL,$pos);
			$pos 	= stripos($AuxSQL,'LIMIT');
			$AuxSQL = substr($AuxSQL,0,$pos);
			$AuxSQL = "SELECT COUNT(*) AS 'total' ".$AuxSQL;
			
			$ROW = $this->getRESULTS($AuxSQL);
			if (isset($ROW[0]))
			{
				$total = $ROW[0]['total'];
				$AiLimPerPage = ($AiLimPerPage <= 0) ? 1 : $AiLimPerPage;
				$Result = ceil($total/$AiLimPerPage);
				#http://php.net/manual/en/function.round.php
			}
			return $Result;
		}

		public function TotalRows($sTableName)
		{
			$Result	= 0;

			$SQL = 'SELECT COUNT(*) AS "total_rows" FROM '.$sTableName;
			$ROW = $this->getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				$Result = $ROW[0]['total_rows'];
			}

			return $Result;
		}
		public function getPagination($sTableName,$sPage,$iItemsPerPage=10,$iViewPagesMax=10)
		{
			$iTotalRows = $this->TotalRows($sTableName);
			$iTotalPages = round($iTotalRows/$iItemsPerPage);

			$p = new pagination;
			$p->items($iTotalRows);
			$p->limit($iItemsPerPage);
			$p->currentPage($sPage);
			$p->parameterName("page");
			$HTML = $p->show();

			return $HTML;
			# 1.,2,3,4,5,6,7,8,9,10,11,12,13,14,15
			# 1,2,3,4,5,6,...,14,15
			# 1,2,...,8,9,10.,11,12,...,14,15
		}
		/* *********************************************************************************** */
		#http://dadomingues.blogspot.pt/2008/10/select-ignorando-acento.html
		public function WHEREUPDATE_PARSER($vFieldsName,$vFieldsValue,$strGlue,$sSignal='=',$bProtect=true)
		{
			$sP 		= ($bProtect === true) ? '`' : '';
			$count 		= count($vFieldsName);
			$arrWHERE 	= array();
			$k 			= 0;

			if ($sSignal === '=')
			{
				$sBegin = "'";
				$sEnd 	= "' ";
			}
			else
			if ($sSignal === 'LIKE')
			{
				$sBegin = " _utf8 '%";
				$sEnd 	= "%' ";
			}
			for ($i=0; $i<$count; $i++)
			{
				if ($vFieldsValue[$i] !== false)
				{
					$arrWHERE[$k] = $sP.$vFieldsName[$i].$sP.' '.$sSignal.' '.$sBegin.$vFieldsValue[$i].$sEnd;
					$k++;
				}
			}
			return implode($strGlue,$arrWHERE);
		}
		/* *********************************************************************************** */
		#se (_POST !== false) 
		#se (_fieldrequired === false)
		#se (_POST !== "")
		public function checkInformation()
		{
			#$ccI = count($this->FieldsValue);
			$k = 0;
			foreach($this->FieldsValue as $key => &$value)
			{
				if (($value !== false) && ($value !== ''))
				{
					$value = $this->StringProtect($value);
					switch($this->FieldsType[$k])
					{
						case 'STRING'	: {} break;
						case 'STRINGINT': { $value = $this->setAsInteger($value); 	} break;
						case 'STRINGNEW': { $value = str_replace(' ','',$value); 	} break;
						case 'INTEGER'	: { 
											if ($value !== "NULL")							// IF field name == 'id'
											{
												$value = $this->setAsInteger($value);
											}
										  } break;
						case 'FLOAT'	: { $value = $this->setAsFloat($value); 	} break;
						case 'DATE'		: { $value = $this->setAsDate($value); 		} break;
						case 'DATETIME'	: {} break;
						case 'MEMO'		: {} break;
						case 'DECRYPT'	: { $value = $this->decryptVar($value); 	} break;
					}
				}
				else
				if (($value === '') && ($this->FieldsRequired[$k] === true))
				{
					return false;
				}
				$k++;
			}

			return true;
		}
		/* *********************************************************************************** */
		public function checkIfExist($ATableName,$ArrSELECTFields,$ArrWHEREFieldsName,$AarrWHEREFieldsValue)
		{
			$ROW 	= $this->SQL_SELECT($ATableName,$ArrSELECTFields,$ArrWHEREFieldsName,$AarrWHEREFieldsValue);

			return (isset($ROW[0])) ? $ROW : false;
		}
		
		public function ExecSQL($vSQL)
		{
			$vSQL 	= $this->ms_escape_string($vSQL);
			$Result = $this->conLink->query($vSQL) or $this->ERROR_SQL();

			return $Result;
		}
		public function BuildSQL($sAction,$vFieldsNameWHERE='',$vFieldsValueWHERE='')
		{
			switch($sAction)
			{
				case 'INSERT': 	{
									$sqlFields 	= "`".implode("`,`",$this->FieldsName)."`";
									$sqlValues 	= implode("','",$this->FieldsValue);
									$SQL 		= 'INSERT INTO '.$this->TableName.' ('.$sqlFields.") VALUES ('".$sqlValues."');";
								} break;
				case 'UPDATE': 	{
									$FValues 	= $this->WHEREUPDATE_PARSER($this->FieldsName,$this->FieldsValue,' , ');
									$WHERE		= $this->WHEREUPDATE_PARSER($vFieldsNameWHERE,$vFieldsValueWHERE,' AND ');
									$SQL 		= 'UPDATE '.$this->TableName.' SET '.$FValues.' WHERE '.$WHERE.';';
								} break;
				case 'DELETE': 	{
									$WHERE 		= $this->WHEREUPDATE_PARSER($vFieldsNameWHERE,$vFieldsValueWHERE,' AND ');
									$SQL 		= 'DELETE FROM '.$this->TableName.' WHERE '.$WHERE.';';
								} break;
			}

			return $SQL;
		}
		public function SQL_SELECT($strTable, $arrFieldName, $vFieldsNameWHERE, $vFieldsValueWHERE, $strLimit = '0,1', $strOther = '')
		{
			$WHERE	= $this->WHEREUPDATE_PARSER($vFieldsNameWHERE,$vFieldsValueWHERE,' AND ');
			$LIMIT	= ($strLimit !== '') ? 'LIMIT '.$strLimit : '';
			$SQL	= 'SELECT '.implode(", ",$arrFieldName).' FROM '.$strTable.' WHERE '.$WHERE.' '.$strOther.' '.$LIMIT.';';

			return $this->getRESULTS($SQL);
		}
		public function getRESULTS($vSQL)
		{
			$aARRAY = array();

			$res = $this->ExecSQL($vSQL);
			if ($res)
			{
				$num_rows = $this->conLink->affected_rows;
				if ($num_rows > 0)
				{
					while ($row = $res->fetch_assoc())
					{
						$aARRAY[] = $row;
					}
					mysqli_free_result($res);
				}
			}

			return $aARRAY;
		}
		/* *********************************************************************************** */
		/* ****************************** FUNCOES GERAIS - HTML ****************************** */
		public function getImagePath($AFolderName,$AFileName,$AExtFolderName,$AExtFileName)
		{
			$sFileName = SETPATH('ROOT',$AFolderName).$AFileName.'.jpg';
			if (file_exists($sFileName))
			{
				$sImagePath = SETPATH('URL',$AFolderName).$AFileName.'.jpg';
			}
			else
			{
				$sImagePath 	= SETPATH('URL',$AExtFolderName).$AExtFileName;			
			}

			return $sImagePath;
		}
		public function getCTTINFO($ACodigoPostal)
		{
			$concelho = '';
			$distrito = '';
			if ($ACodigoPostal !== '')
			{
				$cttcode = explode('-',$ACodigoPostal);

				$objCTTSearch = new TObj_CTTSearch();
				$json_ctt = $objCTTSearch->getCTTINFO($cttcode[0],$cttcode[1]);
				$json_ctt = (array)json_decode($json_ctt);
				$concelho = (isset($json_ctt['concelho'])) ? $json_ctt['concelho'] : '';
				$distrito = (isset($json_ctt['distrito'])) ? $json_ctt['distrito'] : '';
				unset($objCTTSearch);				
			}

			return array('concelho'=>$concelho, 'distrito'=>$distrito);
		}
		public function DATA_EXTENSO($aData)
		{
			$meses = array('Janeiro','Fevereiro','Março','Abril','Maio','Junho',
							'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
			$dia = date('d',strtotime($aData));
			$mes = date('m',strtotime($aData));
			$ano = date('Y',strtotime($aData));
			return $dia.' de '.$meses[$mes-1].' de '.$ano;
		}
		public function encryptVar($AValue)
		{
			return encrypt($AValue,'N5KyEXAnDdwGL4eV');
		}
		public function decryptVar($AValue)
		{
			return decrypt($AValue,'N5KyEXAnDdwGL4eV');
		}
		public function decodeJSONtoArray($AJSON)
		{
			$jsonData = base64_decode($AJSON);
			$jsonData = urldecode($jsonData);
			$jsonData = utf8_encode($jsonData);
			$jsonData = json_decode($jsonData,true);
			return $jsonData;
		}
/*
		public function AddHistory($ASection,$ATitulo,$ADescricao,$AScriptDone,$AScriptRecover,$AEnabled='0')
		{
			$Result = false;

			$A_POST = array();
			$A_POST['titulo']			= $ASection;
			$A_POST['titulo']			= $ATitulo;
			$A_POST['descricao']		= $ADescricao;
			$A_POST['script_done']		= $AScriptDone;
			$A_POST['script_recover']	= $AScriptRecover;
			$A_POST['enabled']			= $AEnabled;

			$dbActHistory = new dbTblActionHistory();
			$dbActHistory->set_IDHISTORY(0);
			$dbActHistory->set_IDUSER($id_user);
			$dbActHistory->set_IDASSOC($id_assoc);
			$Result = $dbActHistory->INSERT($A_POST);
			unset($A_POST);
			unset($dbActHistory);
			
			return $Result;
		}
		public function setHistory($AID_HISTORY,$AID_ASSOC,$AID_USER, $ASection,$AOperation,$ATitulo,$ADescricao,$AScriptDone,$AScriptRecover,$AEnabled='0')
		{
			$Result = false;

			$A_POST = array();
			$A_POST['section']			= $ASection;
			$A_POST['operation']		= $AOperation;
			$A_POST['titulo']			= $ATitulo;
			$A_POST['descricao']		= $ADescricao;
			$A_POST['script_done']		= $AScriptDone;
			$A_POST['script_recover']	= $AScriptRecover;
			$A_POST['enabled']			= $AEnabled;

			$dbActHistory = new dbTblActionHistory($this->conLink);
			$dbActHistory->set_IDHISTORY($AID_HISTORY);
			$dbActHistory->set_IDUSER($AID_USER);
			$dbActHistory->set_IDASSOC($AID_ASSOC);
			$Result = $dbActHistory->ProcessREQUEST($A_POST);
			unset($A_POST);
			unset($dbActHistory);
			
			return $Result;
		}
		public function unsetHistory($AID_HISTORY,$AID_ASSOC,$AID_USER)
		{
			$Result = false;

			$dbActHistory = new dbTblActionHistory($this->conLink);
			$dbActHistory->set_IDHISTORY($AID_HISTORY);
			$dbActHistory->set_IDUSER($AID_USER);
			$dbActHistory->set_IDASSOC($AID_ASSOC);
			$Result = $dbActHistory->DELETE($A_POST);
			unset($dbActHistory);
			
			return $Result;
		}
*/
		/* *********************************************************************************** */

	}
?>