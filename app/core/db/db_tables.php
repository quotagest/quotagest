<?php
	/* ******************************************************************************** */
	/* 																					*/
	/* 				ARRAY DE TODAS AS TABELAS EXISTENTES NA BASE DE DADOS				*/
	/* 																					*/
	/* 																					*/
	/* 																					*/
	/* ******************************************************************************** */
	
	/* http://www.w3schools.com/sql/sql_datatypes.asp
ALTER TABLE nome_tabela
ADD
(
id_user_created INT(11) not null,
id_user_edited INT(11) not null,
data_criacao DATETIME not null,
data_alterado DATETIME not null
);
ALTER TABLE nome_tabela MODIFY enabled TINYINT(2) AFTER data_alterado;

	*/
	
	global $arr_ASSOCIACAO_FIELDS;
	global $arr_ASSOC_SOCIOS_FIELDS;
	global $arr_ASSOC_MODALIDADES_FIELDS;
	global $arr_ASSOC_UTILIZADORES_FIELDS;
	global $arr_BANCOS_PT_FIELDS;
	global $arr_CTT_CODIGOS_POSTAIS_FIELDS;
	global $arr_CTT_CONCELHOS_FIELDS;
	global $arr_CTT_DISTRITOS_FIELDS;
	global $arr_ENTIDADES_FIELDS;
	global $arr_MODALIDADES_FIELDS;
	global $arr_MOD_SOCIO_FIELDS;
	global $arr_MOD_SOCIO_NIVEL_FIELDS;
	global $arr_MOD_TURMA_FIELDS;
	global $arr_ORGAOS_DEPARTAMENTO_FIELDS;
	global $arr_ORGAOS_GRUPO_FIELDS;
	global $arr_PAG_FORMA_FIELDS;
	global $arr_PAG_OPCAO_FIELDS;
	global $arr_QUOTAS_SOCIOS_FIELDS;
	global $arr_SOCIO_FIELDS;
	global $arr_SOCIO_ESTADO_FIELDS;
	global $arr_SOCIO_TIPO_FIELDS;
	global $arr_USER_EMAILS_TESTE_FIELDS;
	global $arr_USER_ENTIDADES_FIELDS;
	
	global $arr_ASSOCIACAO_TYPES;
	global $arr_ASSOC_SOCIOS_TYPES;
	global $arr_ASSOC_MODALIDADES_TYPES;
	global $arr_ASSOC_UTILIZADORES_TYPES;
	global $arr_BANCOS_PT_TYPES;
	global $arr_CTT_CODIGOS_POSTAIS_TYPES;
	global $arr_CTT_CONCELHOS_TYPES;
	global $arr_CTT_DISTRITOS_TYPES;
	global $arr_ENTIDADES_TYPES;
	global $arr_MODALIDADES_TYPES;
	global $arr_MOD_SOCIO_TYPES;
	global $arr_MOD_SOCIO_NIVEL_TYPES;
	global $arr_MOD_TURMA_TYPES;
	global $arr_ORGAOS_DEPARTAMENTO_TYPES;
	global $arr_ORGAOS_GRUPO_TYPES;
	global $arr_PAG_FORMA_TYPES;
	global $arr_PAG_OPCAO_TYPES;
	global $arr_QUOTAS_SOCIOS_TYPES;
	global $arr_SOCIO_TYPES;
	global $arr_SOCIO_ESTADO_TYPES;
	global $arr_SOCIO_TIPO_TYPES;
	global $arr_USER_EMAILS_TESTE_TYPES;
	global $arr_USER_ENTIDADES_TYPES;
	
	$arr_ASSOCIACAO_FIELDS			= array('id','id_user','nome','abreviatura','nif','nib','telefone','telemovel','fax','email','site','morada','codigo_postal','observacoes','slogan','historia','tipo_actividade','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
	$arr_ASSOCIACAO_TYPES			= array('INTEGER','INTEGER','STRING','STRING','INTEGER','STRINGINT','INTEGER','INTEGER','INTEGER','STRING','STRING','MEMO','STRING','MEMO','STRING','MEMO','INTEGER','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');

	$arr_ASSOC_SOCIOS_FIELDS 		= array('id','id_assoc','id_socio','id_tipo','id_estado','codigo','data_entrada','data_saida','divida','enabled','activo','id_user_created','id_user_edited','data_criacao','data_alterado','observacoes');
	$arr_ASSOC_SOCIOS_TYPES			= array('INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','STRING','DATE','FLOAT','INTEGER','INTEGER','INTEGER','INTEGER','DATETIME','DATETIME','MEMO');
	
	$arr_ASSOC_MODALIDADES_FIELDS	= array('id','id_assoc','id_modalidade','responsavel','id_user_created','id_user_edited','data_criacao','data_alterado','observacoes');
	$arr_ASSOC_MODALIDADES_TYPES	= array('INTEGER','INTEGER','INTEGER','STRING','INTEGER','INTEGER','DATETIME','DATETIME','MEMO');
	
	$arr_ASSOC_UTILIZADORES_FIELDS	= array('id','login','password');
	$arr_ASSOC_UTILIZADORES_TYPES	= array('INTEGER','STRING','STRING');	
	
	$arr_BANCOS_PT_FIELDS			= array('codigo','nome');
	$arr_BANCOS_PT_TYPES			= array('STRING','STRING');
	
	$arr_CTT_CODIGOS_POSTAIS_FIELDS = array('DD','CC','LLLL','LOCALIDADE','ART_COD','ART_TIPO','PRI_PREP','ART_TITULO','SEG_PREP','ART_DESIG','ART_LOCAL','TROCO','PORTA','CLIENTE','CP4','CP3','CPALF');
	$arr_CTT_CODIGOS_POSTAIS_TYPES	= array('STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING');
	
	$arr_CTT_CONCELHOS_FIELDS		= array('DD','CC','DESIG');
	$arr_CTT_CONCELHOS_TYPES		= array('STRING','STRING','STRING');
	
	$arr_CTT_DISTRITOS_FIELDS		= array('DD','DESIG');
	$arr_CTT_DISTRITOS_TYPES		= array('STRING','STRING');
	
	$arr_ENTIDADES_FIELDS			= array('id','nome','abreviatura','nif','nib','telefone','telemovel','fax','email','url','outro1','outro2','codigo_postal','morada','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
	$arr_ENTIDADES_TYPES			= array('INTEGER','STRING','STRING','STRINGINT','INTEGERNEW','STRINGINT','STRINGINT','STRINGINT','STRING','STRING','STRING','STRING','STRING','MEMO','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');
	
	$arr_MODALIDADES_FIELDS			= array('id','id_assoc','nome','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
	$arr_MODALIDADES_TYPES			= array('INTEGER','INTEGER','STRING','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');
	
	$arr_MOD_SOCIO_FIELDS			= array('id','id_assoc','id_modalidade','id_socio','data_entrada','data_saida','desconto','total_divida','total_pago','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
	$arr_MOD_SOCIO_TYPES			= array('INTEGER','INTEGER','INTEGER','INTEGER','DATE','DATE','FLOAT','FLOAT','FLOAT','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');

	$arr_MOD_SOCIO_NIVEL_FIELDS		= array('id','id_assoc','id_modalidade','nome','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
	$arr_MOD_SOCIO_NIVEL_TYPES		= array('INTEGER','INTEGER','INTEGER','STRING','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');
	
	$arr_MOD_TURMA_FIELDS			= array('id','id_assoc','id_modalidade','nome','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
	$arr_MOD_TURMA_TYPES			= array('INTEGER','INTEGER','INTEGER','STRING','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');

	$arr_ORGAOS_DEPARTAMENTO_FIELDS	= array('id','id_user','nome');
	$arr_ORGAOS_DEPARTAMENTO_TYPES	= array('INTEGER','INTEGER','STRING');
	
	$arr_ORGAOS_GRUPO_FIELDS		= array('id','id_assoc','nome');
	$arr_ORGAOS_GRUPO_TYPES			= array('INTEGER','INTEGER','STRING');
	
	$arr_PAG_FORMA_FIELDS			= array('id','id_assoc','nome','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
	$arr_PAG_FORMA_TYPES			= array('INTEGER','INTEGER','STRING','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');
	
	$arr_PAG_OPCAO_FIELDS			= array('id','id_assoc','nome','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
	$arr_PAG_OPCAO_TYPES			= array('INTEGER','INTEGER','STRING','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');

	$arr_QUOTAS_SOCIOS_FIELDS		= array('id','id_assoc','id_modalidade','id_socio','id_pag_forma','id_pag_opcao','descricao','dataquota','isquota','ano','mes','montante','total_pago','total_divida','desconto','total_total','prazo_pagamento','data_pagamento','action','observacoes','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
	$arr_QUOTAS_SOCIOS_TYPES		= array('INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','STRING','DATE','INTEGER','INTEGER','INTEGER','FLOAT','FLOAT','FLOAT','FLOAT','FLOAT','DATE','DATE','INTEGER','MEMO','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');
		
	$arr_SOCIO_FIELDS				= array('id','nome','telefone','telemovel','fax','email','outro1','outro2','morada','codigo_postal','data_nascimento','sexo','bicc','nif','nib','enabled');
	$arr_SOCIO_TYPES				= array('INTEGER','STRING','STRINGINT','STRINGINT','STRINGINT','STRING','STRING','STRING','MEMO','STRING','DATE','STRING','STRINGINT','STRINGINT','STRINGNEW','INTEGER');
	
	$arr_SOCIO_ESTADO_FIELDS		= array('id','id_assoc','nome','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
	$arr_SOCIO_ESTADO_TYPES			= array('INTEGER','INTEGER','STRING','INTEGER','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');
	
	$arr_SOCIO_TIPO_FIELDS			= array('id','id_assoc','nome','joia','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
	$arr_SOCIO_TIPO_TYPES			= array('INTEGER','INTEGER','STRING','FLOAT','INTEGER','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');
	
	$arr_USER_EMAILS_TESTE_FIELDS 	= array('id','email','login','password','nome','apelido','data_criacao','data_alterado','enabled');
	$arr_USER_EMAILS_TESTE_TYPES  	= array('INTEGER','STRING','STRING','STRING','STRING','STRING','DATETIME','DATETIME','INEGER');

	$arr_USER_ENTIDADES_FIELDS		= array('id','id_user','id_entidade','tipo','observacoes','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
	$arr_USER_ENTIDADES_TYPES		= array('INTEGER','INTEGER','INTEGER','INTEGER','MEMO','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');

	function CHANGE_ARRAY_FIELD($arrARRAY,$aIndex,$aNewName)
	{
		$arrARRAY[$aIndex] = $aNewName;
		return $arrARRAY;
	}
	function REMOVE_ARRAY_FIELD($arrARRAY,$aIndex)
	{
		array_splice($arrARRAY,$aIndex,1);
		return $arrARRAY;
	}
	function REMOVE_ARRAY_FIELD_BYARRAY($arrARRAY,$arrIndex)
	{
		$ccI	= count($arrIndex);
		for ($i=($ccI-1); $i>-1; $i--)
		{
			unset($arrARRAY[$arrIndex[$i]]);
			$arrARRAY = array_values($arrARRAY);
		}
		return array_values($arrARRAY);
	}
	
?>