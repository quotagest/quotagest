<?
	/* ************************************************************************************************************ */
	/* 																												*/
	/* 	ACÇÃO		: INSERT & UPDATE																				*/
	/* 	TABELA		: ALL												 											*/
	/* 																												*/
	/* 	CAMPOS_IN	:																								*/
	/* 		- $_SESSION['id_user']																					*/
	/* 		- $_SESSION['id_assoc']																					*/
	/* 																												*/
	/* 		- $tblName					= NOME DA TABELA															*/
	/* 		- $arrWFNExist				= CAMPOS para pesquisa WHERE (existe registo?)								*/
	/* 		- $arrWFVExist				= VALORES para pesquisa WHERE (existe registo?)								*/
	/* 																												*/
	/* 		- $arrFieldsINSERT			= NOME dos CAMPOS para INSERT												*/
	/* 		- $arrValuesINSERT			= VALOR dos CAMPOS para INSERT												*/
	/* 		- $arrTypesINSERT			= TYPE dos CAMPOS para INSERT												*/
	/* 																												*/
	/* 		- $arrFieldsUPDATE			= NOME dos CAMPOS para UPDATE												*/
	/* 		- $arrValuesUPDATE			= VALOR dos CAMPOS para UPDATE												*/
	/* 		- $arrTypesUPDATE			= TYPE dos CAMPOS para UPDATE												*/
	/* 																												*/
	/* 																												*/
	/* 	CAMPOS_OUT	:																								*/
	/* 		- $Result																								*/
	/* 		- $ID_NEW																								*/
	/* 																												*/
	/* ************************************************************************************************************ */

	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	
	$Result = false;

	/*
	http://stackoverflow.com/questions/3834470/preg-match-for-a-date
	*/

	$tbl1 = new dbTable();
	$tbl1->setTableName($tblName);

	// VERIFICAR SE REGISTO JA EXISTE
	$ROW = $tbl1->SQL_SELECT($tblName,array('id'),$arrWFNExist,$arrWFVExist);
	if (isset($ROW[0]))
	{
		$tbl1->SetFieldsName($arrFieldsUPDATE);
		$tbl1->SetFieldsValue($arrValuesUPDATE);
		$tbl1->SetFieldsType($arrTypesUPDATE);
		$tbl1->setFieldsRequired($arrRequiUPDATE);

		if ($tbl1->checkInformation())
		{
			$SQL = $tbl1->BuildSQL('UPDATE',$arrWFNExist,$arrWFVExist);
			$res = $tbl1->ExecSQL($SQL);

			$Result = $res;			
		}
	}
	else // REGISTO NAO EXISTE!
	{
		$tbl1->SetFieldsName($arrFieldsINSERT);
		$tbl1->SetFieldsValue($arrValuesINSERT);
		$tbl1->SetFieldsType($arrTypesINSERT);
		$tbl1->setFieldsRequired($arrRequiINSERT);

		if ($tbl1->checkInformation())
		{
			$SQL = $tbl1->BuildSQL('INSERT');
			$res = $tbl1->ExecSQL($SQL);

			$ID_NEW = $tbl1->encryptVar($tbl1->getInsertedID());
			$Result = $res;

			echo $ID_NEW;
		}
	}
	
	unset($tbl1);

	$resstr = (!$Result) ? 'false' : 'true';
	echo $resstr;
?>