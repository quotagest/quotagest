<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	
	class dbTblActionHistory extends dbTable
	{
		public $ID_HISTORY_ENCRYPTED= '';
		public $ID_HISTORY_DECRYPTED= '';
		public $ID_USER 			= '';
		public $ID_ASSOC 			= '';

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName("action_history");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_USER);
			unset($this->ID_ASSOC);
		}

		public function setFieldsInformation()
		{
			# OPERATION
			# 1 - INSERT
			# 2 - UPDATE
			# 3 - DELETE
			parent::SetFieldsName(array('id','id_assoc','section','operation','titulo','descricao','script_done','script_recover','action', 'id_user_created','id_user_edited','data_criacao','data_alterado','enabled'));
			parent::SetFieldsType(array('INTEGER','INTEGER','INTEGER','INTEGER','STRING','MEMO','MEMO','MEMO','INTEGER', 'INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));
			parent::setFieldsRequired(array(true,true,true,true,true,true,true,true,true,true,true,true,true));
		}

		public function set_IDHISTORY($AsVar)
		{
			$this->ID_HISTORY_ENCRYPTED = $AsVar;
			$this->ID_HISTORY_DECRYPTED = parent::decryptVar($AsVar);
		}
		public function set_IDUSER($AsVar)  { $this->ID_USER = $AsVar; }
		public function set_IDASSOC($AsVar) { $this->ID_ASSOC = $AsVar; }

		public function INSERT($A_POST)
		{
			$Result = false;
			$NEW_ID = false;

			$this->SetFieldsValue(array('NULL',
										$this->ID_ASSOC,
										$A_POST['section'],
										$A_POST['operation'],
										$A_POST['titulo'],
										$A_POST['descricao'],
										$A_POST['script_done'],
										$A_POST['script_recover'],
										'0',
										$this->ID_USER,
										$this->ID_USER,
										date('Y-m-d H:i:s'),
										date('Y-m-d H:i:s'),
										'0')
									);

			if (parent::checkInformation())
			{
				$SQL 	= parent::BuildSQL('INSERT');
				$Result = parent::ExecSQL($SQL);
				$NEW_ID = parent::getInsertedID();
				$NEW_ID = parent::encryptVar($NEW_ID);
			}

			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ID);
		}
		public function UPDATE($A_POST)
		{
			$A_POST['section'] 			= (isset($A_POST['section'])) 		? $A_POST['section'] : false;
			$A_POST['operation'] 		= (isset($A_POST['operation'])) 	? $A_POST['operation'] : false;
			$A_POST['titulo'] 			= (isset($A_POST['titulo'])) 		? $A_POST['titulo'] : false;
			$A_POST['descricao'] 		= (isset($A_POST['descricao'])) 	? $A_POST['descricao'] : false;
			$A_POST['script_done'] 		= (isset($A_POST['script_done'])) 	? $A_POST['script_done'] : false;
			$A_POST['script_recover']	= (isset($A_POST['script_recover']))? $A_POST['script_recover'] : false;
			$A_POST['action']			= (isset($A_POST['action']))		? $A_POST['action'] : false;
			$A_POST['enabled']			= (isset($A_POST['enabled'])) 		? $A_POST['enabled'] : false;

			parent::setFieldsValue(array(false,
										 false,
										 $A_POST['section'],
										 $A_POST['operation'],
										 $A_POST['titulo'],
										 $A_POST['descricao'],
										 $A_POST['script_done'],
										 $A_POST['script_recover'],
										 $A_POST['action'],
										 false,
										 $this->ID_USER,
										 false,
										 date('Y-m-d H:i:s'),
										 $A_POST['enabled'])
										);
			$Result = false;

			if (($this->ID_HISTORY_DECRYPTED !== '') && (parent::checkInformation())) 
			{
				$SQL 	= $this->BuildSQL('UPDATE',array('id_assoc','id'),array($this->ID_ASSOC,$this->ID_HISTORY_DECRYPTED));
				$Result = parent::ExecSQL($SQL);
			}

			return $Result;
		}
		public function DELETE()
		{
			$Result = false;
			if ($this->ID_HISTORY_DECRYPTED !== '')
			{
				$SQL 	= parent::BuildSQL('DELETE',array('id','id_assoc'),array($this->ID_HISTORY_DECRYPTED,$this->ID_ASSOC));
				$Result = parent::ExecSQL($SQL);
			}
			return $Result;
		}
		public function ProcessREQUEST($A_POST)
		{
			$Result = false;
			$ResAux = parent::checkIfExist('action_history',array('id'),array('id','id_assoc'),array($this->ID_HISTORY_DECRYPTED,$this->ID_ASSOC));
			$Result = ($ResAux) ? $this->UPDATE($A_POST) : $this->INSERT($A_POST);
			return $Result;
		}

		/* ****************************************************************** */
		/* ****************************************************************** */
		public function getData()
		{
			$ROW	= array();
			$EXIST 	= false;

			if ($this->ID_HISTORY_DECRYPTED !== '') 
			{
				$SQL  = 'SELECT action_history.id AS "id_history",
								action_history.section AS "section",
								action_history.operation AS "operation",
								action_history.titulo AS "titulo",
								action_history.descricao AS "descricao",
								action_history.script_done AS "script_done",
								action_history.script_recover AS "script_recover",
								action_history.action AS "action",

								action_history.id_user_created AS "id_user_created",
								action_history.id_user_edited AS "id_user_edited",
								action_history.data_criacao AS "data_criacao",
								action_history.data_alterado AS "data_alterado",
								action_history.enabled AS "enabled"

						';
				$SQL .= ' FROM action_history ';
				$SQL .= ' WHERE action_history.id_assoc="'.$this->ID_ASSOC.'" ';
				$SQL .= ' AND action_history.id="'.$this->ID_HISTORY_DECRYPTED.'" ';
				$SQL .= ' AND action_history.enabled="0" ';
				$SQL .= ' LIMIT 0,1 ';

				$ROW = parent::getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					parent::setFieldsType(array('ENCRYPT','INTEGER','INTEGER','STRING','MEMO','MEMO','MEMO','INTEGER', 'INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));
					parent::FieldsOutput($ROW);
					
					$EXIST = true;
				}
			}
			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function getListSearch($AarrFieldName,$AarrValue, $FieldToOrder,$OrderBy, $iPage=0, $iLimPerPage=10)
		{
			$ROW 	= array();
			$EXIST 	= false;
			$TotalPages = 0;

			$AarrValue  = parent::processArraySearch($AarrFieldName,$AarrValue);
			
			$OrderBy = ($OrderBy === '') ? ' ASC ' : $OrderBy;
			switch ($FieldToOrder)
			{
				case 'section'		: $FieldToOrder = 'action_history.section '.$OrderBy; 		break;
				case 'titulo'		: $FieldToOrder = 'action_history.titulo '.$OrderBy; 		break;
				case 'data_alterado': $FieldToOrder = 'action_history.data_alterado '.$OrderBy; break;
				default 			: $FieldToOrder = 'action_history.data_alterado '.$OrderBy; break;
			}
			$FieldToOrder = ' ORDER BY '.$FieldToOrder;
	
			$sSQLLIKE = ($AarrValue[0] === '*') ? '' : ' AND ('.parent::WHEREUPDATE_PARSER($AarrFieldName,$AarrValue,' OR ','LIKE',false).') ';
			$sLIMIT = parent::build_LIMITStatement($iPage,$iLimPerPage);
			#$sLIMIT   = ((isset($iPage)) && ($iPage!=='') && ($iPage > 1)) ? ' LIMIT '.(($iPage-1)*$iLimPerPage).','.($iLimPerPage) : (' LIMIT 0,'.$iLimPerPage) ;

			$SQL  = 'SELECT action_history.id AS "id_history",
							action_history.section AS "section",
							action_history.operation AS "operation",
							action_history.titulo AS "titulo",
							action_history.descricao AS "descricao",
							action_history.script_done AS "script_done",
							action_history.script_recover AS "script_recover",
							action_history.action AS "action",

							action_history.id_user_created AS "id_user_created",
							action_history.id_user_edited AS "id_user_edited",
							action_history.data_criacao AS "data_criacao",
							action_history.data_alterado AS "data_alterado",
							action_history.enabled AS "enabled"
					';
			$SQL .= ' FROM action_history ';
			$SQL .= ' WHERE action_history.id_assoc="'.$this->ID_ASSOC.'" ';
			$SQL .= ' AND action_history.enabled="0" ';
			$SQL .= $sSQLLIKE;
			$SQL .= $FieldToOrder;
			$SQL .= $sLIMIT;

			$ROW = parent::getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				parent::setFieldsType(array('ENCRYPT','INTEGER','INTEGER','STRING','MEMO','MEMO','MEMO','INTEGER', 'INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));
				parent::FieldsOutput($ROW);
				
				$EXIST = true;
				$TotalPages = parent::getTotalPages($SQL,$iLimPerPage);
			}
			
			return array('ROW'=>$ROW, 'EXIST'=>$EXIST, 'TotalPages'=>$TotalPages);
		}
	}
?>