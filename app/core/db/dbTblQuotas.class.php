<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	
	class dbTblQuotas extends dbTable
	{
		public $ID_QUOTA_ENCRYPTED 	= '';
		public $ID_QUOTA_DECRYPTED 	= '';
		public $ID_SOCIO_ENCRYPTED 	= '';
		public $ID_SOCIO_DECRYPTED 	= '';
		public $ID_MOD_ENCRYPTED 	= '';
		public $ID_MOD_DECRYPTED 	= '';
		public $ID_USER 			= '';
		public $ID_ASSOC 			= '';

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName("quotas_socios");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_QUOTA_DECRYPTED);
			unset($this->ID_QUOTA_ENCRYPTED);
			unset($this->ID_SOCIO_DECRYPTED);
			unset($this->ID_SOCIO_ENCRYPTED);
			unset($this->ID_MOD_DECRYPTED);
			unset($this->ID_MOD_ENCRYPTED);
			unset($this->ID_USER);
			unset($this->ID_ASSOC);
		}

		public function setFieldsInformation()
		{
			parent::setFieldsName(array('id','id_assoc','id_modalidade','id_socio','id_pag_forma','id_pag_opcao','descricao','dataquota','isquota','ano','mes','montante','total_pago','total_divida','desconto','total_total','prazo_pagamento','data_pagamento','action','observacoes','id_user_created','id_user_edited','data_criacao','data_alterado','enabled'));
			parent::setFieldsType(array('INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','STRING','DATE','INTEGER','INTEGER','INTEGER','FLOAT','FLOAT','FLOAT','FLOAT','FLOAT','DATE','DATE','INTEGER','MEMO','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));
			parent::setFieldsRequired(array(true,true,true,true,false,false,true,true,true,true,true,true,true,true,false,true,false,false,false,false,true,true,true,true,true));
		}
		public function setFieldsInformation_Header()
		{
			parent::setTableName("quotas_header");
			parent::setFieldsName(array('id','id_assoc','id_modalidade','id_socio','ano_inicio','ano_fim','total','total_pago','total_divida','observacoes','id_user_created','id_user_edited','data_criacao','data_alterado','enabled'));
			parent::setFieldsType(array('INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','FLOAT','FLOAT','FLOAT','FLOAT','MEMO','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));
			parent::setFieldsRequired(array(true,true,true,true,true,true,true,true,true,false,true,true,true,true,false,true));
		}
		public function setFieldsInformation_Detalhes()
		{
			parent::setTableName("quotas_detalhes");
			parent::setFieldsName(array('id','id_header','id_assoc','id_modalidade','id_socio','id_pag_forma','id_pag_opcao','descricao','dataquota','isquota','ano','mes','montante','total_pago','total_divida','desconto','total_total','prazo_pagamento','data_pagamento','action','observacoes','id_user_created','id_user_edited','data_criacao','data_alterado','enabled'));
			parent::setFieldsType(array('INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','STRING','DATE','INTEGER','INTEGER','INTEGER','FLOAT','FLOAT','FLOAT','FLOAT','FLOAT','DATE','DATE','INTEGER','MEMO','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));
			parent::setFieldsRequired(array(true,true,true,true,true,false,false,true,true,true,true,true,true,true,true,false,true,false,false,false,false,true,true,true,true,true));
		}

		public function set_IDQUOTA($AsVar)
		{
			$this->ID_QUOTA_ENCRYPTED = $AsVar;
			$this->ID_QUOTA_DECRYPTED = ($AsVar !== '') ? parent::decryptVar($AsVar) : '';		# decrypts a given variable.
		}
		public function set_IDSOCIO($AsVar)
		{
			$this->ID_SOCIO_ENCRYPTED = $AsVar;
			$this->ID_SOCIO_DECRYPTED = ($AsVar !== '') ? parent::decryptVar($AsVar) : '';		# decrypts a given variable.
		}
		public function set_IDMODALIDADE($AsVar)
		{
			$this->ID_MOD_ENCRYPTED = $AsVar;
			$this->ID_MOD_DECRYPTED = ($AsVar !== '') ? parent::decryptVar($AsVar) : '';			# decrypts a given variable.
		}
		public function set_IDUSER($AsVar)  { $this->ID_USER = $AsVar; }
		public function set_IDASSOC($AsVar) { $this->ID_ASSOC = $AsVar; }

		private function checkAno($A_POST)
		{
			$Result = false;

			$ANO1 = (isset($A_POST['ano'])) 	  ? $A_POST['ano'] : 0;
			$ANO2 = (isset($A_POST['ano-outro'])) ? $A_POST['ano-outro'] : 0;
			$AnoAux = ($ANO1!=0) ? $ANO1 : $ANO2;
			$AnoAux = ($AnoAux===0) ? false : $AnoAux;
			preg_match_all('!\d+!', $AnoAux, $value);
			$AnoAux = implode('',$value[0]);
			$Result = (strlen($AnoAux) == 4) ? $AnoAux : $Result;

			return $Result;
		}
		
		public function INSERT_QUOTAS_PERSONAL($A_JPOST)
		{
			$Result = false;
			$NEW_ID = 0;
			
			$this->ID_MOD_DECRYPTED = '70';
			
			$SQL_filtro  = " SELECT mod_socio.id_socio AS 'id_socio' ";
			$SQL_filtro .= " FROM mod_socio ";
			$SQL_filtro .= " LEFT JOIN socio ON socio.id=mod_socio.id_socio ";
			$SQL_filtro .= " LEFT JOIN assoc_socios ON assoc_socios.id=mod_socio.id_socio ";
			$SQL_filtro .= " WHERE mod_socio.id_assoc='".$this->ID_ASSOC."' ";
			$SQL_filtro .= " AND mod_socio.id_modalidade='".$this->ID_MOD_DECRYPTED."' ";
			
			$sql_filter = array();
			if (isset($A_JPOST['_FILTROS']))
			{
				$arrFiltro = $A_JPOST['_FILTROS'];
				foreach($arrFiltro['estado_socio'] as $key => $value)
				{
					if ($value !== 'all')
					{
						$sql_filter[] = 'assoc_socios.id_estado="'.parent::decryptVar($value).'" ';
					}
				}
				foreach($arrFiltro['tipo_socio'] as $key => $value)
				{
					if ($value !== 'all')
					{
						$sql_filter[] = 'assoc_socios.id_tipo="'.parent::decryptVar($value).'" ';
					}
				}
				if ($arrFiltro['sexo'][0] !== 'all')
				{
					$sql_filter[] = ' socio.sexo="'.$arrFiltro['sexo'][0].'" ';
				}
				
				$SQL_filtro .= implode(' AND ',$sql_filter);
				if (count($sql_filter) > 0)
				{
					$SQL_filtro = ' AND '.$SQL_filtro;
				}
			}
			else
			{
				$SQL_filtro .= " AND mod_socio.id_socio='".$this->ID_SOCIO_DECRYPTED."' ";
			}

			if ($this->ID_MOD_DECRYPTED !== '')
			{
				$ROW = parent::getRESULTS($SQL_filtro);
				var_dump($ROW);
				if (isset($ROW[0]))
				{
					$A_JPOST 	= $A_JPOST['_POST'];
					$ano_inicio = $A_JPOST['ano_inicio'];
					$ano_fim 	= $A_JPOST['ano_fim'];
					$total 		= $A_JPOST['total'];
					$tamanho 	= $A_JPOST['length'];
					$SQL_detalhes = $this->buildQuotasINSERT($A_JPOST,$A_JPOST['length']);
					var_dump($SQL_detalhes);
					
					$ccI = count($ROW);
					for($i=0; $i<$ccI; $i++)
					{
						$this->setFieldsInformation_Header(); # ALTERAR NO FIM!!
						$sID_SOCIO = $ROW[$i]['id_socio'];
						
						$arrHeader = array(	'NULL',
											$this->ID_ASSOC,
											$this->ID_MOD_DECRYPTED,
											$sID_SOCIO,
											$ano_inicio,
											$ano_fim,
											$total,
											'0.00',
											$total,
											'',
											$this->ID_USER,
											$this->ID_USER,
											date('Y-m-d H:i:s'),
											date('Y-m-d H:i:s'),
											'0');
						$this->setFieldsValue($arrHeader);
						
						if (parent::checkInformation())
						{
							$SQL = $this->BuildSQL('INSERT');
							$Result = $this->ExecSQL($SQL);
							
							# INSERE LISTAGEM DE QUOTAS
							if ($Result)
							{
								$NEW_ID = parent::getInsertedID();
								
								$SQL = str_ireplace("99919991999",$NEW_ID,$SQL_detalhes);
								$SQL = str_ireplace("99929992999",$sID_SOCIO,$SQL);
								$Result = $this->ExecSQL($SQL);
							}
						}
					}
				}
			}
			
			return array('Result'=>$Result, 'NEW_ID'=>parent::encryptVar($NEW_ID));
		}
		private function buildQuotasINSERT($A_DATA,$A_LENGTH)
		{
			$SQL_detalhes = 'INSERT INTO `quotas_detalhes` (
													`id`,`id_header`,`id_assoc`,`id_modalidade`,`id_socio`,`id_pag_forma`,`id_pag_opcao`,
													`descricao`,`dataquota`,`isquota`,`ano`,`mes`,
													`montante`,`total_pago`,`total_divida`,`desconto`,`total_total`,
													`prazo_pagamento`,`data_pagamento`,
													`action`,`observacoes`,
													`id_user_created`,`id_user_edited`,`data_criacao`,`data_alterado`,`enabled`
												) VALUES ';
			
			$this->setFieldsInformation_Detalhes(); # ALTERAR NO FIM!!
			$arrSQL = array();
			for($i=0; $i<$A_LENGTH; $i++)
			{
				$arrSQL[$i] = array('NULL',
									'99919991999',
									$this->ID_ASSOC,
									$this->ID_MOD_DECRYPTED,
									'99929992999',
									'NULL',
									'NULL',
									$A_DATA[$i]['descricao'],
									($A_DATA[$i]['ano'].'-'.$A_DATA[$i]['lastmonth'].'-01'),
									$A_DATA[$i]['isquota'],
									$A_DATA[$i]['ano'],
									$A_DATA[$i]['lastmonth'],
									$A_DATA[$i]['montante'],
									'0.00',
									$A_DATA[$i]['montante'],
									'0.0',
									$A_DATA[$i]['montante'],
									$A_DATA[$i]['prazpag'],
									'0000-00-00',
									'0',
									'',
									$this->ID_USER,
									$this->ID_USER,
									date('Y-m-d H:i:s'),
									date('Y-m-d H:i:s'),
									'0');
				
				$this->setFieldsValue($arrSQL[$i]);
				if (parent::checkInformation())
				{
					$arrSQL[$i] = "('".implode("','",$this->FieldsValue)."')";
				}
			}
			return $SQL_detalhes.implode(', ',$arrSQL);
		}
		
		public function INSERT_QUOTAS_TODOS($A_POST)
		{
			$Result = true;
			/* VERIFICA SE A MODALIDADE EXISTE */
			$ROW = $this->SQL_SELECT('modalidade',array('id'),array('id','id_assoc'),array($this->ID_MOD_DECRYPTED,$this->ID_ASSOC));
			if (isset($ROW[0]))											// EXISTE MODALIDADE
			{
				$sANO = $this->checkAno($A_POST);
				if ($sANO != false)
				{
					if (isset($A_POST['TipoMensalidade']) && isset($A_POST['Montante']))
					{
						$fMontante = $A_POST['Montante'];

						if ($this->isStrFloat($fMontante,'€'))	// VALOR FLOAT VALIDO
						{
							#RECEBER LISTA DE SOCIOS CONSUANTE FILTRO
							$SQLFiltro_SEXO 	= '';
							$SQLFiltro_TIPO 	= '';
							$SQLFiltro_ESTADO 	= '';
							if (isset($A_POST['SexoSocio']) &&
								isset($A_POST['TipoSocio']) &&
								isset($A_POST['EstadoSocio']) )
							{
								$SQLFiltro_SEXO 	= ($A_POST['SexoSocio'] === 'all') 		? '' : ' AND socio.sexo="'.$A_POST['SexoSocio'].'"';
								$SQLFiltro_TIPO 	= ($A_POST['TipoSocio'] === 'all') 		? '' : parent::build_ORStatments('assoc_socios.id_tipo',$A_POST['TipoSocio']);
								$SQLFiltro_ESTADO 	= ($A_POST['EstadoSocio'] === 'all') 	? '' : parent::build_ORStatments('assoc_socios.id_estado',$A_POST['EstadoSocio']);
							}

							$SQL  = ' SELECT assoc_socios.id_socio AS "id_socio" ';
							$SQL .= ' FROM  mod_socio';
							$SQL .= ' INNER JOIN modalidade ON modalidade.id=mod_socio.id_modalidade';
							$SQL .= ' INNER JOIN assoc_socios ON assoc_socios.id_socio=mod_socio.id_socio';
							$SQL .= ' INNER JOIN socio ON socio.id=mod_socio.id_socio';
							$SQL .= ' WHERE mod_socio.id_assoc="'.$this->ID_ASSOC.'"';
							$SQL .= ' AND mod_socio.id_modalidade="'.$this->ID_MOD_DECRYPTED.'"';
							$SQL .= ' AND mod_socio.enabled="0"';
							$SQL .= $SQLFiltro_SEXO;
							$SQL .= $SQLFiltro_TIPO;
							$SQL .= $SQLFiltro_ESTADO;
							$SQL .= ' ORDER BY mod_socio.id_socio ASC ';
							$ROW = parent::getRESULTS($SQL);
#var_dump($SQL);
#var_dump($ROW);
#die('');
### 							echo($SQL);
### 							echo '<br>###################';
### 							var_dump($A_POST);
### 							echo '<br>###################';
### 							echo '<br>';
							if (isset($ROW[0]))
							{
### 							var_dump($ROW);
### 							echo '<br>';
### 							echo '<br>';
								$ccI = count($ROW);
								for ($i=0; $i<$ccI; $i++)
								{
									$ROW_ID_SOCIO = $ROW[$i]['id_socio'];
									###########################################################################
									###########################################################################
									if (!parent::checkIfExist('quotas_socios',array('id'),array('id_assoc','id_socio','id_modalidade','isquota','ano','enabled'),array($this->ID_ASSOC,$ROW_ID_SOCIO,$this->ID_MOD_DECRYPTED,'1',$sANO,'0')))	# AINDA NAO EXISTEM QUOTAS ADICIONAS
									{
										/* PROCESSA O TIPO DE DISTRIBUICAO E MONTANTE PARA CADA MES/QUOTA */
										$fMontante  = $A_POST['Montante'];
										$nMeses 	= $A_POST['TipoMensalidade'];
										$nDistrib	= $A_POST['Distribuicao'];
										$sDescricao = 'Mensalidade';

										switch($A_POST['TipoMensalidade'])
										{
											case 1	:{
														$nMeses = 12;
														$cmes = 12;
													 } break;
											case 2	: $cmes = 12/$nMeses; break;
											case 3 	: $cmes = 12/$nMeses; break;
											case 4	: $cmes = 12/$nMeses; break;
											case 6	: $cmes = 12/$nMeses; break;
											case 12	:{
														$nMeses = 1;
														$str_descricao = 'Anual';
														$cmes = 1;
													 } break;
										}
										$cmes ++;
										$kmes = 1;

										switch($nDistrib)
										{
											case 1: $fMontante = round(($fMontante/$cmes),5); break;
											case 2: break; # MESMO MONTANTE PARA TODOS
										}
										/* ************************************************************* */
										$meses = array("Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro");
										$iaux = 0;

										for ($iY=$kmes; $iY<$cmes; $iY++)
										{
											if (($nMeses != 12) && ($nMeses != 1))
											{
												$sDescricao = '';
												for($j=$iaux; $j<($iY*$nMeses); $j++)
												{
													$sDescricao .= strtoupper(substr($meses[$j],0,3)).'/';
												}
												$sDescricao = substr($sDescricao,0,-1);
												$iaux = ($iY*$nMeses);
											}
											else
											if ($nMeses == 1)
											{
												$sDescricao = 'Anual';
												$iaux	 = 12;
											}
											else
											{
												$sDescricao = utf8_decode($meses[$iY-1]);
												$iaux	 = $iY;
											}

											/* INSERIR QUOTAS */
											$sDayLast = ($iaux === 2) ? '28' : '30';
											$prazoPag = $sANO.'-'.$iaux.'-'.$sDayLast;
											$arr_QUOTAS_SOCIOS_VALUES = array('NULL',					# id
																			$this->ID_ASSOC,			# id_assoc
																			$this->ID_MOD_DECRYPTED,	# id_modalidade
																			$ROW_ID_SOCIO,				# id_socio
																			'0',						# id_pag_forma
																			'0',						# id_pag_opcao
																			$sDescricao,				# descricao
																			($sANO.'-'.$iaux.'-1'),		# dataquota date('Y-m-d')
																			'1',						# isquota
																			$sANO,						# ano
																			$iaux,						# mes
																			$fMontante,					# montante
																			'0',						# total_pago
																			$fMontante,					# total_divida
																			'0',						# desconto
																			$fMontante,					# total_total
																			$prazoPag,					# prazo_patamento
																			'0000-00-00',				# data_patamento
																			'0',						# action
																			'',							# observacoes
																			$this->ID_USER,				# id_user_created
																			$this->ID_USER,				# id_user_edited
																			date('Y-m-d H:i:s'),		# data_criacao
																			date('Y-m-d H:i:s'),		# data_alterado
																			'0'							# enabled
																			);
											$this->setFieldsValue($arr_QUOTAS_SOCIOS_VALUES);
											$SQL = $this->BuildSQL('INSERT');
###  											var_dump($SQL);
###  											echo '<br>';
### 											echo '<br>';
											$Result .= $this->ExecSQL($SQL);
										}
										$this->ID_SOCIO_DECRYPTED = $ROW_ID_SOCIO;
										$this->updateTOTAL();
### 										echo '<br><br>##########-----#########<br><br>';
									}
									else
									{
										// JA EXISTEM QUOTAS ADICIONAS
									}
									###########################################################################
									###########################################################################
								} # END FOR ROW
								
							}
							else
							{
								# NAO EXISTEM SOCIOS INSCRITOS
							}
						}
						else
						{
							# MONTANTE FLOAT INVALIDO
						}
					}
					else
					{
						# NAO EXISTE TIPO-MENSALIDADE
					}
				}
				else
				{
					# ANO INVALIDO
				}
			}
			else
			{
				# MODALIDADE NAO EXISTE
			}
### 		die('');
			return $Result;
		}

		public function INSERT_QUOTAS_ANO($A_POST)
		{
			$Result = true;
			/* VERIFICA SE A MODALIDADE EXISTE */
			#if (parent::checkIfExist('modalidade',array('id'),array('id','id_assoc'),array($this->ID_MOD_DECRYPTED,$this->ID_ASSOC)))	# EXISTE MODALIDADE
			#{
				$sANO = $this->checkAno($A_POST);
				if ($sANO != false)
				{
					if (isset($A_POST['TipoMensalidade']) && isset($A_POST['Montante']))
					{
						$fMontante = $A_POST['Montante'];

						if ($this->isStrFloat($fMontante,'€'))	// VALOR FLOAT VALIDO
						{
							###########################################################################
							###########################################################################
							if (!parent::checkIfExist('quotas_socios',array('id'),array('id_assoc','id_socio','id_modalidade','isquota','ano','enabled'),array($this->ID_ASSOC,$this->ID_SOCIO_DECRYPTED,$this->ID_MOD_DECRYPTED,'1',$sANO,'0')))	# AINDA NAO EXISTEM QUOTAS ADICIONAS
							{
								/* PROCESSA O TIPO DE DISTRIBUICAO E MONTANTE PARA CADA MES/QUOTA */
								$fMontante  = $A_POST['Montante'];
								$nMeses 	= $A_POST['TipoMensalidade'];
								$nDistrib	= $A_POST['Distribuicao'];
								$sDescricao = 'Mensalidade';

								switch($A_POST['TipoMensalidade'])
								{
									case 1	:{
												$nMeses = 12;
												$cmes = 12;
											 } break;
									case 2	: $cmes = 12/$nMeses; break;
									case 3 	: $cmes = 12/$nMeses; break;
									case 4	: $cmes = 12/$nMeses; break;
									case 6	: $cmes = 12/$nMeses; break;
									case 12	:{
												$nMeses = 1;
												$str_descricao = 'Anual';
												$cmes = 1;
											 } break;
								}
								$cmes ++;
								$kmes = 1;

								switch($nDistrib)
								{
									case 1: $fMontante = round(($fMontante/$cmes),5); break;
									case 2: break; # MESMO MONTANTE PARA TODOS
								}
								/* ************************************************************* */
								$meses = array("Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro");
								$iaux = 0;

								for ($iY=$kmes; $iY<$cmes; $iY++)
								{
									if (($nMeses != 12) && ($nMeses != 1))
									{
										$sDescricao = '';
										for($j=$iaux; $j<($iY*$nMeses); $j++)
										{
											$sDescricao .= strtoupper(substr($meses[$j],0,3)).'/';
										}
										$sDescricao = substr($sDescricao,0,-1);
										$iaux = ($iY*$nMeses);
									}
									else
									if ($nMeses == 1)
									{
										$sDescricao = 'Anual';
										$iaux	 = 12;
									}
									else
									{
										$sDescricao = utf8_decode($meses[$iY-1]);
										$iaux	 = $iY;
									}

									/* INSERIR QUOTAS */
									$sDayLast = ($iaux === 2) ? '28' : '30';
									$prazoPag = $sANO.'-'.$iaux.'-'.$sDayLast;
									$arr_QUOTAS_SOCIOS_VALUES = array('NULL',					# id
																	$this->ID_ASSOC,			# id_assoc
																	$this->ID_MOD_DECRYPTED,	# id_modalidade
																	$this->ID_SOCIO_DECRYPTED,	# id_socio
																	'0',						# id_pag_forma
																	'0',						# id_pag_opcao
																	$sDescricao,				# descricao
																	($sANO.'-'.$iaux.'-1'),		# dataquota date('Y-m-d')
																	'1',						# isquota
																	$sANO,						# ano
																	$iaux,						# mes
																	$fMontante,					# montante
																	'0',						# total_pago
																	$fMontante,					# total_divida
																	'0',						# desconto
																	$fMontante,					# total_total
																	$prazoPag,					# prazo_patamento
																	'0000-00-00',				# data_patamento
																	'0',						# action
																	'',							# observacoes
																	$this->ID_USER,				# id_user_created
																	$this->ID_USER,				# id_user_edited
																	date('Y-m-d H:i:s'),		# data_criacao
																	date('Y-m-d H:i:s'),		# data_alterado
																	'0'							# enabled
																	);
									$this->setFieldsValue($arr_QUOTAS_SOCIOS_VALUES);
									$SQL = $this->BuildSQL('INSERT');
									$Result .= $this->ExecSQL($SQL);
								}
								$this->updateTOTAL();
							}
							else
							{
								// JA EXISTEM QUOTAS ADICIONAS
							}
							###########################################################################
							###########################################################################
						}
						else
						{
							# MONTANTE FLOAT INVALIDO
						}
					}
					else
					{
						# NAO EXISTE TIPO-MENSALIDADE
					}
				}
				else
				{
					# ANO INVALIDO
				}
			#}
			#else
			#{
				# MODALIDADE NAO EXISTE
			#}
### 		die('');
			return $Result;
		}
		public function addQuota($A_POST)
		{
			$Result			= false;
			$NEW_QUOTA_ID	= false;

			if (($this->ID_MOD_DECRYPTED !== '') && ($this->ID_SOCIO_DECRYPTED !== ''))
			{
				#if (parent::checkIfExist('modalidade',array('id'),array('id','id_assoc'),array($this->ID_MOD_DECRYPTED,$this->ID_ASSOC)))
				#{
					$sANO = $this->checkAno($A_POST);
					if ($sANO != false)
					{
						if (isset($A_POST['Montante']) && isset($A_POST['TotalPago']) && isset($A_POST['Desconto']))
						{
							$fMontante 	= $A_POST['Montante'];
							$fDesconto 	= str_replace('','0',$A_POST['Desconto']);
							$fTotalPago = str_replace('','0',$A_POST['TotalPago']);

							if ($this->isStrFloat($fMontante,'€') &&
								$this->isStrFloat($fDesconto,'%') &&
								$this->isStrFloat($fTotalPago,'€') )	// VALOR FLOAT VALIDO
							{
								$fTotalTotal  = ($fMontante) - ($fMontante * ($fDesconto / 100));
								$FTotalDivida = ($fTotalTotal - $fTotalPago);

								$iIDPagForma = (isset($A_POST['MetodoPagamento'])) ? $A_POST['MetodoPagamento'] : '';	#id_pagamento_tipo
								$iIDPagForma = parent::decryptVar($iIDPagForma);
								$iIDPagOpcao = (isset($A_POST['OpcaoPagamento'])) ? $A_POST['OpcaoPagamento'] : '';		#id_pagamento_opcao
								$iIDPagOpcao = parent::decryptVar($iIDPagOpcao);

								$sDescricao  = $A_POST['Descricao'];
								$dPrazoPag	 = $A_POST['data-limite'];
								$dDataPag	 = $A_POST['data-pagamento'];
								$sObservacoes= $A_POST['Observacoes'];

								if ($dPrazoPag === '')
								{
									$dPrazoPag 	= date('Y-m-d');
									$Next7 		= strtotime("+1 week", strtotime($dPrazoPag));
									$dPrazoPag 	= date('Y-m-d',$Next7);
								}

								parent::setFieldsValue(array('NULL',					# id
															$this->ID_ASSOC,			# id_assoc
															$this->ID_MOD_DECRYPTED,	# id_modalidade
															$this->ID_SOCIO_DECRYPTED,	# id_socio
															$iIDPagForma,				# id_pag_forma
															$iIDPagOpcao,				# id_pag_opcao
															$sDescricao,				# descricao
															date('Y-m-d'),				# dataquota
															'0',						# isquota
															$sANO,						# ano
															'0',						# mes
															$fMontante,					# montante
															$fTotalPago,				# total_pago
															$FTotalDivida,				# total_divida
															$fDesconto,					# desconto
															$fTotalTotal,				# total_total
															$dPrazoPag,					# prazo_patamento
															$dDataPag,					# data_patamento
															($FTotalDivida==0), 		# enabled
															$sObservacoes,				# observacoes
															$this->ID_USER,
															$this->ID_USER,
															date('Y-m-d H:i:s'),
															date('Y-m-d H:i:s'),
															'0')
														);
								if (parent::checkInformation())
								{
									$SQL 	= parent::BuildSQL('INSERT');
									$Result = parent::ExecSQL($SQL);

									if ($Result)								# QUOTA INSERIDA COM SUCESSO
									{
										$NEW_QUOTA_ID = parent::getInsertedID();

										$this->updateTOTAL();
									}
								}
							}
						}
					}
				#}
			}

			return array('Result'=>$Result, 'NEW_ID'=>$NEW_QUOTA_ID);
		}
		public function updateQuota($A_POST)
		{
			$Result	= false;

			if (($this->ID_QUOTA_DECRYPTED !== '') && 
				($this->ID_MOD_DECRYPTED !== '') &&
				($this->ID_SOCIO_DECRYPTED !== ''))
			{
						/* VERIFICA SE A MODALIDADE EXISTE */
				#if (parent::checkIfExist('modalidade',array('id'),array('id','id_assoc'),array($this->ID_MOD_DECRYPTED,$this->ID_ASSOC)))
				#{
					$sANO = $this->checkAno($A_POST);
					if ($sANO != false)
					{
						if (isset($A_POST['Montante']) && isset($A_POST['TotalPago']) && isset($A_POST['Desconto']))
						{
							$fMontante 	= $A_POST['Montante'];
							$fDesconto 	= $A_POST['Desconto'];
							$fTotalPago = $A_POST['TotalPago'];

							if ($this->isStrFloat($fMontante,'€') &&
								$this->isStrFloat($fDesconto,'%') &&
								$this->isStrFloat($fTotalPago,'€') )
							{
								$fTotalTotal  = ($fMontante) - ($fMontante * ($fDesconto / 100));
								$FTotalDivida = ($fTotalTotal - $fTotalPago);

								$iIDPagForma = (isset($A_POST['MetodoPagamento'])) ? $A_POST['MetodoPagamento'] : '';		#id_pagamento_tipo
								$iIDPagForma = parent::decryptVar($iIDPagForma);
								$iIDPagOpcao = (isset($A_POST['OpcaoPagamento'])) ? $A_POST['OpcaoPagamento'] : '';		#id_pagamento_opcao
								$iIDPagOpcao = parent::decryptVar($iIDPagOpcao);

								$sDescricao  = $A_POST['Descricao'];
								$dPrazoPag	 = $A_POST['data-limite'];
								$dDataPag	 = $A_POST['data-pagamento'];
								$sObservacoes= $A_POST['Observacoes'];

								if ($dPrazoPag === '')
								{
									$dPrazoPag 	= date('Y-m-d');
									$Next7 		= strtotime("+1 week", strtotime($dPrazoPag));
									$dPrazoPag 	= date('Y-m-d',$Next7);
								}

								parent::setFieldsValue(array(false,				# id
															false,				# id_assoc
															false,				# id_modalidade
															false,				# id_socio
															$iIDPagForma,		# id_pag_forma
															$iIDPagOpcao,		# id_pag_opcao
															$sDescricao,		# descricao
															false,				# dataquota
															false,				# isquota
															$sANO,				# ano
															false,				# mes
															$fMontante,			# montante
															$fTotalPago,		# total_pago
															$FTotalDivida,		# total_divida
															$fDesconto,			# desconto
															$fTotalTotal,		# total_total
															$dPrazoPag,			# prazo_patamento
															$dDataPag,			# data_patamento
															($FTotalDivida==0), # action
															$sObservacoes,		# observacoes
															false,
															$this->ID_USER,
															false,
															date('Y-m-d H:i:s'),
															false)
														);

								if (parent::checkInformation())
								{
									$SQL 	= parent::BuildSQL('UPDATE',array('id','id_assoc','id_modalidade','id_socio'),array($this->ID_QUOTA_DECRYPTED,$this->ID_ASSOC,$this->ID_MOD_DECRYPTED,$this->ID_SOCIO_DECRYPTED));
									$Result = parent::ExecSQL($SQL);

									if ($Result)									# QUOTA INSERIDA COM SUCESSO
									{
										$this->updateTOTAL();	
									}
								}
							}
						}
					}
				#}
			}

			return $Result;
		}
		public function removeQuota()
		{
			$Result	= false;

			if (($this->ID_QUOTA_DECRYPTED !== '') &&
				($this->ID_MOD_DECRYPTED !== '') &&
				($this->ID_SOCIO_DECRYPTED !== ''))
			{
				$SQL 	= parent::BuildSQL('DELETE',array('id','id_assoc','id_modalidade','id_socio'),array($this->ID_QUOTA_DECRYPTED,$this->ID_ASSOC,$this->ID_MOD_DECRYPTED,$this->ID_SOCIO_DECRYPTED));
				$Result = parent::ExecSQL($SQL);

				if ($Result) # QUOTA REMOVIDA COM SUCESSO
				{
					$this->updateTOTAL();
				}	
			}

			return $Result;			
		}
		public function updateTOTAL()
		{
			/*
				SELECT
				SUM(total_divida) as totald,
				SUM(total_pago) as totalp

				FROM quotas_socios

				WHERE id_assoc='56'
				AND id_modalidade='59'
				AND id_socio='95'
				AND dataquota <=  '2013/08/28'
				AND dataquota <>  '0000-00-00'
				AND enabled='0'
			*/

			# VAI BUSCAR TOTAL PARA AQUELE ANO!!
			# ACTUALIZA DIVIDA DO SOCIO
			$SQL  = " SELECT ";
			$SQL .= " SUM(total_divida) AS totald, ";
			$SQL .= " SUM(total_pago) AS totalp ";
			$SQL .= " FROM quotas_socios ";
			$SQL .= " WHERE id_assoc='".$this->ID_ASSOC."' ";
			$SQL .= " AND id_modalidade='".$this->ID_MOD_DECRYPTED."' ";
			$SQL .= " AND id_socio='".$this->ID_SOCIO_DECRYPTED."' ";
			$SQL .= " AND dataquota<='".(date('Y-m-d'))."' ";
			$SQL .= " AND dataquota<>'0000-00-00' ";
			$SQL .= " AND enabled='0' ";
#var_dump($SQL);
			$ROW = parent::getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				$fTotalDivida 	= $ROW[0]['totald'];
				$fTotalPago 	= $ROW[0]['totalp'];
			}
			else # NAO ENCONTROU REGISTOS
			{
				$fTotalDivida 	= 0;
				$fTotalPago 	= 0;
			}
			# ===== ACTUALIZAR DIVIDA! =====
			$SQL = "UPDATE mod_socio SET ";
			$SQL .= " total_divida = '".$fTotalDivida."', ";
			$SQL .= " total_pago = '".$fTotalPago."' ";
			$SQL .= " WHERE id_assoc='".$this->ID_ASSOC."' ";
			$SQL .= " AND id_modalidade='".$this->ID_MOD_DECRYPTED."' ";
			$SQL .= " AND id_socio='".$this->ID_SOCIO_DECRYPTED."'";
			$Result = parent::ExecSQL($SQL);
			if ($Result)
			{

			}
			else
			{
				# ERRO: Actualizar Divida Socio!
			}
			####################################################
			############## ACTUALIZAR ASSOC_SOCIO ##############
			####################################################
			$SQL = "SELECT SUM(total_divida) AS totald FROM mod_socio";
			$SQL .= " WHERE id_assoc='".$this->ID_ASSOC."' ";
			$SQL .= " AND id_socio='".$this->ID_SOCIO_DECRYPTED."'";		
			$ROW = parent::getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				$fTotalDivida 	= $ROW[0]['totald'];
			}
			else # NAO ENCONTROU REGISTOS
			{
				$fTotalDivida 	= 0;
			}
			# ===== ACTUALIZAR DIVIDA! =====
			$SQL = "UPDATE assoc_socios SET ";
			$SQL .= " divida = '".(0-$fTotalDivida)."' ";
			$SQL .= " WHERE id_assoc='".$this->ID_ASSOC."' ";
			$SQL .= " AND id_socio='".$this->ID_SOCIO_DECRYPTED."'";
			$Result = parent::ExecSQL($SQL);
			if ($Result)
			{
				return true;
			}
			else
			{
				# ERRO: Actualizar Divida Socio!
			}
			####################################################
		}
		public function getListSearch($AarrFieldName,$AarrValue, $FieldToOrder,$OrderBy, $iPage=0, $iLimPerPage=10)
		{
			$ROW 	= array();
			$EXIST 	= false;
			$TotalPages = 0;
			$AarrValue  = parent::processArraySearch($AarrFieldName,$AarrValue);
			
			$OrderBy = ($OrderBy === '') ? ' ASC ' : $OrderBy;
			switch ($FieldToOrder)
			{
				case 'forma'		: $FieldToOrder = 'pag_forma.nome '.$OrderBy; 				break;
				case 'opcao'		: $FieldToOrder = 'pag_opcao.nome '.$OrderBy; 				break;
				case 'descricao'	: $FieldToOrder = 'quotas_socios.descricao '.$OrderBy; 		break;
				case 'ano'			: $FieldToOrder = 'quotas_socios.ano '.$OrderBy; 			break;
				case 'mes'			: $FieldToOrder = 'quotas_socios.mes '.$OrderBy; 			break;
				case 'montante' 	: $FieldToOrder = 'quotas_socios.montante '.$OrderBy;		break;
				case 'total_pago' 	: $FieldToOrder = 'quotas_socios.total_pago '.$OrderBy;		break;
				case 'total_divida' : $FieldToOrder = 'quotas_socios.total_divida '.$OrderBy;	break;
				case 'descondo' 	: $FieldToOrder = 'quotas_socios.desconto '.$OrderBy;		break;
				case 'total_total' 	: $FieldToOrder = 'quotas_socios.total_total '.$OrderBy;	break;
				case 'observacoes' 	: $FieldToOrder = 'quotas_socios.observacoes '.$OrderBy;	break;
				default 			: $FieldToOrder = 'quotas_socios.ano '.$OrderBy; 			break;
			}
			$FieldToOrder = ' ORDER BY '.$FieldToOrder;

			$sSQLLIKE = ($AarrValue[0] === '*') ? '' : ' AND ('.parent::WHEREUPDATE_PARSER($AarrFieldName,$AarrValue,' OR ','LIKE',false).') ';
			//$sLIMIT = parent::build_LIMITStatement($iPage,$iLimPerPage);
			#$sLIMIT   = ((isset($iPage)) && ($iPage!=='') && ($iPage > 1)) ? ' LIMIT '.(($iPage-1)*$iLimPerPage).','.($iLimPerPage) : (' LIMIT 0,'.$iLimPerPage) ;

			$SQL  = 'SELECT pag_forma.nome AS "pag_forma_nome",
							pag_opcao.nome AS "pag_opcao_nome",
							quotas_socios.id AS "id_quota",
							quotas_socios.descricao AS "descricao",
							quotas_socios.isquota AS "isquota",
							quotas_socios.ano AS "ano",
							quotas_socios.mes AS "mes",
							quotas_socios.montante AS "montante",
							quotas_socios.total_pago AS "total_pago",
							quotas_socios.total_divida AS "total_divida",
							quotas_socios.desconto AS "desconto",
							quotas_socios.total_total AS "total_total",
							quotas_socios.prazo_pagamento AS "prazo_pagamento",
							quotas_socios.data_pagamento AS "data_pagamento",
							quotas_socios.action AS "action",
							quotas_socios.observacoes AS "observacoes",
							quotas_socios.enabled AS "enabled"
					';
			$SQL .= ' FROM quotas_socios ';
			$SQL .= ' LEFT JOIN pag_forma ON pag_forma.id=quotas_socios.id_pag_forma AND pag_forma.id_assoc="'.$this->ID_ASSOC.'" ';
			$SQL .= ' LEFT JOIN pag_opcao ON pag_opcao.id=quotas_socios.id_pag_opcao AND pag_forma.id_assoc="'.$this->ID_ASSOC.'" ';
			$SQL .= ' WHERE quotas_socios.id_assoc="'.$this->ID_ASSOC.'" ';
			$SQL .= ' AND quotas_socios.id_modalidade="'.$this->ID_MOD_DECRYPTED.'" ';
			$SQL .= ' AND quotas_socios.id_socio="'.$this->ID_SOCIO_DECRYPTED.'" ';
			$SQL .= ' AND quotas_socios.enabled="0" ';
			$SQL .= $sSQLLIKE;
			$SQL .= $FieldToOrder;
			#$SQL .= $sLIMIT;
#var_dump($SQL);
			$ROW = parent::getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				parent::setFieldsType(array('STRING','STRING','ENCRYPT','STRING','STRINGINT','STRINGINT','STRINGINT',
											'FLOAT','FLOAT','FLOAT','FLOAT','FLOAT','DATE','DATE','STRINGINT','MEMO','STRINGINT'));
				parent::FieldsOutput($ROW);

				$EXIST 	= true;
				$TotalPages = parent::getTotalPages($SQL,$iLimPerPage);
			}
			
			return array('ROW'=>$ROW, 'EXIST'=>$EXIST, 'TotalPages'=>$TotalPages);
		}
		public function getListNEW($AANO)
		{
			$ROW	= array();
			$EXIST 	= false;
			
			if ($this->ID_SOCIO_DECRYPTED !== '')
			{
				if ($AANO !== '')
				{
					$extSQL  = ' AND quotas_socios.ano="'.$AANO.'" ';
					$extSQL .= ' ORDER BY quotas_socios.id ASC ';
					$extSQL .= ' LIMIT 0,15 ';
				}
				else
				{
					$extSQL  = ' AND quotas_socios.ano="'.date('Y').'" ';
					$extSQL .= ' ORDER BY quotas_socios.mes ASC ';
				}
	
				$SQL  = 'SELECT pag_forma.nome AS "pag_forma_nome",
								pag_opcao.nome AS "pag_opcao_nome",
								quotas_socios.id AS "id_quota",
								quotas_socios.id_modalidade AS "id_modalidade",
								quotas_socios.descricao AS "descricao",
								quotas_socios.isquota AS "isquota",
								quotas_socios.ano AS "ano",
								quotas_socios.mes AS "mes",
								quotas_socios.montante AS "montante",
								quotas_socios.total_pago AS "total_pago",
								quotas_socios.total_divida AS "total_divida",
								quotas_socios.desconto AS "desconto",
								quotas_socios.total_total AS "total_total",
								quotas_socios.prazo_pagamento AS "prazo_pagamento",
								quotas_socios.data_pagamento AS "data_pagamento",
								quotas_socios.action AS "action",
								quotas_socios.observacoes AS "observacoes",
								quotas_socios.enabled AS "enabled"
						';
				$SQL .= ' FROM quotas_socios ';
				$SQL .= ' LEFT JOIN pag_forma ON pag_forma.id=quotas_socios.id_pag_forma ';
				$SQL .= ' LEFT JOIN pag_opcao ON pag_opcao.id=quotas_socios.id_pag_opcao ';
				$SQL .= ' WHERE quotas_socios.id_assoc="'.$this->ID_ASSOC.'" ';
				if ($this->ID_MOD_DECRYPTED !== '')
				{
					$SQL .= ' AND quotas_socios.id_modalidade="'.$this->ID_MOD_DECRYPTED.'" ';
				}
				$SQL .= ' AND quotas_socios.id_socio="'.$this->ID_SOCIO_DECRYPTED.'" ';
				$SQL .= ' AND quotas_socios.enabled="0" ';
				$SQL .= $extSQL;
				
				$ROW = parent::getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					parent::setFieldsType(array('STRING','STRING','ENCRYPT','ENCRYPT','STRING','STRINGINT','STRINGINT','STRINGINT',
												'FLOAT','FLOAT','FLOAT','FLOAT','FLOAT','DATE','DATE','STRINGINT','MEMO','STRINGINT'));
					parent::FieldsOutput($ROW);
					
					$EXIST = true;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function getList($AANO)
		{
			$ROW	= array();
			$EXIST 	= false;

			if (($this->ID_MOD_DECRYPTED !== '') && ($this->ID_SOCIO_DECRYPTED !== ''))
			{		
				# VERIFICA SE O SOCIO ESTA INSCRITO NA MODALIDADE
				#if (parent::checkIfExist('mod_socio',array('id'),array('id_assoc','id_socio','id_modalidade'),array($this->ID_ASSOC,$this->ID_SOCIO_DECRYPTED,$this->ID_MOD_DECRYPTED)))
				#{
					if ($AANO !== '')
					{
						$extSQL = ' AND quotas_socios.ano="'.$AANO.'" ';
						$extSQL .= ' ORDER BY quotas_socios.id ASC ';
						$extSQL .= ' LIMIT 0,15 ';
					}
					else
					{
						$extSQL = ' ORDER BY quotas_socios.ano DESC ';
					}

					$SQL  = 'SELECT pag_forma.nome AS "pag_forma_nome",
									pag_opcao.nome AS "pag_opcao_nome",
									quotas_socios.id AS "id_quota",
									quotas_socios.descricao AS "descricao",
									quotas_socios.isquota AS "isquota",
									quotas_socios.ano AS "ano",
									quotas_socios.mes AS "mes",
									quotas_socios.montante AS "montante",
									quotas_socios.total_pago AS "total_pago",
									quotas_socios.total_divida AS "total_divida",
									quotas_socios.desconto AS "desconto",
									quotas_socios.total_total AS "total_total",
									quotas_socios.prazo_pagamento AS "prazo_pagamento",
									quotas_socios.data_pagamento AS "data_pagamento",
									quotas_socios.action AS "action",
									quotas_socios.observacoes AS "observacoes",
									quotas_socios.enabled AS "enabled"
							';
					$SQL .= ' FROM quotas_socios ';
					$SQL .= ' LEFT JOIN pag_forma ON pag_forma.id=quotas_socios.id_pag_forma ';
					$SQL .= ' LEFT JOIN pag_opcao ON pag_opcao.id=quotas_socios.id_pag_opcao ';
					$SQL .= ' WHERE quotas_socios.id_assoc="'.$this->ID_ASSOC.'" ';
					$SQL .= ' AND quotas_socios.id_modalidade="'.$this->ID_MOD_DECRYPTED.'" ';
					$SQL .= ' AND quotas_socios.id_socio="'.$this->ID_SOCIO_DECRYPTED.'" ';
					$SQL .= ' AND quotas_socios.enabled="0" ';
					$SQL .= $extSQL;
					#$SQL .= ' ORDER BY quotas_socios.ano DESC ';
//var_dump($SQL);
					$ROW = parent::getRESULTS($SQL);
					if (isset($ROW[0]))
					{
						parent::setFieldsType(array('STRING','STRING','ENCRYPT','STRING','STRINGINT','STRINGINT','STRINGINT',
													'FLOAT','FLOAT','FLOAT','FLOAT','FLOAT','DATE','DATE','STRINGINT','MEMO','STRINGINT'));
						parent::FieldsOutput($ROW);

						#############################################################################
						########################## HTML TEMPLATE VARIABLES ##########################
						#############################################################################
						$ANO = (isset($_GET['ano'])) ? '&ano='.$_GET['ano'] : '';

						$ccI = count($ROW);
						for ($i=0; $i<$ccI; $i++)
						{
							if ($ROW[$i]['total_divida'] == 0.0)
							{
								$sEtiqueta 	= '<span class="col center"><span class="etiqueta verde">Pago</span></span>';
								$sColor		= 'green';
								$sData		= $ROW[$i]['data_pagamento'];
								$sDataTipo	= 'Data do pagamento';
							}
							else
							{
								$sEtiqueta 	= '€'.$ROW[$i]['total_divida'];
								$sEtiqueta	= '<span class="col center numero red">'.$sEtiqueta.'</span>';
								$sColor		= 'red';
								$sData		= $ROW[$i]['prazo_pagamento'];
								$sDataTipo	= 'Prazo de pagamento';
							}

							$ROW[$i]['edit-link']	= SETPATH('URL','PATH_APP_UI_QUOTAS').'editar-quota-outro.php?idm='.$this->ID_MOD_ENCRYPTED.'&ids='.$this->ID_SOCIO_ENCRYPTED.'&idq='.$ROW[$i]['id_quota'].$ANO;
							$ROW[$i]['spanExt']		= $sEtiqueta;
							$ROW[$i]['sData']		= $sData;
							$ROW[$i]['sDataColor']	= $sColor;
							$ROW[$i]['i']			= $i;
							$ROW[$i]['data_tipo']	= $sDataTipo;
							$ROW[$i]['row_json']	 = '"index":"'.($i).'",';
							$ROW[$i]['row_json'] 	.= '"value":"'.($ROW[$i]['id_quota']).'",';
							$ROW[$i]['row_json'] 	.= '"q_descricao":"'.($ROW[$i]['descricao']).'",';
							$ROW[$i]['row_json'] 	.= '"q_total":"'.($ROW[$i]['total_total']).'",';
							$ROW[$i]['row_json'] 	.= '"q_totalpago":"'.($ROW[$i]['total_pago']).'",';
							$ROW[$i]['row_json'] 	.= '"q_divida":"'.($ROW[$i]['total_divida']).'",';
							$ROW[$i]['row_json'] 	.= '"q_datapag":"'.($ROW[$i]['data_pagamento']).'",';
							$ROW[$i]['row_json'] 	.= '"q_datapraz":"'.($ROW[$i]['prazo_pagamento']).'"';
							$ROW[$i]['row_json'] 	= base64_encode('{'.$ROW[$i]['row_json'].'}');
						}
						#############################################################################
						$EXIST 	= true;
					}
				#}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function getListaDeAnos()
		{
			$ROW   = array();
			$EXIST = false;

			if (($this->ID_MOD_DECRYPTED !== '') && ($this->ID_SOCIO_DECRYPTED !== ''))
			{		
				$SQL = 'SELECT 
							quotas_socios.ano AS "ano",
							SUM(quotas_socios.total_divida) AS "total_divida"
						FROM quotas_socios
						WHERE quotas_socios.id_assoc="'.$this->ID_ASSOC.'" 
						AND quotas_socios.id_modalidade="'.$this->ID_MOD_DECRYPTED.'"
						AND quotas_socios.id_socio="'.$this->ID_SOCIO_DECRYPTED.'"
						AND quotas_socios.enabled="0"
						GROUP BY quotas_socios.ano 
						ORDER BY quotas_socios.ano DESC';

				$ROW = parent::getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					parent::setFieldsType(array('STRINGINT','FLOAT'));
					parent::FieldsOutput($ROW);
					
					$EXIST = true;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function createSelect_DeAnos($AANO)
		{
			$htmlANOS = '';

			if (($this->ID_MOD_DECRYPTED !== '') && ($this->ID_SOCIO_DECRYPTED !== ''))
			{
				$ResAnos 	= $this->getListaDeAnos();
				$ROWAnos 	= $ResAnos['ROW'];
				$EXISTAnos 	= $ResAnos['EXIST'];
				
				if ($EXISTAnos)
				{
					$ccI = count($ROWAnos);
					for ($i=0; $i<$ccI; $i++)
					{
						$sValue	 = $ROWAnos[$i]['ano'];
						$sSelect = ($sValue === $AANO) ? 'selected' : '';

						$htmlANOS .='<option value="'.$sValue.'" '.$sSelect.' >'.$sValue.'</option>';
					}
				}
			}
			return $htmlANOS;
		}
		public function getQuotaData()
		{
			$ROW	= array();
			$EXIST 	= false;

			if (($this->ID_QUOTA_DECRYPTED !== '') && 
				($this->ID_MOD_DECRYPTED!== '') &&
				($this->ID_SOCIO_DECRYPTED !== ''))
			{
				$SQL  = 'SELECT quotas_socios.id AS "id_quota",
								quotas_socios.id_pag_forma AS "id_pag_forma",
								quotas_socios.id_pag_opcao AS "id_pag_opcao",
								quotas_socios.descricao AS "descricao",
								quotas_socios.isquota AS "isquota",
								quotas_socios.ano AS "ano",
								quotas_socios.mes AS "mes",
								quotas_socios.montante AS "montante",
								quotas_socios.total_pago AS "total_pago",
								quotas_socios.total_divida AS "total_divida",
								quotas_socios.desconto AS "desconto",
								quotas_socios.total_total AS "total_total",
								quotas_socios.prazo_pagamento AS "prazo_pagamento",
								quotas_socios.data_pagamento AS "data_pagamento",
								quotas_socios.action AS "action",
								quotas_socios.observacoes AS "observacoes",
								quotas_socios.enabled AS "enabled"
						';
				$SQL .= ' FROM quotas_socios ';
				#$SQL .= ' LEFT JOIN pag_forma ON pag_forma.id=quotas_socios.id_pag_forma AND pag_forma.id_user="'.$this->ID_USER.'" ';
				#$SQL .= ' LEFT JOIN pag_opcao ON pag_opcao.id=quotas_socios.id_pag_opcao AND pag_forma.id_user="'.$this->ID_USER.'" ';
				$SQL .= ' WHERE quotas_socios.id_assoc="'.$this->ID_ASSOC.'" ';
				$SQL .= ' AND quotas_socios.id_modalidade="'.$this->ID_MOD_DECRYPTED.'" ';
				$SQL .= ' AND quotas_socios.id_socio="'.$this->ID_SOCIO_DECRYPTED.'" ';
				$SQL .= ' AND quotas_socios.id="'.$this->ID_QUOTA_DECRYPTED.'" ';
				#$SQL .= ' ORDER BY quotas_socios.ano DESC ';
				$SQL .= ' LIMIT 0,1 ';

				$ROW = parent::getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					parent::setFieldsType(array('ENCRYPT','STRINGINT','STRINGINT','STRING','STRINGINT','STRINGINT','STRINGINT',
												'FLOAT','FLOAT','FLOAT','FLOAT','FLOAT','DATEINPUT','DATEINPUT','STRINGINT','MEMO','STRINGINT'));
					parent::FieldsOutput($ROW);
					$ANO = ((isset($_GET['ano'])) && ($_GET['ano'] !== '')) ? '&ano='.$_GET['ano'] : '';
					$ROW[0]['LINK_ANULAR'] 	  = 'listagem.php?ids='.$this->ID_SOCIO_ENCRYPTED.'&idm='.$this->ID_MOD_ENCRYPTED.$ANO;
					
					$EXIST = true;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function getModalidadesTotal_SOCIO()
		{
			$ROW	= array();
			$EXIST 	= false;

			if ($this->ID_SOCIO_DECRYPTED !== '')
			{		
				$SQL = 'SELECT  modalidade.id AS "id_modalidade",
								modalidade.nome AS "nome_modalidade",
								mod_socio.total_divida AS "total_divida",
								mod_socio.total_pago AS "total_pago"
						';
				$SQL .= ' FROM  mod_socio';
				$SQL .= ' LEFT JOIN modalidade ON modalidade.id = mod_socio.id_modalidade';
				$SQL .= ' WHERE mod_socio.id_assoc="'.$this->ID_ASSOC.'"';
				$SQL .= ' AND mod_socio.id_socio="'.$this->ID_SOCIO_DECRYPTED.'"';
				$SQL .= ' ORDER BY modalidade.nome ASC ';

				$ROW = parent::getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					parent::setFieldsType(array('ENCRYPT','STRING','FLOAT','FLOAT'));
					parent::FieldsOutput($ROW);

					$EXIST = true;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
	}
?>