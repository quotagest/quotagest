<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	
	class dbTblSociosFamiliar extends dbTable
	{
		public $ID_SOCIO_ENCRYPTED 		= '';
		public $ID_SOCIO_DECRYPTED 		= '';
		public $ID_FAMILIAR_ENCRYPTED 	= '';
		public $ID_FAMILIAR_DECRYPTED 	= '';
		public $ID_USER 				= '';
		public $ID_ASSOC 				= '';
		public $arrTipoAfinidade 		= array('Pai',
												'Mãe',
												'Irmão',
												'Irmã',
												'Tio Paterno',
												'Tia Paterna',
												'Tio Materno',
												'Tia Materna',
												'Avô Paterno',
												'Avó Paterna',
												'Avô Materno',
												'Avó Materna');

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName('socio_familia');
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_SOCIO_DECRYPTED);
			unset($this->ID_SOCIO_ENCRYPTED);
			unset($this->ID_USER);
			unset($this->ID_ASSOC);
		}

		public function set_IDSOCIO($AsVar)
		{
			$this->ID_SOCIO_ENCRYPTED = $AsVar;
			$this->ID_SOCIO_DECRYPTED = parent::decryptVar($AsVar);
		}
		public function set_IDFAMILIAR($AsVar)
		{
			$this->ID_FAMILIAR_ENCRYPTED = $AsVar;
			$this->ID_FAMILIAR_DECRYPTED = parent::decryptVar($AsVar);
		}
		public function set_IDUSER($AsVar)  { $this->ID_USER = $AsVar; }
		public function set_IDASSOC($AsVar) { $this->ID_ASSOC = $AsVar; }

		public function setFieldsInformation()
		{
			parent::setFieldsName(array('id','id_assoc','id_socio','id_socio_familia','afinidade','id_user_created','id_user_edited','data_criacao','data_alterado','enabled'));
			parent::setFieldsType(array('INTEGER','INTEGER','INTEGER','INTEGER','STRING','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));
			parent::setFieldsRequired(array(true,true,true,true,true,true,true,true,true,true));
		}

		public function decodeJSONtoArray($AJSON)
		{
			$jsonData = base64_decode($AJSON);
			$jsonData = urldecode($jsonData);
			$jsonData = utf8_encode($jsonData);
			$jsonData = json_decode($jsonData,true);
			
			return $jsonData;
		}
		public function addFamiliar($A_POST)
		{
			if (isset($A_POST['jsonPOST']))
			{
				$arrJSON = $this->decodeJSONtoArray($A_POST['jsonPOST']);
				$arrJSON = $arrJSON['_POST'];
				$A_POST['CodSocioFamiliar'] = parent::decryptVar($arrJSON['CodSocioFamiliar']);
				$A_POST['Afinidade'] 		= $arrJSON['Afinidade'];
			}
			$Result			= false;
			$NEW_ASSOC_ID 	= false;

			parent::setFieldsValue(array('NULL',
										 $this->ID_ASSOC,	
										 $this->ID_SOCIO_DECRYPTED,
										 $A_POST['CodSocioFamiliar'],
										 $A_POST['Afinidade'],
										 $this->ID_USER,				#id_user_created
										 $this->ID_USER,				#id_user_edited
										 date('Y-m-d H:i:s'),			#data_criacao
										 date('Y-m-d H:i:s'),			#data_alterado
										 '0')							#enabled
										);
			
			if (parent::checkInformation())
			{
				$SQL 		  = parent::BuildSQL('INSERT');
				$Result 	  = parent::ExecSQL($SQL);
				$NEW_FAMIL_ID = parent::getInsertedID();
			}
			
			return array('Result'=>$Result, 'NEW_ID'=>$NEW_FAMIL_ID);
		}
		public function updateFamiliar($A_POST)
		{
			$Result		= false;

			if ($this->ID_FAMILIAR_ENCRYPTED !== '')
			{
				parent::setFieldsValue(array(false,
											 false,
											 $A_POST['CodSocioFamiliar'],
											 $A_POST['Afinidade'],
											 false,						#id_user_created
											 $this->ID_USER,			#id_user_edited
											 false,						#data_criacao
											 date('Y-m-d H:i:s'),		#data_alterado
											 false)						#enabled
										);

				if (parent::checkInformation())
				{
					$SQL 	= parent::BuildSQL('UPDATE',array('id','id_assoc','id_socio'),array($this->ID_FAMILIAR_DECRYPTED,$this->ID_ASSOC,$this->ID_SOCIO_DECRYPTED));
					$Result = parent::ExecSQL($SQL);
				}
			}
			
			return $Result;
		}
		public function removeFAMILIAR()
		{
			$Result		= false;

			if ($this->ID_FAMILIAR_ENCRYPTED !== '')
			{
				#if (parent::checkIfExist('socio_familia',array('id'),array('id','id_assoc'),array($this->ID_FAMILIAR_ENCRYPTED,$this->ID_ASSOC)))
				#{
					$SQL 	= parent::BuildSQL('DELETE',array('id','id_assoc'),array($this->ID_FAMILIAR_ENCRYPTED,$this->ID_ASSOC));
					$Result = parent::ExecSQL($SQL);
				#}
			}
			
			return $Result;
		}
		public function getFamiliarData()
		{
			$ROW	= array();
			$EXIST	= false;

			if ($this->ID_ASSOC !== '')
			{
				$SQL  = "SELECT socio.nome AS 'nome',
								socio_familia.id_socio AS 'id_socio_parent',
								socio_familia.id_socio_familia AS 'id_socio_familia',
								socio_familia.afinidade AS 'afinidade',
								socio_familia.enabled AS 'enabled',

								socio_familia.id_user_edited AS 'id_user_edited',
								socio_familia.data_alterado AS 'data_alterado'
						";
				$SQL .= ' FROM socio_familia';
				$SQL .= ' LEFT JOIN socio ON socio.id=socio_familia.id_socio_familia ';
				$SQL .= ' WHERE socio_familia.id_socio="'.$this->ID_FAMILIAR_DECRYPTED.'"';
				$SQL .= ' AND socio_familia.id_socio_familiar="'.$this->ID_FAMILIAR_DECRYPTED.'"';
				$SQL .= ' AND socio_familia.id_assoc="'.$this->ID_ASSOC.'"';
				$SQL .= ' AND socio_familia.enabled="0"';
				$SQL .= ' LIMIT 0,1;';

				$ROW = parent::getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					parent::setFieldsType(array('STRING','ENCRYPT','ENCRYPT','STRING','INTEGER',
												'INTEGER','DATETIME'));
					parent::FieldsOutput($ROW);

					$EXIST = true;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
/*
http://stackoverflow.com/a/5291149

SELECT 
socio.nome AS 'nome_parent',
S1.nome AS 'nome_familiar',
socio_familia.id_socio AS 'id_socio_parent',
socio_familia.id_socio_familia AS 'id_socio_familia',
socio_familia.afinidade AS 'afinidade',
socio_familia.enabled AS 'enabled',

socio_familia.id_user_edited AS 'id_user_edited',
socio_familia.data_alterado AS 'data_alterado'

FROM socio_familia

LEFT JOIN socio ON socio.id=socio_familia.id_socio
LEFT JOIN socio S1 ON S1.id=socio_familia.id_socio_familia

WHERE socio_familia.id_socio="104"
OR socio_familia.id_socio_familia="104"
AND socio_familia.id_assoc="56"
AND socio_familia.enabled="0"
*/
		public function getFamiliaresLista()
		{
			$ROW	= array();
			$EXIST	= false;

			if ($this->ID_ASSOC !== '')
			{
				$SQL  = "SELECT socio.nome AS 'nome',
								socio_familia.id AS 'id_tblfamiliar',
								socio_familia.id_socio AS 'id_socio_parent',
								socio_familia.id_socio_familia AS 'id_socio_familia',
								socio_familia.afinidade AS 'afinidade',
								socio_familia.enabled AS 'enabled',

								socio_familia.id_user_edited AS 'id_user_edited',
								socio_familia.data_alterado AS 'data_alterado'
						";
				$SQL .= ' FROM socio_familia';
				$SQL .= ' LEFT JOIN socio ON socio.id=socio_familia.id_socio_familia';
				$SQL .= ' WHERE socio_familia.id_socio="'.$this->ID_SOCIO_DECRYPTED.'"';
				//$SQL .= ' OR socio_familia.id_socio_familia="'.$this->ID_SOCIO_DECRYPTED.'"';
				$SQL .= ' AND socio_familia.id_assoc="'.$this->ID_ASSOC.'"';
				$SQL .= ' AND socio_familia.enabled="0"';
				//$SQL .= ' LIMIT 0,1;';

				$ROW1 = parent::getRESULTS($SQL);
				if (isset($ROW1[0]))
				{
					parent::setFieldsType(array('STRING','ENCRYPT','ENCRYPT','ENCRYPT','STRING','INTEGER',
												'INTEGER','DATETIME'));
					parent::FieldsOutput($ROW1);

					$EXIST = true;
				}
				################################################################
				################################################################
				$SQL  = "SELECT socio.nome AS 'nome',
								socio_familia.id AS 'id_tblfamiliar',
								socio_familia.id_socio AS 'id_socio_parent',
								socio_familia.id_socio_familia AS 'id_socio_familia',
								socio_familia.afinidade AS 'afinidade',
								socio_familia.enabled AS 'enabled',

								socio_familia.id_user_edited AS 'id_user_edited',
								socio_familia.data_alterado AS 'data_alterado'
						";
				$SQL .= ' FROM socio_familia';
				$SQL .= ' LEFT JOIN socio ON socio.id=socio_familia.id_socio';
				$SQL .= ' WHERE socio_familia.id_socio_familia="'.$this->ID_SOCIO_DECRYPTED.'"';
				$SQL .= ' AND socio_familia.id_assoc="'.$this->ID_ASSOC.'"';
				$SQL .= ' AND socio_familia.enabled="0"';
				//$SQL .= ' LIMIT 0,1;';

				$ROW2 = parent::getRESULTS($SQL);
				if (isset($ROW2[0]))
				{
					parent::setFieldsType(array('STRING','ENCRYPT','ENCRYPT','ENCRYPT','STRING','INTEGER',
												'INTEGER','DATETIME'));
					parent::FieldsOutput($ROW2);

					$EXIST = true;
				}

				if (isset($ROW1[0]) || isset($ROW2[0]))
				{
					$arrAux = array();
					$size1 = count($ROW1);
					$size2 = count($ROW2);
					$posA = 0;
					for ($i=0; $i<$size1; $i++)
					{
						$arrAux[$posA] = $ROW1[$i];
						$posA++;
					}
					for ($i=0; $i<$size2; $i++)
					{
						$arrAux[$posA] = $ROW2[$i];
						$posA++;
					}
					$ROW = $arrAux;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
	}

?>