<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'db_tables.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	
	class dbTblUserAssoc extends dbTable
	{
		public $ID_USER 	= '';
		public $ID_ASSOC 	= '';

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName("user_assoc");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_USER);
			unset($this->ID_USER);
			unset($this->ID_ASSOC);
			unset($this->ID_ASSOC);
		}

		public function setFieldsInformation()
		{
			parent::SetFieldsName(array('id','id_user','id_assoc','tipo','observacoes','id_user_created','id_user_edited','data_criacao','data_alterado','enabled'));
			parent::SetFieldsType(array('INTEGER','INTEGER','INTEGER','INTEGER','MEMO','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));
			parent::setFieldsRequired(array(true,true,true,true,false,true,true,true,true,true));
		}

		public function set_IDUSER($AsVar)  { $this->ID_USER = $AsVar; }
		public function set_IDASSOC($AsVar) { $this->ID_ASSOC = $AsVar; }

		public function INSERT($A_POST)
		{
			$Result = false;
			$NEW_ID = false;

			$this->SetFieldsValue(array('NULL',
										$this->ID_USER,
										$this->ID_ASSOC,
										$A_POST['tipo'],
										$A_POST['observacoes'],
										$this->ID_USER,
										$this->ID_USER,
										date('Y-m-d H:i:s'),
										date('Y-m-d H:i:s'),
										'0')
									);

			if (parent::checkInformation())	
			{
				if (!parent::checkIfExist('user_assoc',array('id'),array('id_user','id_assoc'),array($this->ID_USER,$this->ID_ASSOC)))
				{
					$SQL 	= parent::BuildSQL('INSERT');
					$Result = parent::ExecSQL($SQL);
					$NEW_ID = parent::getInsertedID();
					$NEW_ID = parent::encryptVar($NEW_ID);
				}
			}

			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ID);
		}
		public function UPDATE($A_POST)
		{
			parent::setFieldsValue(array(false,
										false,
										false,
										$A_POST['tipo'],
										$A_POST['observacoes'],
										false,
										$this->ID_USER,
										false,
										date('Y-m-d H:i:s'),
										false)
									);
			$Result = false;

			if (($this->ID_USER !== '') && (parent::checkInformation())) 
			{
				#if (parent::checkIfExist('user_assoc',array('id'),array('id_user','id_assoc'),array($this->ID_USER,$this->ID_ASSOC)))
				#{
					$SQL 	= $this->BuildSQL('UPDATE',array('id_user','id_assoc'),array($this->ID_USER,$this->ID_ASSOC));
					$Result = parent::ExecSQL($SQL);
				#}
			}

			return $Result;
		}
	}
?>