<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'db_tables.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	
	class dbTblAssocSocios extends dbTable
	{
		public $ID_SOCIO_ENCRYPTED 	= '';
		public $ID_SOCIO_DECRYPTED 	= '';
		public $ID_MOD_ENCRYPTED 	= '';
		public $ID_MOD_DECRYPTED 	= '';
		public $ID_USER 			= '';
		public $ID_ASSOC 			= '';

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName("assoc_socios");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_SOCIO_DECRYPTED);
			unset($this->ID_SOCIO_ENCRYPTED);
			unset($this->ID_MOD_DECRYPTED);
			unset($this->ID_MOD_ENCRYPTED);
			unset($this->ID_USER);
			unset($this->ID_ASSOC);
		}

		public function setFieldsInformation()
		{
			parent::SetFieldsName(array('id','id_assoc','id_socio','id_tipo','id_estado','codigo','codigo_sufixo','data_entrada','data_saida','divida','enabled','activo','id_user_created','id_user_edited','data_criacao','data_alterado','observacoes'));
			parent::SetFieldsType(array('INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','STRING','DATE','DATE','FLOAT','INTEGER','INTEGER','INTEGER','INTEGER','DATETIME','DATETIME','MEMO'));
			parent::setFieldsRequired(array(true,true,true,true,true,false,false,false,false,false,true,true,true,true,true,true,false));
		}

		public function set_IDSOCIO($AsVar)
		{
			$this->ID_SOCIO_ENCRYPTED = $AsVar;
			$this->ID_SOCIO_DECRYPTED = parent::decryptVar($AsVar);
		}
		public function set_IDUSER($AsVar)  { $this->ID_USER = $AsVar; }
		public function set_IDASSOC($AsVar) { $this->ID_ASSOC = $AsVar; }

		public function INSERT($A_POST,$NEW_SOCIO_ID)
		{
			$Result = false;
			$NEW_ID = false;

			if (isset($NEW_SOCIO_ID) && ($NEW_SOCIO_ID !== ''))		// O SOCIO FOI INSERIDO COM SUCESSO?
			{
				$NEW_SOCIO_ID = parent::decryptVar($NEW_SOCIO_ID);
				$this->SetFieldsValue(array('NULL',
											$this->ID_ASSOC,
											$NEW_SOCIO_ID,
											decrypt($A_POST['TipoSocio'],'N5KyEXAnDdwGL4eV'),
											decrypt($A_POST['EstadoSocio'],'N5KyEXAnDdwGL4eV'),
											$A_POST['NumSocio'],
											'',
											$A_POST['data-entrada'],
											$A_POST['data-saida'],
											'0',
											'0',
											'1',
											$this->ID_USER,
											$this->ID_USER,
											date('Y-m-d H:i:s'),
											date('Y-m-d H:i:s'),
											$A_POST['Observacoes'])
										);
				$Result = false;
				$NEW_ID = 0;

				if (parent::checkInformation())																# check information of fields and protects the data.
				{
					$preBICC = parent::protectVar($A_POST['BICC']);											# cleans and protects variable
					#if (!parent::checkIfExist('assoc_socios',array('id','id_socio'),array('id_socio','id_assoc'),array($NEW_SOCIO_ID,$this->ID_ASSOC)))	# check if value exists in table, and returns value.
					#{
						$SQL 	= parent::BuildSQL('INSERT');
						$Result = parent::ExecSQL($SQL);
						$NEW_ID = parent::getInsertedID();
						$NEW_ID = parent::encryptVar($NEW_ID);												# encrypts given variable
					#}
				}
			}
			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ID);
		}
		public function UPDATE($A_POST)
		{
			parent::setFieldsValue(array(false,
										 false,
										 false,
										 parent::decryptVar($A_POST['TipoSocio']),
										 parent::decryptVar($A_POST['EstadoSocio']),
										 $A_POST['NumSocio'],
										 '',
										 $A_POST['data-inscricao'],
										 $A_POST['data-saida'],
										 false,
										 false,
										 false,
										 false,
										 $this->ID_USER,
										 false,
										 date('Y-m-d H:i:s'),
										 $A_POST['Observacoes'])
										);
			$Result = false;

			if (($this->ID_SOCIO_ENCRYPTED !== '') && (parent::checkInformation())) 
			{
				#if (parent::checkIfExist('assoc_socios',array('id','activo'),array('id_socio','id_assoc'),array($this->ID_SOCIO_DECRYPTED,$this->ID_ASSOC)));
				#{
					$SQL 	= $this->BuildSQL('UPDATE',array('id_assoc','id_socio'),array($this->ID_ASSOC,$this->ID_SOCIO_DECRYPTED));
					$Result = parent::ExecSQL($SQL);
				#}
			}

			return $Result;
		}
	}
?>