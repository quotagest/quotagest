<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'db_tables.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	
	class dbTblEmailsTeste extends dbTable
	{
		var $arr_USER_EMAILS_TESTE_FIELDS = array('id','email','login','password','nome','apelido','HTTP_USER_AGENT','REMOTE_ADDR','REMOTE_PORT','data_criacao','data_alterado','enabled');
		var $arr_USER_EMAILS_TESTE_TYPES  = array('INTEGER','STRING','STRING','STRING','STRING','STRING','STRING','STRING','DATETIME','DATETIME','INTEGER');

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName("user_emails_teste");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
		}

		public function setFieldsInformation()
		{
			parent::setFieldsName(array('id','email','login','password','nome','apelido','HTTP_USER_AGENT','REMOTE_ADDR','REMOTE_PORT','data_criacao','data_alterado','enabled'));
			parent::setFieldsType(array('INTEGER','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','DATETIME','DATETIME','INTEGER'));
			parent::setFieldsRequired(array(true,true,true,true,true,true,true,true,true,true,true,true));
		}

		public function AddUserEmailsTeste($A_POST)
		{
			parent::setFieldsValue(array('NULL',
										$A_POST['email-teste'],
										'NULL',
										$this->encryptVar($A_POST['password-teste']),
										$A_POST['nome-teste'],
										$A_POST['apelido-teste'],
										$_SERVER["HTTP_USER_AGENT"],
										$_SERVER["REMOTE_ADDR"],
										$_SERVER["REMOTE_PORT"],
										date('Y-m-d H:i:s'),
										date('Y-m-d H:i:s'),
										'0')
										);
			$Result	= false;
			$NEW_ID	= false;
			
			if (isset($A_POST['email-teste']))
			{
				if (parent::checkInformation())
				{
					if (!parent::checkIfExist('user_emails_teste',array('id','email'),array('email'),array($A_POST['email-teste'])))
					{
						$SQL 	= parent::BuildSQL('INSERT');
						$Result = parent::ExecSQL($SQL);
						$NEW_ID = parent::getInsertedID();
						$NEW_ID = parent::encryptVar($NEW_ID);

					}
				}
			}
			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ID);
		}
		public function UPDATE($A_POST)
		{
			parent::setFieldsValue(array(false,
										false,
										'NULL',
										$this->encryptVar($A_POST['password-teste']),
										$A_POST['nome-teste'],
										$A_POST['apelido-teste'],
										$_SERVER["HTTP_USER_AGENT"],
										$_SERVER["REMOTE_ADDR"],
										$_SERVER["REMOTE_PORT"],
										false,
										date('Y-m-d H:i:s'),
										'0')
										);
			$Result	= false;
			
			if (isset($A_POST['email-teste']))
			{
				if (parent::checkInformation())
				{
					if (parent::checkIfExist('user_emails_teste',array('id','email'),array('email'),array($A_POST['email-teste'])))
					{
						$SQL 	= parent::BuildSQL('UPDATE',array('email'),array($A_POST['email-teste']));
						$Result = parent::ExecSQL($SQL);
					}
				}
			}
			return $Result;
		}
		public function updateLogin($AEmail)
		{
			parent::setFieldsValue(array(false,
										false,
										false,
										false,
										false,
										false,
										$_SERVER["HTTP_USER_AGENT"],
										$_SERVER["REMOTE_ADDR"],
										$_SERVER["REMOTE_PORT"],
										false,
										date('Y-m-d H:i:s'),
										false)
										);
			$Result	= false;
			
			if (isset($AEmail))
			{
				if (parent::checkInformation())
				{
					if (!parent::checkIfExist('user_emails_teste',array('id','email'),array('email'),array($A_POST['email-teste'])))
					{
						$SQL 	= parent::BuildSQL('UPDATE',array('email'),array($AEmail));
						$Result = parent::ExecSQL($SQL);
					}
				}
			}
			return $Result;
		}
		public function updatePassword($AEmail,$AOldPassword,$ANewPassword,$ANewPasswordRepeat)
		{
			parent::setFieldsValue(array(false,
										false,
										false,
										$this->encryptVar($ANewPassword),
										false,
										false,
										false,
										false,
										false,
										false,
										date('Y-m-d H:i:s'),
										false)
										);
			$Result	= false;
			
			if (($AEmail !== '') && ($ANewPassword === $ANewPasswordRepeat))
			{
				if (parent::checkInformation())
				{
					if (parent::checkIfExist('user_emails_teste',array('id','email'),array('email','password'),array($AEmail,$this->encryptVar($AOldPassword))))
					{
						$SQL 	= parent::BuildSQL('UPDATE',array('email'),array($AEmail));
						$Result = parent::ExecSQL($SQL);
					}
				}
			}
			return $Result;
		}
		public function getIDbyLoginInfo($AEmail,$APassword)
		{
			$Result	= false;
			
			if (($AEmail !== '') && ($APassword !== ''))
			{
				if ($ROW = parent::checkIfExist('user_emails_teste',array('id'),array('email','password'),array($AEmail,$this->encryptVar($APassword))))
				{
					
					$Result = $ROW[0]['id'];
				}
			}
			return $Result;
		}
	}

?>