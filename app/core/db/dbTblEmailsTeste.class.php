<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'db_tables.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE').'db_functions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	
	class dbTblEmailsTeste extends dbTable
	{
		public $AID_USER = '';

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName("user_emails_teste");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->AID_USER);
		}

		public function setFieldsInformation()
		{
			parent::setFieldsName(array('id','email','password','nome','apelido','HTTP_USER_AGENT','REMOTE_ADDR','REMOTE_PORT','data_criacao','data_alterado','enabled'));
			parent::setFieldsType(array('INTEGER','STRING','STRING','STRING','STRING','STRING','STRING','STRING','DATETIME','DATETIME','INTEGER'));
			parent::setFieldsRequired(array(true,true,true,true,true,true,true,true,true,true,true));
		}
		public function set_IDUSER($AValue)
		{
			$this->AID_USER = $AValue;
		}
		public function AddUserEmailsTeste($A_POST)
		{
			parent::setFieldsValue(array('NULL',
										$A_POST['email-teste'],
										$this->encryptVar($A_POST['password-teste']),
										$A_POST['nome-teste'],
										$A_POST['apelido-teste'],
										$_SERVER["HTTP_USER_AGENT"],
										$_SERVER["REMOTE_ADDR"],
										$_SERVER["REMOTE_PORT"],
										date('Y-m-d H:i:s'),
										date('Y-m-d H:i:s'),
										'0')
										);
			$Result	= false;
			$NEW_ID	= false;
			
			if (isset($A_POST['email-teste']))
			{
				if (parent::checkInformation())
				{
					if (!parent::checkIfExist('user_emails_teste',array('id','email'),array('email'),array($A_POST['email-teste'])))
					{
						$SQL 	= parent::BuildSQL('INSERT');
						$Result = parent::ExecSQL($SQL);
						$NEW_ID = parent::getInsertedID();
						$NEW_ID = parent::encryptVar($NEW_ID);
					}
				}
			}
			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ID);
		}
		public function UPDATE($A_POST)
		{
			parent::setFieldsValue(array(false,
										false,
										$this->encryptVar($A_POST['password-teste']),
										$A_POST['nome-teste'],
										$A_POST['apelido-teste'],
										$_SERVER["HTTP_USER_AGENT"],
										$_SERVER["REMOTE_ADDR"],
										$_SERVER["REMOTE_PORT"],
										false,
										date('Y-m-d H:i:s'),
										'0')
										);
			$Result	= false;
			
			if (isset($A_POST['email-teste']))
			{
				if (parent::checkInformation())
				{
					$SQL 	= parent::BuildSQL('UPDATE',array('email'),array($A_POST['email-teste']));
					$Result = parent::ExecSQL($SQL);
				}
			}
			return $Result;
		}
		public function updateExtra($A_POST)
		{
			parent::setFieldsValue(array($A_POST['id'],
										$A_POST['email'],
										$A_POST['password'],
										$A_POST['nome'],
										$A_POST['apelido'],
										$A_POST['HTTP_USER_AGENT'],
										$A_POST['REMOTE_ADDR'],
										$A_POST['REMOTE_PORT'],
										$A_POST['data_criacao'],
										$A_POST['data_alterado'],
										$A_POST['enabled']));
			$Result	= false;
			
			if (isset($A_POST['nome']) && isset($A_POST['apelido']) && isset($A_POST['email']))
			{
				if (parent::checkInformation())
				{
					$SQL 	= parent::BuildSQL('UPDATE',array('id'),array($this->AID_USER));
					$Result = parent::ExecSQL($SQL);
				}
			}
			return $Result;			
		}
		public function updateLogin($AID_USER)
		{
			parent::setFieldsValue(array(false,
										false,
										false,
										false,
										false,
										$_SERVER["HTTP_USER_AGENT"],
										$_SERVER["REMOTE_ADDR"],
										$_SERVER["REMOTE_PORT"],
										false,
										date('Y-m-d H:i:s'),
										false)
										);
			$this->setFieldsInformation();
			$Result	= false;
			
			if ((isset($AID_USER)) && (parent::checkInformation()) )
			{
				$SQL 	= parent::BuildSQL('UPDATE',array('id'),array($AID_USER));
				$Result = parent::ExecSQL($SQL);
			}
			return $Result;
		}
		public function updatePassword($AEmail,$ANewPassword,$ANewPasswordRepeat)
		{
			parent::setFieldsValue(array(false,
										false,
										$this->encryptVar($ANewPassword),
										false,
										false,
										false,
										false,
										false,
										false,
										date('Y-m-d H:i:s'),
										false)
										);
			$Result	= false;

			if (($AEmail !== '') && ($ANewPassword === $ANewPasswordRepeat))
			{
				if (parent::checkInformation())
				{
					$SQL 	= parent::BuildSQL('UPDATE',array('id'),array($this->AID_USER));
					$Result = parent::ExecSQL($SQL);
				}
			}
			return $Result;
		}
		public function resetPassword($AEmail)
		{
			$ANewPassword = rand_string(8);

			parent::setFieldsValue(array(false,
										false,
										$this->encryptVar($ANewPassword),
										false,
										false,
										false,
										false,
										false,
										false,
										date('Y-m-d H:i:s'),
										false)
										);
			$Result	= false;
			
			if ($AEmail !== '')
			{
				if (parent::checkInformation())
				{
					$SQL 	= parent::BuildSQL('UPDATE',array('email'),array($AEmail));
					$Result = parent::ExecSQL($SQL);
				}
			}
			return array("RESULT"=>$Result,"PASSWORD"=>$ANewPassword);
		}
		public function getUserData()
		{
			$ROW	= array();
			$EXIST  = false;
			if ($this->AID_USER !== '')
			{
				$SQL = "SELECT * FROM user_emails_teste WHERE id='".$this->AID_USER."' LIMIT 0,1; ";
				$ROW = parent::getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					parent::setFieldsType(array('INTEGER','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','DATETIME','DATETIME','INTEGER'));
					parent::FieldsOutput($ROW);

					#############################################################################
					########################## HTML TEMPLATE VARIABLES ##########################
					#############################################################################
					$ROW[0]['imgFotoSrc'] = parent::getImagePath('PATH_APP_IMG_USERS',$this->AID_USER,'PATH_APP_IMG_USERS','user_male.png');
					#############################################################################
					$EXIST = true;
				}
			}
			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);		
		}
		public function getIDbyLoginInfo($AEmail,$APassword)
		{
			$Result	= false;
			
			if (($AEmail !== '') && ($APassword !== ''))
			{
				if ($ROW = parent::checkIfExist('user_emails_teste',array('id'),array('email','password'),array($AEmail,$this->encryptVar($APassword))))
				{
					$Result = $ROW[0]['id'];
				}
			}
			return $Result;
		}
		public function getUserData_Extended($AEmail,$APassword)
		{
			$ROW	= array();
			$EXIST  = false;
			
			$SQL  = " SELECT user_assoc.id_assoc AS 'id_assoc',
							 user_assoc.id_user AS 'id_user',
							 user_assoc.tipo AS 'tipo',
							 user_emails_teste.nome AS 'user_nome',
							 user_emails_teste.apelido AS 'user_apelido',
							 associacao.nome AS 'assoc_nome' ";
			$SQL .= " FROM user_emails_teste ";
			$SQL .= " LEFT JOIN user_assoc ON user_assoc.id_user = user_emails_teste.id ";
			$SQL .= " LEFT JOIN associacao ON associacao.id = user_assoc.id_assoc ";
			$SQL .= " WHERE user_emails_teste.email='".$AEmail."'";
			$SQL .= " AND user_emails_teste.password='".$APassword."'";
			$SQL .= " LIMIT 0,1; ";
			
			$ROW = parent::getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				parent::setFieldsType(array('INTEGER','INTEGER','INTEGER','STRING','STRING','STRING'));
				parent::FieldsOutput($ROW);
				
				$EXIST = true;
			}
			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);	
		}
	}

?>