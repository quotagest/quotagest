<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'db_tables.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	
	class dbTblModSocio extends dbTable
	{
		public $ID_SOCIO_ENCRYPTED 	= '';
		public $ID_SOCIO_DECRYPTED 	= '';
		public $ID_MOD_ENCRYPTED 	= '';
		public $ID_MOD_DECRYPTED 	= '';
		public $ID_USER 			= '';
		public $ID_ASSOC 			= '';

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName("mod_socio");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_SOCIO_DECRYPTED);
			unset($this->ID_SOCIO_ENCRYPTED);
			unset($this->ID_MOD_DECRYPTED);
			unset($this->ID_MOD_ENCRYPTED);
			unset($this->ID_USER);
			unset($this->ID_ASSOC);
		}

		public function setFieldsInformation()
		{
			parent::setFieldsName(array('id','id_assoc','id_modalidade','id_socio','data_entrada','data_saida','desconto','id_user_created','id_user_edited','data_criacao','data_alterado','enabled'));
			parent::setFieldsType(array('INTEGER','INTEGER','INTEGER','INTEGER','DATE','DATE','FLOAT','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));
			parent::setFieldsRequired(array(true,true,true,true,false,false,false,true,true,true,true,false));
		}

		public function set_IDSOCIO($AsVar)
		{
			$this->ID_SOCIO_ENCRYPTED = $AsVar;
			$this->ID_SOCIO_DECRYPTED = parent::decryptVar($AsVar);
		}
		public function set_IDMODALIDADE($AsVar)
		{
			$this->ID_MOD_ENCRYPTED = $AsVar;
			$this->ID_MOD_DECRYPTED = parent::decryptVar($AsVar);
		}
		public function set_IDUSER($AsVar)  { $this->ID_USER = $AsVar; }
		public function set_IDASSOC($AsVar) { $this->ID_ASSOC = $AsVar; }

		public function addModSocio($A_POST)
		{
			$Result = false;
			$NEW_ID = 0;

			if (($this->ID_MOD_ENCRYPTED !== '') &&
				($this->ID_SOCIO_ENCRYPTED !== ''))
			{
				parent::setFieldsValue(array('NULL',
											  $this->ID_ASSOC,
											  $this->ID_MOD_DECRYPTED,
											  $this->ID_SOCIO_DECRYPTED,	#*$_POST['CodSocio']*
											  $A_POST['data-inscricao'],	#data_entrada
											  $A_POST['data-saida'],		#data_saida
											  $A_POST['desconto'],			#desconto
											  $this->ID_USER,				#id_user_created
											  $this->ID_USER,				#id_user_edited
											  date('Y-m-d H:i:s'),			#data_criacao
											  date('Y-m-d H:i:s'),			#data_alterado
											  '0'							#enabled
											  ));
				if (parent::checkInformation())
				{
					#if (!parent::checkIfExist('mod_socio',array('id'),array('id_socio','id_modalidade','id_assoc'),array($this->ID_SOCIO_DECRYPTED,$this->ID_MOD_DECRYPTED,$this->ID_ASSOC)))	# check if value exists in table, and returns value.
					#{
						$SQL 	= parent::BuildSQL('INSERT');
						$Result = parent::ExecSQL($SQL);
						$NEW_ID = parent::getInsertedID();
						$NEW_ID = parent::encryptVar($NEW_ID);
					#}

				}
			}

			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ID);
		}
		public function updateModSocio($A_POST)
		{
			$Result = false;

			if (($this->ID_MOD_ENCRYPTED !== '') &&
				($this->ID_SOCIO_ENCRYPTED !== ''))
			{
				parent::setFieldsValue(array(false,
											 false,						#id_assoc
											 false,						#id_modalidade
											 false,						#*$_POST['CodSocio']*
											 $A_POST['data-inscricao'],	#data_entrada
											 $A_POST['data-saida'],		#data_saida
											 $A_POST['desconto'],		#desconto
											 false,						#id_user_created
											 $this->ID_USER,			#id_user_edited
											 false,						#data_criacao
											 date('Y-m-d H:i:s'),		#data_alterado
											 '0'						#enabled
											 ));

				if (parent::checkInformation())
				{
					#if ($ROW = parent::checkIfExist('mod_socio',array('id'),array('id_socio','id_modalidade','id_assoc'),array($this->ID_SOCIO_DECRYPTED,$this->ID_MOD_DECRYPTED,$this->ID_ASSOC)))	# check if value exists in table, and returns value.
					#{
						$SQL 	= parent::BuildSQL('UPDATE',array('id'),array($ROW[0]['id']));
						$Result = parent::ExecSQL($SQL);
					#}
				}
			}

			return $Result;
		}
		public function removeModSocio()
		{
			$Result = false;

			if (($this->ID_MOD_ENCRYPTED !== '') &&
				($this->ID_SOCIO_ENCRYPTED !== ''))
			{
				#if ($ROW = parent::checkIfExist('mod_socio',array('id'),array('id_socio','id_modalidade','id_assoc'),array($this->ID_SOCIO_DECRYPTED,$this->ID_MOD_DECRYPTED,$this->ID_ASSOC)))	# check if value exists in table, and returns value.
				#{
					$SQL 	= parent::BuildSQL('UPDATE',array('id','id_modalidade','id_socio'),array($ROW[0]['id'],$this->ID_MOD_DECRYPTED,$this->ID_SOCIO_DECRYPTED));
					$Result = parent::ExecSQL($SQL);
				#}
			}

			return $Result;
		}
		public function getModSocio_ModalidadesETotal()
		{
			$ROW	= array();
			$EXIST 	= false;

			if ($this->ID_SOCIO_ENCRYPTED !== '')
			{
				$SQL = 'SELECT  modalidade.id AS "id_modalidade",
								modalidade.nome AS "nome_modalidade",
								mod_socio.total_divida AS "total_divida",
								mod_socio.total_pago AS "total_pago"
						';
				$SQL .= ' FROM  mod_socio';
				$SQL .= ' INNER JOIN modalidade ON modalidade.id = mod_socio.id_modalidade';
				$SQL .= ' WHERE mod_socio.id_assoc="'.$this->ID_ASSOC.'"';
				$SQL .= ' AND mod_socio.id_socio="'.$this->ID_SOCIO_DECRYPTED.'"';
				$SQL .= ' ORDER BY modalidade.nome ASC ';

				$ROW = parent::getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					parent::setFieldsType(array('ENCRYPT','STRING','FLOAT','FLOAT'));
					parent::FieldsOutput($ROW);

					$EXIST = true;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
	}
?>