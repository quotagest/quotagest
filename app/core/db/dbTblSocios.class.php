<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblAssocSocios.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblModSocio.class.php');

	class dbTblSocios extends dbTable
	{
		public $ID_SOCIO_ENCRYPTED 	= '';
		public $ID_SOCIO_DECRYPTED 	= '';
		public $ID_MOD_ENCRYPTED 	= '';
		public $ID_MOD_DECRYPTED 	= '';
		public $ID_USER 			= '';
		public $ID_ASSOC 			= '';

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName("socio");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_SOCIO_DECRYPTED);
			unset($this->ID_SOCIO_ENCRYPTED);
			unset($this->ID_MOD_DECRYPTED);
			unset($this->ID_MOD_ENCRYPTED);
			unset($this->ID_USER);
			unset($this->ID_ASSOC);
		}

		public function setFieldsInformation()
		{
			parent::setFieldsName(array('id','nome','telefone','telemovel','fax','email','outro1','outro2','morada','codigo_postal','data_nascimento','sexo','bicc','nif','nib','enabled'));
			parent::setFieldsType(array('INTEGER','STRING','STRINGINT','STRINGINT','STRINGINT','STRING','STRING','STRING','MEMO','STRING','DATE','STRING','STRINGINT','STRINGINT','STRINGNEW','INTEGER'));
			parent::setFieldsRequired(array(true,true,false,false,false,false,false,false,false,false,false,true,false,false,false,false));
		}

		public function set_IDSOCIO($AsVar)
		{
			$this->ID_SOCIO_ENCRYPTED = $AsVar;
			$this->ID_SOCIO_DECRYPTED = ($AsVar !== '') ? parent::decryptVar($AsVar) : '';		# decrypts a given variable.
		}
		public function set_IDMODALIDADE($AsVar)
		{
			$this->ID_MOD_ENCRYPTED = $AsVar;
			$this->ID_MOD_DECRYPTED = ($AsVar !== '') ? parent::decryptVar($AsVar) : '';		# decrypts a given variable.
		}
		public function set_IDUSER($AsVar)  { $this->ID_USER = $AsVar; }
		public function set_IDASSOC($AsVar) { $this->ID_ASSOC = $AsVar; }
		
		public function addSocio($A_POST)
		{
			parent::setFieldsValue(array('NULL',
										$A_POST['Nome'],
										$A_POST['Telefone'],
										$A_POST['Telemovel'],
										$A_POST['Fax'],
										$A_POST['Email'],
										$A_POST['Outro1'],
										$A_POST['Outro2'],
										$A_POST['Morada'],
										$A_POST['Codigo-Postal'],
										$A_POST['data-nascimento'],
										$A_POST['socio-sexo'],
										$A_POST['BICC'],
										$A_POST['NIF'],
										$A_POST['NIB'],
										'0'));
			$Result = false;
			$NEW_ID = 0;

			if (parent::checkInformation())																# check information of fields and protects the data.
			{
				#$A_POST['BICC'] = ($A_POST['BICC'] === '') ? '0' : $A_POST['BICC'];
				#$preBICC = parent::protectVar($A_POST['BICC']);											# cleans and protects variable
				#if (parent::checkIfExist('socio',array('id','bicc'),array('bicc'),array($preBICC)))	# check if value exists in table, and returns value.
				#{
					$SQL 	= parent::BuildSQL('INSERT');
					$Result = parent::ExecSQL($SQL);
					$NEW_ID = parent::getInsertedID();
					$NEW_ID = parent::encryptVar($NEW_ID);												# encrypts given variable

					if ($Result)
					{
						$ResultAssoc = $this->addAssociacaoInscricao($A_POST,$NEW_ID);
						return array('Result'=>$Result, 'NEW_ID'=>$NEW_ID,
									 'ResultAssoc'=>$ResultAssoc['Result'],
									 'NEW_ID_ASSOC'=>$ResultAssoc['NEW_ID']);
					}
				#}
			}

			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ID);
		}
		public function updateSocio($A_POST)
		{
			# SE "enabled" = 1 : socio ainda não autorizou ver os dados.
			# SE "activo"  = 1 : socio permite que se veja os dados e que sejam editados.
			# SE "activo"  = 2 : socio permite que se veja os dados mas que não sejam editados.
			parent::setFieldsValue(array(false,
										$A_POST['Nome'],
										$A_POST['Telefone'],
										$A_POST['Telemovel'],
										$A_POST['Fax'],
										$A_POST['Email'],
										$A_POST['Outro1'],
										$A_POST['Outro2'],
										$A_POST['Morada'],
										$A_POST['Codigo-Postal'],
										$A_POST['data-nascimento'],
										$A_POST['socio-sexo'],
										$A_POST['BICC'],
										$A_POST['NIF'],
										$A_POST['NIB'],
										false));
			$Result = false;

			if (($this->ID_SOCIO_ENCRYPTED !== '') && (parent::checkInformation()))
			{
				#$preBICC = parent::protectVar($A_POST['BICC']);													# cleans and protects variable
				#$ROW 	 = parent::checkIfExist('socio',array('id','bicc'),array('bicc'),array($preBICC));			# check if value exists in table, and returns value.
				#$ROWExp  = parent::checkIfExist('assoc_socios',array('id','activo'),array('id_socio','id_assoc'),array($this->ID_SOCIO_DECRYPTED,$this->ID_ASSOC));			# check if value exists in table, and returns value.

				#if ((!$ROW) ||																					# bicc alterado.
				#	(isset($ROW[0]) && ($ROWExp[0]['activo'] === '1') && ($ROW[0]['id'] === $this->ID_SOCIO_DECRYPTED))) 	# mesmo bicc e id.
				#if (parent::checkIfExist('assoc_socios',array('id','activo'),array('id_socio','id_assoc'),array($this->ID_SOCIO_DECRYPTED,$this->ID_ASSOC)))
				#{
					$SQL 	= parent::BuildSQL('UPDATE',array('id'),array($this->ID_SOCIO_DECRYPTED));
					$Result = parent::ExecSQL($SQL);

					if ($Result)
					{
						#parent::setHistory('',$this->ID_ASSOC,$this->ID_USER, '1','2','Sócio editado','O Sócio foi editado.',$SQL,'');

						$ResultAssoc = $this->updateAssociacaoInscricao($A_POST);
						return array('Result'=>$Result, 'ResultAssoc'=>$ResultAssoc);
					}
				#}
			}

			return $Result;
		}
		public function removeSocio()
		{
			$Result = false;

			if (($this->ID_SOCIO_ENCRYPTED !== '') && (parent::checkInformation())) 
			{				
				#if (parent::checkIfExist('socio',array('id'),array('id'),array($this->ID_SOCIO_DECRYPTED)))	# check if value exists in table, and returns value.
				#{
					$SQL 	= parent::BuildSQL('DELETE',array('id'),array($this->ID_SOCIO_DECRYPTED));
					$Result = parent::ExecSQL($SQL);
				#}
			}
			// REMOVER assoc_socios!!

			return $Result;
		}
		public function getSocioData()
		{
			$ROW 	= array();
			$EXIST 	= false;

			if ($this->ID_SOCIO_ENCRYPTED !== '')
			{
				$SQL  = "SELECT socio.nome AS 'nome',
								socio.telefone AS 'telefone',
								socio.telemovel AS 'telemovel',
								socio.fax AS 'fax',
								socio.email AS 'email',
								socio.outro1 AS 'outro1',
								socio.outro2 AS 'outro2',
								socio.morada AS 'morada',
								socio.codigo_postal AS 'codigo_postal',
								socio.data_nascimento AS 'data_nascimento',
								socio.sexo AS 'sexo',
								socio.bicc AS 'bicc',
								socio.nif AS 'nif',
								socio.nib AS 'nib',

								assoc_socios.codigo AS 'codigo',
								assoc_socios.codigo_sufixo AS 'codigo_sufixo',
								assoc_socios.id_socio AS 'id_socio',
								assoc_socios.id_tipo AS 'id_tipo',
								assoc_socios.id_estado AS 'id_estado',
								socio_tipo.nome AS 'socio_nome_tipo',
								socio_estado.nome AS 'socio_nome_estado',
								assoc_socios.divida AS 'divida',
								assoc_socios.data_entrada AS 'data_entrada',
								assoc_socios.data_saida AS 'data_saida',
								assoc_socios.observacoes AS 'observacoes',
								assoc_socios.enabled AS 'enabled',
								assoc_socios.activo AS 'activo',

								assoc_socios.id_user_edited AS 'id_user_edited',
								assoc_socios.data_alterado AS 'data_alterado',

								ctt_concelhos.DESIG AS 'concelho_desig',
								ctt_distritos.DESIG AS 'distrito_desig',
								
								user_emails_teste.nome AS 'user_nome',
								user_emails_teste.apelido AS 'user_apelido'
						";

				$SQL .= ' FROM assoc_socios';
				$SQL .= ' LEFT JOIN user_emails_teste ON user_emails_teste.id=assoc_socios.id_user_edited';
				$SQL .= ' LEFT JOIN socio ON socio.id=assoc_socios.id_socio';
				$SQL .= ' LEFT JOIN socio_estado ON socio_estado.id=assoc_socios.id_estado AND socio_estado.id_assoc="'.$this->ID_ASSOC.'"';
				$SQL .= ' LEFT JOIN socio_tipo ON socio_tipo.id=assoc_socios.id_tipo AND socio_tipo.id_assoc="'.$this->ID_ASSOC.'"';
				$SQL .= " 	LEFT JOIN ctt_codigos_postais ON 
								CONCAT(ctt_codigos_postais.CP4,'-',ctt_codigos_postais.CP3) = socio.codigo_postal

							LEFT JOIN ctt_concelhos ON ctt_concelhos.DD=ctt_codigos_postais.DD
													AND ctt_concelhos.CC=ctt_codigos_postais.CC
							LEFT JOIN ctt_distritos ON ctt_distritos.DD=ctt_codigos_postais.DD
						";
				$SQL .= ' WHERE assoc_socios.id_assoc="'.$this->ID_ASSOC.'"';
				$SQL .= ' AND assoc_socios.enabled="0"';
				$SQL .= ' AND id_socio="'.$this->ID_SOCIO_DECRYPTED.'"';
				$SQL .= ' LIMIT 0,1;';
#echo '<!--';
#echo($SQL);
#echo '-->';
#var_dump($SQL);
				$ROW = parent::getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					parent::setFieldsType(array('STRING','INTEGER','INTEGER','INTEGER','STRING','STRING','STRING','STRING','STRING','DATE','STRING','INTEGER','INTEGER','STRINGINT',
												'STRING','STRING','ENCRYPT','STRINGINT','STRINGINT','STRING','STRING','FLOAT','DATE','DATE','MEMO','STRINGINT','STRINGINT',
												'STRINGINT','DATETIME',
												'STRING','STRING',
												'STRING','STRING'));
					parent::FieldsOutput($ROW);

					#############################################################################
					########################## HTML TEMPLATE VARIABLES ##########################
					#############################################################################
					$imgGender 	= ($ROW[0]['sexo'] === 'Feminino') ? 'user-female-a-01.jpg' : 'user-male-a-01.jpg';
					$ROW[0]['imgSocioSrc'] = parent::getImagePath('PATH_APP_IMG_SOCIOS',$this->ID_SOCIO_ENCRYPTED,'PATH_APP_IMG_ICONS_USERS',$imgGender);
					#$Result = parent::getCTTINFO($ROW[0]['codigo_postal']);
					#$ROW[0]['concelho_desig'] = $Result['concelho'];
					#$ROW[0]['distrito_desig'] = $Result['distrito'];
					#############################################################################

					$EXIST = true;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		/*
		public function checkIfExistSocio()
		{
			if ($this->ID_SOCIO_ENCRYPTED !== '')
			{
				$SQL  = "SELECT assoc_socios.id AS 'id' ";
				$SQL .= ' FROM assoc_socios ';
				$SQL .= ' LEFT JOIN socio ON socio.id=assoc_socios.id_socio ';
				$SQL .= ' WHERE assoc_socios.id_assoc="'.$this->ID_ASSOC.'" ';
				$SQL .= ' AND assoc_socios.id_socio="'.$this->ID_SOCIO_DECRYPTED.'" ';
				$SQL .= ' LIMIT 0,1 ';

				$ROW = parent::getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					return true;
				}
			}
			return false;
		}
		*/
		public function getListSearch($AarrFieldName,$AarrValue, $FieldToOrder,$OrderBy, $iPage=0, $iLimPerPage=10)
		{
			$ROW 	= array();
			$EXIST 	= false;
			$TotalPages = 0;
			$AarrValue  = parent::processArraySearch($AarrFieldName,$AarrValue);
			
			$OrderBy = ($OrderBy === '') ? ' ASC ' : $OrderBy;
			switch ($FieldToOrder)
			{
				case 'nome'			: $FieldToOrder = 'socio.nome '.$OrderBy; 			break;
				case 'tipo'			: $FieldToOrder = 'socio_tipo.nome '.$OrderBy; 		break;
				case 'estado'		: $FieldToOrder = 'socio_estado.nome '.$OrderBy; 	break;
				case 'saldo'		: $FieldToOrder = 'assoc_socios.divida '.$OrderBy; 	break;
				case 'codigo'		: $FieldToOrder = 'assoc_socios.codigo '.$OrderBy; 	break;
				default 			: $FieldToOrder = 'assoc_socios.codigo+0 '.$OrderBy;break;
			}
			$FieldToOrder = ' ORDER BY '.$FieldToOrder;
	
			$sSQLLIKE = ($AarrValue[0] === '*') ? '' : ' AND ('.parent::WHEREUPDATE_PARSER($AarrFieldName,$AarrValue,' OR ','LIKE',false).') ';
			$sLIMIT   = parent::build_LIMITStatement($iPage,$iLimPerPage);
			#$sLIMIT   = ((isset($iPage)) && ($iPage!=='') && ($iPage > 1)) ? ' LIMIT '.(($iPage-1)*$iLimPerPage).','.($iLimPerPage) : (' LIMIT 0,'.$iLimPerPage) ;

			$SQL  = "SELECT socio.nome AS 'nome',
							socio.sexo AS 'sexo',
							assoc_socios.codigo AS 'codigo',
							assoc_socios.codigo_sufixo AS 'codigo_sufixo',
							assoc_socios.id_socio AS 'id_socio',
							assoc_socios.divida AS 'divida',
							socio_tipo.nome AS 'socio_nome_tipo',
							socio_estado.nome AS 'socio_nome_estado'
					";
			$SQL .= ' FROM assoc_socios ';
			$SQL .= ' LEFT JOIN socio ON socio.id=assoc_socios.id_socio ';
			$SQL .= ' LEFT JOIN socio_estado ON socio_estado.id=assoc_socios.id_estado ';
			$SQL .= ' LEFT JOIN socio_tipo ON socio_tipo.id=assoc_socios.id_tipo ';
			/*
				$SQL .= " 	LEFT JOIN ctt_codigos_postais ON 
								CONCAT(ctt_codigos_postais.CP4,'-',ctt_codigos_postais.CP3) = socio.codigo_postal

							LEFT JOIN ctt_concelhos ON ctt_concelhos.DD=ctt_codigos_postais.DD
													AND ctt_concelhos.CC=ctt_codigos_postais.CC
							LEFT JOIN ctt_distritos ON ctt_distritos.DD=ctt_codigos_postais.DD
						";				
			*/
			$SQL .= ' WHERE assoc_socios.id_assoc="'.$this->ID_ASSOC.'" ';
			$SQL .= ' AND assoc_socios.enabled="0" ';
			$SQL .= $sSQLLIKE;
			$SQL .= $FieldToOrder;
			$SQL .= $sLIMIT;
#var_dump($SQL);
			$ROW = parent::getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				parent::setFieldsType(array('STRING','STRING','INTEGER','STRING','ENCRYPT','FLOAT','STRING','STRING'));
				parent::FieldsOutput($ROW);
				
				$EXIST 		= true;
				$TotalPages = parent::getTotalPages($SQL,$iLimPerPage);
			}
			
			return array('ROW'=>$ROW, 'EXIST'=>$EXIST, 'TotalPages'=>$TotalPages);
		}
		/* APAGAR NO FIM*/
		public function getList($FieldToOrder,$OrderBy,$iPage=0, $iLimPerPage=10)
		{
			$ROW 	= array();
			$EXIST 	= false;

			$sLIMIT = parent::build_LIMITStatement($iPage,$iLimPerPage);
			#$sLIMIT = ((isset($iPage)) && ($iPage!=='') && ($iPage > 1)) ? 'LIMIT '.(($iPage-1)*$iLimPerPage).','.($iLimPerPage) : (' LIMIT 0,'.$iLimPerPage) ;

			$SQL  = "SELECT socio.nome AS 'nome',
							assoc_socios.codigo AS 'codigo',
							assoc_socios.codigo_sufixo AS 'codigo_sufixo',
							assoc_socios.id_socio AS 'id_socio',
							socio_tipo.nome AS 'socio_nome_tipo',
							socio_estado.nome AS 'socio_nome_estado',
							assoc_socios.divida AS 'divida'
					";
			$SQL .= ' FROM assoc_socios ';
			$SQL .= ' LEFT JOIN socio ON socio.id=assoc_socios.id_socio ';
			$SQL .= ' LEFT JOIN socio_estado ON socio_estado.id=assoc_socios.id_estado AND socio_estado.id_assoc="'.$this->ID_ASSOC.'"';
			$SQL .= ' LEFT JOIN socio_tipo ON socio_tipo.id=assoc_socios.id_tipo AND socio_tipo.id_assoc="'.$this->ID_ASSOC.'"';
			$SQL .= ' WHERE assoc_socios.id_assoc="'.$this->ID_ASSOC.'" ';
			$SQL .= ' AND assoc_socios.enabled="0" ';

			switch ($FieldToOrder)
			{
				case 'nome'			: $FieldToOrder = 'socio.nome '.$OrderBy; 			break;
				case 'tipo'			: $FieldToOrder = 'socio_tipo.nome '.$OrderBy; 		break;
				case 'estado'		: $FieldToOrder = 'socio_estado.nome '.$OrderBy; 	break;
				case 'saldo'		: $FieldToOrder = 'assoc_socios.divida '.$OrderBy; 	break;
				default 			: $FieldToOrder = 'assoc_socios.codigo+0 ASC '; 	break;
			}
			$SQL .= ' ORDER BY '.$FieldToOrder;
			$SQL .= $sLIMIT;
#var_dump($SQL);
			$ROW = parent::getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				parent::setFieldsType(array('STRING','STRING','STRING','ENCRYPT','STRING','STRING','FLOAT'));
				parent::FieldsOutput($ROW);

				$ccI = count($ROW);
				for ($i=0; $i<$ccI; $i++)
				{
					#############################################################################
					########################## HTML TEMPLATE VARIABLES ##########################
					#############################################################################
					$fDivida = (float) ($ROW[$i]['divida']+0.0);
					if ($fDivida > 0)
						$numCLass = 'green';
					else
					if ($fDivida == 0)
						$numCLass = '';
					else
						$numCLass = 'red';

					$ROW[$i]['numClass'] = $numCLass;
					#############################################################################
				}
				$EXIST = true;
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		/* APAGAR NO FIM*/
		public function searchSocios($AarrFieldName,$AarrValue,$AsLimit)
		{
			if ((!is_array($AarrValue)) || (count($AarrValue)==1))
			{
				$arrAux = array();
				$isArr  = is_array($AarrValue);
				$isStr	= (!$isArr);

				$ccI 	= count($AarrFieldName);
				for ($i=0; $i<$ccI; $i++)
				{
					$arrAux[] = ($isArr) ? $AarrValue[0] : $AarrValue;
				}

				$AarrValue = $arrAux;
			}
			
			$sSQLLIKE = ($AarrValue[0] === '*') ? '' : ' AND ('.parent::WHEREUPDATE_PARSER($AarrFieldName,$AarrValue,' OR ','LIKE',false).') ';

			$SQL  = "SELECT socio.nome AS 'nome',
							socio.bicc AS 'bicc',
							socio.nif AS 'nif',
							socio.email AS 'email',
							assoc_socios.codigo AS 'codigo',
							assoc_socios.codigo_sufixo AS 'codigo_sufixo',
							assoc_socios.id_socio AS 'id_socio',
							assoc_socios.divida AS 'divida',

							socio_tipo.nome AS 'socio_nome_tipo',
							socio_estado.nome AS 'socio_nome_estado'
					";
			$SQL .= ' FROM assoc_socios ';
			$SQL .= ' LEFT JOIN socio ON socio.id=assoc_socios.id_socio ';
			$SQL .= ' LEFT JOIN socio_estado ON socio_estado.id=assoc_socios.id_estado AND socio_estado.id_assoc="'.$this->ID_ASSOC.'"';
			$SQL .= ' LEFT JOIN socio_tipo ON socio_tipo.id=assoc_socios.id_tipo AND socio_tipo.id_assoc="'.$this->ID_ASSOC.'"';
			$SQL .= ' WHERE assoc_socios.id_assoc="'.$this->ID_ASSOC.'" ';
			$SQL .= $sSQLLIKE;
			$SQL .= ' AND assoc_socios.enabled="0" ';
			$SQL .= ' ORDER BY assoc_socios.codigo+0 ASC ';
			#$SQL .= ' ORDER BY socio.nome ASC ';
			$SQL .= ' LIMIT '.$AsLimit;
			$ROW = parent::getRESULTS($SQL);

			if (isset($ROW[0]))
			{
				parent::setFieldsType(array('STRING','INTEGER','INTEGER','STRING','STRING','STRING','ENCRYPT','FLOAT','STRING','STRING'));
				parent::FieldsOutput($ROW);
			}
			return $ROW;
		}
		public function filterSocios($AEstado='',$ATipo='',$AModalidade='',$ADividaInicio='',$ADividaFim='',$EstadoQuotas='',$AInscAntes='',$AInscDepois='',$ANumSocioInicio='',$ANumSocioFim='')
		{
			$ROW 	= array();
			$EXIST 	= false;

			if ($EstadoQuotas !== '')
			{
				if ($EstadoQuotas === 'all-payed')
					$EstadoQuotas = ' AND assoc_socios.divida>="0" ';
				else
				if ($EstadoQuotas === 'none-payed')
					$EstadoQuotas = ' AND assoc_socios.divida<"0" ';			
			}

			$AEstado		= ($AEstado !== 'all')  	? parent::build_ORStatments('socio_estado.id',$AEstado) : '';
			$ATipo 			= ($ATipo !== 'all') 	 	? parent::build_ORStatments('socio_tipo.id',$ATipo) 	: '';
			$AModalidade 	= ($AModalidade !== '') 	? parent::build_ORStatments('mod_socio.id_modalidade',$AModalidade) : '';
			$ADividaInicio 	= ($ADividaInicio !== '') 		? ' AND assoc_socios.divida<="'.$ADividaInicio.'" '		: '';
			$ADividaFim 	= ($ADividaFim !== '') 			? ' AND assoc_socios.divida>="'.$ADividaFim.'" '		: '';
			$AInscAntes 	= ($AInscAntes !== '') 			? ' AND assoc_socios.data_entrada<="'.$AInscAntes.'" '	: '';
			$AInscDepois 	= ($AInscDepois !== '') 		? ' AND assoc_socios.data_entrada>="'.$AInscDepois.'" '	: '';
			$ANumSocioInicio= ($ANumSocioInicio !== '') 	? ' AND assoc_socios.codigo>="'.$ANumSocioInicio.'" '	: '';
			$ANumSocioFim 	= ($ANumSocioFim !== '') 		? ' AND assoc_socios.codigo<="'.$ANumSocioFim.'" '		: '';
			
			$SQLFilter	 = $AEstado.$ATipo.$AModalidade;
			$SQLFilter	.= $ADividaInicio.$ADividaFim;
			$SQLFilter	.= $AInscAntes.$AInscDepois;
			$SQLFilter	.= $ANumSocioInicio.$ANumSocioFim;
			$SQLFilter	.= $EstadoQuotas;
			
			$SQLSelect = "SELECT socio.nome AS 'nome',
								socio.bicc AS 'bicc',
								assoc_socios.codigo AS 'codigo',
								assoc_socios.codigo_sufixo AS 'codigo_sufixo',
								assoc_socios.id_socio AS 'id_socio',
								assoc_socios.data_entrada AS 'data_entrada',
								socio_tipo.nome AS 'socio_nome_tipo',
								socio_estado.nome AS 'socio_nome_estado',
								assoc_socios.divida AS 'divida'
						";

			$SQL  = $SQLSelect;
			$SQL .= ' FROM assoc_socios ';
			$SQL .= ' LEFT JOIN socio ON socio.id=assoc_socios.id_socio ';
			$SQL .= ' LEFT JOIN socio_estado ON socio_estado.id=assoc_socios.id_estado AND socio_estado.id_assoc="'.$this->ID_ASSOC.'"';
			$SQL .= ' LEFT JOIN socio_tipo ON socio_tipo.id=assoc_socios.id_tipo AND socio_tipo.id_assoc="'.$this->ID_ASSOC.'"';
			$SQL .= ' WHERE assoc_socios.id_assoc="'.$this->ID_ASSOC.'" ';
			$SQL .= ' AND assoc_socios.enabled="0" ';
			$SQL .= $SQLFilter;
			$SQL .= ' ORDER BY assoc_socios.codigo+0 ASC ';
			#$SQL .= ' ORDER BY socio.nome ';
			###############################
			if ($AModalidade !== '')
			{
				$SQL  = $SQLSelect;
				$SQL .= ' FROM mod_socio ';
				$SQL .= ' LEFT JOIN socio ON socio.id=mod_socio.id_socio ';
				$SQL .= ' LEFT JOIN assoc_socios ON socio.id=assoc_socios.id_socio ';
				$SQL .= ' LEFT JOIN socio_estado ON socio_estado.id=assoc_socios.id_estado AND socio_estado.id_assoc="'.$this->ID_ASSOC.'"';
				$SQL .= ' LEFT JOIN socio_tipo ON socio_tipo.id=assoc_socios.id_tipo AND socio_tipo.id_assoc="'.$this->ID_ASSOC.'"';
				$SQL .= ' WHERE assoc_socios.id_assoc="'.$this->ID_ASSOC.'" ';
				$SQL .= ' AND assoc_socios.enabled="0" ';
				#$SQL .= ' AND mod_socio.id_modalidade="'.(parent::decryptVar($AModalidade)).'" ';
				$SQL .= $SQLFilter;
				$SQL .= ' GROUP BY mod_socio.id_socio ';
				$SQL .= ' ORDER BY assoc_socios.codigo+0 ASC ';
			}
			###############################
		#echo '<!--';
		#var_dump($SQL);
		#echo '-->';

			$ROW = parent::getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				parent::setFieldsType(array('STRING','STRINGNEW','STRING','STRING','ENCRYPT','DATE','STRING','STRING','FLOAT'));
				parent::FieldsOutput($ROW);
				$EXIST = true;
			}
			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function addAssociacaoInscricao($A_POST,$ANEW_SOCIO_ID)
		{
			$dbLink = parent::getDBConnection();
			$TblAssocSocios = new dbTblAssocSocios($dbLink);
			$TblAssocSocios->set_IDUSER($this->ID_USER);
			$TblAssocSocios->set_IDASSOC($this->ID_ASSOC);
			$resArr	= $TblAssocSocios->INSERT($A_POST,$ANEW_SOCIO_ID,$this->ID_USER,$this->ID_ASSOC);
			$Result 			= $resArr['Result'];
			$NEW_ASSOCSOCIO_ID  = $resArr['NEW_ID'];
			unset($TblAssocSocios);
			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ASSOCSOCIO_ID);
		}
		public function updateAssociacaoInscricao($A_POST)
		{
			$dbLink = parent::getDBConnection();
			$TblAssocSocios = new dbTblAssocSocios($dbLink);
			$TblAssocSocios->set_IDSOCIO($this->ID_SOCIO_ENCRYPTED);
			$TblAssocSocios->set_IDUSER($this->ID_USER);
			$TblAssocSocios->set_IDASSOC($this->ID_ASSOC);
			$Result 		= $TblAssocSocios->UPDATE($A_POST,$this->ID_SOCIO_ENCRYPTED,$this->ID_USER,$this->ID_ASSOC);
			unset($TblAssocSocios);
			return $Result;
		}
		public function addModalidadeInscricao($A_POST)
		{
			$Result = false;
			# SOCIO TEM DE EXISTIR NA BD
			#if ($this->checkIfExistSocio($this->ID_SOCIO_DECRYPTED,$this->ID_USER,$this->ID_ASSOC))
			#{
				$dbLink = parent::getDBConnection();
				$TblModSocio = new dbTblModSocio($dbLink);
				$TblModSocio->set_IDSOCIO($this->ID_SOCIO_ENCRYPTED);
				$TblModSocio->set_IDMODALIDADE($this->ID_MOD_ENCRYPTED);
				$TblModSocio->set_IDUSER($this->ID_USER);
				$TblModSocio->set_IDASSOC($this->ID_ASSOC);
				$Result 		= $TblModSocio->addModSocio($A_POST);
				unset($TblModSocio);
			#}
			return $Result;
		}
		public function updateModalidadeInscricao($A_POST)
		{
			$Result = false;
			#if ($this->checkIfExistSocio($this->ID_SOCIO_DECRYPTED,$this->ID_USER,$this->ID_ASSOC))
			#{
				$dbLink = parent::getDBConnection();
				$TblModSocio = new dbTblModSocio($dbLink);
				$TblModSocio->set_IDSOCIO($this->ID_SOCIO_ENCRYPTED);
				$TblModSocio->set_IDMODALIDADE($this->ID_MOD_ENCRYPTED);
				$TblModSocio->set_IDUSER($this->ID_USER);
				$TblModSocio->set_IDASSOC($this->ID_ASSOC);
				$Result 		= $TblModSocio->updateModSocio($A_POST);
				unset($TblModSocio);
			#}
			return $Result;
		}
		public function removeModalidadeInscricao()
		{
			$dbLink = parent::getDBConnection();
			$TblModSocio = new dbTblModSocio($dbLink);
			$TblModSocio->set_IDSOCIO($this->ID_SOCIO_ENCRYPTED);
			$TblModSocio->set_IDMODALIDADE($this->ID_MOD_ENCRYPTED);
			$TblModSocio->set_IDUSER($this->ID_USER);
			$TblModSocio->set_IDASSOC($this->ID_ASSOC);
			$Result 		= $TblModSocio->removeModSocio();
			unset($TblModSocio);
			return $Result;
		}
		public function getModalidadesTotal()
		{
			$dbLink = parent::getDBConnection();
			$TblModSocio = new dbTblModSocio($dbLink);
			$TblModSocio->set_IDSOCIO($this->ID_SOCIO_ENCRYPTED);
			$TblModSocio->set_IDMODALIDADE($this->ID_MOD_ENCRYPTED);
			$TblModSocio->set_IDUSER($this->ID_USER);
			$TblModSocio->set_IDASSOC($this->ID_ASSOC);
			$Result 		= $TblModSocio->getModSocio_ModalidadesETotal();
			unset($TblModSocio);
			return $Result;
		}
	}
?>