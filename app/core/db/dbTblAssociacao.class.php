<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblUserAssoc.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'db_tables.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'ctt.api.class.php');
	
	class dbTblAssociacao extends dbTable
	{
		public $ID_USER 			= '';
		public $ID_ASSOC 			= '';
		public $arrTipoActividade 	= array('Ambiental',
											'Agrícola',
											'Bombeiros',
											'Caçadores e Pescadores',
											'Cultural',
											'Cultural, Desportiva e Recreativa',
											'Dadores de Sangue',
											'Desenvolvimento Local',
											'Desportiva',
											'Diversos (outras)',
											'Educação',
											'Escolar',
											'Escuteiros',
											'Juventude',
											'Lions / Rotary',
											'Moradores',
											'Musical',
											'Recreativa',
											'Reformados',
											'Religiosa',
											'Solidariedade Social');

		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName('associacao');
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_USER);
			unset($this->ID_ASSOC);
		}

		public function set_IDUSER($AsVar)  { $this->ID_USER = $AsVar; }
		public function set_IDASSOC($AsVar) { $this->ID_ASSOC = $AsVar; }

		public function setFieldsInformation()
		{
			parent::setFieldsName(array('id','nome','abreviatura','nif','nib','telefone','telemovel','fax','email','site','outro1','outro2','morada','codigo_postal','observacoes','slogan','historia','tipo_actividade','id_user_created','id_user_edited','data_criacao','data_alterado','enabled'));
			parent::setFieldsType(array('INTEGER','STRING','STRING','INTEGER','STRINGINT','INTEGER','INTEGER','INTEGER','STRING','STRING','STRING','STRING','MEMO','STRING','MEMO','STRING','MEMO','INTEGER','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));
			parent::setFieldsRequired(array(true,true,false,false,false,false,false,false,false,false,false,false,true,true,false,false,false,true,true,true,true,true,true));
		}

		public function addAssociacao($A_POST)
		{
			$Result			= false;
			$NEW_ASSOC_ID 	= false;

			parent::setFieldsValue(array('NULL',
									  $A_POST['Nome'],
									  $A_POST['Abreviatura'],
									  $A_POST['NIF'],
									  $A_POST['NIB'],
									  $A_POST['Telefone'],
									  $A_POST['Telemovel'],
									  $A_POST['Fax'],
									  $A_POST['Email'],
									  $A_POST['Site'],
									  $A_POST['Outro1'],
									  $A_POST['Outro2'],
									  $A_POST['Morada'],
									  $A_POST['Codigo-Postal'],
									  $A_POST['Observacoes'],
									  $A_POST['Slogan'],
									  $A_POST['DescricaoHistoria'],
									  $A_POST['TipoActividade'],
									  $this->ID_USER,				#id_user_created
									  $this->ID_USER,				#id_user_edited
									  date('Y-m-d H:i:s'),			#data_criacao
									  date('Y-m-d H:i:s'),			#data_alterado
									  '0')							#enabled
									);
			
			if (parent::checkInformation())
			{
				$SQL 		  = parent::BuildSQL('INSERT');
				$Result 	  = parent::ExecSQL($SQL);
				$NEW_ASSOC_ID = parent::getInsertedID();

				if ($Result)
				{
					$A_POST = array();
					$A_POST['tipo'] = 0;
					$A_POST['observacoes'] = '';
					$resUserAssoc = $this->INSERT_USERASSOC($A_POST,$this->ID_USER,$NEW_ASSOC_ID);
					$this->INSERT_MULTIPLE();
					$ResultUA = $resUserAssoc['Result'];
					$NEW_IDUA = $resUserAssoc['NEW_ID'];
				}
			}
			
			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ASSOC_ID);
		}
		public function updateAssociacao($A_POST)
		{
			$Result		= false;

			if ($this->ID_ASSOC !== '')
			{
				#if (parent::checkIfExist('associacao',array('id'),array('id','id_user'),array($this->ID_ASSOC,$this->ID_USER)))
				#{
					parent::setFieldsValue(array(false,
												 $A_POST['Nome'],
												 $A_POST['Abreviatura'],
												 $A_POST['NIF'],
												 $A_POST['NIB'],
												 $A_POST['Telefone'],
												 $A_POST['Telemovel'],
												 $A_POST['Fax'],
												 $A_POST['Email'],
												 $A_POST['Site'],
												 $A_POST['Outro1'],
												 $A_POST['Outro2'],
												 $A_POST['Morada'],
												 $A_POST['Codigo-Postal'],
												 $A_POST['Observacoes'],
												 $A_POST['Slogan'],
												 $A_POST['Historia'],
												 $A_POST['TipoActividade'],
												 false,						#id_user_created
												 $this->ID_USER,			#id_user_edited
												 false,						#data_criacao
												 date('Y-m-d H:i:s'),		#data_alterado
												 false)						#enabled
											);

					if (parent::checkInformation())
					{
						$SQL 		  = parent::BuildSQL('UPDATE',array('id','id_user'),array($this->ID_ASSOC,$this->ID_USER));
						$Result 	  = parent::ExecSQL($SQL);
					}
				#}
			}
			
			return $Result;
		}
		public function removeAssociacao()
		{
			$Result		= false;

			if ($this->ID_ASSOC !== '')
			{
				#if (parent::checkIfExist('associacao',array('id'),array('id','id_user'),array($this->ID_ASSOC,$this->ID_USER)))
				#{
					$SQL 	= parent::BuildSQL('DELETE',array('id'),array($this->ID_ASSOC));
					$Result = parent::ExecSQL($SQL);
				#}
			}
			
			return $Result;
		}
		public function getAssociacaoData()
		{
			$ROW	= array();
			$EXIST	= false;

			if ($this->ID_ASSOC !== '')
			{
				$SQL  = "SELECT associacao.nome AS 'nome',
								associacao.abreviatura AS 'abreviatura',
								associacao.nif AS 'nif',
								associacao.nib AS 'nib',
								associacao.telefone AS 'telefone',
								associacao.telemovel AS 'telemovel',
								associacao.fax AS 'fax',
								associacao.email AS 'email',
								associacao.site AS 'site',
								associacao.outro1 AS 'outro1',
								associacao.outro2 AS 'outro2',
								associacao.morada AS 'morada',
								associacao.codigo_postal AS 'codigo_postal',
								associacao.observacoes AS 'observacoes',
								associacao.slogan AS 'slogan',
								associacao.historia AS 'historia',
								associacao.tipo_actividade AS 'tipo_actividade',
								associacao.enabled AS 'enabled',

								associacao.id_user_edited AS 'id_user_edited',
								associacao.data_alterado AS 'data_alterado',

								ctt_concelhos.DESIG AS 'concelho_desig',
								ctt_distritos.DESIG AS 'distrito_desig'
						";
				$SQL .= ' FROM associacao';
				$SQL .= " 	LEFT JOIN ctt_codigos_postais ON 
								CONCAT(ctt_codigos_postais.CP4,'-',ctt_codigos_postais.CP3) = associacao.codigo_postal

							INNER JOIN ctt_concelhos ON ctt_concelhos.DD=ctt_codigos_postais.DD
													AND ctt_concelhos.CC=ctt_codigos_postais.CC
							INNER JOIN ctt_distritos ON ctt_distritos.DD=ctt_codigos_postais.DD

							LEFT JOIN user_assoc ON user_assoc.id_assoc=associacao.id
						";
				$SQL .= ' WHERE user_assoc.id_user="'.$this->ID_USER.'"';
				$SQL .= ' AND associacao.id="'.$this->ID_ASSOC.'"';
				$SQL .= ' LIMIT 0,1;';

				$ROW = parent::getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					parent::setFieldsType(array('STRING','STRING','INTEGER','STRINGINT','INTEGER','INTEGER','INTEGER','STRING','STRING','STRING','STRING',
												'MEMO','STRING','MEMO','MEMO','MEMO','STRING','STRINGINT',
												'STRINGINT','DATETIME',
												'STRING','STRING'));
					parent::FieldsOutput($ROW);

					$ID_ASSOC	= parent::encryptVar($this->ID_ASSOC);
					#############################################################################
					########################## HTML TEMPLATE VARIABLES ##########################
					#############################################################################
					$ROW[0]['imgLogoSrc'] = parent::getImagePath('PATH_APP_IMG_ASSOCS',$ID_ASSOC,'PATH_APP_IMG_ASSOCS','quotagest-logo.png');

					#$Result = parent::getCTTINFO($ROW[0]['codigo_postal']);
					#$ROW[0]['concelho_desig'] = $Result['concelho'];
					#$ROW[0]['distrito_desig'] = $Result['distrito'];
					#############################################################################
					$EXIST = true;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		private function INSERT_USERASSOC($A_POST,$AID_USER,$AID_ASSOC)
		{
			$dbLink = parent::getDBConnection();
			$TblUserAssoc = new dbTblUserAssoc($dbLink);
			$TblUserAssoc->set_IDUSER($AID_USER);
			$TblUserAssoc->set_IDASSOC($AID_ASSOC);
			$arrResult = $TblUserAssoc->INSERT($A_POST);
			unset($TblUserAssoc);

			return $arrResult;
		}
		private function INSERT_MULTIPLE()
		{
			// modalidades
			$SQL 	 = 'INSERT INTO modalidade (id,id_assoc,nome,id_user_created,id_user_edited,data_criacao,data_alterado,enabled)';
			$SQL 	.= " VALUES (NULL,'".$this->ID_ASSOC."','MODALIDADE GERAL','".$this->ID_USER."','".$this->ID_USER."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','0')";
			$Result = parent::ExecSQL($SQL);
			
			// métudos de pagamento
			$SQL 	 = 'INSERT INTO pag_forma (id,id_assoc,nome,id_user_created,id_user_edited,data_criacao,data_alterado,enabled)';
			$SQL 	.= " VALUES (NULL,'".$this->ID_USER."','Dinheiro','".$this->ID_USER."','".$this->ID_USER."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','0'), 
								(NULL,'".$this->ID_USER."','Multibanco','".$this->ID_USER."','".$this->ID_USER."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','0'),
								(NULL,'".$this->ID_USER."','".utf8_decode('Transferência Bancária')."','".$this->ID_USER."','".$this->ID_USER."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','0')
						";
			$Result = parent::ExecSQL($SQL);
			/*
			// opções de pagamento
			$SQL 	 = 'INSERT INTO pag_opcao (id,id_user,nome,id_user_created,id_user_edited,data_criacao,data_alterado,enabled)';
			$SQL 	.= " VALUES (NULL,'".$this->ID_USER."','".utf8_decode('Jóia')."','".$this->ID_USER."','".$this->ID_USER."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','0'), 
								(NULL,'".$this->ID_USER."','Quota','".$this->ID_USER."','".$this->ID_USER."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','0'),
								(NULL,'".$this->ID_USER."','Donativo','".$this->ID_USER."','".$this->ID_USER."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','0'),
								(NULL,'".$this->ID_USER."','Outros','".$this->ID_USER."','".$this->ID_USER."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','0')
						";
			$Result = parent::ExecSQL($SQL);
			*/
			// estado do sócio
			$SQL 	 = 'INSERT INTO socio_estado (id,id_assoc,nome,id_user_created,id_user_edited,data_criacao,data_alterado,enabled)';
			$SQL 	.= " VALUES (NULL,'".$this->ID_USER."','Activo','".$this->ID_USER."','".$this->ID_USER."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','0'), 
								(NULL,'".$this->ID_USER."','Inactivo','".$this->ID_USER."','".$this->ID_USER."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','0'),
								(NULL,'".$this->ID_USER."','Caducado','".$this->ID_USER."','".$this->ID_USER."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','0')
						";
			$Result = parent::ExecSQL($SQL);

			// tipo de sócio
			$SQL 	 = 'INSERT INTO socio_tipo (id,id_assoc,nome,joia,id_user_created,id_user_edited,data_criacao,data_alterado,enabled)';
			$SQL 	.= " VALUES (NULL,'".$this->ID_USER."','Normal',0.00,'".$this->ID_USER."','".$this->ID_USER."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','0')"; 
			$Result = parent::ExecSQL($SQL);
		}
	}

?>