<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	
	class dbTblEntidades extends dbTable
	{
		public $ID_ENTIDADE_DECRYPTED 	= '';
		public $ID_ENTIDADE_ENCRYPTED 	= '';
		public $ID_USER 				= '';
		public $ID_ASSOC 				= '';

		var $arr_ENTIDADES_FIELDS			= array('id','nome','abreviatura','nif','nib','telefone','telemovel','fax','email','url','outro1','outro2','codigo_postal','morada','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
		var $arr_ENTIDADES_TYPES			= array('INTEGER','STRING','STRING','STRINGINT','INTEGERNEW','STRINGINT','STRINGINT','STRINGINT','STRING','STRING','STRING','STRING','STRING','MEMO','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');
		var $arr_USER_ENTIDADES_FIELDS		= array('id','id_user','id_entidade','tipo','observacoes','id_user_created','id_user_edited','data_criacao','data_alterado','enabled');
		var $arr_USER_ENTIDADES_TYPES		= array('INTEGER','INTEGER','INTEGER','INTEGER','MEMO','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER');
		
		public function __construct($ADBConnection=false)
		{
			parent::__construct($ADBConnection);

			$this->setTableName('entidades');
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_USER);
			unset($this->ID_ASSOC);
		}

		public function set_IDENTIDADE($AsVar)
		{
			$this->ID_ENTIDADE_ENCRYPTED = $AsVar;
			$this->ID_ENTIDADE_DECRYPTED = parent::decryptVar($AsVar);
		}
		public function set_IDUSER($AsVar)  { $this->ID_USER = $AsVar; }
		public function set_IDASSOC($AsVar) { $this->ID_ASSOC = $AsVar; }

		public function setFieldsInformation()
		{
			parent::setFieldsName(array('id','nome','abreviatura','nif','nib','telefone','telemovel','fax','email','url','outro1','outro2','codigo_postal','morada','id_user_created','id_user_edited','data_criacao','data_alterado','enabled'));
			parent::setFieldsType(array('INTEGER','STRING','STRING','STRINGINT','INTEGERNEW','STRINGINT','STRINGINT','STRINGINT','STRING','STRING','STRING','STRING','STRING','MEMO','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));
			parent::setFieldsRequired(array(true,true,false,false,false,false,false,false,false,false,false,false,true,true,true,true,true,true,true));
		}
		public function setFieldsInformation_UserEntidades()
		{
			parent::setTableName('user_entidades');
			parent::setFieldsName(array('id','id_user','id_entidade','tipo','observacoes','id_user_created','id_user_edited','data_criacao','data_alterado','enabled'));
			parent::setFieldsType(array('INTEGER','INTEGER','INTEGER','INTEGER','MEMO','INTEGER','INTEGER','DATETIME','DATETIME','INTEGER'));	
			parent::setFieldsRequired(array(true,true,true,true,false,true,true,true,true,true));
		}
		
		public function INSERT($A_POST)
		{
			return $this->addEntidade($A_POST);
		}
		public function UPDATE($A_POST)
		{
			return $this->updateEntidade($A_POST);
		}
		
		public function addEntidade($A_POST) /* RETURNS ARRAY */
		{
			$Result			 = false;
			$NEW_ENTIDADE_ID = false;

			/* VERIFICA SE NIF JA EXISTE NA BD */
			#$ROW = $this->SQL_SELECT('entidades',array('id'),array('nif'),array($A_POST['NIF']));
			#if (!isset($ROW[0]))
			#{
				parent::setFieldsValue(array('NULL',
									 $A_POST['Nome'],
									 $A_POST['Abreviatura'],
									 $A_POST['NIF'],
									 $A_POST['NIB'],
									 $A_POST['Telefone'],
									 $A_POST['Telemovel'],
									 $A_POST['Fax'],
									 $A_POST['Email'],
									 $A_POST['Site'],
									 $A_POST['Outro1'],
									 $A_POST['Outro2'],
									 $A_POST['Codigo-Postal'],
									 $A_POST['Morada'],
									 $this->ID_USER,			#id_user_created
									 $this->ID_USER,			#id_user_edited
									 date('Y-m-d H:i:s'),		#data_criacao
									 date('Y-m-d H:i:s'),		#data_alterado
									 '0')						#enabled
								);
			if (parent::checkInformation())
			{
				$SQL 	= parent::BuildSQL('INSERT');
				$Result = parent::ExecSQL($SQL);
				$NEW_ENTIDADE_ID = parent::getInsertedID();
			}
			#}
			#else
			#{
			#	/* REGISTO JA SE ENCONTRA INSERIDO */
			#	$NEW_ENTIDADE_ID = $ROW[0]['id'];
			#}

			#$ROW = $this->SQL_SELECT('user_entidades',array('id','id_entidade'),array('id_entidade','id_user'),array($NEW_ENTIDADE_ID,$this->ID_USER));
			#if (!isset($ROW[0]))
			#{
			parent::setFieldsValue(array('NULL',
										$this->ID_USER,
										$NEW_ENTIDADE_ID,
										$A_POST['Tipo-Entidade'],
										$A_POST['Observacoes'],
										$this->ID_USER,				#id_user_created
										$this->ID_USER,				#id_user_edited
										date('Y-m-d H:i:s'),		#data_criacao
										date('Y-m-d H:i:s'),		#data_alterado
										'0')						#enabled
									);
			$this->setFieldsInformation_UserEntidades();
			if (parent::checkInformation())
			{
				$SQL 	= parent::BuildSQL('INSERT');
				$ResultAux = parent::ExecSQL($SQL);
			}
			if ($NEW_ENTIDADE_ID != false)
			{
				$NEW_ENTIDADE_ID = parent::encryptVar($NEW_ENTIDADE_ID);
			}
			#}

			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ENTIDADE_ID);
		}
		public function updateEntidade($A_POST)
		{
			$Result			 = false;
			$NEW_ENTIDADE_ID = false;

			if ($this->ID_ENTIDADE_DECRYPTED !== '')
			{
				#if (parent::checkIfExist('entidades',array('id'),array('id'),array($this->ID_ENTIDADE_DECRYPTED)))
				#{
					parent::setFieldsValue(array(false,
												 $A_POST['Nome'],
												 $A_POST['Abreviatura'],
												 $A_POST['NIF'],
												 $A_POST['NIB'],
												 $A_POST['Telefone'],
												 $A_POST['Telemovel'],
												 $A_POST['Fax'],
												 $A_POST['Email'],
												 $A_POST['Site'],
												 $A_POST['Outro1'],
												 $A_POST['Outro2'],
												 $A_POST['Codigo-Postal'],
												 $A_POST['Morada'],
												 false,						#id_user_created
												 $this->ID_USER,			#id_user_edited
												 false,						#data_criacao
												 date('Y-m-d H:i:s'),		#data_alterado
												 false)						#enabled
												);
					if (parent::checkInformation())
					{
						$SQL 	= $this->BuildSQL('UPDATE',array('id'),array($this->ID_ENTIDADE_DECRYPTED));
						$Result = $this->ExecSQL($SQL);

						if ($Result)
						{
							#if (parent::checkIfExist('user_entidades',array('id','id_entidade'),array('id_entidade','id_user'),array($this->ID_ENTIDADE_DECRYPTED,$this->ID_USER)))
							#{
								parent::setFieldsValue(array(false,
															false,
															false,
															$A_POST['Tipo-Entidade'],
															$A_POST['Observacoes'],
															false,						#id_user_created
															$this->ID_USER,				#id_user_edited
															false,						#data_criacao
															date('Y-m-d H:i:s'),		#data_alterado
															false)						#enabled
															);
								$this->setFieldsInformation_UserEntidades();
								if (parent::checkInformation())
								{
									$SQL 	= parent::BuildSQL('UPDATE',array('id_user','id_entidade'),array($this->ID_USER,$this->ID_ENTIDADE_DECRYPTED));
									$Result = parent::ExecSQL($SQL);
								}
							#}
						}
					}
				#}
			}

			return $Result;
		}
		public function getDadosEntidade()
		{
			$ROW 	= array();
			$EXIST 	= false;

			if ($this->ID_ENTIDADE_DECRYPTED !== '')
			{			
				$SQL  = "SELECT entidades.nome AS 'nome',
								entidades.abreviatura AS 'abreviatura',
								entidades.nif AS 'nif',
								entidades.nib AS 'nib',
								entidades.telefone AS 'telefone',
								entidades.telemovel AS 'telemovel',
								entidades.fax AS 'fax',
								entidades.email AS 'email',
								entidades.url AS 'url',
								entidades.outro1 AS 'outro1',
								entidades.outro2 AS 'outro2',
								entidades.codigo_postal AS 'codigo_postal',
								entidades.morada AS 'morada',
								
								user_entidades.id_user AS 'id_user',
								user_entidades.id_entidade AS 'id_entidade',
								user_entidades.tipo AS 'tipo',
								user_entidades.observacoes AS 'observacoes',
								CASE user_entidades.tipo
									WHEN 1 THEN 'Patrocinio' 
									WHEN 2 THEN 'Fornecedor' 
									WHEN 3 THEN 'Parceiro'
									ELSE 'OUTRO' 
								END AS 'tipo_nome',

								user_entidades.id_user_edited AS 'id_user_edited',
								user_entidades.data_alterado AS 'data_alterado',

								ctt_concelhos.DESIG AS 'concelho_desig',
								ctt_distritos.DESIG AS 'distrito_desig',

								user_emails_teste.nome AS 'user_nome',
								user_emails_teste.apelido AS 'user_apelido'
						";
				$SQL .= ' FROM user_entidades ';
				$SQL .= ' LEFT JOIN user_emails_teste ON user_emails_teste.id=user_entidades.id_user_edited';
				$SQL .= ' LEFT JOIN entidades ON entidades.id=user_entidades.id_entidade ';
				$SQL .= " 	LEFT JOIN ctt_codigos_postais ON 
								CONCAT(ctt_codigos_postais.CP4,'-',ctt_codigos_postais.CP3) = entidades.codigo_postal

							INNER JOIN ctt_concelhos ON ctt_concelhos.DD=ctt_codigos_postais.DD
													AND ctt_concelhos.CC=ctt_codigos_postais.CC
							INNER JOIN ctt_distritos ON ctt_distritos.DD=ctt_codigos_postais.DD
						";
				$SQL .= ' WHERE user_entidades.id_user="'.$this->ID_USER.'" ';
				$SQL .= ' AND user_entidades.id_entidade="'.$this->ID_ENTIDADE_DECRYPTED.'" ';
				$SQL .= ' AND user_entidades.enabled="0" ';
				$SQL .= ' LIMIT 0,1 ';

				$ROW = parent::getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					parent::setFieldsType(array('STRING','STRING','INTEGER','INTEGER','INTEGER','INTEGER','INTEGER','STRING','STRING','STRING','STRING','STRING','MEMO',
												'STRINGINT','ENCRYPT','STRINGINT','MEMO','STRING',
												'STRINGINT','DATETIME',
												'STRING','STRING',
												'STRING','STRING'));
					parent::FieldsOutput($ROW);
					################################################################
					$ROW[0]['codigo_tipo'] = $ROW[0]['tipo'];
					# LINK: http://ahdinheiro.blogspot.pt/2007/01/sites-de-bancos-operar-em-portugal.html
					# LINK: http://pt.wikipedia.org/wiki/N%C3%BAmero_de_Identifica%C3%A7%C3%A3o_Banc%C3%A1ria
					# PT50 0002 0123 1234 5678 901 54
					# PT50 BBBB AAAA CCCC CCCC CCC KK
					# PT50 0035 0450 0002 1993 400 45
					$nib		= $ROW[0]['nib'];
					$nibbanco	= substr($nib,0,4);
					$nibbalcao	= substr($nib,4,4);
					$nibconta	= substr($nib,8,11);
					$nibcontrol	= substr($nib,19,2);

					$SQLstr = 'SELECT nome FROM bancos_pt WHERE codigo="'.$nibbanco.'" LIMIT 0,1;';
					$ROWPlus = $this->getRESULTS($SQLstr);
					$ROW[0]['banco_nome']= (isset($ROWPlus[0])) ? utf8_encode($ROWPlus[0]['nome']) : '';
					$ROW[0]['nibbanco']  = $nibbanco;
					$ROW[0]['nibbalcao'] = $nibbalcao;
					$ROW[0]['nibconta']  = $nibconta;
					$ROW[0]['nibcontrol']= $nibcontrol;
					################################################################

					$EXIST = true;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		/* APAGAR NO FIM*/
		public function getList($FieldToOrder,$OrderBy,$iPage=0, $iLimPerPage=10)
		{
			$ROW 	= array();
			$EXIST 	= false;

			$sLIMIT = parent::build_LIMITStatement($iPage,$iLimPerPage);
			#$sLIMIT = ((isset($iPage)) && ($iPage!=='') && ($iPage > 1)) ? 'LIMIT '.(($iPage-1)*$iLimPerPage).','.($iLimPerPage) : (' LIMIT 0,'.$iLimPerPage) ;
			
			$SQL  = "SELECT entidades.nome AS 'nome',
							entidades.abreviatura AS 'abreviatura',
							CASE user_entidades.tipo
								WHEN 1 THEN 'Patrocinio' 
								WHEN 2 THEN 'Fornecedor' 
								WHEN 3 THEN 'Parceiro'
								ELSE 'OUTRO' 
							END AS 'tipo',
							user_entidades.id_entidade AS 'id_entidade',
							user_entidades.id_user AS 'id_user'
					";
			$SQL .= ' FROM user_entidades ';
			$SQL .= ' LEFT JOIN entidades ON entidades.id=user_entidades.id_entidade ';
			$SQL .= ' WHERE user_entidades.id_user="'.$this->ID_USER.'" ';
			$SQL .= ' AND user_entidades.enabled="0" ';

			switch ($FieldToOrder)
			{
				case 'nome'		: $FieldToOrder = 'entidades.nome '.$OrderBy; 			break;
				case 'abrev'	: $FieldToOrder = 'entidades.abreviatura '.$OrderBy; 	break;
				case 'tipo'		: $FieldToOrder = 'user_entidades.tipo '.$OrderBy; 		break;
				default 		: $FieldToOrder = 'entidades.nome ASC '; 				break;
			}
			$SQL .= ' ORDER BY '.$FieldToOrder;

			$SQL .= $sLIMIT;

			parent::setTableName('user_entidades');
			$ROW = parent::getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				parent::setFieldsType(array('STRING','STRING','STRING','ENCRYPT','ENCRYPT'));
				parent::FieldsOutput($ROW);

				$EXIST = true;
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		/* APAGAR NO FIM*/
		public function SearchEntidades($AarrFieldName,$AarrValue,$AsLimit)
		{
			if ((!is_array($AarrValue)) || (count($AarrValue)==1))
			{
				$arrAux = array();
				$isArr  = is_array($AarrValue);
				$isStr	= (!$isArr);

				$ccI 	= count($AarrFieldName);
				for ($i=0; $i<$ccI; $i++)
				{
					if ($isArr)
						$arrAux[] = $AarrValue[0];
					else
						$arrAux[] = $AarrValue;
				}

				$AarrValue = $arrAux;
			}
			if ($AarrValue[0] !== '*')
				$sSQLLIKE = ' AND ('.parent::WHEREUPDATE_PARSER($AarrFieldName,$AarrValue,' OR ','LIKE',false).') ';
			else
				$sSQLLIKE = '';

			$SQL  = "SELECT entidades.nome AS 'nome',
							entidades.abreviatura AS 'abreviatura',
							entidades.nif AS 'nif',
							entidades.email AS 'email',
							CASE user_entidades.tipo
								WHEN 1 THEN 'Patrocinio' 
								WHEN 2 THEN 'Fornecedor' 
								WHEN 3 THEN 'Parceiro'
								ELSE 'OUTRO' 
							END AS 'tipo',
							user_entidades.id_entidade AS 'id_entidade'
					";
			$SQL .= ' FROM user_entidades ';
			$SQL .= ' LEFT JOIN entidades ON entidades.id=user_entidades.id_entidade ';
			$SQL .= ' WHERE user_entidades.id_user="'.$this->ID_USER.'" ';
			$SQL .= $sSQLLIKE;
			$SQL .= ' ORDER BY entidades.nome ASC ';
			$SQL .= ' LIMIT 0,10 ';

			parent::setTableName('user_entidades');
			$ROW = parent::getRESULTS($SQL);
			
			if (isset($ROW[0]))
			{
				parent::setFieldsType(array('STRING','STRING','INTEGER','STRING','STRING','ENCRYPT'));
				parent::FieldsOutput($ROW);
			}

			return $ROW;
		}
		public function getListSearch($AarrFieldName,$AarrValue, $FieldToOrder,$OrderBy, $iPage=0, $iLimPerPage=10)
		{
			$ROW 	= array();
			$EXIST 	= false;
			$TotalPages = 0;
			$AarrValue  = parent::processArraySearch($AarrFieldName,$AarrValue);
			
			$OrderBy = ($OrderBy === '') ? ' ASC ' : $OrderBy;
			switch ($FieldToOrder)
			{
				case 'nome'			: $FieldToOrder = 'entidades.nome '.$OrderBy; 			break;
				case 'abreviatura'	: $FieldToOrder = 'entidades.abreviatura '.$OrderBy; 	break;
				case 'tipo'			: $FieldToOrder = 'user_entidades.tipo '.$OrderBy; 		break;
				default 			: $FieldToOrder = 'entidades.nome '.$OrderBy; 			break;
			}
			$FieldToOrder = ' ORDER BY '.$FieldToOrder;
	
			$sSQLLIKE = ($AarrValue[0] === '*') ? '' : ' AND ('.parent::WHEREUPDATE_PARSER($AarrFieldName,$AarrValue,' OR ','LIKE',false).') ';
			$sLIMIT = parent::build_LIMITStatement($iPage,$iLimPerPage);
			#$sLIMIT   = ((isset($iPage)) && ($iPage!=='') && ($iPage > 1)) ? ' LIMIT '.(($iPage-1)*$iLimPerPage).','.($iLimPerPage) : (' LIMIT 0,'.$iLimPerPage) ;
			
			$SQL  = "SELECT entidades.nome AS 'nome',
							entidades.abreviatura AS 'abreviatura',
							entidades.nif AS 'nif',
							entidades.email AS 'email',
							CASE user_entidades.tipo
								WHEN 1 THEN 'Patrocinio' 
								WHEN 2 THEN 'Fornecedor' 
								WHEN 3 THEN 'Parceiro'
								ELSE 'OUTRO' 
							END AS 'tipo',
							user_entidades.id_entidade AS 'id_entidade'
					";
			$SQL .= ' FROM user_entidades ';
			$SQL .= ' LEFT JOIN entidades ON entidades.id=user_entidades.id_entidade ';
			$SQL .= ' WHERE user_entidades.id_user="'.$this->ID_USER.'" ';
			$SQL .= ' AND user_entidades.enabled="0" ';
			$SQL .= $sSQLLIKE;
			$SQL .= $FieldToOrder;
			$SQL .= $sLIMIT;
	
			$ROW = parent::getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				parent::setFieldsType(array('STRING','STRING','INTEGER','STRING','STRING','ENCRYPT'));
				parent::FieldsOutput($ROW);
				
				$EXIST 		= true;
				$TotalPages = parent::getTotalPages($SQL,$iLimPerPage);
			}
			
			return array('ROW'=>$ROW, 'EXIST'=>$EXIST, 'TotalPages'=>$TotalPages);
		}

		public function getPerfilEntidade()
		{
			$ROW 	= array();
			$EXIST 	= false;

			if ($this->ID_ENTIDADE_DECRYPTED !== '')
			{
				$Result = $this->getDadosEntidade();
				$ROW 	= $Result['ROW'];
				$EXIST 	= $Result['EXIST'];

				if (isset($ROW[0]))
				{
					#############################################################################
					########################## HTML TEMPLATE VARIABLES ##########################
					#############################################################################
					switch($ROW[0]['tipo'])
					{
						case 1: $ROW[0]['tipo'] = 'Patrocínio'; break;
						case 2: $ROW[0]['tipo'] = 'Fornecedor'; break;
						case 3: $ROW[0]['tipo'] = 'Parceiro'; break;
					}
					if ($ROW[0]['observacoes'] !== '') $ROW[0]['observacoes'] = 'Sem observações...';

					$ROW[0]['nib'] = $ROW[0]['nibbalcao'].'<span class="nib">'.wordwrap($ROW[0]['nibconta'],4,' ',true).'</span>'.$ROW[0]['nibcontrol'];
					#############################################################################
					$EXIST = true;
				}
			}

			#return $ROW;
			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function getPerfilEditEntidade()
		{
			$ROW 	= array();
			$EXIST 	= false;

			if ($this->ID_ENTIDADE_DECRYPTED !== '')
			{
				$Result = $this->getDadosEntidade();
				$ROW 	= $Result['ROW'];
				$EXIST 	= $Result['EXIST'];
				if (isset($ROW[0]))
				{
					#############################################################################
					########################## HTML TEMPLATE VARIABLES ##########################
					#############################################################################
					$tipo_entidade 	 = $ROW[0]['tipo'];
        			$ROW[0]['tipo']  = '<option value="1" '.(($tipo_entidade==1 ) ? 'selected': '').'>Patrocínio</option>';
        			$ROW[0]['tipo'] .= '<option value="2" '.(($tipo_entidade==2 ) ? 'selected': '').'>Fornecedor</option>';
        			$ROW[0]['tipo'] .= '<option value="3" '.(($tipo_entidade==3 ) ? 'selected': '').'>Parceiro</option>';

					$ROW[0]['nib'] = trim($ROW[0]['nibbanco'].' '.$ROW[0]['nibbalcao'].wordwrap($ROW[0]['nibconta'],4,' ',true).$ROW[0]['nibcontrol']);
					#############################################################################
					$EXIST = true;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
	}
?>