<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);

	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplModule.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblEmailsTeste.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	class tplmdlUser extends TobjTemplateModule
	{
		public $sHTML			= '';
		public $tplFileName 	= '';
		public $tplFolderPath	= '';
		public $tblEmailsTeste 	= false;

		public function __construct()
		{
			parent::__construct();
			$this->setVars();
		}
		public function __destruct()
		{
			parent::__destruct();
			$this->unsetVars();
		}

		public function getSHTML() 					{ return $this->sHTML; }
		public function setTplFileName($AValue) 	{ $this->tplFileName 	= $AValue; }
		public function setTplFolderPath($AValue) 	{ $this->tplFolderPath 	= $AValue; }
		private function setVars()
		{
			$this->setTplFolderPath(SETPATH('ROOT','PATH_APP_UI_USER'));
			$this->tblEmailsTeste = new dbTblEmailsTeste();
			parent::setDBTable($this->tblEmailsTeste);
		}
		private function unsetVars()
		{
			unset($tblEmailsTeste);
			unset($sHTML);
		}
		public function finishTemplate()
		{
			$this->sHTML = $this->objTemplate->echohtml();
		}
		#############################################################################
		#############################################################################
		public function getPerfilUser($AID_USER)
		{
			$this->tblEmailsTeste->set_IDUSER($AID_USER);
			$Result = $this->tblEmailsTeste->getUserData();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
	}
?>