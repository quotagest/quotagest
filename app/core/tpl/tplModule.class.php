<?php
# Template Module - HTML / PHP
# @author: 	Paulo Mota
# @version: 0.0.0.1
# @date: 	22-09-2013
# LINKS:
# 
#
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');

	class TobjTemplateModule
	{
		public $Titulo		= '';
		public $Ajuda		= '';
		public $LeftMenu 	= array();
		public $TopMenu		= array();
		public $dbTable		= false;
		public $objTemplate = false;

		public function __construct()
		{

		}
		public function __destruct()
		{
			unset($Titulo);
			unset($Ajuda);
			unset($LeftMenu);
			unset($TopMenu);
			unset($dbTable);
			unset($objTemplate);
		}

		public function setTitulo($AValue) 			{ $this->objTemplate->setVar('{$titulo}',$AValue); }
		public function setAjuda($AValue)  			{ $this->objTemplate->setVar('{$ajuda}',$AValue); }
		public function addLeftMenuItem($AItem) 	{ $this->LeftMenu[]  = $AItem; }
		public function addTopMenuItem($AItem)		{ $this->TopMenu[] 	 = $AItem; }
		public function setDBTable($AdbTable) 		{ $this->dbTable 	 = $AdbTable; }
		public function setObjTemplate($AFileName) 	{ $this->objTemplate = new TobjTemplate($AFileName); }
		public function getLeftMenuItem()			{ return $this->LeftMenu; }
		public function setTopMenuItem()			{ return $this->TopMenu; }
		#############################################################################
		########################## HTML TEMPLATE VARIABLES ##########################
		#############################################################################
		public function buildLeftMenu()
		{
			$html = '';
			foreach($this->LeftMenu as $item)
			{
				$item['href'] = (isset($item['javascript'])) ? "javascript:".$item['javascript'] : $item['href'];

				$html .='<li><a class="item" href="'.$item['href'].'" >'.$item['titulo'].'</a></li>'."\n";
			}
			return $html;
		}
		public function buildTopMenu()
		{

		}

		#
		# @vInputParam: 		SQL Query || Array of rows.
		# @vIDValueMaster: 		if (ROW ID === vIDValueMaster) --> option status: "selected"
		# @vIDFieldName: 		Field name ID to compare with @vIDValueMaster
		# @vValueFieldName: 	Field name Value to echo as "value"
		# @vFieldNameOption: 	Field name Title to echo as InnerText
		# @AAddText: 			Boolean, to echo Title in "value" (qual o texto do select selecionado?)
		#
		public function buildSELECTS($vInputParam,$vIDValueMaster,$vIDFieldName,$vValueFieldName,$vFieldNameOption,$AAddText=false)
		{
			$html = '';
			$ROW  = array();
			if (is_array($vInputParam))	# is_array
			{
				$ROW = $vInputParam;
			}
			else # !is_array
			{
				$ROW = $this->dbTable->getRESULTS($vInputParam);
			}

			if (isset($ROW[0]))
			{
				$cc	= count($ROW);
				for ($i=0; $i<$cc; $i++)
				{
					$select = ($vIDValueMaster == $ROW[$i][$vIDFieldName]) ? 'selected' : '';
					if (stripos($vValueFieldName,'id') !== false)
						$ROW[$i][$vValueFieldName] = encrypt($ROW[$i][$vValueFieldName],'N5KyEXAnDdwGL4eV');
					$Text = utf8_encode($ROW[$i][$vFieldNameOption]);

					$html .= '<option value="'.$ROW[$i][$vValueFieldName].(($AAddText === true) ? ':'.$Text : '').'" '.$select.' >'.$Text.'</option>'."\n";
				}
			}
			unset($ROW);

			return $html;
		}
		public function buildLABLES($ATitleElem,$vSQL,$vIDValueMaster,$vIDFieldName,$vValueFieldName,$vFieldNameOption,$AAddText=false)
		{
			$html = '';
			$ROW  = array();
			if (is_array($vSQL))	# is_array
			{
				$ROW = $vSQL;
			}
			else # !is_array
			{
				$ROW = $this->dbTable->getRESULTS($vSQL);
			}

			if (isset($ROW[0]))
			{
				$cc	= count($ROW);
				for ($i=0; $i<$cc; $i++)
				{
					if (stripos($vValueFieldName,'id') !== false)
						$ROW[$i][$vValueFieldName] = encrypt($ROW[$i][$vValueFieldName],'N5KyEXAnDdwGL4eV');
					$Text = utf8_encode($ROW[$i][$vFieldNameOption]);
					$labelname = $ATitleElem.'-'.$i;
					$html .= '<input type="checkbox" class="tag" name="'.$ATitleElem.'[]" id="'.$labelname.'" value="'.$ROW[$i][$vValueFieldName].(($AAddText === true) ? ':'.$Text : '').'">'."\n";
					$html .= '<label for="'.$labelname.'">'.$Text.'</label>'."\n";
				}
			}
			unset($ROW);

			return $html;
		}
	} #/Close Class
?>