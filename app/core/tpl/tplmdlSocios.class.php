<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplModule.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocios.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	class tplmdlSocios extends TobjTemplateModule
	{
		public $sHTML			= '';
		public $tplFileName 	= '';
		public $tplFolderPath	= '';
		public $tblSocios 		= false;
		public $URL					= '';
		public $ROOT				= '';
		protected $URL_PERFILEDIT	= 'neo/?op=socios&ids=';
		protected $URL_PERFILNOVO	= 'neo/?op=socios';

		public function __construct($ADBConnection=false)
		{
			parent::__construct();
			$this->setObjectVars($ADBConnection);
		}
		public function __destruct()
		{
			parent::__destruct();
			$this->unsetObjectVars();
		}

		# ########## FUNCTIONS TEMPLATE ########## #
		public function getSHTML() 					{ return $this->sHTML; }
		public function setTplFileName($AValue) 	{ $this->tplFileName 	= $AValue; }
		private function setTplFolderPath($AValue) 	{ $this->tplFolderPath 	= $AValue; }
		private function setTemplateFile()
		{
			parent::setObjTemplate($this->tplFolderPath.$this->tplFileName);
		}
		public function finishTemplate()
		{
			$this->sHTML = $this->objTemplate->echohtml();
		}

		# ########## VAIRAVEIS ########## #
		private function setObjectVars($ADBConnection)
		{
			$this->setTplFolderPath(SETPATH('ROOT','PATH_APP_UI_SOCIOS'));
			$this->tblSocios = new dbTblSocios($ADBConnection);
			parent::setDBTable($this->tblSocios);
			$this->URL  = SETPATH('URL','PATH_APP');
			$this->ROOT = SETPATH('ROOT','PATH_APP');
		}
		private function unsetObjectVars()
		{
			unset($tblSocios);
			unset($sHTML);
		}
		public function setIDValues($AID_SOCIO=0,$AID_MOD=0,$AID_USER=0,$AID_ASSOC=0)
		{
			$this->tblSocios->set_IDSOCIO($AID_SOCIO);
			$this->tblSocios->set_IDMODALIDADE($AID_MOD);
			$this->tblSocios->set_IDUSER($AID_USER);
			$this->tblSocios->set_IDASSOC($AID_ASSOC);
		}
		
		private function SQL_SUCESS()
		{
			return CREATE_POP_MSG('Registo salvo com sucesso!',3000);
		}
		private function SQL_ERROR()
		{
			return CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
		#############################################################################
		#############################################################################
		
		public function forkTemplate_SocioNovoInscricao($AID_SOCIO,$AID_MODALIDADE,$AID_USER,$AID_ASSOC)
		{
			$this->setTplFileName('novo-inscricao.html');
			$this->setTplFolderPath(SETPATH('ROOT','PATH_APP_UI_MODALIDADES'));
			$this->setTemplateFile();
			parent::setTitulo('Nova Inscrição em Actividade');
			parent::setAjuda('Inscreva o Sócio numa Actividade.');

			$this->tblSocios->set_IDSOCIO($AID_SOCIO);
			$this->tblSocios->set_IDMODALIDADE($AID_MODALIDADE);
			$this->tblSocios->set_IDUSER($AID_USER);
			$this->tblSocios->set_IDASSOC($AID_ASSOC);

			$Result 				= $this->getHTML_INPUTSOCIO($AID_SOCIO,$AID_USER,$AID_ASSOC);
			$SOCIO_NOME 			= $Result['SOCIO_NOME']; 
			$SOCIO_NOME_PLACEHOLDER = ($SOCIO_NOME === '') ? 'Nome do Sócio' : $SOCIO_NOME; 
			$SOCIO_INPUT_COD 		= $Result['SOCIO_INPUT_COD'];
			$SOCIO_INPUT_NOME 		= '<input type="text" name="NomeSocio" placeholder="'.$SOCIO_NOME.'" autocomplete="off" maxlength="80" value="'.$SOCIO_NOME.'" disabled required />';
			$HTML_MODALIDADE_SELECT = $this->getHTML_SELECTActividadesNAOInscrito($AID_MODALIDADE,$AID_SOCIO,$AID_USER,$AID_ASSOC);

			parent::addLeftMenuItem(array("titulo" => "Guardar", "href" => "/", "javascript" => "ValidateForm('novo-inscricao-modalidade');"));
			if ($AID_SOCIO !== '')
			{
				parent::addLeftMenuItem(array("titulo" => "Voltar", "href" => "/ui/socios/perfil.php?id=".$AID_SOCIO));
			}
			$this->objTemplate->setVar('{$FormAction}', htmlentities($_SERVER['PHP_SELF']));
			$this->objTemplate->setVar('{$HTML_MODALIDADE_SELECT}', $HTML_MODALIDADE_SELECT);
			$this->objTemplate->setVar('{$data_hoje}', date('Y-m-d'));
			if ($SOCIO_INPUT_COD !== '')
				$this->objTemplate->replaceDOM('[data-id=fieldsocio]','innertext',$SOCIO_INPUT_COD.$SOCIO_INPUT_NOME);
			$this->objTemplate->setVar('{$SOCIO_NOME_PLACEHOLDER}', $SOCIO_NOME_PLACEHOLDER);
			$this->objTemplate->setVar('{$SOCIO_NOME}', $SOCIO_NOME);
			$this->objTemplate->setVar('{$SOCIO_INPUT_COD}', $SOCIO_INPUT_COD);
			$this->objTemplate->setVar('{$HTML_MODALIDADE_SELECT}', $HTML_MODALIDADE_SELECT);

			$this->sHTML = $this->objTemplate->echohtml();
		}
		public function INSERT_SOCIO($A_POST,$A_GET)
		{
			$Result = false;
			if ( (isset($A_POST['Nome'])) || (isset($A_POST['jsonPOST'])) )
			{
				if (isset($A_POST['jsonPOST']))
				{
					$jsonData = $this->tblSocios->decodeJSONtoArray($A_POST['jsonPOST']);
					$A_POST	  = $jsonData['_POST'];
				}

				if ( (isset($A_POST['Nome'])) &&
					 (isset($A_POST['EstadoSocio'])) &&
					 (isset($A_POST['TipoSocio'])) &&
					 (isset($A_POST['Codigo-Postal'])) 
					 &&
					 ($A_POST['Nome'] !== '') &&
					 ($A_POST['EstadoSocio'] !== '') &&
					 ($A_POST['TipoSocio'] !== '') &&
					 ($A_POST['Codigo-Postal'] !== '') )
				{
					$A_POST['NIB'] 			= '';
					$A_POST['Telemovel'] 	= '';
					$A_POST['Fax'] 			= '';
					$A_POST['Outro1'] 		= '';
					$A_POST['Outro2'] 		= '';
					$A_POST['data-entrada'] = date('Y-m-d');
					$A_POST['data-saida'] 	= '';

					$arrRes 	  = $this->tblSocios->addSocio($A_POST);
					$Result 	  = $arrRes['Result'];
					$NEW_SOCIO_ID = $arrRes['NEW_ID'];

					if ($Result)
					{
						$Result = $arrRes;
						$imgResult = $this->SAVE_SOCIO_IMAGE($A_POST,$A_GET,$NEW_SOCIO_ID);
					}
				}
			}

			return $Result;
		}
		public function UPDATE_SOCIO($A_POST,$A_GET)
		{
			$Result = false;
			if ( (isset($A_POST['Nome']) && isset($A_POST['socio-sexo'])) || (isset($A_POST['jsonPOST'])) )
			{
				if (isset($A_POST['jsonPOST']))
				{
					$jsonData = $this->tblSocios->decodeJSONtoArray($A_POST['jsonPOST']);
					$A_POST	  = $jsonData['_POST'];
				}
				
				if ( ($A_POST['Nome'] !== '') &&
					 ($A_POST['EstadoSocio'] !== '') &&
					 ($A_POST['TipoSocio'] !== '') &&
					 ($A_POST['Codigo-Postal'] !== '') )
				{
					
					$Result 	= $this->tblSocios->updateSocio($A_POST);
					$AID_SOCIO 	= $this->tblSocios->ID_SOCIO_ENCRYPTED;

					if ($Result)
					{
						$imgResult = $this->SAVE_SOCIO_IMAGE($A_POST,$A_GET,$AID_SOCIO);
					}
				}
			}
			return $Result;
		}
		private function SAVE_SOCIO_IMAGE($A_POST,$A_GET,$AID_SOCIO)
		{
			$Result = false;
			if (isset($A_POST['file-name']))
			{
				require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjUploadFile.class.php');

				$objUploadFile = new TobjUploadFile();
				$objUploadFile->setFileName(SETPATH('ROOT','PATH_APP_IMG_SOCIOS').$AID_SOCIO.'.jpg');
				$objUploadFile->setTempFileName(SETPATH('ROOT','PATH_APP_TMP').$A_POST['file-name']);
				$objUploadFile->setMaxSize(524288);
				$objUploadFile->setFileType('image');
				$objUploadFile->setInputFileName('upload-image');
				$objUploadFile->setMaxWidth('160');
				$objUploadFile->setMaxHeigth('125');
				$objUploadFile->setIsBase64((isset($A_GET['base64'])) ? true : false);
				$objUploadFile->setIsBinary((isset($A_GET['up'])) ? true : false);
				$objUploadFile->setEchoFileName((isset($A_GET['printname'])) ? true : false);
				$Result = $objUploadFile->SaveFile();
				unset($objUploadFile);
			}
			return $Result;
		}
		#############################################################################
		########################## HTML TEMPLATE FUNCTIONS ##########################
		#############################################################################
		public function getModalidadesTotal($AID_SOCIO)
		{
			$ResultQuotas = $this->tblSocios->getModalidadesTotal();
			$ROWQuotas    = $ResultQuotas['ROW'];
			$EXISTQuotas  = $ResultQuotas['EXIST'];
	
			$tDivida = 0.0;
			$tPago	 = 0.0;
			if ($EXISTQuotas)
			{
				$sPathQuotas = SETPATH('URL','PATH_APP_UI_QUOTAS');
				$ccI = count($ROWQuotas);
				for ($i=0; $i<$ccI; $i++)
				{
					$tDivida += $ROWQuotas[$i]['total_divida'];
					$tPago	 += $ROWQuotas[$i]['total_pago'];
					$ROWQuotas[$i]['goto-link'] = $sPathQuotas.'listagem.php?ids='.$AID_SOCIO.'&idm='.$ROWQuotas[$i]['id_modalidade'].'&ano='.date('Y');
				}
			}
			$total_divida = number_format($tDivida, 2, '.', '');
			$total_link   = 'javascript:void(0);';
			
			return array("ROW"=>$ROWQuotas, "EXIST"=>$EXISTQuotas,
						"total_divida"=>$total_divida, "total_link"=>$total_link);
		}
		public function buildSELECT_SocioEstado($AIDPos,$AID_ASSOC,$AAddText=false)
		{
			if ($AIDPos === '0')
			{
				$SQL  = 'SELECT id FROM socio_estado WHERE socio_estado.nome LIKE "activo%" AND socio_estado.id_assoc="'.$AID_ASSOC.'" AND enabled="0" LIMIT 0,1;';
				$ROW = $this->tblSocios->getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					$AIDPos = $ROW[0]['id'];
				}				
			}

			$SQL  = 'SELECT id,nome FROM socio_estado WHERE socio_estado.id_assoc="'.$AID_ASSOC.'" AND enabled="0"';
			$html = parent::buildSELECTS($SQL,$AIDPos,'id','id','nome',$AAddText);
			return $html;
		}
		public function buildSELECT_SocioTipo($AIDPos,$AID_ASSOC,$AAddText=false)
		{
			if ($AIDPos === '0')
			{
				$SQL  = 'SELECT id FROM socio_tipo WHERE socio_tipo.nome LIKE "%normal%" AND socio_tipo.id_assoc="'.$AID_ASSOC.'" AND enabled="0" LIMIT 0,1;';
				$ROW = $this->tblSocios->getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					$AIDPos = $ROW[0]['id'];
				}				
			}

			$SQL  = 'SELECT id,nome FROM socio_tipo WHERE socio_tipo.id_assoc="'.$AID_ASSOC.'" AND enabled="0"';
			$html = parent::buildSELECTS($SQL,$AIDPos,'id','id','nome',$AAddText);
			return $html;
		}
		public function buildSelectSocioExtra($ATableName,$AIDPos,$AID_ASSOC,$AAddText=false)
		{
			$SQL = 'SELECT id,nome FROM '.$ATableName.' WHERE '.$ATableName.'.id_assoc="'.$AID_ASSOC.'" AND enabled="0"';
			$html = parent::buildSELECTS($SQL,$AIDPos,'id','id','nome',$AAddText);
			return $html;
		}
		public function buildLABLESSocioExta($AElemType,$ATitleElem,$ATableName,$AIDPos,$AID_ASSOC,$AAddText=false)
		{
			$SQL = 'SELECT id,nome FROM '.$ATableName.' WHERE '.$ATableName.'.id_assoc="'.$AID_ASSOC.'" AND enabled="0"';
			$html = $this->buildLABLES($AElemType,$ATitleElem,$SQL,$AIDPos,'id','id','nome',$AAddText);
			return $html;
		}


		public function check_InscritoEmTudo($AID_SOCIO,$AID_USER,$AID_ASSOC)
		{
			$Result = true;

			if (isset($AID_SOCIO) && ($AID_SOCIO !== ''))
			{
				$AID_SOCIO = $this->tblSocios->decryptVar($AID_SOCIO);

				$SQL = 'SELECT 
							(SELECT COUNT(*) FROM modalidade WHERE modalidade.id_assoc="'.$AID_ASSOC.'" AND modalidade.enabled="0") AS "num_modalidades",
							(SELECT COUNT(*) FROM mod_socio WHERE mod_socio.id_socio="'.$AID_SOCIO.'" AND mod_socio.id_assoc="'.$AID_ASSOC.'") AS "num_mod_inscrito"';
				
				$ROW = $this->getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					if ($ROW[0]['num_modalidades']>$ROW[0]['num_mod_inscrito'])
					{
						$Result = false;
					}
				}
			}
			return $Result;
		}


		public function getHTML_SELECTActividadesInscrito($AID_MODALIDADE,$AID_SOCIO,$AID_USER,$AID_ASSOC)
		{
			$ResultHTML = '<option selected disabled>Sem inscrições</option>';

			if (isset($AID_SOCIO) && ($AID_SOCIO !== ''))
			{
				$AID_MODALIDADE = $this->tblSocios->decryptVar($AID_MODALIDADE);
				$AID_SOCIO 		= $this->tblSocios->decryptVar($AID_SOCIO);

				#$SQL = 'SELECT id,nome FROM modalidade WHERE modalidade.id_assoc="'.$id_assoc.'" AND enabled="0"';
				$SQL = 'SELECT mod_socio.id_modalidade AS "id", modalidade.nome AS "nome" FROM mod_socio INNER JOIN modalidade ON mod_socio.id_modalidade=modalidade.id WHERE mod_socio.id_socio="'.$AID_SOCIO.'" AND mod_socio.id_assoc="'.$AID_ASSOC.'" AND modalidade.enabled="0"';

				$htmlSELECT = parent::buildSELECTS($SQL,$AID_MODALIDADE,'id','id','nome');
				if ($htmlSELECT !== '')
					$ResultHTML = $htmlSELECT;
			}
			else
			{
				$ResultHTML = '';
			}
			return $ResultHTML;
		}
		public function getHTML_SELECTActividades($AID_MODALIDADE,$AID_USER,$AID_ASSOC,$AAddText=false)
		{
			$SQL = 'SELECT id,nome FROM modalidade WHERE modalidade.id_assoc="'.$AID_ASSOC.'" AND enabled="0"';
			return parent::buildSELECTS($SQL,$AID_MODALIDADE,'id','id','nome',$AAddText);
		}
		public function getHTML_SELECTActividadesTodas($AID_MODALIDADE,$AID_SOCIO,$AID_USER,$AID_ASSOC)
		{			
			$ID_MODALIDADE  = $this->tblSocios->decryptVar($AID_MODALIDADE);
			$ID_SOCIO 		= $this->tblSocios->decryptVar($AID_SOCIO);

			if (($AID_SOCIO !== '') && ($AID_MODALIDADE !== ''))
			{
				
				$strAux = '';

				$SQL = 'SELECT modalidade.id AS  "id",
							  mod_socio.id_modalidade AS  "id_modalidade",
							  modalidade.nome AS  "nome"
						FROM mod_socio
						RIGHT JOIN modalidade ON modalidade.id = mod_socio.id_modalidade
						AND mod_socio.id_assoc =  "'.$AID_ASSOC.'"
						AND mod_socio.id_socio =  "'.$ID_SOCIO.'"
						WHERE modalidade.id_assoc =  "'.$AID_ASSOC.'"	
						AND modalidade.enabled ="0"	
					';
				#AND mod_socio.id_modalidade IS NULL
				
				$ROW = $this->getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					$cc	= count($ROW);
					for ($i=0; $i<$cc; $i++)
					{
						$select = ($ROW[$i]['id'] === $ID_MODALIDADE) ? 'selected' : '';
						$ID 	= $this->tblSocios->encryptVar($ROW[$i]['id']);
						$sExt	= ($ROW[$i]['id_modalidade'] !== NULL) ? ' (Inscrito)' : ' (Inscrever ao Guardar)';
						$strAux .= '<option value="'.$ID.'" '.$select.' >'.utf8_encode($ROW[$i]['nome']).$sExt.'</option>';				
						$strAux .= "\n";
					}
					unset($ROW);
				}

				$HTML_MODALIDADE_SELECT = $strAux;
			}
			else
			{
				$HTML_MODALIDADE_SELECT = $this->getHTML_SELECTActividades($AID_MODALIDADE,$AID_USER,$AID_ASSOC);
			}

			return $HTML_MODALIDADE_SELECT;
		}
		public function getHTML_SELECTActividadesNAOInscrito($AID_MODALIDADE,$AID_SOCIO,$AID_USER,$AID_ASSOC)
		{
			$ID_MODALIDADE = $this->tblSocios->decryptVar($AID_MODALIDADE);
			$ID_SOCIO      = $this->tblSocios->decryptVar($AID_SOCIO);

			if ($AID_SOCIO !== '')
			{
				$strAux = '';

				$SQL = 'SELECT modalidade.id AS  "id",
							  mod_socio.id_modalidade AS  "id_modalidade",
							  modalidade.nome AS  "nome"
						FROM mod_socio
						RIGHT JOIN modalidade ON modalidade.id = mod_socio.id_modalidade
						AND mod_socio.id_assoc =  "'.$AID_ASSOC.'"
						AND mod_socio.id_socio =  "'.$ID_SOCIO.'"
						WHERE modalidade.id_assoc =  "'.$AID_ASSOC.'"
						AND modalidade.enabled="0"
					';
				#AND mod_socio.id_modalidade IS NULL
				$ROW = $this->tblSocios->getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					$cc	= count($ROW);
					for ($i=0; $i<$cc; $i++)
					{
						$isIDModNULL = ($ROW[$i]['id_modalidade'] !== NULL);
						$select = ($isIDModNULL) 	? 'disabled' : '';
						$ID 	= (!$isIDModNULL) 	? $this->tblSocios->encryptVar($ROW[$i]['id']) : '';
						$sExt	= ($isIDModNULL) 	? ' (Inscrito)' : '';
						$strAux .= '<option value="'.$ID.'" '.$select.' >'.utf8_encode($ROW[$i]['nome']).$sExt.'</option>';
						$strAux .= "\n";
					}
				}
				unset($ROW);

				$HTML_MODALIDADE_SELECT = $strAux;
			}
			else
			{
				$HTML_MODALIDADE_SELECT = $this->getHTML_SELECTActividades($AID_MODALIDADE,$AID_USER,$AID_ASSOC);		
			}
			return $HTML_MODALIDADE_SELECT;
		}


		public function buildLABLES_TplSocios($AElemType,$ATitleElem,$vSQL,$vIDValueMaster,$vIDFieldName,$vValueFieldName,$vFieldNameOption,$AAddText=false)
		{
			$str = '';
			$ROW = $this->tblSocios->getRESULTS($vSQL);
			if (isset($ROW[0]))
			{
				$sIDTitle = str_ireplace('[]','',$ATitleElem);
				$cc = count($ROW);
				for ($i=0; $i<$cc; $i++)
				{
					if (stripos($vValueFieldName,'id') !== false)
						$sIDValue = $this->tblSocios->encryptVar($ROW[$i][$vValueFieldName]);
					$Text = utf8_encode($ROW[$i][$vFieldNameOption]);
					
					$labelname = $sIDTitle.'-'.$i;
					
					$str .= '<input type="'.$AElemType.'" class="tag" name="'.$ATitleElem.'" id="'.$labelname.'" value="'.$sIDValue.(($AAddText === true) ? ':'.$Text : '').'">';
					$str .= "\n";
					$str .= '<label for="'.$labelname.'">'.$Text.'</label>';
					$str .= "\n";
				}
			}
			unset($ROW);

			return $str;
		}
		#############################################################################
		#############################################################################
		public function getHTML_INPUTSOCIO($AID_SOCIO,$AID_USER,$AID_ASSOC)
		{
			$SOCIO_NOME  	 = '';
			$SOCIO_INPUT_COD = '';

			if (isset($AID_SOCIO) && ($AID_SOCIO !== ''))
			{
				$Result = $this->getFieldFromSocio(array('socio.nome'),array('STRING'),$AID_SOCIO,$AID_USER,$AID_ASSOC);
				if ($Result['EXIST'] == true)
				{
					$SOCIO_NOME = $Result['ROW'][0]['nome'];
					$SOCIO_INPUT_COD= '<input type="hidden" name="CodSocio" value="'.$AID_SOCIO.'" />';
				}
			}
			return array('SOCIO_NOME'=>$SOCIO_NOME,'SOCIO_INPUT_COD'=>$SOCIO_INPUT_COD);
		}
		#############################################################################
		########################## HTML TEMPLATE VARIABLES ##########################
		#############################################################################
		public function getTPL_QUOTASLISTAGEM($AID_MODALIDADE,$AID_SOCIO,$AID_USER,$AID_ASSOC)
		{
			$arrMENU = array();

			$htmlSELECT_ACTIVIDADES = $this->getHTML_SELECTActividadesInscrito($AID_MODALIDADE,$AID_SOCIO,$AID_USER,$AID_ASSOC);
			
			$Result = $this->check_InscritoEmTudo($AID_SOCIO,$AID_USER,$AID_ASSOC);
			if (!$Result)
			{
				$arrMENU["soc1"] = array("titulo" => "Inscrever numa Actividade",
										  "href" 	=> "/ui/modalidades/novo-inscricao.php"."?ids=".$AID_SOCIO);
			}
			if ($AID_SOCIO !== '')
			{
				$arrMENU["soc2"] = array("titulo" => "Ver perfil do Sócio",
										  "href" 	=> "/ui/socios/perfil.php?id=".$AID_SOCIO);
			}
			
			$Result = $this->getHTML_INPUTSOCIO($AID_SOCIO,$AID_USER,$AID_ASSOC);
			$SOCIO_NOME  	 = $Result['SOCIO_NOME'];
			$SOCIO_INPUT_COD = $Result['SOCIO_INPUT_COD'];


			return array('arrMENU'=>$arrMENU,
						 'htmlSELECT_ACTIVIDADES'=>$htmlSELECT_ACTIVIDADES,
						 'SOCIO_NOME'=>$SOCIO_NOME,
						 'SOCIO_INPUT_COD'=>$SOCIO_INPUT_COD
						);
		}
		#############################################################################
		#############################################################################
		public function getLastNumSocio($AID_USER,$AID_ASSOC)
		{
			$Result = '';
			if ($AID_ASSOC !== '')
			{
				$SQL  = 'SELECT assoc_socios.codigo AS "codigo"';
				$SQL .= ' FROM assoc_socios';
				$SQL .= ' LEFT JOIN socio ON socio.id=assoc_socios.id_socio';
				$SQL .= ' WHERE assoc_socios.id_assoc="'.$AID_ASSOC.'"';
				$SQL .= ' AND assoc_socios.enabled="0"';
				$SQL .= ' ORDER BY assoc_socios.codigo DESC';
				$SQL .= ' LIMIT 0,1;';
				
				$ROW = $this->tblSocios->getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					$Result = $ROW[0]['codigo'];
				}
			}
			return $Result;
		}
		public function getFieldFromSocio($AarrFieldName,$ArrType,$AID_SOCIO,$AID_USER,$AID_ASSOC)
		{
			$ROW = array();
			$EXIST = false;

			if (isset($AID_SOCIO) && ($AID_SOCIO !== ''))
			{
				$AID_SOCIO = $this->tblSocios->decryptVar($AID_SOCIO);

				$SQL  = 'SELECT '.implode(', ',$AarrFieldName);
				$SQL .= ' FROM assoc_socios';
				$SQL .= ' LEFT JOIN socio ON socio.id=assoc_socios.id_socio';
				$SQL .= ' WHERE assoc_socios.id_assoc="'.$AID_ASSOC.'"';
				$SQL .= ' AND id_socio="'.$AID_SOCIO.'"';
				$SQL .= ' LIMIT 0,1;';

				$ROW = $this->tblSocios->getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					$this->tblSocios->setFieldsType($ArrType);
					$this->tblSocios->FieldsOutput($ROW);

					#############################################################################
					# SE "enabled" = 1 SIGNIFICA QUE: socio ainda não autorizou ver os dados.
					# SE "activo" = 1 SIGNIFICA QUE: socio permite que se veja os dados e que sejam editados.
					# SE "activo" = 2 SIGNIFICA QUE: socio permite que se veja os dados mas que não sejam editados.
					#############################################################################

					$EXIST = true;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function getPerfilSocio()
		{
			$Result = $this->tblSocios->getSocioData();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];

			if (isset($ROW[0]))
			{
				$valid = ($ROW[0]['enabled'] == '0');
				if (!$valid)
				{
					#$ROW[0]['nome'] 				= ;
					$ROW[0]['telefone']				= 'Escondido';
					$ROW[0]['telemovel']			= 'Escondido';
					$ROW[0]['fax']					= 'Escondido';
					$ROW[0]['email']				= 'Escondido';
					$ROW[0]['outro1']				= 'Escondido';
					$ROW[0]['outro2']				= 'Escondido';
					$ROW[0]['morada']				= 'Escondido';
					$ROW[0]['codigo_postal']		= 'Escondido';
					$ROW[0]['data_nascimento']		= 'Escondido';
					$ROW[0]['sexo']					= 'Escondido';
					$ROW[0]['bicc'] 				= 'Escondido';
					$ROW[0]['nif']					= 'Escondido';
					$ROW[0]['nib']					= 'Escondido';
					$ROW[0]['concelho_desig'] 		= 'Escondido';
					$ROW[0]['distrito_desig'] 		= 'Escondido';
				}

				if ($ROW[0]['observacoes'] === '') 
					$ROW[0]['observacoes'] = 'Sem observações...';

				$ROW[0]['data_nascimento']		= str_replace("0000-00-00", "", $ROW[0]['data_nascimento']);
				$ROW[0]['data_entrada']			= str_replace("0000-00-00", "", $ROW[0]['data_entrada']);
				$ROW[0]['data_saida']			= str_replace("0000-00-00", "", $ROW[0]['data_saida']);

				$EXIST = true;
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function getPerfilEditSocio()
		{
			$Result = $this->tblSocios->getSocioData();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];
			$jsonData = '';

			/* SE FOI ENCONTRADO REGISTO */
			if ($EXIST)
			{
				if ($ROW[0]['sexo'] === 'Masculino')
				{
					$ROW[0]['sexFem'] = '';
					$ROW[0]['sexMas'] = ' checked ';
				}
				else
				{
					$ROW[0]['sexFem'] = ' checked ';
					$ROW[0]['sexMas'] = '';
				}
				# (activo === 1) -> Sócio permite ver dados e respectiva edição.
				# (activo === 2) -> Sócio permite ver dados mas não os editar.
				#
				#	SE "enabled" = 1 SIGNIFICA QUE: socio ainda não autorizou ver os dados.
				#	SE "activo" = 1 SIGNIFICA QUE: socio permite que se veja os dados e que sejam editados.
				#	SE "activo" = 2 SIGNIFICA QUE: socio permite que se veja os dados mas que não sejam editados.
				#	SE "activo" = 3 SIGNIFICA QUE: socio NÃO permite que se veja os dados e que NÃO sejam editados.
				#
				$ROW[0]['data_entrada'] 	= $this->tblSocios->getAsDate($ROW[0]['data_entrada'],'',true);
				$ROW[0]['data_saida'] 		= $this->tblSocios->getAsDate($ROW[0]['data_saida'],'',true);
				$ROW[0]['data_nascimento'] 	= $this->tblSocios->getAsDate($ROW[0]['data_nascimento'],'',true);
				$ROW[0]['observacoes'] 		= $this->tblSocios->br2nl($ROW[0]['observacoes']);

				#$valid 					= ($ROW[0]['enabled'] !== '1');
				#$actbool 				= ($ROW[0]['activo'] == 2);
				
				$jsonData = array();
				$jsonData[] = array('name'=> 'BICC', 			'type'=>'input', 'value'=>$ROW[0]['bicc']);
				$jsonData[] = array('name'=> 'Codigo-Postal', 	'type'=>'input', 'value'=>$ROW[0]['codigo_postal']);
				$jsonData[] = array('name'=> 'Email', 			'type'=>'input', 'value'=>$ROW[0]['email']);
				$jsonData[] = array('name'=> 'EstadoSocio', 	'type'=>'select', 'value'=>$this->tblSocios->encryptVar($ROW[0]['id_estado']));
				$jsonData[] = array('name'=> 'TipoSocio', 		'type'=>'select', 'value'=>$this->tblSocios->encryptVar($ROW[0]['id_tipo']));
				$jsonData[] = array('name'=> 'Fax', 			'type'=>'input', 'value'=>$ROW[0]['fax']);
				$jsonData[] = array('name'=> 'Morada', 			'type'=>'input', 'value'=>$ROW[0]['morada']);
				$jsonData[] = array('name'=> 'NIB', 			'type'=>'input', 'value'=>$ROW[0]['nib']);
				$jsonData[] = array('name'=> 'NIF', 			'type'=>'input', 'value'=>$ROW[0]['nif']);
				$jsonData[] = array('name'=> 'Nome', 			'type'=>'input', 'value'=>$ROW[0]['nome']);
				$jsonData[] = array('name'=> 'NumSocio', 		'type'=>'input', 'value'=>$ROW[0]['codigo']);
				$jsonData[] = array('name'=> 'Observacoes', 	'type'=>'textarea', 'value'=>$ROW[0]['observacoes']);
				$jsonData[] = array('name'=> 'Outro1', 			'type'=>'input', 'value'=>$ROW[0]['outro1']);
				$jsonData[] = array('name'=> 'Outro2', 			'type'=>'input', 'value'=>$ROW[0]['outro2']);
				$jsonData[] = array('name'=> 'Telefone', 		'type'=>'input', 'value'=>$ROW[0]['telefone']);
				$jsonData[] = array('name'=> 'Telemovel', 		'type'=>'input', 'value'=>$ROW[0]['telemovel']);
				$jsonData[] = array('name'=> 'data-inscricao', 	'type'=>'input', 'value'=>$ROW[0]['data_entrada']);
				$jsonData[] = array('name'=> 'data-nascimento', 'type'=>'input', 'value'=>$ROW[0]['data_nascimento']);
				$jsonData[] = array('name'=> 'data-saida', 		'type'=>'input', 'value'=>$ROW[0]['data_saida']);
				$jsonData[] = array('name'=> 'socio-sexo', 		'type'=>'radio', 'value'=>$ROW[0]['sexo']);
				$jsonData = json_encode($jsonData);
				$jsonData = htmlentities($jsonData,ENT_QUOTES);
				#$jsonData = base64_encode($jsonData);
				
				#$ROW[0]['EDIT_LINK'] 	  = ($valid) ? ('perfil-edit.php?id='.$this->tblSocios->ID_SOCIO_ENCRYPTED) : '#';
				#$ROW[0]['LINK_ANULAR'] 	= 'perfil.php?id='.$this->tblSocios->ID_SOCIO_ENCRYPTED;
				#$ROW[0]['infoEdit'] 	  = ($actbool) ? ' (Edição desabilitada pelo Sócio)' : '';
				$ROW[0]['resHTML_ESTADO'] = $this->buildSELECT_SocioEstado($ROW[0]['id_estado'],$this->tblSocios->ID_ASSOC);
				$ROW[0]['resHTML_TIPO']   = $this->buildSELECT_SocioTipo($ROW[0]['id_tipo'],$this->tblSocios->ID_ASSOC);
				##########################################################################################
				$EXIST = true;
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST, 'JSON'=>$jsonData);
		}
	}
?>