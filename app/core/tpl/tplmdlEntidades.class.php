<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplModule.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblEntidades.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	class tplmdlEntidades extends TobjTemplateModule
	{
		public $sHTML			= '';
		public $tplFileName 	= '';
		public $tplFolderPath	= '';
		public $tblEntidades 		= false;
		public $URL					= '';
		public $ROOT				= '';
		protected $URL_PERFILEDIT	= 'neo/?op=entidades&ide=';
		protected $URL_PERFILNOVO	= 'neo/?op=entidades';

		public function __construct($ADBConnection=false)
		{
			parent::__construct();
			$this->setObjectVars($ADBConnection);
		}
		public function __destruct()
		{
			parent::__destruct();
			$this->unsetObjectVars();
		}

		# ########## FUNCTIONS TEMPLATE ########## #
		public function getSHTML() 					{ return $this->sHTML; }
		public function setTplFileName($AValue) 	{ $this->tplFileName 	= $AValue; }
		private function setTplFolderPath($AValue) 	{ $this->tplFolderPath 	= $AValue; }
		private function setTemplateFile()
		{
			parent::setObjTemplate($this->tplFolderPath.$this->tplFileName);
		}
		public function finishTemplate()
		{
			$this->sHTML = $this->objTemplate->echohtml();
		}

		# ########## VAIRAVEIS ########## #
		private function setObjectVars($ADBConnection)
		{
			$this->setTplFolderPath(SETPATH('ROOT','PATH_APP_UI_ENTIDADES'));
			$this->tblEntidades = new dbTblEntidades($ADBConnection);
			parent::setDBTable($this->tblEntidades);
			$this->URL  = SETPATH('URL','PATH_APP');
			$this->ROOT = SETPATH('ROOT','PATH_APP');
		}
		private function unsetObjectVars()
		{
			unset($tblEntidades);
			unset($sHTML);
		}
		public function setIDValues($AID_ENTIDADE=0,$AID_USER=0,$AID_ASSOC=0)
		{
			$this->tblEntidades->set_IDENTIDADE($AID_ENTIDADE);
			$this->tblEntidades->set_IDUSER($AID_USER);
			$this->tblEntidades->set_IDASSOC($AID_ASSOC);
		}
		
		private function SQL_SUCESS()
		{
			return CREATE_POP_MSG('Registo salvo com sucesso!',3000);
		}
		private function SQL_ERROR()
		{
			return CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
		#############################################################################
		#############################################################################
		public function decodeJSONtoArray($AJSON)
		{
			$jsonData = base64_decode($AJSON);
			$jsonData = urldecode($jsonData);
			$jsonData = utf8_encode($jsonData);
			$jsonData = json_decode($jsonData,true);
			
			return $jsonData;
		}

		public function INSERT_ENTIDADE($A_POST,$A_GET)
		{
			$Result = false;
			if ( (isset($A_POST['Nome'])) || (isset($A_POST['jsonPOST'])) )
			{
				if (isset($A_POST['jsonPOST']))
				{
					$jsonData = $this->decodeJSONtoArray($A_POST['jsonPOST']);
					$A_POST	  = $jsonData['_POST'];
				}

				if ( ($A_POST['Nome'] !== '') &&
					 ($A_POST['Tipo-Entidade'] !== '') &&
					 ($A_POST['Codigo-Postal'] !== '')
					 &&
					 ($A_POST['Nome'] !== '') &&
					 ($A_POST['Tipo-Entidade'] !== '') &&
					 ($A_POST['Codigo-Postal'] !== '') )
				{
					$A_POST['NIB'] 			= '';
					$A_POST['Telefone'] 	= '';
					$A_POST['Telemovel'] 	= '';
					$A_POST['Fax'] 			= '';
					$A_POST['Outro1'] 		= '';
					$A_POST['Outro2'] 		= '';
					$A_POST['Site'] 		= '';

					$resArr 		 = $this->tblEntidades->INSERT($A_POST);
					$Result 		 = $resArr['Result'];
					$NEW_ENTIDADE_ID = $resArr['NEW_ID'];

					if ($Result)
					{
						$Result = $resArr;
					}
				}
			}
			return $Result;
		}
		public function UPDATE_ENTIDADE($A_POST,$A_GET)
		{
			$Result = false;
			if ( (isset($A_POST['Nome'])) || (isset($A_POST['jsonPOST'])) )
			{
				if (isset($A_POST['jsonPOST']))
				{
					$jsonData = $this->decodeJSONtoArray($A_POST['jsonPOST']);
					$A_POST	  = $jsonData['_POST'];
				}

				if ( ($A_POST['Nome'] !== '') &&
					 ($A_POST['Tipo-Entidade'] !== '') &&
					 ($A_POST['Codigo-Postal'] !== '') )
				{
					$Result = $this->tblEntidades->UPDATE($A_POST);
				}
			}
			return $Result;
		}
		#############################################################################
		########################## HTML TEMPLATE FUNCTIONS ##########################
		#############################################################################

		#############################################################################
		########################## HTML TEMPLATE VARIABLES ##########################
		#############################################################################

		#############################################################################
		#############################################################################
		public function getPerfilEntidade()
		{
			$Result = $this->tblEntidades->getDadosEntidade();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];

			if (isset($ROW[0]))
			{
				if ($ROW[0]['observacoes'] === '')
					$ROW[0]['observacoes'] = 'Sem observações...';

				$EXIST = true;
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function getPerfilEditEntidade()
		{
			$Result = $this->tblEntidades->getPerfilEditEntidade();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];
			$jsonData = '';

			if ($EXIST)
			{
				$ROW[0]['observacoes'] = $this->tblEntidades->br2nl($ROW[0]['observacoes']);

				################################################################
				$jsonData = array();
				$jsonData[] = array('name'=> 'Abreviatura', 	'type'=>'input', 'value'=>$ROW[0]['abreviatura']);
				$jsonData[] = array('name'=> 'Codigo-Postal', 	'type'=>'input', 'value'=>$ROW[0]['codigo_postal']);
				$jsonData[] = array('name'=> 'Email', 			'type'=>'input', 'value'=>$ROW[0]['email']);
				$jsonData[] = array('name'=> 'Tipo-Entidade', 	'type'=>'select', 'value'=>$ROW[0]['codigo_tipo']);
				$jsonData[] = array('name'=> 'Fax', 			'type'=>'input', 'value'=>$ROW[0]['fax']);
				$jsonData[] = array('name'=> 'Morada', 			'type'=>'input', 'value'=>$ROW[0]['morada']);
				$jsonData[] = array('name'=> 'NIB', 			'type'=>'input', 'value'=>$ROW[0]['nib']);
				$jsonData[] = array('name'=> 'NIF', 			'type'=>'input', 'value'=>$ROW[0]['nif']);
				$jsonData[] = array('name'=> 'Nome', 			'type'=>'input', 'value'=>$ROW[0]['nome']);
				$jsonData[] = array('name'=> 'Observacoes', 	'type'=>'textarea', 'value'=>$ROW[0]['observacoes']);
				$jsonData[] = array('name'=> 'Outro1', 			'type'=>'input', 'value'=>$ROW[0]['outro1']);
				$jsonData[] = array('name'=> 'Outro2', 			'type'=>'input', 'value'=>$ROW[0]['outro2']);
				$jsonData[] = array('name'=> 'Telefone', 		'type'=>'input', 'value'=>$ROW[0]['telefone']);
				$jsonData[] = array('name'=> 'Telemovel', 		'type'=>'input', 'value'=>$ROW[0]['telemovel']);
				$jsonData[] = array('name'=> 'Site', 			'type'=>'input', 'value'=>$ROW[0]['url']);
				$jsonData = json_encode($jsonData);
				$jsonData = htmlentities($jsonData,ENT_QUOTES);
				#$jsonData = base64_encode($jsonData);
				################################################################
				
				$EXIST = true;
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST, 'JSON'=>$jsonData);
		}
	}
?>