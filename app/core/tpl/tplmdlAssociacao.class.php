<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);

	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplModule.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblAssociacao.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	class tplmdlAssociacao extends TobjTemplateModule
	{
		public $sHTML			= '';
		public $tplFileName 	= '';
		public $tplFolderPath	= '';
		public $tblAssociacao 	= false;

		public function __construct()
		{
			parent::__construct();
			$this->setVars();
		}
		public function __destruct()
		{
			parent::__destruct();
			$this->unsetVars();
		}

		public function getSHTML() 					{ return $this->sHTML; }
		public function setTplFileName($AValue) 	{ $this->tplFileName 	= $AValue; }
		public function setTplFolderPath($AValue) 	{ $this->tplFolderPath 	= $AValue; }
		private function setVars()
		{
			$this->setTplFolderPath(SETPATH('ROOT','PATH_APP_UI_ASSOCIACAO'));
			$this->tblAssociacao = new dbTblAssociacao();
			parent::setDBTable($this->tblAssociacao);
		}
		private function unsetVars()
		{
			unset($tblAssociacao);
			unset($sHTML);
		}
		public function finishTemplate()
		{
			$this->sHTML = $this->objTemplate->echohtml();
		}
		#############################################################################
		#############################################################################
		public function forkTemplate_AssociacaoNovo($AID_USER,$AID_ASSOC)
		{
			$this->setTplFileName('novo.html');
			parent::setObjTemplate($this->tplFolderPath.$this->tplFileName);
			parent::setTitulo('Registar Associação');
			parent::setAjuda('Indique os dados da sua associação.');

			$this->tblAssociacao->set_IDUSER($AID_USER);
			$this->tblAssociacao->set_IDASSOC($AID_ASSOC);
			$HTML_TIPOACTIVIDADE = $this->getTipoActividade_SELECT('null');

			parent::addLeftMenuItem(array("titulo" => "Guardar", "href" => "/", "javascript" => "ValidateForm('novo-associacao');"));

			$this->objTemplate->setVar('{$titulo}', 'Registar Associação');
			$this->objTemplate->setVar('{$ajuda}', 'Indique os dados da sua associação.');
			$this->objTemplate->setVar('{$imgsPath}', SETPATH('URL','PATH_APP_IMG_ICONS'));
			$this->objTemplate->setVar('{$HTML_TIPOACTIVIDADE}', $HTML_TIPOACTIVIDADE);
			$this->objTemplate->setVar('{$foto-url}',	SETPATH('URL','PATH_APP_IMG_ASSOCS').'quotagest-logo.png');

			$this->sHTML = $this->objTemplate->echohtml();
		}
		public function forkTemplate_AssociacaoPerfil($AID_USER,$AID_ASSOC)
		{
			$this->setTplFileName('perfil.html');
			parent::setObjTemplate($this->tplFolderPath.$this->tplFileName);
			parent::setTitulo('Perfil da Associação');
			parent::setAjuda('Utilize o botão "Editar" para alterar os dados da Associação.');

			$this->tblAssociacao->set_IDUSER($AID_USER);
			$this->tblAssociacao->set_IDASSOC($AID_ASSOC);
			$Result = $this->getPerfilAssociacao();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];

			if ($EXIST)
			{
				parent::addLeftMenuItem(array("titulo" => "Editar", "href" => "/ui/associacao/perfil-edit.php"));

				$this->objTemplate->setVarArray('{field:','}',$ROW[0]);
				$this->objTemplate->setVar('{$reg-update}', getLastDateEdit($ROW[0]['data_alterado']));
				$this->objTemplate->setVar('{$edit-link}', 'perfil-edit.php');
				$this->objTemplate->setVar('{$foto-url}',	$ROW[0]['imgLogoSrc']);
			}
			else
			{
				$sMessage = CREATE_NOTFOUND('O Registo não existe!','Criar Associação','novo.php');
				$this->objTemplate->replaceDOM('[data-id=corpo]','innertext',$sMessage);
			}
			$this->sHTML = $this->objTemplate->echohtml();
		}
		public function forkTemplate_AssociacaoPerfilEdit($AID_USER,$AID_ASSOC)
		{
			$this->setTplFileName('perfil-edit.html');
			parent::setObjTemplate($this->tplFolderPath.$this->tplFileName);
			parent::setTitulo('Editar Perfil da Associação');
			parent::setAjuda('Edite as informações básicas do perfil da Associação.');

			$this->tblAssociacao->set_IDUSER($AID_USER);
			$this->tblAssociacao->set_IDASSOC($AID_ASSOC);
			$Result = $this->getPerfilEditAssociacao();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];

			if ($EXIST)
			{
				$id_assoc_encrypted = $this->tblAssociacao->encryptVar($AID_ASSOC);
				$sKey = $this->tblAssociacao->encryptVar((date('h:i:s').'*'.$id_assoc_encrypted));
				parent::addLeftMenuItem(array("titulo" => "Guardar", "href" => "/", "javascript" => "ValidateForm('associacao-perfil-edit');"));
				parent::addLeftMenuItem(array("titulo" => "Remover Logotipo", "href" => "/", "javascript" => "REMOVE_IMAGEFILE('associacao','".$sKey."','unset');"));
				parent::addLeftMenuItem(array("titulo" => "Voltar", "href" => "/ui/associacao/perfil.php"));

				$this->objTemplate->setVarArray('{field:','}',$ROW[0]);
				$this->objTemplate->setVar('{$imgsPath}', SETPATH('URL','PATH_APP_IMG_ICONS'));
				$this->objTemplate->setVar('{$FormAction}', htmlentities($_SERVER['PHP_SELF']));
				$this->objTemplate->setVar('{$reg-update}', getLastDateEdit($ROW[0]['data_alterado']));
				$this->objTemplate->setVar('{$foto-url}',	$ROW[0]['imgLogoSrc']);
			}
			else
			{
				$sMessage = CREATE_NOTFOUND('O Registo não existe!','Criar Associação','novo.php');
				$this->objTemplate->replaceDOM('[data-id=corpo]','innertext',$sMessage);
			}
			$this->sHTML = $this->objTemplate->echohtml();
		}		
		#############################################################################
		#############################################################################
		public function getPerfilAssociacao()
		{
			$Result = $this->tblAssociacao->getAssociacaoData();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];
			
			if (isset($ROW[0]))
			{
				#############################################################################
				########################## HTML TEMPLATE VARIABLES ##########################
				#############################################################################
				$ROW[0]['observacoes']	= ($ROW[0]['observacoes'] !== '') ? $ROW[0]['observacoes'] : 'Sem observações...';
				#############################################################################
				$ROW[0]['HTML_TIPOACTIVIDADE'] = $this->tblAssociacao->arrTipoActividade[$ROW[0]['tipo_actividade']];
				#############################################################################
				$EXIST = true;
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function getPerfilEditAssociacao()
		{
			$Result = $this->tblAssociacao->getAssociacaoData();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];

			if (isset($ROW[0]))
			{
				#############################################################################
				########################## HTML TEMPLATE VARIABLES ##########################
				#############################################################################
				$ROW[0]['HTML_TIPOACTIVIDADE'] = $this->getTipoActividade_SELECT($ROW[0]['tipo_actividade']);
				#############################################################################
				$EXIST = true;
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}

		public function getTipoActividade_SELECT($AID_MAIN)
		{
			$Result = '';
			$ccI = count($this->tblAssociacao->arrTipoActividade);
			for ($i=0; $i<$ccI; $i++)
			{
				$selectOption = ($i == $AID_MAIN) ? ' selected ' : '';
				$Result .= "<option value='".$i."' ".$selectOption.">".$this->tblAssociacao->arrTipoActividade[$i]."</option>";
			}
			return $Result;
		}
	}
?>