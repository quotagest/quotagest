<?php
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocios.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'db_tables.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'ctt.api.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblAssocSocios.class.php');

	class tplSocios extends dbTblSocios
	{
		public function __construct()
		{
			parent::__construct();
			
			$this->setTableName("socio");
		}
		public function __destruct()
		{
			parent::__destruct();
		}

		#############################################################################
		########################## HTML TEMPLATE FUNCTIONS ##########################
		#############################################################################
		public function getJSON_TipoSocio($AID_USER,$AID_ASSOC)
		{
			$SQL = 'SELECT id, nome FROM socio_tipo WHERE id_assoc="'.$AID_ASSOC.'" AND enabled="0"';
			$ROW = $this->getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				$ccI = count($ROW);
				for ($i=0; $i<$ccI; $i++)
				{
					$ROW[$i]['id'] 	 = $this->encryptVar($ROW[$i]['id']);
					$ROW[$i]['nome'] = utf8_encode($ROW[$i]['nome']);
				}
			}
			return base64_encode(json_encode($ROW));
		}
		public function getJSON_EstadoSocio($AID_USER,$AID_ASSOC)
		{
			$SQL = 'SELECT id, nome FROM socio_estado WHERE id_assoc="'.$AID_ASSOC.'" AND enabled="0"';
			$ROW = $this->getRESULTS($SQL);
			if (isset($ROW[0]))
			{
				$ccI = count($ROW);
				for ($i=0; $i<$ccI; $i++)
				{
					$ROW[$i]['id'] 	 = $this->encryptVar($ROW[$i]['id']);
					$ROW[$i]['nome'] = utf8_encode($ROW[$i]['nome']);
				}
			}
			return base64_encode(json_encode($ROW));			
		}

		public function check_InscritoEmTudo($AID_SOCIO,$AID_USER,$AID_ASSOC)
		{
			$Result = true;

			if (isset($AID_SOCIO) && ($AID_SOCIO !== ''))
			{
				$AID_SOCIO = decrypt($AID_SOCIO,'N5KyEXAnDdwGL4eV');

				$SQL = 'SELECT 
							(SELECT COUNT(*) FROM modalidade WHERE modalidade.id_assoc="'.$AID_ASSOC.'" AND modalidade.enabled="0") AS "num_modalidades",
							(SELECT COUNT(*) FROM mod_socio WHERE mod_socio.id_socio="'.$AID_SOCIO.'" AND mod_socio.id_assoc="'.$AID_ASSOC.'") AS "num_mod_inscrito"';
				
				$ROW = $this->getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					if ($ROW[0]['num_modalidades']>$ROW[0]['num_mod_inscrito'])
					{
						$Result = false;
					}
				}
			}
			return $Result;
		}
		#############################################################################
		#############################################################################
		public function BUILD_SELECT_ELEMENT($vSQL,$vIDValueMaster,$vIDFieldName,$vValueFieldName,$vFieldNameOption,$AAddText=false)
		{
			$str = '';
			$ROW = $this->getRESULTS($vSQL);
			if (isset($ROW[0]))
			{
				$cc	= count($ROW);
				for ($i=0; $i<$cc; $i++)
				{
					$select = ($vIDValueMaster == $ROW[$i][$vIDFieldName]) ? 'selected' : '';
					if (stripos($vValueFieldName,'id') !== false)
						$ROW[$i][$vValueFieldName] = encrypt($ROW[$i][$vValueFieldName],'N5KyEXAnDdwGL4eV');
					$Text = utf8_encode($ROW[$i][$vFieldNameOption]);

					$str .= '<option value="'.$ROW[$i][$vValueFieldName].':'.(($AAddText === true) ? $Text : '').'" '.$select.' >'.$Text.'</option>';				
					$str .= "\n";
				}
			}
			unset($ROW);

			return $str;
		}
		public function getHTML_SELECTActividadesInscrito($AID_MODALIDADE,$AID_SOCIO,$AID_USER,$AID_ASSOC)
		{
			$ResultHTML = '<option selected disabled>Sem inscrições</option>';

			if (isset($AID_SOCIO) && ($AID_SOCIO !== ''))
			{
				$AID_MODALIDADE = decrypt($AID_MODALIDADE,'N5KyEXAnDdwGL4eV');
				$AID_SOCIO 		= decrypt($AID_SOCIO,'N5KyEXAnDdwGL4eV');

				#$SQL = 'SELECT id,nome FROM modalidade WHERE modalidade.id_assoc="'.$id_assoc.'" AND enabled="0"';
				$SQL = 'SELECT mod_socio.id_modalidade AS "id", modalidade.nome AS "nome" FROM mod_socio INNER JOIN modalidade ON mod_socio.id_modalidade=modalidade.id WHERE mod_socio.id_socio="'.$AID_SOCIO.'" AND mod_socio.id_assoc="'.$AID_ASSOC.'" AND modalidade.enabled="0"';

				$htmlSELECT = $this->BUILD_SELECT_ELEMENT($SQL,$AID_MODALIDADE,'id','id','nome');
				if ($htmlSELECT !== '')
					$ResultHTML = $htmlSELECT;
			}
			else
			{
				$ResultHTML = '';
			}
			return $ResultHTML;
		}
		public function getHTML_SELECTActividades($AID_MODALIDADE,$AID_USER,$AID_ASSOC,$AAddText=false)
		{
			$SQL = 'SELECT id,nome FROM modalidade WHERE modalidade.id_assoc="'.$AID_ASSOC.'" AND enabled="0"';
			return $this->BUILD_SELECT_ELEMENT($SQL,$AID_MODALIDADE,'id','id','nome',$AAddText);
		}
		public function getHTML_SELECTActividadesTodas($AID_MODALIDADE,$AID_SOCIO,$AID_USER,$AID_ASSOC)
		{			
			$ID_MODALIDADE  = decrypt($AID_MODALIDADE,'N5KyEXAnDdwGL4eV');
			$ID_SOCIO 		= decrypt($AID_SOCIO,'N5KyEXAnDdwGL4eV');

			if (($AID_SOCIO !== '') && ($AID_MODALIDADE !== ''))
			{
				
				$strAux = '';

				$SQL = 'SELECT modalidade.id AS  "id",
							  mod_socio.id_modalidade AS  "id_modalidade",
							  modalidade.nome AS  "nome"
						FROM mod_socio
						RIGHT JOIN modalidade ON modalidade.id = mod_socio.id_modalidade
						AND mod_socio.id_assoc =  "'.$AID_ASSOC.'"
						AND mod_socio.id_socio =  "'.$ID_SOCIO.'"
						WHERE modalidade.id_assoc =  "'.$AID_ASSOC.'"	
						AND modalidade.enabled ="0"	
					';
				#AND mod_socio.id_modalidade IS NULL
				
				$ROW = $this->getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					$cc	= count($ROW);
					for ($i=0; $i<$cc; $i++)
					{
						$select = ($ROW[$i]['id'] === $ID_MODALIDADE) ? 'selected' : '';
						$ID 	= encrypt($ROW[$i]['id'],'N5KyEXAnDdwGL4eV');
						$sExt	= ($ROW[$i]['id_modalidade'] !== NULL) ? ' (Inscrito)' : ' (Inscrever ao Guardar)';
						$strAux .= '<option value="'.$ID.'" '.$select.' >'.utf8_encode($ROW[$i]['nome']).$sExt.'</option>';				
						$strAux .= "\n";
					}
					unset($ROW);
				}

				$HTML_MODALIDADE_SELECT = $strAux;
			}
			else
			{
				$HTML_MODALIDADE_SELECT = $this->getHTML_SELECTActividades($AID_MODALIDADE,$AID_USER,$AID_ASSOC);
			}

			return $HTML_MODALIDADE_SELECT;
		}
		public function getHTML_SELECTActividadesNAOInscrito($AID_MODALIDADE,$AID_SOCIO,$AID_USER,$AID_ASSOC)
		{
			$ID_MODALIDADE = decrypt($AID_MODALIDADE,'N5KyEXAnDdwGL4eV');
			$ID_SOCIO      = decrypt($AID_SOCIO,'N5KyEXAnDdwGL4eV');

			if ($AID_SOCIO !== '')
			{
				$strAux = '';

				$SQL = 'SELECT modalidade.id AS  "id",
							  mod_socio.id_modalidade AS  "id_modalidade",
							  modalidade.nome AS  "nome"
						FROM mod_socio
						RIGHT JOIN modalidade ON modalidade.id = mod_socio.id_modalidade
						AND mod_socio.id_assoc =  "'.$AID_ASSOC.'"
						AND mod_socio.id_socio =  "'.$ID_SOCIO.'"
						WHERE modalidade.id_assoc =  "'.$AID_ASSOC.'"
						AND modalidade.enabled="0"
					';
				#AND mod_socio.id_modalidade IS NULL
				$ROW = $this->getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					$cc	= count($ROW);
					for ($i=0; $i<$cc; $i++)
					{
						$isIDModNULL = ($ROW[$i]['id_modalidade'] !== NULL);
						$select = ($isIDModNULL) 	? 'disabled' : '';
						$ID 	= (!$isIDModNULL) 	? encrypt($ROW[$i]['id'],'N5KyEXAnDdwGL4eV') : '';
						$sExt	= ($isIDModNULL) 	? ' (Inscrito)' : '';
						$strAux .= '<option value="'.$ID.'" '.$select.' >'.utf8_encode($ROW[$i]['nome']).$sExt.'</option>';				
						$strAux .= "\n";
					}
				}
				unset($ROW);

				$HTML_MODALIDADE_SELECT = $strAux;
			}
			else
			{
				$HTML_MODALIDADE_SELECT = $this->getHTML_SELECTActividades($AID_MODALIDADE,$AID_USER,$AID_ASSOC);		
			}
			return $HTML_MODALIDADE_SELECT;
		}
		public function buildSelectSocioExtra($ATableName,$AIDPos,$AID_ASSOC,$AAddText=false)
		{
			$SQL = 'SELECT id,nome FROM '.$ATableName.' WHERE '.$ATableName.'.id_assoc="'.$AID_ASSOC.'" AND enabled="0"';
			$html = $this->BUILD_SELECT_ELEMENT($SQL,$AIDPos,'id','id','nome',$AAddText);
			return $html;
		}
		public function buildLABLESSocioExta($AElemType,$ATitleElem,$ATableName,$AIDPos,$AID_ASSOC,$AAddText=false)
		{
			$SQL = 'SELECT id,nome FROM '.$ATableName.' WHERE '.$ATableName.'.id_assoc="'.$AID_ASSOC.'" AND enabled="0"';
			$html = $this->buildLABLES($AElemType,$ATitleElem,$SQL,$AIDPos,'id','id','nome',$AAddText);
			return $html;
		}
		public function buildLABLES($AElemType,$ATitleElem,$vSQL,$vIDValueMaster,$vIDFieldName,$vValueFieldName,$vFieldNameOption,$AAddText=false)
		{
			$str = '';
			$ROW = $this->getRESULTS($vSQL);
			if (isset($ROW[0]))
			{
				$cc	= count($ROW);
				for ($i=0; $i<$cc; $i++)
				{
					#$select = ($vIDValueMaster == $ROW[$i][$vIDFieldName]) ? 'selected' : '';
					if (stripos($vValueFieldName,'id') !== false)
						$ROW[$i][$vValueFieldName] = $this->encryptVar($ROW[$i][$vValueFieldName]);
					$Text = utf8_encode($ROW[$i][$vFieldNameOption]);
					$labelname = $ATitleElem.'-'.$i;
					$str .= '<input type="'.$AElemType.'" class="tag" name="'.$ATitleElem.'[]" id="'.$labelname.'" value="'.$ROW[$i][$vValueFieldName].':'.(($AAddText === true) ? $Text : '').'">';
					$str .= "\n";
					$str .= '<label for="'.$labelname.'">'.$Text.'</label>';
					$str .= "\n";
				}
			}
			unset($ROW);

			return $str;
		}
		#############################################################################
		#############################################################################
		public function getHTML_INPUTSOCIO($AID_SOCIO,$AID_USER,$AID_ASSOC)
		{
			$SOCIO_NOME  	 = '';
			$SOCIO_INPUT_COD = '';

			if (isset($AID_SOCIO) && ($AID_SOCIO !== ''))
			{
				$Result = $this->getFieldFromSocio(array('socio.nome'),array('STRING'),$AID_SOCIO,$AID_USER,$AID_ASSOC);
				if ($Result['EXIST'] == true)
				{
					$SOCIO_NOME = $Result['ROW'][0]['nome'];
					$SOCIO_INPUT_COD= '<input type="hidden" name="CodSocio" value="'.$AID_SOCIO.'" />';
				}
			}
			return array('SOCIO_NOME'=>$SOCIO_NOME,'SOCIO_INPUT_COD'=>$SOCIO_INPUT_COD);
		}
		#############################################################################
		########################## HTML TEMPLATE VARIABLES ##########################
		#############################################################################
		public function getTPL_QUOTASLISTAGEM($AID_MODALIDADE,$AID_SOCIO,$AID_USER,$AID_ASSOC)
		{
			$arrMENU = array();
			$IDM = ($AID_MODALIDADE !== '') ? '&idm='.$AID_MODALIDADE : '';

			$htmlSELECT_ACTIVIDADES = $this->getHTML_SELECTActividadesInscrito($AID_MODALIDADE,$AID_SOCIO,$AID_USER,$AID_ASSOC);
			$Result = $this->check_InscritoEmTudo($AID_SOCIO,$AID_USER,$AID_ASSOC);
			if ($AID_SOCIO !== '')
			{
				$arrMENU["soc0"] = array("titulo" => "Novo Ano",
										  "href" 	=> "/ui/quotas/novo-quota-massa.php?ids=".$AID_SOCIO.$IDM);
			}
			if (!$Result)
			{
				$arrMENU["soc1"] = array("titulo" => "Inscrever Sócio numa Actividade",
										  "href" 	=> "/ui/modalidades/novo-inscricao.php"."?ids=".$AID_SOCIO);
			}
			if ($AID_SOCIO !== '')
			{
				$arrMENU["soc2"] = array("titulo" => "Ver perfil do Sócio",
										  "href" 	=> "/ui/socios/perfil.php?id=".$AID_SOCIO);
			}
			
			$Result = $this->getHTML_INPUTSOCIO($AID_SOCIO,$AID_USER,$AID_ASSOC);
			$SOCIO_NOME  	 = $Result['SOCIO_NOME'];
			$SOCIO_INPUT_COD = $Result['SOCIO_INPUT_COD'];


			return array('arrMENU'=>$arrMENU,
						 'htmlSELECT_ACTIVIDADES'=>$htmlSELECT_ACTIVIDADES,
						 'SOCIO_NOME'=>$SOCIO_NOME,
						 'SOCIO_INPUT_COD'=>$SOCIO_INPUT_COD
						);
		}
		#############################################################################
		#############################################################################
		public function getFieldFromSocio($AarrFieldName,$ArrType,$AID_SOCIO,$AID_USER,$AID_ASSOC)
		{
			$ROW = array();
			$EXIST = false;

			if (isset($AID_SOCIO) && ($AID_SOCIO !== ''))
			{
				$AID_SOCIO = decrypt($AID_SOCIO,'N5KyEXAnDdwGL4eV');

				$SQL  = "SELECT ".implode(', ',$AarrFieldName);
				$SQL .= ' FROM assoc_socios';
				$SQL .= ' LEFT JOIN socio ON socio.id=assoc_socios.id_socio';
				$SQL .= ' WHERE assoc_socios.id_assoc="'.$AID_ASSOC.'"';
				$SQL .= ' AND id_socio="'.$AID_SOCIO.'"';
				$SQL .= ' LIMIT 0,1;';

				$ROW = $this->getRESULTS($SQL);
				if (isset($ROW[0]))
				{
					$this->setFieldsType($ArrType);
					$this->FieldsOutput($ROW);

					#############################################################################
					# SE "enabled" = 1 SIGNIFICA QUE: socio ainda não autorizou ver os dados.
					# SE "activo" = 1 SIGNIFICA QUE: socio permite que se veja os dados e que sejam editados.
					# SE "activo" = 2 SIGNIFICA QUE: socio permite que se veja os dados mas que não sejam editados.
					#############################################################################

					$EXIST = true;
				}
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function getPerfilSocio()
		{
			$ROW = array();
			$EXIST = false;

			$Result = $this->getSocioData();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];

			if (isset($ROW[0]))
			{
				$valid = ($ROW[0]['enabled'] == '0');
				if (!$valid)
				{
					#$ROW[0]['nome']					= ;
					$ROW[0]['telefone']				= 'Escondido';
					$ROW[0]['telemovel']			= 'Escondido';
					$ROW[0]['fax']					= 'Escondido';
					$ROW[0]['email']				= 'Escondido';
					$ROW[0]['outro1']				= 'Escondido';
					$ROW[0]['outro2']				= 'Escondido';
					$ROW[0]['morada']				= 'Escondido';
					$ROW[0]['codigo_postal']		= 'Escondido';
					$ROW[0]['data_nascimento']		= 'Escondido';
					$ROW[0]['sexo']					= 'Escondido';
					#$ROW[0]['bicc'] 				= ;
					$ROW[0]['nif']					= 'Escondido';
					$ROW[0]['nib']					= 'Escondido';
					$ROW[0]['concelho_desig'] 		= 'Escondido';
					$ROW[0]['distrito_desig'] 		= 'Escondido';
				}

				if ($ROW[0]['observacoes'] === '') 
					$ROW[0]['observacoes'] = 'Sem observações...';

				$ROW[0]['data_nascimento']		= str_replace("0000-00-00", "", $ROW[0]['data_nascimento']);
				$ROW[0]['data_entrada']			= str_replace("0000-00-00", "", $ROW[0]['data_entrada']);
				$ROW[0]['data_saida']			= str_replace("0000-00-00", "", $ROW[0]['data_saida']);

				$EXIST = true;
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
		public function getPerfilEditSocio()
		{
			$ROW = array();
			$EXIST = false;

			$Result = $this->getSocioData();
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];

			/* SE FOI ENCONTRADO REGISTO */
			if ($EXIST)
			{
				if ($ROW[0]['sexo'] === 'Masculino')
				{
					$ROW[0]['sexFem'] = '';
					$ROW[0]['sexMas'] = ' checked ';
				}
				else
				{
					$ROW[0]['sexFem'] = ' checked ';
					$ROW[0]['sexMas'] = '';
				}
				# (activo === 1) -> Sócio permite ver dados e respectiva edição.
				# (activo === 2) -> Sócio permite ver dados mas não os editar.
				#
				#	SE "enabled" = 1 SIGNIFICA QUE: socio ainda não autorizou ver os dados.
				#	SE "activo" = 1 SIGNIFICA QUE: socio permite que se veja os dados e que sejam editados.
				#	SE "activo" = 2 SIGNIFICA QUE: socio permite que se veja os dados mas que não sejam editados.
				#
				$ROW[0]['data_entrada'] 	= date('Y-m-d',strtotime($ROW[0]['data_entrada']));
				$ROW[0]['data_saida'] 		= date('Y-m-d',strtotime($ROW[0]['data_saida']));
				$ROW[0]['data_nascimento'] 	= date('Y-m-d',strtotime($ROW[0]['data_nascimento']));

				$valid 					= ($ROW[0]['enabled'] !== '1');
				$actbool 				= ($ROW[0]['activo'] == 2);
				$FieldsDisabled 		= ($actbool) ? ' disabled ' : '' ;

				$ROW[0]['plaNome'] 		= ($actbool) ? ' placeholder="'.$ROW[0]['nome'].'" '			: ' placeholder="'.'Sócio Nome'.'" ';
				$ROW[0]['plaTelefone'] 	= ($actbool) ? ' placeholder="'.$ROW[0]['telefone'].'" '		: ' placeholder="'.'Telefone'.'" ';
				$ROW[0]['plaTelemovel'] = ($actbool) ? ' placeholder="'.$ROW[0]['telemovel'].'" '		: ' placeholder="'.'Telemovel'.'" ';
				$ROW[0]['plaFax'] 		= ($actbool) ? ' placeholder="'.$ROW[0]['fax'].'" '				: ' placeholder="'.'Fax'.'" ';
				$ROW[0]['plaEmail'] 	= ($actbool) ? ' placeholder="'.$ROW[0]['email'].'" '			: ' placeholder="'.'Email'.'" ';
				$ROW[0]['plaOutro1'] 	= ($actbool) ? ' placeholder="'.$ROW[0]['outro1'].'" '			: ' placeholder="'.'Outro1'.'" ';
				$ROW[0]['plaOutro2'] 	= ($actbool) ? ' placeholder="'.$ROW[0]['outro2'].'" '			: ' placeholder="'.'Outro2'.'" ';
				$ROW[0]['plaMorada'] 	= ($actbool) ? ' placeholder="'.$ROW[0]['morada'].'" '			: ' placeholder="'.'Morada'.'" ';
				$ROW[0]['plaCodPostal']	= ($actbool) ? ' placeholder="'.$ROW[0]['codigo_postal'].'" '	: ' placeholder="'.'Cód. Postal'.'" ';
				$ROW[0]['plaDataNasc']	= ($actbool) ? ' placeholder="'.$ROW[0]['data_nascimento'].'" '	: ' placeholder="'.'Data Nascimento'.'" ';
				$ROW[0]['plaSexo']		= ($actbool) ? ' placeholder="'.$ROW[0]['sexo'].'" '			: ' placeholder="'.'Sexo'.'" ';
				$ROW[0]['plaBICC']		= ($actbool) ? ' placeholder="'.$ROW[0]['bicc'].'" '			: ' placeholder="'.'B.I/C.C'.'" ';
				$ROW[0]['plaNIF']		= ($actbool) ? ' placeholder="'.$ROW[0]['nif'].'" '				: ' placeholder="'.'N.I.F'.'" ';
				$ROW[0]['plaNIB']		= ($actbool) ? ' placeholder="'.$ROW[0]['nib'].'" '				: ' placeholder="'.'N.I.B'.'" ';

				$ROW[0]['EDIT_LINK'] 	  = ($valid) ? ('perfil-edit.php?id='.$this->ID_SOCIO_ENCRYPTED) : '#';
				$ROW[0]['LINK_ANULAR'] 	  = 'perfil.php?id='.$this->ID_SOCIO_ENCRYPTED;
				$ROW[0]['infoEdit']		  = ($actbool) ? ' (Edição desabilitada pelo Sócio)' : '';
				$ROW[0]['resHTML_ESTADO'] = $this->buildSelectSocioExtra('socio_estado',$ROW[0]['id_estado'],$this->ID_USER);
				$ROW[0]['resHTML_TIPO']   = $this->buildSelectSocioExtra('socio_tipo',$ROW[0]['id_tipo'],$this->ID_USER);
				##########################################################################################
				$EXIST = true;
			}

			return array('ROW'=>$ROW, 'EXIST'=>$EXIST);
		}
	}
?>