<?php
	/* ************************************************************************************************************ */
	/* 																												*/
	/* 	AC��O		: SELECT ROWS																					*/
	/* 	TABELA		: ALL															 								*/
	/* 																												*/
	/* 	CAMPOS_IN	:																								*/
	/* 		- $_SESSION['id_user']																					*/
	/* 		- $_SESSION['id_assoc']																					*/
	/* 		- $_POST['encrypt']																						*/
	/* 																												*/
	/* 	CAMPOS_OUT	:																								*/
	/* 		- $Result																								*/
	/* 																												*/
	/* ************************************************************************************************************ */
	
	if(!isset($_SESSION))
	{
		session_start();
	}
	if(isset($_SESSION['id_user']))
	{
		$id_user  = $_SESSION['id_user'];
		$id_assoc = $_SESSION['id_assoc'];
	}

	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	/* FOLDER VARS */
	require_once($_SERVER['DOCUMENT_ROOT']."/configs/dir-vars.php");	
	require_once(SETPATH('ROOT','PATH_APP_CORE').'db_functions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	//require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'db_tables.php');
	
	##$id_assoc 		= '1';
	##$id_user  		= '2';
	$Result			= false;
	
	if (isset($_POST['encrypt']) && ($_POST['encrypt'] !== ''))
	{
		$aEncrypt 	= explode('#',$_POST['encrypt']);
		$ID_MASTER 	= decrypt($aEncrypt[0],'N5KyEXAnDdwGL4eV');	
		$TABLE		= $aEncrypt[1];

		switch($TABLE)
		{
			case 'modturma'		: $TABLE = 'mod_turma'; 		break;
			case 'modsocnivel'	: $TABLE = 'mod_socio_nivel'; 	break;
		}
		
		$id_fieldName  = '';
		$id_fieldValue = '';
		
		$tbl1 = new dbTable();
		
		/* VERIFICAR SE CAMPO "id_assoc" ou "id_user" existem numa tabela */
		$SQL = 'DESCRIBE '.$TABLE;
		$ROW = $tbl1->getRESULTS($SQL);
		if (isset($ROW[0]))
		{
			if (ExistValue_inArray('id_assoc',$ROW))
			{
					$id_fieldName  = 'id_assoc';
					$id_fieldValue = $id_assoc;		
			}
			else
			if (ExistValue_inArray('id_user',$ROW))
			{
					$id_fieldName  = 'id_user';
					$id_fieldValue = $id_user;		
			}
		}
		//echo(array_key_exists('id_user',$ROW));
		/* ************************************************************** */
		$ROW = $tbl1->SQL_SELECT($TABLE,array('id','nome'),array($id_fieldName,'id_modalidade'),array($id_fieldValue,$ID_MASTER), '');

		if (isset($ROW[0]))
		{
			$a = array();
			for ($i=0; $i<count($ROW); $i++)
			{
				$a[] = encrypt($ROW[$i]['id'],'N5KyEXAnDdwGL4eV').'#'.utf8_encode($ROW[$i]['nome']);
			}
			 
			$res = implode(';',$a);
			$Result = true;
		}
		unset($tbl1);
	}
	$Result = (!$Result) ? 'false' : 'true'.$res;
	echo $Result;
?>