<?php
	#########################################################
	#														#
	#	  RECEBER DADOS DOS CTT BASEADO NO CODIGO POSTAL	#
	#														#
	#														#
	#########################################################
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	
	$Result = 'false';
	
	$tbl1 = new dbTable();
	$CP4 = $tbl1->protectVar($_GET['cod1']); #'3000';
	$CP3 = $tbl1->protectVar($_GET['cod2']); #'340';
	
	$SQL  = '	SELECT 
						ctt_codigos_postais.localidade AS "localidade",
						ctt_codigos_postais.art_tipo AS "art_tipo",
						ctt_codigos_postais.pri_prep AS "pri_prep",
						ctt_codigos_postais.art_titulo AS "art_titulo",
						ctt_codigos_postais.seg_prep AS "seg_prep",
						ctt_codigos_postais.art_desig AS "art_desig",
						ctt_codigos_postais.art_local AS "art_local",
						ctt_codigos_postais.troco AS "troco",
						ctt_codigos_postais.porta AS "porta",
						ctt_codigos_postais.cliente AS "cliente",
						ctt_codigos_postais.cpalf AS "cpalf",
						
						ctt_concelhos.desig AS "concelho_desig",
						ctt_distritos.desig AS "distrito_desig"

				FROM ctt_codigos_postais
				INNER JOIN ctt_concelhos ON ctt_concelhos.DD=ctt_codigos_postais.DD
										AND ctt_concelhos.CC=ctt_codigos_postais.CC
				INNER JOIN ctt_distritos ON ctt_distritos.DD=ctt_codigos_postais.DD
				
				WHERE ctt_codigos_postais.CP4="'.$CP4.'" AND
					  ctt_codigos_postais.CP3="'.$CP3.'"
				LIMIT 0,1';
	/* ***************************************************************************** */
	/* ***************************************************************************** */	
	$ROW = $tbl1->getRESULTS($SQL);
	if (isset($ROW[0]))
	{
		$CP 			= $CP4.'-'.$CP3;
		$Rua 			= array();
		$Rua[] 			= $ROW[0]['art_tipo'];
		$Rua[] 			= $ROW[0]['pri_prep'];
		$Rua[] 			= $ROW[0]['art_titulo'];
		$Rua[] 			= $ROW[0]['seg_prep'];
		$Rua[] 			= $ROW[0]['art_desig'];
		$Rua[] 			= $ROW[0]['art_local'];
		$Rua[] 			= $ROW[0]['troco'];
		$Rua[] 			= $ROW[0]['porta'];
		$Rua[] 			= $ROW[0]['cliente'];

		$Localidade 	= $ROW[0]['localidade'];
		$str_concelho 	= $ROW[0]['concelho_desig'];
		$str_distrito 	= $ROW[0]['distrito_desig'];

		if (isset($_GET['up']) && is_numeric($_GET['up']))
		{
			$Rua = implode(' ',$Rua);
			# http://stackoverflow.com/questions/2326125/remove-multiple-whitespaces-in-php
			$Rua = preg_replace('/\s\s+/', ' ',$Rua);
			# http://www.silverphp.com/remove-multiple-spaces-between-words-using-regex-or-simple-php-function.html
			#while(strpos($Rua,'  ') !== false)
			#{
			#	$Rua = str_replace('  ', ' ', $Rua);
			#}
			$Rua 			= utf8_encode(trim($Rua));
			$Localidade 	= utf8_encode($Localidade);
			$str_concelho 	= utf8_encode($str_concelho);
			$str_distrito 	= utf8_encode($str_distrito);
		}

		$Result = array('cp' 		=> $CP,
						'rua' 		=> $Rua,
						'local' 	=> $Localidade,
						'concelho' 	=> $str_concelho,
						'distrito' 	=> $str_distrito
						);
		$Result = json_encode($Result);
	}

	unset($tbl1);
	echo $Result;
	/* ***************************************************************************** */
	/* ***************************************************************************** */
/*
	require_once($_SERVER['DOCUMENT_ROOT']."/configs/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'ctt.api.class.php');
	
	$Result = 'false';

	$objCTTSearch = new TObj_CTTSearch();
	$Result = $objCTTSearch->getCTTINFO($_GET['cod1'],$_GET['cod2']);
	unset($objCTTSearch);

	echo $Result;
*/
?>