<?php
# Upload Files
# @author: 	Paulo Mota
# @version: 0.0.0.1
# @date: 	25-09-2013
# LINKS:
# 
#
# error_reporting(E_ALL);
# ini_set('display_errors', 1);
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'smart_resize_image.php');

	if (!function_exists('getallheaders')) 
	{
		function getallheaders()
		{
			$headers = '';
			foreach ($_SERVER as $name => $value)
			{
				if (substr($name, 0, 5) == 'HTTP_')
				{
					$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
				}
			}
			return $headers;
		}
	}

	class TobjUploadFile
	{
		public $FileName 		= '';
		public $TempFileName 	= '';
		public $MaxSize 		= 0;
		public $MaxWidth		= 0;
		public $MaxHeigth		= 0;
		public $FileType 		= '';
		public $isBase64		= false;
		public $InputFileName 	= '';
		public $Headers 		= '';
		public $isBinary		= false;
		public $echoFileName	= false;

		public function __construct()
		{

		}
		public function __destruct()
		{

		}
		public function setFileName($AValue) 		{ $this->FileName 		= $AValue; }
		public function setTempFileName($AValue) 	{ $this->TempFileName 	= $AValue; }
		public function setMaxSize($AValue) 		{ $this->MaxSize 		= $AValue; }
		public function setMaxWidth($AValue) 		{ $this->MaxWidth 		= $AValue; }
		public function setMaxHeigth($AValue) 		{ $this->MaxHeigth 		= $AValue; }
		public function setFileType($AValue) 		{ $this->FileType 		= $AValue; }
		public function setIsBase64($AValue) 		{ $this->isBase64 		= $AValue; }
		public function setInputFileName($AValue) 	{ $this->InputFileName 	= $AValue; }
		public function setHeaders($AValue) 		{ $this->Headers 		= $AValue; }
		public function setIsBinary($AValue) 		{ $this->isBinary 		= $AValue; }
		public function setEchoFileName($AValue) 	{ $this->echoFileName 	= $AValue; }

		private function getALLHeadersKey()
		{
			$arrHeaders = getallheaders();
			$arrHeaders = array_change_key_case($arrHeaders, CASE_UPPER);
			return $arrHeaders;
		}
		private function SaveFileStream()
		{
			$Result = false;
			if ($this->isBinary)
			{
				#If the browser does not support sendAsBinary ()
				$this->Headers = $this->getALLHeadersKey();
				
				if ($this->Headers['UP-SIZE'] < $this->MaxSize)
				{
					if ($this->isBase64)
					{
						$content = base64_decode(file_get_contents('php://input'));
					}
					else
					{
						$content = file_get_contents('php://input');
					}
					#$this->Headers['UP-FILENAME'];
					if (file_put_contents($this->FileName, $content))
					{
						if ($this->echoFileName)
						{
							echo substr(strrchr($this->FileName,"/"),1);
						}
						$Result = true;
					}
				}
			}
			else
			{
				#If the browser supports sendAsBinary() can use the array $_FILES
				if (count($_FILES)>0)
				{
					if ($_FILES[$this->InputFileName]["size"] < $this->MaxSize)
					{
						$tmp_name = $_FILES[$this->InputFileName]['tmp_name'];
						if (move_uploaded_file($tmp_name, $this->FileName))
						{
							if ($this->echoFileName)
							{
								echo substr($this->FileName,strrchr($this->FileName,"/"));
							}
							$Result = true;
						}
					}
				}
			}
			return $Result;
		}
		public function getFileType()
		{
			$Result = 'other';
			if ($this->isBinary)
			{
				#If the browser does not support sendAsBinary()
				$this->Headers = $this->getALLHeadersKey();
				
				$Result = $this->Headers['UP-TYPE'];
			}
			else
			{
				#If the browser supports sendAsBinary() can use the array $_FILES
				if (count($_FILES)>0)
				{
					$Result = $_FILES[$this->InputFileName]['type'];
				}
			}		
			return $Result;
		}

		public function SaveFile()
		{
			$Result	= false;
			
			if ($this->TempFileName === '')
			{
				if (($this->FileName !== '') && ($this->MaxSize !== 0) && ($this->FileType !== ''))
				{
					$type_name = $this->getFileType();
					
					switch ($this->FileType)
					{
						case 'text': 	$Result = $this->SaveFileStream();
										break;
						case 'image':	{
											if (($type_name == "image/gif")
											 || ($type_name == "image/jpeg")
											 || ($type_name == "image/jpg")
											 || ($type_name == "image/png")
											 || ($type_name == "image/bmp"))
											{
												$Result = $this->SaveFileStream();
												if (($Result) && ($this->MaxWidth>0) && ($this->MaxHeigth>0))
												{
													$resimg = smart_resize_image($this->FileName,$this->MaxWidth,$this->MaxHeigth,true);
												}
											}
										}
										break;
						default:	break;
					}
				}
			}
			else
			{
				#is_dir($dir)
				if (file_exists($this->TempFileName))
				{
					$Result = rename($this->TempFileName, $this->FileName);
				}
				else
				if (file_exists($this->FileName))
				{
					var_dump(true);
				}
				else
					$Result = true;
					#NENHUM FICHEIRO EXISTE;
			}
			return ($Result) ? 'done' : 'error';
		}
	}
?>