<?php
	/* ************************************************************************************************************ */
	/* 						How to safely encrypt PHP string into alphanumeric characters							*/
	/* 																	 											*/
	/* 	LINK: http://www.wihow.com/index.php?title=How_to_safely_encrypt_PHP_string_into_alphanumeric_characters	*/
	/* 																	 											*/
	/* 	LINK: http://uk.php.net/manual/en/mcrypt.examples.php														*/
	/* 	LINK: http://stackoverflow.com/questions/9262109/php-simplest-two-way-encryption							*/
	/* 	LINK: http://tech.chitgoks.com/2008/03/24/php-encrypt-decrypt-using-base64/									*/
	/* 																	 											*/
	/*	LINK: (EM USO)	http://www.php.net/manual/en/function.mcrypt-encrypt.php#71415			 					*/
	/* 																	 											*/
	/* ************************************************************************************************************ */
	
	function encode_base64($sData)
	{
		$sBase64 = base64_encode($sData);
		return str_replace('=', '', strtr($sBase64, '+/', '-_'));
	}
	function decode_base64($sData)
	{
		$sBase64 = strtr($sData, '-_', '+/');
		return base64_decode($sBase64.'==');
	}
	
	function mcrypt_get_create_iv()
	{
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB); 
		return mcrypt_create_iv($iv_size, MCRYPT_RAND); 
	}
	function encrypt($sData, $secretKey)
	{ 
		$sResult 	= '';
		$iv 		= mcrypt_get_create_iv(); 
		$sResult 	= mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $secretKey, $sData, MCRYPT_MODE_ECB, $iv); 
		return encode_base64($sResult);
	} 

	function decrypt($sData, $secretKey)
	{ 
		$sResult 	= '';
		$sData  	= decode_base64($sData);
		$iv 		= mcrypt_get_create_iv(); 
		$sResult 	= mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $secretKey, $sData, MCRYPT_MODE_ECB, $iv); 
		return trim($sResult); 
	} 
	
?>