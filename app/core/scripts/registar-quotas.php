<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblQuotas.class.php');

function utf8_urldecode($str)
{
	$str = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($str));
	return html_entity_decode($str,null,'UTF-8');
} 

	$Result = 'false';
	if ((isset($_POST['place'])) &&
		(isset($_POST['opera']))
		)
	{
		switch ($_POST['place'])
		{
			case 'quotas-listagem':
				{
					if ((isset($_POST['ids'])) &&
						(isset($_POST['idm']))
					)
					{
						$json_data = base64_decode($_POST['json64']);
						$json_data = utf8_urldecode($json_data);
						$json_data = json_decode($json_data,true);
#var_dump($json_data);
						#prazo_pagamento="{field-prazo_pagamento}",
						$ID_SOCIO = decrypt($_POST['ids'],'N5KyEXAnDdwGL4eV');
						$ID_MODAL = decrypt($_POST['idm'],'N5KyEXAnDdwGL4eV');

						$WHERE  = ' id_assoc="'.$id_assoc.'"';
						$WHERE .= ' AND id_modalidade="'.$ID_MODAL.'"';
						$WHERE .= ' AND id_socio="'.$ID_SOCIO.'"';

						$TableName = 'quotas_socios';
						$vSQL = '';
						
						/* ################################################################## */
						$dbTblQuotas = new dbTblQuotas();
						$dbTblQuotas->set_IDSOCIO($_POST['ids']);
						$dbTblQuotas->set_IDMODALIDADE($_POST['idm']);
						$dbTblQuotas->set_IDUSER($id_user);
						$dbTblQuotas->set_IDASSOC($id_assoc);
						switch ($_POST['opera'])
						{
							case 'paycheck'	:
							{ #ESCONDE REGISTOS
								$Result 		= true;
								$sTotalDivida 	= '0';
								$sDataPag	  	= date('Y-m-d');
								$sIDUser		= $id_user;
								$sDataAlter		= date('Y-m-d H:i:s');
			##var_dump($json_data);
								$ccI = count($json_data['objects']);
								for ($i=0; $i<$ccI; $i++)
								{
									$data 		= $json_data['objects'][$i];
									//$sTotalPago = str_ireplace('€','',$data['q_total']);
									$tTotal = str_ireplace('€','',$data['q_total']);
									$tPago = str_ireplace('€','',$data['q_totalpago']);
									$tDivi = str_ireplace('€','',$data['q_divida']);

									$sTotalPago 	= $tTotal;
									$sTotalDivida 	= '0.00';

									$sDataPag = $data['q_datapag'];
									if (($sDataPag === '') || ($sDataPag === '0000-00-00'))			#QUOTA POR PAGAR
									{
										$sDataPag = date('Y-m-d');	
									}
									else
									{
										$sDataPag = date('Y-m-d',strtotime($sDataPag));
									}

									$sWhere 	= $WHERE.' AND id="'.decrypt($data['value'],'N5KyEXAnDdwGL4eV').'"';

									$expSQL	 = ' UPDATE '.$TableName.' SET ';
									$expSQL	.= ' total_pago="'.$sTotalPago.'", ';
									$expSQL	.= ' total_divida="'.$sTotalDivida.'", ';
									$expSQL	.= ' data_pagamento="'.$sDataPag.'", ';
									$expSQL	.= ' id_user_edited="'.$sIDUser.'", ';
									$expSQL	.= ' data_alterado="'.$sDataAlter.'" ';
									$expSQL	.= ' WHERE '.$sWhere.'';
			##echo($expSQL);
									$Result .= $dbTblQuotas->ExecSQL($expSQL);
								}
							} break;
							case 'payuncheck':
							{ #MOSTRA REGISTOS
								$Result 	= true;
								$sDataPag	= '0000-00-00';
								$sIDUser	= $id_user;
								$sDataAlter	= date('Y-m-d H:i:s');

								$ccI = count($json_data['objects']);
								for ($i=0; $i<$ccI; $i++)
								{ 
									$data 			= $json_data['objects'][$i];
									
									//$sDataPraz = $data['q_datapraz'];
									$tTotal = str_ireplace('€','',$data['q_total']);
									$tPago = str_ireplace('€','',$data['q_totalpago']);
									$tDivi = str_ireplace('€','',$data['q_divida']);

									$sTotalPago 	= $tPago;
									$sTotalDivida 	= $tTotal-$tDivi;
									if ($tDivi === '0.00') # QUOTA JA ESTA PAGA E VAI SER - divida
									{
										$sTotalPago = '0.00';
										$sTotalDivida = $tTotal;
										$sDataPag	= '0000-00-00';
									}
									else 					# QUOTA AINDA NAO ESTA PAGA
									{
										$sTotalPago	= $tPago;
										$sTotalDivida = $tDivi;
										$sDataPag	= $data['q_datapag'];
									}

									$sWhere 		= $WHERE.' AND id="'.decrypt($data['value'],'N5KyEXAnDdwGL4eV').'"';
									
									$expSQL	 = ' UPDATE '.$TableName.' SET ';
									$expSQL	.= ' total_pago="'.$sTotalPago.'", ';
									$expSQL	.= ' total_divida="'.$sTotalDivida.'", ';
									$expSQL	.= ' data_pagamento="'.$sDataPag.'", ';
									//$expSQL	.= ' prazo_pagamento="'.$sDataPraz.'", ';
									$expSQL	.= ' id_user_edited="'.$sIDUser.'", ';
									$expSQL	.= ' data_alterado="'.$sDataAlter.'" ';
									$expSQL	.= ' WHERE '.$sWhere.'';
									$Result .= $dbTblQuotas->ExecSQL($expSQL);
			#echo($expSQL);
								}
							} break;
						}

						$dbTblQuotas->updateTOTAL();
						unset($dbTblQuotas);
						/* ################################################################## */
					}
				} break;
			default:
				{

				} break;
		}

		$Result = ($Result) ? 'true' : 'false';
	}

	echo $Result;
?>