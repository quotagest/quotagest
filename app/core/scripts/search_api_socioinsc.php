<?php
#########################################################
#														#
#	  		RECEBER DADOS .VARIOS. DE PESQUISA			#
#														#
#														#
#########################################################
	
	function SPLIT_STRING_NAME($sValue,$sSearch)
	{
		$pos = strpos($sValue,' ');
		if ($pos === false)
			return $sValue;
		
		$arr 	= explode(' ',$sValue);
		$cn		= count($arr);
		
		for($i=0; $i<$cn; $i++)
		{
			$pos = stripos($arr[$i],$sSearch);
			if ($pos >= 0)
			{
				if ($i == 0)
					return ($arr[0].' '.$arr[$cn-1]);
				else
					return ($arr[0].' '.$arr[$i]);
			}
			else
			{
				return ($arr[0].' '.$arr[$cn-1]);
			}
		}
	}
	function CREATE_RESULT_LIST(&$Table,$sTitle,$sSearch,$SQL,$aFieldsResult)
	{		
		$ROW = $Table->getRESULTS($SQL);

		$html_str = '';
		$cc = count($ROW);
		#$showmore = ($cc > 5) ? '<span>ver todos</span>' : '';
		#$cc		  = ($cc > 5) ? 5 : $cc;
		$html_str .= "<li><a href='#'>".$sTitle/*.$showmore*/."</a>";
		$html_str .= '<ul class="item">';
		if ($ROW[0][$aFieldsResult[0]] != '')
		{
			for ($i=0; $i<$cc; $i++)
			{
				#$sName = SPLIT_STRING_NAME($ROW[$i][$aFieldsResult[1]],$sSearch);
				$sName = $ROW[$i][$aFieldsResult[1]];
				$html_str .= "<li><a href='index.php?op=".$ROW[$i][$aFieldsResult[0]]."'>".$sName."<span>".$ROW[$i][$aFieldsResult[2]]."</span></a></li>";
			}
		}
		else
		{
			$html_str .= '<li><span>Sem registos</span></li>';
		}
		$html_str .= '</ul>';
		$html_str .= '</li>';
		
		return $html_str;
	}

	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	
	$tbl1 = new dbTable();
	$ValueSearch = $tbl1->StringProtect($_POST['pesquisa']);
	
	#************************************************************
	#************************** S�CIOS **************************
	$SQL = "SELECT 
					assoc_socios.id_socio,
					socio.nome,
					#ctt_codigos_postais.localidade
					
			FROM assoc_socios
			LEFT JOIN socio ON socio.id=assoc_socios.id_socio
			#LEFT JOIN ctt_codigos_postais ON 
			#	CONCAT(ctt_codigos_postais.CP4,'-',ctt_codigos_postais.CP3) = socio.codigo_postal
			WHERE
				assoc_socios.id_assoc='1' AND
				(socio.nome LIKE '%".$ValueSearch."%' OR
				 socio.bicc LIKE '%".$ValueSearch."%' OR
				 assoc_socios.codigo LIKE '%".$ValueSearch."%')
			LIMIT 0,5;
		   ";

	$html_socio = CREATE_RESULT_LIST($tbl1,'S�cios',$ValueSearch,$SQL,array('id_socio','nome','localidade'));

	#************************************************************
	#************************************************************
	
	$html = '';
	$html .= '<ul class="categoria">';
	$html .= $html_socio;
	$html .= '</ul>';
	
	echo utf8_encode($html);
	unset($tbl1);
?>