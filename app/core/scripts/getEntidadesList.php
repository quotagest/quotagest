<?php
	if(!isset($_SESSION))
	{
		session_start();
	}
	if(isset($_SESSION['id_user']))
	{
		$id_user  = $_SESSION['id_user'];
		$id_assoc = $_SESSION['id_assoc'];
	}

    /* FOLDER VARS */
    require_once($_SERVER['DOCUMENT_ROOT']."/configs/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblEntidades.class.php');
	
	$res 		= 'false';
	$Result		= '';
	##$id_user	= '2';
	##$id_assoc	= '1';

	if (isset($_POST['q']) && ($_POST['q'] !== ''))
	{
		$sLimit = (isset($_POST['limit'])) ? $_POST['limit'] : '0,5';

		$tblEntidades = new dbTblEntidades();
		$tblEntidades->set_IDUSER($id_user);
		$tblEntidades->set_IDASSOC($id_assoc);
		$ROW = $tblEntidades->SearchEntidades(array('entidades.nome','entidades.abreviatura','entidades.nif','entidades.email','entidades.telefone','entidades.telemovel','user_entidades.tipo'),array($_POST['q']),$sLimit);
		unset($tblEntidades);

		if (isset($ROW[0]))
		{
			$Result = json_encode($ROW);
			$res 	= 'true'; 
		}
	}

	echo $res.'#'.$Result;
?>