<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblQuotas.class.php');

	function utf8_urldecode($str)
	{
		$str = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($str));
		return html_entity_decode($str,null,'UTF-8');
	}
	function decodeJson($AsEncodedJSON)
	{
		$AsEncodedJSON = base64_decode($AsEncodedJSON);
		$AsEncodedJSON = utf8_urldecode($AsEncodedJSON);
		$AsEncodedJSON = utf8_encode($AsEncodedJSON);
		$AsEncodedJSON = json_decode($AsEncodedJSON,true);
		return $AsEncodedJSON;
	}

	$Result = false;
	
	if ((isset($_POST['section'])) &&
		(isset($_POST['opera'])) )
	{
		switch ($_POST['section'])
		{
			case 'quotas-socios':
				{
						$json_data = decodeJson($_POST['encodedJSON']);
						foreach($json_data as $key => $value)
						{
							$json_data[$key] = decodeJson($value);
						}
						$jsonCount = count($json_data);
						
						/* ################################################################## */
						$dbTblQuotas = new dbTblQuotas();
						$dbTblQuotas->set_IDSOCIO($_POST['ids']);
						$dbTblQuotas->set_IDMODALIDADE($_POST['idm']);
						$dbTblQuotas->set_IDUSER($id_user);
						$dbTblQuotas->set_IDASSOC($id_assoc);
						
						$ID_SOCIO = $dbTblQuotas->decryptVar($_POST['ids']);
						$ID_MODAL = $dbTblQuotas->decryptVar($_POST['idm']);
						
						$WHERE  = ' id_assoc="'.$id_assoc.'"';
						$WHERE .= ' AND id_modalidade="'.$ID_MODAL.'"';
						$WHERE .= ' AND id_socio="'.$ID_SOCIO.'"';
						
						$TableName 	= 'quotas_socios';
						$arrSQL 	= array();
						
						switch ($_POST['opera'])
						{
							case 'paycheck'	:
								{
									$Result 		= true;
									$sTotalDivida 	= '0';
									$sDataPag 		= date('Y-m-d');
									$sIDUser 		= $id_user;
									$sDataAlter 	= date('Y-m-d H:i:s');
									
									$Result 		= true;
									
									for ($i=0; $i<$jsonCount; $i++)
									{
										$id_quota 	= $json_data[$i]['id_quota'];
										$sID 		= $dbTblQuotas->decryptVar($id_quota);
										$tTotal 	= $json_data[$i]['total_total'];
										$tPago 		= $json_data[$i]['total_pago'];
										$tDivida 	= $json_data[$i]['total_divida'];
										$sDataPag 	= $json_data[$i]['data_pagamento'];
										
										$sTotalPago 	= $tTotal;
										$sTotalDivida 	= '0.00';
										
										#QUOTA POR PAGAR
										if (($sDataPag === '') || ($sDataPag === '0000-00-00') || ($sDataPag === '00-00-0000'))
										{
											$sDataPag = date('Y-m-d');
										}
										else
										{
											$sDataPag = date('Y-m-d',strtotime($sDataPag));
										}
										
										$expSQL  = ' UPDATE '.$TableName.' SET ';
										$expSQL .= ' total_pago="'.		$sTotalPago		.'", ';
										$expSQL .= ' total_divida="'.	$sTotalDivida	.'", ';
										$expSQL .= ' data_pagamento="'.	$sDataPag		.'", ';
										$expSQL .= ' id_user_edited="'.	$sIDUser		.'", ';
										$expSQL .= ' data_alterado="'.	$sDataAlter		.'" ';
										$expSQL .= ' WHERE '.$WHERE;
										$expSQL .= ' AND id="'.$sID.'"; ';
										$Result .= $dbTblQuotas->ExecSQL($expSQL);
									}
								} break;
							case 'unpaycheck':
								{
									$Result 		= true;
									$sDataPag 		= '0000-00-00';
									$sIDUser 		= $id_user;
									$sDataAlter 	= date('Y-m-d H:i:s');
									
									$Result 		= true;
									
									for ($i=0; $i<$jsonCount; $i++)
									{
										$id_quota 	= $json_data[$i]['id_quota'];
										$sID 		= $dbTblQuotas->decryptVar($id_quota);
										$tTotal 	= $json_data[$i]['total_total'];
										$tPago 		= $json_data[$i]['total_pago'];
										$tDivida 	= $json_data[$i]['total_divida'];
										$sDataPag 	= $json_data[$i]['data_pagamento'];
										
										$sTotalPago 	= $tPago;
										$sTotalDivida 	= $tTotal-$tDivida;
										
										# QUOTA JA ESTA PAGA E VAI SER - divida
										if ($tDivida === '0.00')
										{
											$sTotalPago 	= '0.00';
											$sTotalDivida 	= $tTotal;
											$sDataPag 		= '0000-00-00';
										}
										# QUOTA AINDA NAO ESTA PAGA
										else
										{
											$sTotalPago 	= $tPago;
											$sTotalDivida 	= $tDivida;
											$sDataPag 		= $sDataPag;
										}
										
										$expSQL  = ' UPDATE '.$TableName.' SET ';
										$expSQL .= ' total_pago="'.		$sTotalPago		.'", ';
										$expSQL .= ' total_divida="'.	$sTotalDivida	.'", ';
										$expSQL .= ' data_pagamento="'.	$sDataPag		.'", ';
										$expSQL .= ' id_user_edited="'.	$sIDUser		.'", ';
										$expSQL .= ' data_alterado="'.	$sDataAlter		.'" ';
										$expSQL .= ' WHERE '.$WHERE;
										$expSQL .= ' AND id="'.$sID.'"; ';
										$Result = $dbTblQuotas->ExecSQL($expSQL);
									}
								} break;
						}

						$dbTblQuotas->updateTOTAL();
						unset($dbTblQuotas);
						/* ################################################################## */
				} break;
			default:
				{

				} break;
		}
	}
	$Result = ($Result != false) ? true : false;
	echo json_encode(array('result'=>$Result));
?>