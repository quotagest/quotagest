<?php
#########################################################
#														#
#	  		RECEBER DADOS .VARIOS. DE PESQUISA			#
#														#
#														#
#########################################################
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');

	function SPLIT_STRING_NAME($sValue,$sSearch)
	{
		$pos = strpos($sValue,' ');
		if ($pos === false)
			return $sValue;
		
		$arr 	= explode(' ',$sValue);
		$cn		= count($arr);
		
		for($i=0; $i<$cn; $i++)
		{
			$pos = stripos($arr[$i],$sSearch);
			if ($pos >= 0)
			{
				if ($i == 0)
					return ($arr[0].' '.$arr[$cn-1]);
				else
					return ($arr[0].' '.$arr[$i]);
			}
			else
			{
				return ($arr[0].' '.$arr[$cn-1]);
			}
		}
	}
	function CREATE_RESULT_LIST(&$Table,$sTitle,$AUrlTarget,$sSearch,$ASQL,$aFieldsResult)
	{		
		$ROW = $Table->getRESULTS($ASQL);

		$html_str = '';
		$cc = count($ROW);
		$showmore = ($cc > 3) ? '<span>ver todos</span>' : '';
		$cc		  = ($cc > 3) ? 3 : $cc;
		$html_str .= "<li>\n<a href='#'>".$sTitle.$showmore."</a>"."\n";
		$html_str .= '<ul class="item">'."\n";

		if (isset($ROW[0][$aFieldsResult[0]]))
		{
			for ($i=0; $i<$cc; $i++)
			{
				$sID	= $Table->encryptVar($ROW[$i][$aFieldsResult[0]]);
				$sName 	= SPLIT_STRING_NAME($ROW[$i][$aFieldsResult[1]],$sSearch);
				$sLocal	= $ROW[$i][$aFieldsResult[2]];
				$html_str .= "<li><a href='".$AUrlTarget.$sID."'>".$sName."<span>".$sLocal."</span></a></li>";
				$html_str .= "\n";
			}
		}
		else
		{
			$html_str .= '<li><span>Sem registos</span></li>'."\n";
		}
		$html_str .= '</ul>'."\n";
		$html_str .= '</li>'."\n";
		
		return $html_str;
	}
	
	$tbl1 = new dbTable();
	$tbl1->setTableName('');
	$ValueSearch = $tbl1->StringProtect($_POST['pesquisa']);
	
	#************************************************************
	#************************** S�CIOS **************************
	$SQL = "SELECT 
					assoc_socios.id_socio,
					socio.nome,
					ctt_codigos_postais.localidade
					
			FROM assoc_socios
			LEFT JOIN socio ON socio.id=assoc_socios.id_socio
			LEFT JOIN ctt_codigos_postais ON 
				CONCAT(ctt_codigos_postais.CP4,'-',ctt_codigos_postais.CP3) = socio.codigo_postal
			WHERE
				assoc_socios.id_assoc='".$id_assoc."' AND
				socio.nome LIKE '%".$ValueSearch."%'
			LIMIT 0,4;
		   ";
	
	$html_socio = CREATE_RESULT_LIST($tbl1,'S�cios',SETPATH('URL','PATH_APP_UI_SOCIOS').'perfil.php?id=',$ValueSearch,$SQL,array('id_socio','nome','localidade'));
	#************************************************************
	#****************** FORNECEDORES/ENTIDADES ******************
	$SQL = "SELECT 
					user_entidades.id_entidade,
					entidades.nome,
					ctt_codigos_postais.localidade
					
			FROM user_entidades
			LEFT JOIN entidades ON entidades.id=user_entidades.id_entidade
			LEFT JOIN ctt_codigos_postais ON 
				CONCAT(ctt_codigos_postais.CP4,'-',ctt_codigos_postais.CP3) = entidades.codigo_postal
			WHERE
				user_entidades.id_user='".$id_user."' AND
				entidades.nome LIKE '%".$ValueSearch."%'
			LIMIT 0,4;
		   ";

	$html_forne = CREATE_RESULT_LIST($tbl1,'Fornecedores',SETPATH('URL','PATH_APP_UI_ENTIDADES').'perfil.php?id=',$ValueSearch,$SQL,array('id_entidade','nome','localidade'));
	#************************************************************
	#****************** OUTROS RESULTADOS ***********************
/*
	$SQL = "SELECT 
					assoc_orgaos_sociais.id_membro,
					assoc_orgaos_membros.nome AS 'nome_membro',
					assoc_orgaos_cargos.nome AS 'nome_cargo'
					
			FROM assoc_orgaos_sociais
			LEFT JOIN assoc_orgaos_membros ON assoc_orgaos_membros.id=assoc_orgaos_sociais.id_membro
			LEFT JOIN assoc_orgaos_cargos ON assoc_orgaos_cargos.id=assoc_orgaos_sociais.id_cargo
			WHERE
				assoc_orgaos_sociais.id_assoc='".$id_assoc."' AND
				assoc_orgaos_membros.nome LIKE '%".$ValueSearch."%'
			LIMIT 0,3;
		   ";
	
	$html_outro = CREATE_RESULT_LIST($tbl1,'Outros resultados',$ValueSearch,$SQL,array('id_membro','nome_membro','nome_cargo'));
*/
	#************************************************************
	#************************************************************
	
	$html = '';
	$html .= '<ul class="categoria">';
	$html .= $html_socio;
	$html .= $html_forne;
#	$html .= $html_outro;
	$html .= '</ul>';
	
	echo utf8_encode($html);
	unset($tbl1);
?>