<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');

	$Result = false;
	$ROW	= array();
	if ( ((isset($_POST['pesquisa'])) && ($_POST['pesquisa'] !== '')) &&
		 (isset($_POST['ids'])) )
	{
		/* COMPATIVEL COM VERSAO ANTIGA!! - remover no fim*/
		$ExistID = false;
		$id_socio= '';
		if ($_POST['ids'] !== '')
		{
			$pos = stripos($_POST['ids'],'&id');
			if ($pos == false)
				$pos = stripos($_POST['ids'],'?id');
			if ($pos != false)
			{
				$id_socio = substr($_POST['ids'],$pos+4);
				if ($id_socio != '')
				{
					$ExistID	= true;
				}
			}
			else
			{
				$ExistID	= true;
				$id_socio	= $_POST['ids'];
			}
		}

		$tbl1 		= new dbTable();
		$codigo 	= $tbl1->protectVar($_POST['pesquisa']);
		$id_socio 	= $tbl1->decryptVar($id_socio);
		$ROW 		= $tbl1->checkIfExist('assoc_socios',array('id','id_socio'),array('id_assoc','codigo','enabled'),array($id_assoc,$codigo,"0"));
		unset($tbl1);

		 if (isset($ROW[0]))
		 	$Result = !($id_socio == $ROW[0]['id_socio']);
	}
	echo ($Result) ? 'true' : 'false';
?>