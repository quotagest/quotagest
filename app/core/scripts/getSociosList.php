<?php
	if(!isset($_SESSION))
	{
		session_start();
	}
	if(isset($_SESSION['id_user']))
	{
		$id_user  = $_SESSION['id_user'];
		$id_assoc = $_SESSION['id_assoc'];
	}

    /* FOLDER VARS */
    require_once($_SERVER['DOCUMENT_ROOT']."/configs/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocios.class.php');
	
	$res 		= 'false';
	$Result		= '';
	##$id_user	= '2';
	##$id_assoc	= '1';

	if (isset($_POST['q']) && ($_POST['q'] !== ''))
	{
		$sLimit = (isset($_POST['limit'])) ? $_POST['limit'] : '0,5';

		$tblSocios = new dbTblSocios();
		$tblSocios->set_IDUSER($id_user);
		$tblSocios->set_IDASSOC($id_assoc);
		$ROW = $tblSocios->SearchSocios(array('socio.nome','socio.bicc','socio.nif','socio.email','socio.telefone','socio.telemovel','assoc_socios.codigo','socio_tipo.nome','socio_estado.nome'),array($_POST['q']),$sLimit);
		unset($tblSocios);

		if (isset($ROW[0]))
		{
			$Result = json_encode($ROW);
			$res 	= 'true'; 
		}
	}

	echo $res.'#'.$Result;
?>