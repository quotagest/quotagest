<?php
		require_once($_SERVER['DOCUMENT_ROOT']."/configs/dir-vars.php");
		require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblEmailsTeste.class.php');
		
		$error = true;
		if (($_POST['login'] !== '') && ($_POST['password'] !== ''))
		{
			$dbTblEmailsTeste = new dbTblEmailsTeste();
			$strLogin 	= $dbTblEmailsTeste->StringProtect($_POST['login']);
			$strPass 	= $dbTblEmailsTeste->StringProtect($_POST['password']);
			$strPass	= $dbTblEmailsTeste->encryptVar($strPass);
			
			$Result = $dbTblEmailsTeste->getUserData_Extended($strLogin,$strPass);
			$ROW 	= $Result['ROW'];
			$EXIST 	= $Result['EXIST'];
			
			if ($EXIST)
			{
				$_SESSION['user_login']	= $strLogin;
				$_SESSION['id_user']	= $ROW[0]['id_user'];
				$_SESSION['id_assoc']	= $ROW[0]['id_assoc'];
				$_SESSION['nome_assoc']	= $ROW[0]['assoc_nome'];
				$_SESSION['nome_user']	= $ROW[0]['user_nome'].' '.$ROW[0]['user_apelido'];
				$_SESSION['img_assoc'] = 'quotagest-logo.png';

				if (!file_exists(SETPATH('ROOT','PATH_APP_IMG_ASSOCS').$img_assoc))
					$_SESSION['img_assoc'] = $dbTblEmailsTeste->encryptVar($ROW[0]['id_assoc']).'.jpg';
				
				$error = false;
				$error = $dbTblEmailsTeste->updateLogin($_SESSION['id_user']);
				
				echo '<script>window.location.href="'.SETPATH('URL','PATH_APP').'index.php";</script>';
				exit();
			}
			else
			{
				echo CREATE_POP_MSG('Login e/ou Password incorrectos.',1500);
			}
			unset($dbTblEmailsTeste);
		}
		else
		{
			echo CREATE_POP_MSG('Os campos Login e Password encontram-se vazios.',1500);
		}
?>