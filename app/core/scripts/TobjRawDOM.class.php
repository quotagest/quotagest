<?php
# RAWDOM - HTML DOM PARSER - (PHP RAW)
# @author: 	Paulo Mota
# @version: 0.0.0.1
# @date: 	26-08-2013
# LINKS:
# 
#
	#error_reporting(E_ALL);
	#ini_set('display_errors', 1);

	class TobjRawDOM
	{
		private function getPosClosingTAG($sTag,$sText) 			// Return position
		{
			$Result = 0;
			
			#$sAux 	= $sText;
			$iLen 	= strlen($sTag)+1;								// '<'+$sTag
			$iCLen	= $iLen+1;										// '<'+$sTag+'>'
			$cDiv	= 0;

			$pos 	= stripos($sText,'<'.$sTag);
			$pos2 	= stripos($sText,'</'.$sTag.'>');
			if (($pos === false) || ($pos > $pos2))
			{
				#$pos	= 0;
				return $pos2;
			}
			
			$sAux = substr($sText,$pos+$iLen);
			$cDiv++;
			$Result += $pos;

			do
			{
				$pos2 = stripos($sAux,'</'.$sTag.'>');				// Procura fim do <#></#
				if ($pos2 !== false)								// ENCONTROU </#>
				{
					$ssAux = substr($sAux,0,$pos2);					// Copia String entre <#> e o </#> encontrados
					str_replace('<'.$sTag,'',$ssAux,$iCC1);			// conta nº de <#>
					$cDiv	+= $iCC1-1;								// REMOVE 1, pelo </#> encontrado
					$Result += $pos2+$iCLen;
					
					$sAux = substr($sAux,$pos2+$iCLen);
				}
				else
				{
					$cDiv = 0;
				}
			}
			while ($cDiv > 0);

			return $Result;
			#echo substr($sText,$pos,($Result+$iCLen)-($pos));
			#echo '['.$pos.'-'.$Result.']';
		}
		private function getTagName($sText)
		{
			$sTagName = '';

			$posi = strripos($sText, '<');									#procura '<' inicio da Tag
			if ($posi !== false)
			{
				$sTagName = substr($sText, $posi+1);						#Copia conjunto de caracteres <(.*?)
				$sTagName = substr($sTagName, 0, stripos($sTagName,' '));	#Copia caracteres até ao " " (espaço)
			}
			return $sTagName;
		}
		public function getTagHTML($sFind,$sText)
		{
			$sFind 		= str_ireplace(array('[',']'),'',$sFind);		#remove [] da pesquisa
			$arrInfo	= explode('=',$sFind);							#separa valores foob=bar
			
			$sAtt 		= $arrInfo[0];
			$sValue 	= $arrInfo[1];
			
			$pregReplace = '#.*(<(.*?)'.$sAtt.'=[\\"\\\']?'.$sValue.'[\\"\\\']?).*#is';
			$strRep 	 = preg_replace($pregReplace, '$1', $sText);
			$tagName  	 = $this->getTagName($strRep);							#extrai nome e '<' da TAG

			$iFindPosIni = stripos($sText,$strRep);						#encontra o inicio da TAGFind
			$sFindHTML	 = substr($sText,$iFindPosIni);					#posiciona-se no inicio da TAGFind
			$iFindPosEnd = $this->getPosClosingTAG($tagName,$sFindHTML)+((strlen($tagName)+2));
			$sFindHTML 	 = substr($sText, $iFindPosIni, $iFindPosEnd);	#HTML extraido entre o inicio e fim da TAG
			$iFindPosEnd += $iFindPosIni;								#soma o "Ini" da TAG encontrado no HTML

			$iPosInnerIni = stripos($sFindHTML, '>')+$iFindPosIni+1;	#Inicio do InnerHTML da TAG
			$iPosInnerEnd = strripos($sFindHTML, '<')+$iFindPosIni;		#Fim do InnerHTML da TAG
			
			return array('posSelfIni'=>$iFindPosIni,'posSelfEnd'=>$iFindPosEnd,
						'posInnerIni'=>$iPosInnerIni, 'posInnerEnd'=>$iPosInnerEnd,
						'text'=>$sFindHTML);
		}
		public function replaceFind($AFind,$AReplace,$AHTML,$AWhere='innertext')
		{
			$arr = $this->getTagHTML($AFind,$AHTML);
			switch ($AWhere)
			{
				case 'outertext':{
									$strAuxIni = substr($AHTML,0,$arr['posSelfIni']);
									$strAuxEnd = substr($AHTML,$arr['posSelfEnd']);
								 } break;
				case 'innertext':{
									$strAuxIni = substr($AHTML,0,$arr['posInnerIni']);
									$strAuxEnd = substr($AHTML,$arr['posInnerEnd']);
								 } break;
			}
			return $strAuxIni.$AReplace.$strAuxEnd;
		}
	}
?>