<?php
	function CREATE_POP_MSG($AMessage,$ATimeout=8000,$ARefreshAtEnd=false)
	{
		$scriptIni = "<script type='text/javascript'>CREATE_POP_MSG";
		$scriptEnd = "</script>";

		return $scriptIni."('".$AMessage."',".$ATimeout.",'".$ARefreshAtEnd."');".$scriptEnd;
	}
	function CREATE_POP_MSG2($AMessage,$ATimeout=5000)
	{
		return "<script>CREATE_POP_MSG('".$AMessage."',".$ATimeout.");</script>";
	}

	function CREATE_NOTFOUND($AMessage,$ALinkText,$ALink)
	{
		#<a class="botao guardar" href="'.$ALink.'">'.$ALinkText.'</a>
		return '<span>'.$AMessage.' </span>';
	}

	function otherDiffDate($end, $out_in_array=true)
	{
		$intervalo 	= date_diff(date_create(), date_create($end));
		$out 		= $intervalo->format("Years:%Y,Months:%M,Days:%d,Hours:%H,Minutes:%i,Seconds:%s");
		
		if (!$out_in_array)
			return $out;
		$a_out = array();
		$vars = explode(',',$out);
		array_walk($vars,
					function($val,$key) use(&$a_out)
					{
						$v=explode(':',$val);
						$a_out[$v[0]] = $v[1];
					});
		return $a_out;
	}
	
	function getLastDateEdit_WithUser($ADateTime, $AFirstName,$ALastName)
	{
		$arrValues = getLastDateEdit_Integers($ADateTime);
		$UserName = $AFirstName.' '.$ALastName;
		$UserName = (strlen($UserName) > 30) ? trim(substr($UserName, 0, 30)).'...' : $UserName;
		$htmlAux = array();
		$htmlAux[] = 'Actualizado há ';
		$htmlAux[] = '<span class="last-active">';
		$htmlAux[] = '<span>';
		$htmlAux[] = $arrValues['days'].' ';
		$htmlAux[] = $arrValues['hours'].' ';
		$htmlAux[] = $arrValues['minuts'].' ';
		$htmlAux[] = $arrValues['seconds'].' ';
		$htmlAux[] = '</span>';
		$htmlAux[] = $arrValues['DIA'].' de ';
		$htmlAux[] = $arrValues['MES'].' de ';
		$htmlAux[] = $arrValues['ANO'].' ';
		$htmlAux[] = 'por <span class="user"><b>'.$UserName.'</b></span>';
		$htmlAux[] = '</span>';
		
		return preg_replace('/\s\s+/', ' ',implode('',$htmlAux));
		#return 'Actualizado há <span class="last-active"><span>'.$days.' '.
		#$hours.' '.$minuts.' '.$seconds.
		#' </span>'.$dia.' de '.$meses[$mes-1].' de '.$ano.' </span>';
	}

	function getLastDateEdit($ADateTime)
	{
		$arrValues = getLastDateEdit_Integers($ADateTime);
		
		return 'Actualizado há <span class="last-active"><span>'.$arrValues['days'].' '.$arrValues['hours'].' '.$arrValues['minuts'].' '.$arrValues['seconds'].' </span>'.$arrValues['DIA'].' de '.$arrValues['MES'].' de '.$arrValues['ANO'].'</span>';
	}
	function getLastDateEdit_Integers($ADateTime)
	{
		$meses = array('Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');

		$arrDiff  = otherDiffDate(date('Y-m-d H:i:s',strtotime($ADateTime)),true);
		$days 	= (int)$arrDiff['Days'];
		$hours 	= (int)$arrDiff['Hours'];
		$minuts = (int)$arrDiff['Minutes'];
		$seconds= (int)$arrDiff['Seconds'];

		$dia = date('d',strtotime($ADateTime));
		$mes = date('m',strtotime($ADateTime));
		$ano = date('Y',strtotime($ADateTime));
		
		if ($days>0)
		{
			$days .= ' dias';
			if ($hours>0)
			{
				$days .= ' e';
				$hours.= ' horas em';
			}
			else
			{
				$days .= ' em';
				$hours = '';
			}
				
			$minuts = '';
			$seconds= '';
		}
		else
		if ($hours>0)
		{
			$days = '';
			$hours .= ' horas';
			if ($minuts>0)
			{
				$hours  .= ' e';
				$minuts .= ' minutos em ';
			}
			else
			{
				$hours .= ' em';
				$minuts = '';
			}

			$seconds= '';
		}
		else
		if ($minuts>0)
		{
			$days = '';
			$hours= '';
			$minuts .= ' minutos ';
			if ($seconds>0)
			{
				$minuts .= ' e';
				$seconds.= ' segundos em';
			}
			else
			{
				$minuts .= ' em';
				$seconds = '';
			}
		}
		else
		{
			$days = '';
			$hours= '';
			$minuts = '';
			$seconds .= ' segundos em';
		}
		
		return array('days'=>$days, 'hours'=>$hours, 'minuts'=>$minuts, 'seconds'=>$seconds, 'DIA'=>$dia, 'MES'=>$meses[$mes-1], 'ANO'=>$ano);
	}
	
	/* ********************************************************************** */
	/* ********************************************************************** */
	function processPage($AFileName)
	{
		$Result = '';
		if (file_exists($AFileName))
		{
			ob_start();
				include($AFileName);
				$Result = ob_get_contents();
			ob_end_clean();
		}
		return $Result;
	}
	function buildOptionsFromArray($AArray)
	{
		$Result = '';
		foreach($AArray as $key => $value) 
		{
			$Result .= "<option value='$key'>$value</option>\r\n";
		}
		return $Result;
	}
?>