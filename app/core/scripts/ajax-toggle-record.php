<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblActionHistory.class.php');
	
	
	if (($_POST['opera'] === 'append') || ($_POST['opera'] === 'update'))
	{
		$Result = false;
		if ((isset($_POST['section'])) &&
			(isset($_POST['jsonPOST'])) &&
			(isset($_POST['opera']))
			)
		{
			$extraOutput = '';
			$NEW_ID 	 = '';
			switch($_POST['section'])
			{
				case 'socio':
				case 'socios':
					{
						require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlSocios.class.php');
						$TplMdlSocios = new tplmdlSocios();
						$IDS = (isset($_POST['idrow'])) ? $_POST['idrow'] : '';
						$TplMdlSocios->setIDValues($IDS,0,$id_user,$id_assoc);
						switch ($_POST['opera'])
						{
							case 'append':
								{
									$arrRes = $TplMdlSocios->INSERT_SOCIO($_POST,$_GET);
									$Result = $arrRes['Result'];
									$NEW_ID = $arrRes['NEW_ID'];
									$extraOutput = '#/neo/?op=socios&ids='.$NEW_ID;
								} break;
							case 'update':
								{
									$Result = $TplMdlSocios->UPDATE_SOCIO($_POST,$_GET);
									$Result = ($Result['Result'].$Result['ResultAssoc']) ? true : false;
								} break;
						}
						unset($TplMdlSocios);
					} break;
				case 'sociotipo':
					{
						require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocioTipo.class.php');
						
						$dbSocioTipo 	= new dbTblSocioTipo();
						$arrJSON 		= $dbSocioTipo->decodeJSONtoArray($_POST['jsonPOST']);
						
						$idRow 	= (isset($_POST['idrow'])) ? $_POST['idrow'] : '';
						$dbSocioTipo->set_IDSOCIOTIPO($idRow);
						$dbSocioTipo->set_IDUSER($id_user);
						$dbSocioTipo->set_IDASSOC($id_assoc);
						switch ($_POST['opera'])
						{
							case 'append':
								{
									$arrRes = $dbSocioTipo->INSERT($arrJSON['_POST']);
									$Result = $arrRes['Result'];
									$NEW_ID = $arrRes['NEW_ID'];
								} break;
							case 'update':
								{
									$Result = $dbSocioTipo->UPDATE($arrJSON['_POST']);
								} break;
						}
						unset($dbSocioTipo);
					} break;
				case 'socioestado':
					{
						require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocioEstado.class.php');
						
						$dbSocioEstado  = new dbTblSocioEstado();
						$arrJSON 		= $dbSocioEstado->decodeJSONtoArray($_POST['jsonPOST']);
						
						$idRow 	= (isset($_POST['idrow'])) ? $_POST['idrow'] : '';
						$dbSocioEstado->set_IDSOCIOESTADO($idRow);
						$dbSocioEstado->set_IDUSER($id_user);
						$dbSocioEstado->set_IDASSOC($id_assoc);
						switch ($_POST['opera'])
						{
							case 'append':
								{
									$arrRes = $dbSocioEstado->INSERT($arrJSON['_POST']);
									$Result = $arrRes['Result'];
									$NEW_ID = $arrRes['NEW_ID'];
								} break;
							case 'update':
								{
									$Result = $dbSocioEstado->UPDATE($arrJSON['_POST']);
								} break;
						}
						unset($dbSocioEstado);
					} break;
				case 'familiares':
					{
						require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSociosFamiliar.class.php');
						$dbSociosFamiliar = new dbTblSociosFamiliar();
						$IDS  = (isset($_POST['ids'])) ? $_POST['ids'] : '';
						$IDSF = (isset($_POST['idsf'])) ? $_POST['idsf'] : '';
						$dbSociosFamiliar->set_IDSOCIO($IDS);
						$dbSociosFamiliar->set_IDFAMILIAR($IDSF);
						$dbSociosFamiliar->set_IDUSER($id_user);
						$dbSociosFamiliar->set_IDASSOC($id_assoc);
						switch ($_POST['opera'])
						{
							case 'append':
								{
									$arrRes = $dbSociosFamiliar->addFamiliar($_POST);
									$Result = $arrRes['Result'];
									$NEW_ID = $dbSociosFamiliar->encryptVar($arrRes['NEW_ID']);
									$extraOutput = '#/neo/?op=socios&ids='.$IDS.'&idsf='.$NEW_ID;
								} break;
							case 'update':
								{
									$Result = $dbSociosFamiliar->updateFamiliar($_POST);
								} break;
						}
						unset($dbSociosFamiliar);
					} break;
				case 'entidade':
				case 'entidades':
					{
						require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlEntidades.class.php');
						$TplMdlEntidades = new tplmdlEntidades();
						$IDE = (isset($_POST['idrow'])) ? $_POST['idrow'] : '';
						$TplMdlEntidades->setIDValues($IDE,$id_user,$id_assoc);
						switch ($_POST['opera'])
						{
							case 'append':
								{
									$arrRes = $TplMdlEntidades->INSERT_ENTIDADE($_POST,$_GET);
									$Result = $arrRes['Result'];
									$NEW_ID = $arrRes['NEW_ID'];
									$extraOutput = '#/neo/?op=entidades&ide='.$NEW_ID;
								} break;
							case 'update':
								{
									$Result = $TplMdlEntidades->UPDATE_ENTIDADE($_POST,$_GET);
								} break;
						}
						unset($TplMdlEntidades);
					} break;
				case 'pagforma':
					{
						require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblPagForma.class.php');
						
						$dbPagForma 	= new dbTblPagForma();
						$arrJSON 		= $dbPagForma->decodeJSONtoArray($_POST['jsonPOST']);
						
						$idRow 	= (isset($_POST['idrow'])) ? $_POST['idrow'] : '';
						$dbPagForma->set_IDPAGFORMA($idRow);
						$dbPagForma->set_IDUSER($id_user);
						$dbPagForma->set_IDASSOC($id_assoc);
						switch ($_POST['opera'])
						{
							case 'append':
								{
									$arrRes = $dbPagForma->INSERT($arrJSON['_POST']);
									$Result = $arrRes['Result'];
									$NEW_ID = $arrRes['NEW_ID'];
								} break;
							case 'update':
								{
									$Result = $dbPagForma->UPDATE($arrJSON['_POST']);
								} break;
						}
						unset($dbPagForma);
					} break;
				case 'pagopcao':
					{
						require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblPagOpcao.class.php');
						
						$dbPagOpcao 	= new dbTblPagOpcao();
						$arrJSON 		= $dbPagOpcao->decodeJSONtoArray($_POST['jsonPOST']);
						
						$idRow 	= (isset($_POST['idrow'])) ? $_POST['idrow'] : '';
						$dbPagOpcao->set_IDPAGOPCAO($idRow);
						$dbPagOpcao->set_IDUSER($id_user);
						$dbPagOpcao->set_IDASSOC($id_assoc);
						switch ($_POST['opera'])
						{
							case 'append':
								{
									$arrRes = $dbPagOpcao->INSERT($arrJSON['_POST']);
									$Result = $arrRes['Result'];
									$NEW_ID = $arrRes['NEW_ID'];
								} break;
							case 'update':
								{
									$Result = $dbPagOpcao->UPDATE($arrJSON['_POST']);
								} break;
						}
						unset($dbPagOpcao);
					} break;
				case 'quotas-teste':
					{
						require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblQuotas.class.php');
						
						$dbQuotas = new dbTblQuotas();
						$arrJSON = $dbQuotas->decodeJSONtoArray($_POST['jsonPOST']);
						
						$IDS = (isset($_POST['ids'])) ? $_POST['ids'] : '';
						$IDM = (isset($_POST['idm'])) ? $_POST['idm'] : '';
						$IDS = 'nR2EkjXQ2SocDCS0M21ZOvdwwCsJOM0ZzADJ3xkX8II';
						$IDM = '';
						$idRow = (isset($_POST['idrow'])) ? $_POST['idrow'] : '';
						$dbQuotas->set_IDQUOTA($idRow);
						$dbQuotas->set_IDSOCIO($IDS);
						$dbQuotas->set_IDMODALIDADE($IDM);
						$dbQuotas->set_IDUSER($id_user);
						$dbQuotas->set_IDASSOC($id_assoc);
						switch ($_POST['opera'])
						{
							case 'append':
								{
									$arrRes = $dbQuotas->INSERT_QUOTAS_PERSONAL($arrJSON);
									$Result = $arrRes['Result'];
									$NEW_ID = $arrRes['NEW_ID'];
								} break;
							case 'update':
								{
									$Result = $dbQuotas->UPDATE($arrJSON['_POST']);
								} break;
						}
						
						unset($dbQuotas);
					} break;
			}
		}
		$ResultJSON = array('result'=>$Result, 'idinserted'=>$NEW_ID);
		echo json_encode($ResultJSON);
	}
	else
	if (($_POST['opera'] === 'bunk') || ($_POST['opera'] === 'debunk'))
	{
		include(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'ajax-toggle-unset-record.php');
	}
	else
	{
		$ResultJSON = array('result'=>false, 'idinserted'=>'');
		echo json_encode($ResultJSON);
	}
?>