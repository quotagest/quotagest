<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblQuotas.class.php');
	
	function replaceSQL($ASQL,$TableName='',$FieldName='',$Where)
	{
		$ASQL = str_ireplace('{tablename}', $TableName, $ASQL);
		$ASQL = str_ireplace('{field-value}', $FieldName, $ASQL);
		$ASQL = str_ireplace('{where}', $Where, $ASQL);

		return $ASQL;
	}

	$Result = 'false';
	if ((isset($_POST['place'])) &&
		(isset($_POST['array'])) &&
		(isset($_POST['opera']))
		)
	{
		$arrIDs = array();
		$arrIDVALUES = explode(';', $_POST['array']);
		$ccI = count($arrIDVALUES);
		for ($i=0; $i<$ccI; $i++)
		{
			$arrIDs[$i] = decrypt($arrIDVALUES[$i],'N5KyEXAnDdwGL4eV');
		}
		$arrIDs = implode('","', $arrIDs);

		switch ($_POST['opera'])
		{
			case 'bunk':{ #ESCONDE REGISTOS
							$expSQL		= 'UPDATE {tablename} SET {field-value} WHERE {where}';
							$FieldName 	= 'enabled="1"';
						} break;
			case 'debunk':{ #MOSTRA REGISTOS
							$expSQL		= 'UPDATE {tablename} SET {field-value} WHERE {where}';
							$FieldName 	= 'enabled="0"';
						} break;
			case 'unset':{ #APAGA REGISTOS
							$expSQL		= 'DELETE FROM {tablename} WHERE {where}';
							$FieldName 	= '';
						} break;
			default: 	{
							$expSQL		= 'UPDATE {tablename} SET {field-value} WHERE {where}';
							$FieldName 	= 'enabled="0"';
						} break;
		}
		switch ($_POST['place'])
		{
			case 'quotas-listagem':
				{
					if ((isset($_POST['ids'])) &&
						(isset($_POST['idm']))
					)
					{
						$ID_SOCIO = decrypt($_POST['ids'],'N5KyEXAnDdwGL4eV');
						$ID_MODAL = decrypt($_POST['idm'],'N5KyEXAnDdwGL4eV');

						$TableName = 'quotas_socios';
						$WHERE  = ' id_assoc="'.$id_assoc.'"';
						$WHERE .= ' AND id_modalidade="'.$ID_MODAL.'"';
						$WHERE .= ' AND id_socio="'.$ID_SOCIO.'"';

						$expSQL = replaceSQL($expSQL,$TableName,$FieldName,$WHERE);

						$extSQL  = $expSQL.' AND id IN ("'.$arrIDs.'")';

						$dbTblQuotas = new dbTblQuotas();
						$dbTblQuotas->set_IDSOCIO($_POST['ids']);
						$dbTblQuotas->set_IDMODALIDADE($_POST['idm']);
						$dbTblQuotas->set_IDUSER($id_user);
						$dbTblQuotas->set_IDASSOC($id_assoc);

						$Result = $dbTblQuotas->ExecSQL($extSQL);
						$dbTblQuotas->updateTOTAL($_POST['idm'],$_POST['ids'],$id_user,$id_assoc);
						unset($dbTblQuotas);
					}
				} break;
			case 'socios-listagem':
				{
					$TableName = 'socio';
					$WHERE  = ' id_assoc="'.$id_assoc.'"';

					$expSQL2 = replaceSQL($expSQL,'assoc_socios',$FieldName,$WHERE);
					$expSQL  = replaceSQL($expSQL,$TableName,$FieldName,'');

					$extSQL  = $expSQL.' id IN ("'.$arrIDs.'")';
					$extSQL2 = $expSQL2.' AND id_socio IN ("'.$arrIDs.'")';

					$tbl1 = new dbTable();
					$Result  = $tbl1->ExecSQL($extSQL);
					$Result .= $tbl1->ExecSQL($extSQL2);
					unset($tbl1);
				} break;
			case 'entidades-listagem':
				{
					$TableName = 'entidades';
					$WHERE  = ' id_user="'.$id_user.'"';

					$expSQL2= replaceSQL($expSQL,'user_entidades',$FieldName,$WHERE);
					$expSQL = replaceSQL($expSQL,$TableName,$FieldName,'');

					$extSQL  = $expSQL.' id IN ("'.$arrIDs.'")';
					$extSQL2 = $expSQL2.' AND id_entidade IN ("'.$arrIDs.'")';

					$tbl1 = new dbTable();
					$Result  = $tbl1->ExecSQL($extSQL);
					$Result .= $tbl1->ExecSQL($extSQL2);
					unset($tbl1);
				} break;
			default:
				{

				} break;
		}
		$Result = true;


/*
		for ($i=0; $i<$ccI; $i++)
		{
			$Result .= $dbTblQuotas->UPDATE($arrIDVALUES[$i],$_POST['idm'],$_POST['ids'],$id_user,$id_assoc);
		}
		$tbl1 = new dbTable();
		unset($tbl1);
*/
		$Result = ($Result) ? 'true' : 'false';
	}

	echo $Result;
?>