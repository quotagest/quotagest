<?php
	#require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblActionHistory.class.php');
	
	function replaceSQL($ASQL,$TableName='',$FieldName='',$Where)
	{
		$ASQL = str_ireplace('{tablename}', $TableName, $ASQL);
		$ASQL = str_ireplace('{field-value}', $FieldName, $ASQL);
		$ASQL = str_ireplace('{where}', $Where, $ASQL);

		return $ASQL;
	}
	
	$Result = false;
	$ID_HISTORY = '';
	
	if (isset($_POST['idhistory']) && ($_POST['idhistory'] !== ''))
	{
		$dbTblHistory = new dbTblActionHistory();
		$dbTblHistory->set_IDHISTORY($_POST['idhistory']);
		$dbTblHistory->set_IDUSER($id_user);
		$dbTblHistory->set_IDASSOC($id_assoc);
		$arrResult 	= $dbTblHistory->getData();
		$ROW 		= $arrResult['ROW'];

		$arrSQL 	= explode('#',$ROW[0]['script_recover']);
		if ($arrSQL[0] != '')
			$Result = $dbTblHistory->ExecSQL($arrSQL[0]);
		if ($arrSQL[1] != '')
			$Result .= $dbTblHistory->ExecSQL($arrSQL[1]);

		$Result 	.= $dbTblHistory->DELETE();
		unset($dbTblHistory);
	}
	else
	{
		if ((isset($_POST['section'])) &&
			(isset($_POST['jsonPOST'])) &&
			(isset($_POST['opera']))
			)
		{
			$dbTblHistory = new dbTblActionHistory();
			$dbTblHistory->set_IDHISTORY(0);
			$dbTblHistory->set_IDUSER($id_user);
			$dbTblHistory->set_IDASSOC($id_assoc);
			$jsonData = $dbTblHistory->decodeJSONtoArray($_POST['jsonPOST']);
			
			$objects 	 = $jsonData['objects'];
			$extraparams = $jsonData['extraparams'];
			
			$arrTitles = array();
			$arrIDs = array();
			
			$objects = json_decode($objects,true);
			
			if ($objects)
			{
				foreach($objects as $key => $value)
				{
					#$sindex = $value['index'];
					$svalue = $value['value'];
					$stitle = $value['title'];
					$arrIDs[] = $dbTblHistory->decryptVar($svalue);
					$arrTitles[] = $stitle;
				}
			}
			$arrIDs = implode('","', $arrIDs);

			switch ($_POST['opera'])
			{
				case 'bunk':{ #ESCONDE REGISTOS
								$expSQL		= 'UPDATE {tablename} SET {field-value} WHERE {where}';
								$FieldName 	= 'enabled="1"';
								$sFNUndo 	= 'enabled="0"';
								$iOP		= 2;
								$sTitle		= 'Apagar';
								$sDescript	= 'Apagou '.count($objects).' registos de ';
							} break;
				case 'debunk':{ #MOSTRA REGISTOS
								$expSQL		= 'UPDATE {tablename} SET {field-value} WHERE {where}';
								$FieldName 	= 'enabled="0"';
								$sFNUndo 	= 'enabled="1"';
								$iOP		= 2;
								$sTitle		= 'Recuperar';
								$sDescript	= 'Recuperou '.count($objects).' registos de ';
							} break;
				/*
				case 'unset':{ #APAGA REGISTOS
								$expSQL		= 'DELETE FROM {tablename} WHERE {where}';
								$FieldName 	= '';
								$sFNUndo 	= '';
								$iOP		= 3;
								$sTitle		= 'Apagar defenitivamente';
								$sDescript	= 'Apagou "DELETE" '.count($objects).' registos de ';
							} break;
				*/
				default: 	{
								$expSQL		= 'UPDATE {tablename} SET {field-value} WHERE {where}';
								$FieldName 	= 'enabled="0"';
								$sFNUndo 	= 'enabled="1"';
								$iOP		= 2;
								$sTitle		= 'Apagar';
								$sDescript	= 'Apagou '.count($objects).' registos de ';
							} break;
			}
			
			switch($_POST['section'])
			{
				case 'socio':
				case 'socios':
					{
						$TableName = 'socio';
						$WHERE     = ' id_assoc="'.$id_assoc.'"';
	
						$extSQLTable1 = replaceSQL($expSQL,$TableName,$FieldName,'');
						$extSQLTable2 = replaceSQL($expSQL,'assoc_socios',$FieldName,$WHERE);
						$extSQLTable1 .= ' id IN ("'.$arrIDs.'")';
						$extSQLTable2 .= ' AND id_socio IN ("'.$arrIDs.'")';
						
						$extUndo1 = replaceSQL($expSQL,$TableName,$sFNUndo,'');
						$extUndo2 = replaceSQL($expSQL,'assoc_socios',$sFNUndo,$WHERE);
						$extUndo1 .= ' id IN ("'.$arrIDs.'")';
						$extUndo2 .= ' AND id_socio IN ("'.$arrIDs.'")';
						
						$sSection   = '1';
						$sDescript .= 'Sócios';
					} break;
				case 'entidade':
				case 'entidades':
					{
						$TableName = 'entidades';
						$WHERE     = ' id_user="'.$id_user.'"';
	
						$extSQLTable1 = replaceSQL($expSQL,$TableName,$FieldName,'');
						$extSQLTable2 = replaceSQL($expSQL,'user_entidades',$FieldName,$WHERE);
						$extSQLTable1 .= ' id IN ("'.$arrIDs.'")';
						$extSQLTable2 .= ' AND id_entidade IN ("'.$arrIDs.'")';
						
						$extUndo1 = replaceSQL($expSQL,$TableName,$sFNUndo,'');
						$extUndo2 = replaceSQL($expSQL,'user_entidades',$sFNUndo,$WHERE);
						$extUndo1 .= ' id IN ("'.$arrIDs.'")';
						$extUndo2 .= ' AND id_entidade IN ("'.$arrIDs.'")';
						
						$sSection   = '2';
						$sDescript .= 'Entidades';
					} break;
				case 'familiares':
					{
						$TableName = 'socio_familia';
						$WHERE     = ' id_assoc="'.$id_assoc.'"';
	
						$extSQLTable1 = replaceSQL($expSQL,$TableName,$FieldName,$WHERE);
						$extSQLTable2 = '';
						$extSQLTable1 .= ' AND id IN ("'.$arrIDs.'")';
						$extSQLTable2 = '';
	
						$extUndo1 = replaceSQL($expSQL,$TableName,$sFNUndo,$WHERE);
						$extUndo2 = '';
						$extUndo1 .= ' AND id IN ("'.$arrIDs.'")';
						$extUndo2 = '';
						
						$sSection   = '3';
						$sDescript .= 'Sócios Familiares';
					} break;
				case 'pagforma':
					{
						$TableName = 'pag_forma';
						$WHERE     = ' id_assoc="'.$id_assoc.'"';
	
						$extSQLTable1 = replaceSQL($expSQL,$TableName,$FieldName,'');
						$extSQLTable2 = '';
						$extSQLTable1 .= ' id IN ("'.$arrIDs.'")';
						$extSQLTable2 = '';
						
						$extUndo1 = replaceSQL($expSQL,$TableName,$sFNUndo,'');
						$extUndo2 = '';
						$extUndo1 .= ' id IN ("'.$arrIDs.'")';
						$extUndo2 = '';
						
						$sSection   = '4';
						$sDescript .= 'Formas de Pagamento';
					} break;
				case 'pagopcao':
					{
						$TableName = 'pag_opcao';
						$WHERE     = ' id_assoc="'.$id_assoc.'"';
	
						$extSQLTable1 = replaceSQL($expSQL,$TableName,$FieldName,'');
						$extSQLTable2 = '';
						$extSQLTable1 .= ' id IN ("'.$arrIDs.'")';
						$extSQLTable2 = '';
						
						$extUndo1 = replaceSQL($expSQL,$TableName,$sFNUndo,'');
						$extUndo2 = '';
						$extUndo1 .= ' id IN ("'.$arrIDs.'")';
						$extUndo2 = '';
						
						$sSection   = '5';
						$sDescript .= 'Opções de Pagamento';
					} break;
				case 'socioestado':
					{
						$TableName = 'socio_estado';
						$WHERE     = ' id_assoc="'.$id_assoc.'"';
	
						$extSQLTable1 = replaceSQL($expSQL,$TableName,$FieldName,'');
						$extSQLTable2 = '';
						$extSQLTable1 .= ' id IN ("'.$arrIDs.'")';
						$extSQLTable2 = '';
						
						$extUndo1 = replaceSQL($expSQL,$TableName,$sFNUndo,'');
						$extUndo2 = '';
						$extUndo1 .= ' id IN ("'.$arrIDs.'")';
						$extUndo2 = '';
						
						$sSection   = '6';
						$sDescript .= 'Estado de Sócio';
					} break;
				case 'sociotipo':
					{
						$TableName = 'socio_tipo';
						$WHERE     = ' id_assoc="'.$id_assoc.'"';
	
						$extSQLTable1 = replaceSQL($expSQL,$TableName,$FieldName,'');
						$extSQLTable2 = '';
						$extSQLTable1 .= ' id IN ("'.$arrIDs.'")';
						$extSQLTable2 = '';
						
						$extUndo1 = replaceSQL($expSQL,$TableName,$sFNUndo,'');
						$extUndo2 = '';
						$extUndo1 .= ' id IN ("'.$arrIDs.'")';
						$extUndo2 = '';
						
						$sSection   = '7';
						$sDescript .= 'Tipo de Sócio';
					} break;
				case 'quotas-socio':
					{
						$extraparams = json_decode($extraparams,true);
						
						$TableName = 'quotas_socios';
						$WHERE     = ' id_assoc="'.$id_assoc.'"';
						$WHERE    .= ' AND id_socio="'.$dbTblHistory->decryptVar($extraparams['ids']).'"';
						$WHERE    .= ' AND id_modalidade="'.$dbTblHistory->decryptVar($extraparams['idm']).'"';
						
						$extSQLTable1 = replaceSQL($expSQL,$TableName,$FieldName,'');
						$extSQLTable2 = '';
						$extSQLTable1 .= ' id IN ("'.$arrIDs.'")';
						$extSQLTable2 = '';
						
						$extUndo1 = replaceSQL($expSQL,$TableName,$sFNUndo,'');
						$extUndo2 = '';
						$extUndo1 .= ' id IN ("'.$arrIDs.'")';
						$extUndo2 = '';
						
						$sSection   = '8';
						$sDescript .= 'Tipo de Quotas';
					} break;
			}
			
			if ($extSQLTable1 != '')
				$Result  = $dbTblHistory->ExecSQL($extSQLTable1);
			if ($extSQLTable2 != '')
				$Result .= $dbTblHistory->ExecSQL($extSQLTable2);
				
			switch($_POST['section'])
			{
				case 'quotas-socio':
					{
						//$extraparams = json_decode($extraparams,true);
						
						require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblQuotas.class.php');
						$dbQuotas = new dbTblQuotas();
						$dbQuotas->set_IDMODALIDADE($extraparams['idm']);
						$dbQuotas->set_IDSOCIO($extraparams['ids']);
						$dbQuotas->set_IDUSER($id_user);
						$dbQuotas->set_IDASSOC($id_assoc);
						$dbQuotas->updateTOTAL();
						unset($dbQuotas);
					} break;
			}
			
			$A_POST = array();
			$A_POST['section'] 			= $sSection;
			$A_POST['operation'] 		= $iOP;
			$A_POST['titulo'] 			= $sTitle;
			$A_POST['descricao'] 		= $sDescript.'#'.implode(';',$arrTitles);
			$A_POST['script_done'] 		= $extSQLTable1.'#'.$extSQLTable2;
			$A_POST['script_recover'] 	= $extUndo1.'#'.$extUndo2;
			$ResultID 	= $dbTblHistory->INSERT($A_POST);
			$ID_HISTORY = $ResultID['NEW_ID'];

			unset($dbTblHistory);
		}
		#echo ($Result) ? 'true'.$ID_HISTORY : 'false';
	}
	$ResultJSON = array('result'=>(($Result) ? true : false), 'idhistory'=>$ID_HISTORY);
	echo json_encode($ResultJSON);

?>