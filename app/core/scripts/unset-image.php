<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');

	$Result = false;

	if ((isset($_POST['section'])) &&
		(isset($_POST['key'])) &&
		(isset($_POST['action'])))
	{
		$sPathFolder = SETPATH('ROOT','PATH_APP_IMG');
		$sKey		 = decrypt($_POST['key'],'N5KyEXAnDdwGL4eV');
		$arrKey		 = explode('*', $sKey);
		$sFileName 	 = $arrKey[1].'.jpg';

		switch ($_POST['section'])
		{
			case 'associacao':	{
									$sPathFolder .= 'assocs/';
								}
								break;
			case 'socio':		{
									$sPathFolder .= 'socios/';
								}
								break;
			case 'user':		{
									$sPathFolder .= 'users/';
								}
								break;
			default:
				{

				}
				break;
		}

		if ($_POST['action'] === 'unset')
		{
			if (file_exists($sPathFolder.$sFileName))
			{
				$Result = rename($sPathFolder.$sFileName,$sPathFolder.'unset_'.$sFileName);
			}
		}
		else
		if ($_POST['action'] === 'recover')
		{
			if (file_exists($sPathFolder.'unset_'.$sFileName))
			{
				$Result = rename($sPathFolder.'unset_'.$sFileName,$sPathFolder.$sFileName);
			}
		}
	}

	echo ($Result) ? 'true' : 'false';
?>