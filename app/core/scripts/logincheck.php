<?php
		require_once($_SERVER['DOCUMENT_ROOT']."/configs/dir-vars.php");
		require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
		require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
		
		$error = true;
		if (($_POST['login'] !== '') && ($_POST['password'] !== ''))
		{
			// *** VERIFICA TAMANHO DA PASSWORD ***
			$check 		= false;
			$lenLogin 	= strlen(utf8_decode($_POST['login']));
			$lenPass 	= strlen(utf8_decode($_POST['password']));
			
			if (($lenPass <= 20) && ($lenPass >= 6) && ($lenLogin <= 20) && ($lenLogin >= 6))
			{
				$check = true;
			}
			// **********************************************************
			// **********************************************************
			if ($check)
			{
				$tbl1 	= new dbTable();
				$tbl1->setTableName('');
				//$tbl1->SetFieldsName($arr_TABELA_ASSOC_UTILIZADORES);
				//$tbl1->SetFieldsValue(array('NULL',$_POST['login'],$_POST['password']));
				//$tbl1->SetFieldsType($arr_TYPES_ASSOC_UTILIZADORES);
				
				$strLogin 	= $tbl1->StringProtect($_POST['login']);
				$strPass 	= $tbl1->StringProtect($_POST['password']);
				$strPass	= encrypt($strPass,'N5KyEXAnDdwGL4eV');
				
				$ROW = $tbl1->SQL_SELECT('assoc_utilizadores',array('id','nome'),array('login','password'),array($strLogin,$strPass));
				if (isset($ROW[0]))
				{
					$ID_USER 	= $ROW[0]['id'];
					$NOME		= $ROW[0]['nome'];
					$LOGIN 		= $strLogin;
					
					$ROW = $tbl1->SQL_SELECT('associacao',array('id','nome'),array('id_user'),array($ID_USER));
					if (isset($ROW[0]))
					{
						$ID_ASSOC 	= $ROW[0]['id'];
						$ASSOC 		= $ROW[0]['nome'];

						/* ***** INICIA vars $_SESSION ***** */
						$_SESSION['user_login']	= $LOGIN;
						$_SESSION['id_user']	= $ID_USER;
						$_SESSION['id_assoc']	= $ID_ASSOC;
						$_SESSION['nome_assoc']	= $ASSOC;
						$_SESSION['nome_user']	= $NOME;
						$_SESSION['img_assoc'] = 'quotagest-logo.png';

						if (file_exists(SETPATH('ROOT','PATH_APP_IMG_ASSOCS').$img_assoc))
							$_SESSION['img_assoc'] = encrypt($ID_ASSOC,'N5KyEXAnDdwGL4eV').'.jpg';

						$error = false;
					}
				}
				unset($tbl1);
			}
			else
			{
				echo 'Login ou Password, devem ser maiores do que 6 caracteres!';
			}	
		}
?>