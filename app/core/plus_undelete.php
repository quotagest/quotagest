<?php
	/* ************************************************************************************************************ */
	/* 																												*/
	/* 	AC��O		: DELETE																						*/
	/* 	TABELA		: ALL															 								*/
	/* 																												*/
	/* 	CAMPOS_IN	:																								*/
	/* 		- $_SESSION['id_user']																					*/
	/* 		- $_SESSION['id_assoc']																					*/
	/* 		- $_POST['encrypt']																						*/
	/* 																												*/
	/* 	CAMPOS_OUT	:																								*/
	/* 		- $Result																								*/
	/* 																												*/
	/* ************************************************************************************************************ */
	
	if(!isset($_SESSION))
	{
		session_start();
	}
	if(isset($_SESSION['id_user']))
	{
		$id_user  = $_SESSION['id_user'];
		$id_assoc = $_SESSION['id_assoc'];
	}

	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	
	/* FOLDER VARS */
	require_once($_SERVER['DOCUMENT_ROOT']."/configs/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE').'db_functions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'db_tables.php');
	
	##$id_assoc 		= '1';
	##$id_user  		= '2';
	$Result			= false;
	
	if (isset($_POST['encrypt']) && ($_POST['encrypt'] !== ''))
	{
		$aEncrypt 	= explode('#',$_POST['encrypt']);
		$ID_MASTER 	= decrypt($aEncrypt[0],'N5KyEXAnDdwGL4eV');	
		$TABLE		= $aEncrypt[1];
		
		switch($TABLE)
		{
			case 'pagforma'		: $TABLE = 'pag_forma'; 	break;
			case 'pagopcao'		: $TABLE = 'pag_opcao'; 	break;
			case 'socioestado'	: $TABLE = 'socio_estado'; 	break;
			case 'sociotipo'	: $TABLE = 'socio_tipo'; 	break;
			case 'modalidade-new': $TABLE = 'modalidade'; 	break;
			case 'modalidade'	: $TABLE = 'modalidade'; 	break;
			case 'modsocnivel'	: $TABLE = 'mod_socio_nivel'; 	break;
			case 'modturma'		: $TABLE = 'mod_turma'; 		break;
		}
		
		$id_fieldName  = '';
		$id_fieldValue = '';
		
		$tbl1 = new dbTable();
		/* VERIFICAR SE CAMPO "id_assoc" ou "id_user" existem numa tabela */
		$SQL = 'DESCRIBE '.$TABLE;
		$ROW = $tbl1->getRESULTS($SQL);
		if (isset($ROW[0]))
		{
			if (ExistValue_inArray('id_assoc',$ROW))
			{
					$id_fieldName  = 'id_assoc';
					$id_fieldValue = $id_assoc;
			}
			else
			if (ExistValue_inArray('id_user',$ROW))
			{
					$id_fieldName  = 'id_user';
					$id_fieldValue = $id_user;
			}
		}
		/* ************************************************************** */
		
		$ROW = $tbl1->SQL_SELECT($TABLE,array('id'),array('id',$id_fieldName),array($ID_MASTER,$id_fieldValue));		
		if (isset($ROW[0]))
		{
			/*
			$tbl1->setTableName($TABLE);
		
			$SQL = $tbl1->SQL_DELETE(array('id',$id_fieldName),array($ID_MASTER,$id_fieldValue));
			$res = $tbl1->ExecSQL($SQL,'DELETE');
			
			$Result = $res;
			*/

			$tbl1->setTableName($TABLE);
			$tbl1->SetFieldsName(array('enabled'));
			$tbl1->SetFieldsValue(array('0'));
			$tbl1->SetFieldsType(array('INTEGER'));

			$SQL 	= $tbl1->BuildSQL('UPDATE',array('id',$id_fieldName),array($ID_MASTER,$id_fieldValue));
			$Result = $tbl1->ExecSQL($SQL);
		}
		unset($tbl1);
	}
	$Result = (!$Result) ? 'false' : 'true';
	echo $Result;
?>