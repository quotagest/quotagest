<?php
	/* FOLDER VARS */
	require_once($_SERVER['DOCUMENT_ROOT']."/configs/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjUploadFile.class.php');

	function nameImage($imgExtension)
	{
		return time() . substr(md5(microtime()), 0, rand(5, 12)) . $imgExtension;
	}

	$objFileName = new TobjUploadFile();
	$objFileName->setFileName(SETPATH('ROOT','PATH_APP_TMP').nameImage('.jpg'));
	$objFileName->setTempFileName('');
	$objFileName->setMaxSize(524288);
	$objFileName->setFileType('image');
	$objFileName->setInputFileName('upload-image');
	$objFileName->setMaxWidth('160');
	$objFileName->setMaxHeigth('125');
	$objFileName->setIsBase64((isset($_GET['base64'])) ? true : false);
	$objFileName->setIsBinary((isset($_GET['up'])) ? true : false);
	$objFileName->setEchoFileName(true);
	echo $objFileName->SaveFile();
	unset($objFileName);
?>