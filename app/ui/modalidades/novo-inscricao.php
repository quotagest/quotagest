<?php 
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlSocios.class.php');
	############################################################################
	$Msg 		= '';
	$GET_IDM 	= (isset($_GET['idm'])) ? $_GET['idm'] : '';
	$GET_IDS 	= (isset($_GET['ids'])) ? $_GET['ids'] : '';

	$objTplMdlSocios = new tplmdlSocios();
	$objTplMdlSocios->forkTemplate_SocioNovoInscricao($GET_IDS,$GET_IDM,$id_user,$id_assoc);
	############################## GUARDAR REGISTO ##############################
	if ((isset($_POST['lista-modalidade'])) && (isset($_POST['CodSocio'])))
	{
		$objTplMdlSocios->tblSocios->set_IDMODALIDADE($_POST['lista-modalidade']);
		$objTplMdlSocios->tblSocios->set_IDSOCIO($_POST['CodSocio']);
		$Result = $objTplMdlSocios->tblSocios->addModalidadeInscricao($_POST);

		if ($Result)
		{
			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'/ui/socios/perfil.php?id='.$_POST['CodSocio']);
		}
		else
		{
			if ($Result === 'EXIST')
				$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
			else
			if (!$Result)
				$Msg = CREATE_POP_MSG('O Sócio já foi inscrito nesta Actividade.');
		}
	}
	#############################################################################
	$HTML 		 = $objTplMdlSocios->getSHTML();
	$arrLeftMenu = $objTplMdlSocios->getLeftMenuItem();
	unset($objTplMdlSocios);

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $Msg;
	echo $HTML;
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
?>