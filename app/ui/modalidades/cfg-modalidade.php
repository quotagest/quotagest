<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
?>
<div id="corpo">
<section id="configuracoes">
	<h2 class="titulo">Configurar Modalidades</h2>
	<p class="ajuda">Aqui pode ver e editar as modalidades, níveis e turmas.</p>
<?php
	echo '<!--';
		/* ************************************************************************************ */
		require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
		require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
		
		function AddLI($tblName, $aFieldsGet, $aFieldsWhereName, $aFieldsWhereValue, $sFieldsName, $aFieldsType)
		{				
			$tbl1 = new dbTable();
			#$sDataFields 	= implode(';',$sFieldsName);
			#$sFields 		= implode(',',$aFieldsGet);
			#$sWhere  		= $tbl1->SQLWHERE_FieldsBuilder($aFieldsWhereName,$aFieldsWhereValue);
			#$SQLstr  		= 'SELECT '.$sFields.' FROM '.$tblName.' WHERE '.$sWhere.' ORDER BY nome ASC;';	
			#$ROW = $tbl1->GET_VALUES_SELECT($SQLstr,$aFieldsGet);
			$ROW = $tbl1->SQL_SELECT($tblName, $aFieldsGet, $aFieldsWhereName, $aFieldsWhereValue,'','ORDER BY nome ASC');
			unset($tbl1);
			
			$html = ''; 
			if (isset($ROW[0]))
			{
				$sDataFields = implode(';',$sFieldsName);

				$ccI = count($ROW);
				$ccJ = count($aFieldsGet);
				for ($i=0; $i<$ccI; $i++)
				{
					$id_encrypted 	= encrypt($ROW[$i]['id'],'N5KyEXAnDdwGL4eV');
					for ($j=1; $j<$ccJ; $j++)
					{
						$fv = $ROW[$i][$aFieldsGet[$j]];
						switch ($aFieldsType[$j-1])
						{
							case "string" : $fv = utf8_encode($fv); 					break;
							case "float"  : $fv = number_format($fv, 2, ',', ' ').' €'; break;
						}

						$a[$j-1] = $fv;
					}
					$aStrA = implode(' | ',$a);
					unset($a); // This deletes the whole array

					$html .= '<li>';
					$html .= '<a href="#" data-fields="'.$sDataFields.'" onclick="EditarLI(this,'."'".$id_encrypted."'".'); DONTSCROLL(); return false;" >'.$aStrA.'</a>';
					$html .= '</li>';
				}
			}

			return $html;
		}
		function ColunasLI($sTitulo, $sULDataName, $tblName, $aFieldsGet, $aFieldsWhereName, $aFieldsWhereValue, $sFieldsName, $aFieldsType, /* */ $sSELECTTblName="",$sSELECTDataName="",$sSELECTShowField="",$aSELECTWhereFields=false,$aSELECTWhereValues=false)
		{
			$sSELECTElem = "";
			if ($sSELECTTblName !== "") /* ARRAY de VALORES para o SELECT */
			{
				#$sWhere  = $tbl1->SQLWHERE_FieldsBuilder($aSELECTWhereFields,$aSELECTWhereValues);
				#$SQLstr  = 'SELECT id,'.$sSELECTShowField.' FROM '.$sSELECTTblName.' WHERE '.$sWhere.' ORDER BY nome ASC;';	
				#$ROWplus = $tbl1->GET_VALUES_SELECT($SQLstr,$aFieldsGet);
				$arrShowField = array('id',$sSELECTShowField);		// ADICIONA o campo ID	

				$tbl1 = new dbTable();
				$ROWplus = $tbl1->SQL_SELECT($sSELECTTblName, $arrShowField, $aFieldsWhereName, $aFieldsWhereValue,'','ORDER BY nome ASC');
				unset($tbl1);

				$sSELECTElem .= '<select data-name="'.$sSELECTDataName.'" class="select" onchange="SELECTChangeLI(this);">';
				$sSELECTElem .= '<option value="" disabled selected="selected" >Seleccionar Modalidade...</option>';
				
				$ccI = count($ROWplus);
				for ($i=0; $i<$ccI; $i++)
				{
					$id_encrypted = encrypt($ROWplus[$i]['id'],'N5KyEXAnDdwGL4eV');
					$valueField	  = utf8_encode($ROWplus[$i][$sSELECTShowField]);
					$sSELECTElem .= '<option value="'.$id_encrypted.'">'.$valueField.'</option>';
				}
				$sSELECTElem .= '</select>';
				
				/* ******************************************************************************* */
				$html = '';
				if (isset($ROWplus[0]))
				{
					$aFieldsWhereName[] = 'id_modalidade';
					$aFieldsWhereValue[] = $ROWplus[0]['id'];
					$html = AddLI($tblName,$aFieldsGet,$aFieldsWhereName,$aFieldsWhereValue,$sFieldsName,$aFieldsType);
				}
				/* ******************************************************************************* */
			}
			else
			if ($sSELECTTblName === "")
			{
				$html = AddLI($tblName,$aFieldsGet,$aFieldsWhereName,$aFieldsWhereValue,$sFieldsName,$aFieldsType);
			}

			
#//FIXME: Colocar abaixos apos corrigir. (Campo Pesquisar)			
# <input data-name="'.$sULDataName.'" class="cfg-search" type="text" name="Nome" placeholder="pesquisar" onkeyup="SearchLI(this,'."'".$sULDataName."'".')">			
#
			$htmlIni = '<div class="coluna">
							<h3>'.$sTitulo.'</h3>'
							.$sSELECTElem.
						   '<fieldset class="fieldset">
							<legend>Adicionar '.$sTitulo.'</legend>
							<form name="form-'.$sULDataName.'">
								<input class="input block" type="text" name="Nome" placeholder="Nome" autocomplete="off" />
							</form>		   
						   <span class="botoes">
								<a class="botao novo" href="#" onclick="InserirLI('."'".'form-'.$sULDataName."','".$sULDataName."','Nome'); DONTSCROLL(); return false;".'"'.'>adicionar</a>
							</span>
							</fieldset>   
							<ul class="lista" data-name="'.$sULDataName.'">
						';
			$htmlEnd = '
							</ul>
						</div>';
					
			return $htmlIni.$html.$htmlEnd;			
		}
		/* ************************************************************************************ */
	echo '-->';
?>
<?php
		echo ColunasLI('Modalidades', 'modalidade', 'modalidade', array('id','nome'), array('id_assoc','enabled'), array($id_assoc,'0'), array('Nome'), array('string'));
		echo ColunasLI('Turmas', 'modturma', 'mod_turma', array('id','nome'), array('id_assoc','enabled'), array($id_assoc,'0'), array('Nome'), array('string'),
				  'modalidade','modturma','nome',array('id_assoc','enabled'),array($id_assoc,'0'));
		echo ColunasLI('Níveis', 'modsocnivel', 'mod_socio_nivel', array('id','nome'), array('id_assoc','enabled'), array($id_assoc,'0'), array('Nome'), array('string'),
				  'modalidade','modsocnivel','nome',array('id_assoc','enabled'),array($id_assoc,'0'));
?>
<!--			
		<div class="coluna">
			<h3>Modalidades</h3>
			<input class="cfg-search" type="text" name="Nome" placeholder="pesquisar">
			<span class="botoes">
			<a class="botao novo" href="#" 
				onclick="InserirLI('modalidade','Nome'); 
						DONTSCROLL(); return false;">adicionar</a>
			</span>
			<ul class="lista" data-name="modalidade">
				<li><a href="#" data-fields="Nome" 
						onclick="EditarLI(this,''); DONTSCROLL(); return false;">
					Natação</a></li>
				<li><a href="#" data-fields="Nome" 
						onclick="EditarLI(this,''); DONTSCROLL(); return false;">
					Karaté</a></li>
				<li><a href="#" data-fields="Nome" 
						onclick="EditarLI(this,''); DONTSCROLL(); return false;">
					Futebol</a></li>
			</ul>
		</div>

		<div class="coluna">
			<h3>Turmas</h3>
			<select data-name="modalidades" class="cfg-search">
				<option value="1" >Natação</option>
				<option value="2" >Karaté</option>
			</select>
			<span class="botoes">
			<a class="botao novo" href="#" 
				onclick="InserirLI('modturma','Nome'); 
						DONTSCROLL(); return false;">adicionar</a>
			</span>
			<ul class="lista" data-name="modturma">
				<li><a href="#" data-fields="Nome" onclick="EditarLI(this,''); DONTSCROLL(); return false;">
					Natação A</a></li>
				<li><a href="#" data-fields="Nome" onclick="EditarLI(this,''); DONTSCROLL(); return false;">
					Karaté B</a></li>
				<li><a href="#" data-fields="Nome" onclick="EditarLI(this,''); DONTSCROLL(); return false;">
					Futebol C</a></li>
			</ul>
		</div>

		<div class="coluna">
			<h3>Níveis</h3>
			<select class="cfg-search">
				<option>Natação</option>
				<option>Karaté</option>
			</select>
			<span class="botoes">
			<a class="botao novo" href="#" 
				onclick="InserirLI('modsocnivel','Nome'); 
						DONTSCROLL(); return false;">adicionar</a>
			</span>
			<ul class="lista" data-name="modsocnivel">
				<li><a href="#" data-fields="Nome" 
						onclick="EditarLI(this,''); DONTSCROLL(); return false;">
					Juvenil</a></li>
				<li><a href="#" data-fields="Nome" 
						onclick="EditarLI(this,''); DONTSCROLL(); return false;">
					Normal</a></li>
				<li><a href="#" data-fields="Nome" 
						onclick="EditarLI(this,''); DONTSCROLL(); return false;">
					Veterano</a></li>
			</ul>
		</div
-->   
</section>
</div>
<script>
	var eObj = GetObjectsByTagNameData("SELECT","data-name","modturma");
		eObj.selectedIndex = 1;
	var eObj = GetObjectsByTagNameData("SELECT","data-name","modsocnivel");
		eObj.selectedIndex = 1;
</script>
<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>