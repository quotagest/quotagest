<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	$titulo = 'Gestão | QuotaGest';
	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
?>
<div id="corpo">

	<form class="formulario" action="#" method="POST" name="nova-quota-todos">
		<h2>Adicionar Quota para Todos <a href="#" class="close"></a></h2>
		
		<input name="Ano" placeholder="Ano" type="text"  maxlength="4" required >
		<input name="Montante" placeholder="0.00 €" type="text"  maxlength="15" required >
		<select class="line" name="Tipo-Mensalidade" placeholder="Tipo de Mensalidade" required >
			<option value="Estado de Mensalidade" disabled selected>Tipo de Mensalidade</option>		
			<option value="1">Mensal (12 meses)</option>
		</select>
		<select class="line" name="Distribuicao-Montante" placeholder="Distribuição de Montante" required >
			<option value="Distribuição do Montante" disabled selected>Distribuição de Montante</option>		
			<option value="1">Divir Montante por Todos os meses</option>
		</select>		

		<div class="line">
			<select name="Filtrar-Socios" placeholder="Filtrar Sócios" required >
				<option value="Filtrar Sócios" disabled selected>Filtrar Sócios</option>		
				<option value="1">Nenhum</option>
				<option value="2">Sexo</option>
				<option value="3">Tipo de Sócio</option>
				<option value="4">Estado de Sócio</option>
			</select>
			<select name="Outro-Filtro" placeholder="Distribuição de Montante" >
				<option value="Outro-Filtro" disabled selected>Outro Filtro</option>		
				<option value="1">Masculino/Feminino</option>
				<option value="1">Juvenil/Veterano</option>
				<option value="1">Activo/Inactivo</option>
			</select>		
		</div>		

		<div class="line botoes">
			<a href="#" data-name="clear" class="botao clear" onClick="CleanFormElements('nova-quota-todos');">Limpar</a>
			<a href="#" data-name="guardar" class="botao ok" onClick="ValidateForm('nova-quota-todos');">Guardar</a>
		</div>
	</form>
	
<?php
	if (isset($_POST['Ano']))
	{
		include(SETPATH('ROOT','PATH_APP_CORE').'inserir_quota-todos.php');
	}
?>
	
<div class="darken"></div>
   
</div>

<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>