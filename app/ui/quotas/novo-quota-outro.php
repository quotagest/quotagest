<?php 
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblQuotas.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocios.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplSocios.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	############################################################################
	$GET_IDM 	= (isset($_GET['idm'])) ? $_GET['idm'] : '';
	$GET_IDS 	= (isset($_GET['ids'])) ? $_GET['ids'] : '';
	$GET_IDQ 	= (isset($_GET['idq'])) ? $_GET['idq'] : '';

	$CHECK_IDM_URL = (!empty($GET_IDM));
	$CHECK_IDS_URL = (!empty($GET_IDS));
	$CHECK_IDQ_URL = (!empty($GET_IDQ));

	$URL		= ($CHECK_IDM_URL && $CHECK_IDS_URL) ? "?idm=".$GET_IDM."&ids=".$GET_IDS : '';
	$URL		= ($CHECK_IDM_URL || $CHECK_IDS_URL) ? '?' : '';
	$URL		.= ($CHECK_IDM_URL) ? "idm=".$GET_IDM : '';
	$URL		.= ($CHECK_IDS_URL) ? "&ids=".$GET_IDS : '';

	$ANO 		= (isset($_GET['ano'])) ? $_GET['ano'] : '';
	$urlANO 	= ($ANO !== '') ? '&ano='.$ANO : '';

	#$URL		= ((!empty($GET_IDM) && (!empty($GET_IDS)))) ? "?idm=".$GET_IDM."&ids=".$GET_IDS : '';
	$Msg 		= '';

	
	############################## GUARDAR REGISTO ##############################
	if ( (isset($_POST['lista-modalidade'])) &&
		 (isset($_POST['CodSocio'])) &&
		 (isset($_POST['Descricao'])))
	{
		$ID_MODALIDADE 	= $_POST['lista-modalidade'];
		$ID_SOCIO 		= $_POST['CodSocio'];
		#############################################################################
		####### INSERIR SÓCIO SE NAO ESTIVER INSCRITO NA CURRENTE ACTIVIDADE ########
		#############################################################################
		$tblSocios = new dbTblSocios();
		$tblSocios->set_IDMODALIDADE($ID_MODALIDADE);
		$tblSocios->set_IDSOCIO($ID_SOCIO);
		$tblSocios->set_IDUSER($id_user);
		$tblSocios->set_IDASSOC($id_assoc);
		$A_POST['data-inscricao'] = date('Y-m-d');
		$A_POST['data-saida'] = '';
		$A_POST['desconto'] = '';
		$Result = $tblSocios->addModalidadeInscricao($A_POST);
		unset($tblSocios);

		if ($Result)
		{
			echo '<!-- O Registo foi salvo com sucesso! -->';
		}
		else
		{
			if ($Result === 'EXIST')
				echo '<!-- Não foi possivel guardar o Registo. -->';
			else
			if (!$Result)
				echo '<!-- O Sócio já foi inscrito nesta Actividade. -->';
		}

		#############################################################################
		############################### INSERIR QUOTA ###############################
		#############################################################################
		$tblQuotas = new dbTblQuotas();
		$tblQuotas->set_IDQUOTA($GET_IDQ);
		$tblQuotas->set_IDMODALIDADE($ID_MODALIDADE);
		$tblQuotas->set_IDSOCIO($ID_SOCIO);
		$tblQuotas->set_IDUSER($id_user);
		$tblQuotas->set_IDASSOC($id_assoc);
		$Result = $tblQuotas->addQuota($_POST);
		unset($tblQuotas);

		if ($Result)
		{
			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'/ui/quotas/listagem.php'.$URL.$urlANO);
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
	}
	#############################################################################
	########################## HTML TEMPLATE VARIABLES ##########################
	#############################################################################
	$tplSocios = new tplSocios();
	$Result = $tplSocios->getHTML_INPUTSOCIO($GET_IDS,$id_user,$id_assoc);
	$SOCIO_NOME 			= $Result['SOCIO_NOME']; 
	$SOCIO_NOME_PLACEHOLDER = ($SOCIO_NOME === '') ? 'Nome do Sócio' : $SOCIO_NOME; 
	$SOCIO_INPUT_COD 		= $Result['SOCIO_INPUT_COD'];
	$SOCIO_INPUT_NOME 		= '<input type="text" name="NomeSocio" placeholder="'.$SOCIO_NOME.'" autocomplete="off" maxlength="80" value="'.$SOCIO_NOME.'" disabled required />';
	$HTML_PAGFORMA_SELECT 	= $tplSocios->buildSelectSocioExtra('pag_forma','0',$id_user);
	#$HTML_PAGOPCAO_SELECT 	= $tplSocios->buildSelectSocioExtra('pag_opcao','0',$id_user);
	$HTML_MODALIDADE_SELECT = $tplSocios->getHTML_SELECTActividadesTodas($GET_IDM,$GET_IDS,$id_user,$id_assoc);
	unset($tplSocios);
	#############################################################################
	#############################################################################
	$HTML_ANOS_SELECT = '';
	for ($i=(date('Y')+1); $i>=2005; $i--) #Inicio da actividade da Associação
	{
		$HTML_ANOS_SELECT .= '<option value="'.$i.'">'.$i.'</option>';
	}
	#############################################################################
	#############################################################################
	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_QUOTAS').'novo-quota-outro.html');
	$objTemp->setVar('{$titulo}', 'Adicionar Quota/Outro');
	$objTemp->setVar('{$ajuda}', 'Adicione uma nova quota ou outro tipo de pagamento.');

	############################### MENU ESQUERDO ###############################
	$arrLeftMenu = array('1' => array("titulo" 	=> "Guardar",
									  "href" 	=> "/",
									  "javascript" => "ValidateForm('nova-quota-outro');"),
						 '2' => array("titulo" 	=> "Voltar",
									  "href" 	=> "/ui/quotas/listagem.php".$URL.$urlANO)
						);
	################################ FORMULARIO ################################
	$objTemp->setVar('{$FormAction}', htmlentities($_SERVER['PHP_SELF']).$URL);
	$objTemp->setVar('{$HTML_MODALIDADE_SELECT}', $HTML_MODALIDADE_SELECT);
	$objTemp->setVar('{$HTML_PAGFORMA_SELECT}', $HTML_PAGFORMA_SELECT);
	#$objTemp->setVar('{$HTML_PAGOPCAO_SELECT}', $HTML_PAGOPCAO_SELECT);

	$ANO = ($ANO !== '') ? $ANO : date('Y');
	$objTemp->setVar('{$YearNow-1}', ($ANO-1));
	$objTemp->setVar('{$YearNow}', $ANO);
	$objTemp->setVar('{$YearNow+1}', ($ANO+1));
	$objTemp->setVar('{$data_hoje}', (date('Y-m-d')));
	$objTemp->setVar('{$data_hoje+7}', date('Y-m-d',strtotime("+1 week", strtotime(date('Y-m-d')))) );
	
	$objTemp->setVar('{$HTML_ANOS_SELECT}', $HTML_ANOS_SELECT);
	$objTemp->setVar('{$ActividadeDisabled}', (($GET_IDM !== '') ? ' disabled ':''));
	$objTemp->setVar('{$SOCIO_NOME_PLACEHOLDER}', $SOCIO_NOME_PLACEHOLDER);
	$objTemp->setVar('{$SOCIO_NOME}', $SOCIO_NOME);
	$objTemp->setVar('{$SOCIO_INPUT_COD}', $SOCIO_INPUT_COD);
	#if ($ExistSocio)
	#	$objTemp->replaceDOM('[data-id=fieldsocio]','innertext',$inputSocioID.$inputSocio);
	#############################################################################
	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $Msg;
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
	#############################################################################
?>