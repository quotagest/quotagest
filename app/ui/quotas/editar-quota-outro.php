<?php 
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblQuotas.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocios.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplSocios.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	############################################################################
	$GET_IDM 	= (isset($_GET['idm'])) ? $_GET['idm'] : '';
	$GET_IDS 	= (isset($_GET['ids'])) ? $_GET['ids'] : '';
	$GET_IDQ 	= (isset($_GET['idq'])) ? $_GET['idq'] : '';
	$URL		= ((!empty($GET_IDM) && (!empty($GET_IDS)))) ? "?idm=".$GET_IDM."&ids=".$GET_IDS : '';
	$Msg 		= '';
	$ANO 		= (isset($_GET['ano'])) ? '&ano='.$_GET['ano'] : '';

	$tblQuotas = new dbTblQuotas();
	$tblQuotas->set_IDQUOTA($GET_IDQ);
	$tblQuotas->set_IDMODALIDADE($GET_IDM);
	$tblQuotas->set_IDSOCIO($GET_IDS);
	$tblQuotas->set_IDUSER($id_user);
	$tblQuotas->set_IDASSOC($id_assoc);
	############################## GUARDAR REGISTO ##############################
	if ( (isset($GET_IDQ)) &&
		 (isset($GET_IDM)) &&
		 (isset($GET_IDS)) &&
		 (isset($_POST['Descricao'])))
	{
		$Result = $tblQuotas->updateQuota($_POST);

		if ($Result)
		{
			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'/ui/quotas/listagem.php'.$URL.$ANO);
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
	}
	############################## RECEBER REGISTO ##############################
	$Result = $tblQuotas->getQuotaData();
	unset($tblQuotas);
	
	$ROW 	= $Result['ROW'];
	$EXIST 	= $Result['EXIST'];
	#############################################################################
	########################## HTML TEMPLATE VARIABLES ##########################
	#############################################################################
	$tplSocios = new tplSocios();
	$Result = $tplSocios->getHTML_INPUTSOCIO($GET_IDS,$id_user,$id_assoc);
	$SOCIO_NOME 			= $Result['SOCIO_NOME']; 
	$SOCIO_INPUT_COD 		= $Result['SOCIO_INPUT_COD'];
	$SOCIO_INPUT_NOME 		= '<input type="text" name="NomeSocio" placeholder="'.$SOCIO_NOME.'" autocomplete="off" maxlength="80" value="'.$SOCIO_NOME.'" disabled required />';
	$HTML_PAGFORMA_SELECT 	= $tplSocios->buildSelectSocioExtra('pag_forma',$ROW[0]['id_pag_forma'],$id_user);
	#$HTML_PAGOPCAO_SELECT 	= $tplSocios->buildSelectSocioExtra('pag_opcao',$ROW[0]['id_pag_opcao'],$id_user);
	$HTML_MODALIDADE_SELECT = $tplSocios->getHTML_SELECTActividadesTodas($GET_IDM,$GET_IDS,$id_user,$id_assoc);
	unset($tplSocios);
	#############################################################################
	#############################################################################

	$HTML_ANOS_SELECT = '';
	for ($i=(date('Y')+1); $i>=2005; $i--)
	{
		$HTML_ANOS_SELECT .= '<option value="'.$i.'">'.$i.'</option>';
	}
	#############################################################################
	#############################################################################
	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_QUOTAS').'editar-quota-outro.html');
	$objTemp->setVar('{$titulo}', 'Editar Quota/Outro');
	$objTemp->setVar('{$ajuda}', 'Edite as informações da Quota/Outro.');

	if ($EXIST)
	{
		############################### MENU ESQUERDO ###############################
		$arrLeftMenu = array('1' => array("titulo" 	=> "Guardar",
										  "href" 	=> "/",
										  "javascript" => "ValidateForm('nova-quota-outro');"),
							 '2' => array("titulo" 	=> "Voltar",
										  "href" 	=> "/ui/quotas/listagem.php".$URL.$ANO)
							);
		################################ FORMULARIO ################################
		$objTemp->setVarArray('{field:','}',$ROW[0]);
		$objTemp->setVar('{$FormAction}', htmlentities($_SERVER['PHP_SELF']).$URL.$ANO."&idq=".$GET_IDQ);
		$objTemp->setVar('{$HTML_MODALIDADE_SELECT}', $HTML_MODALIDADE_SELECT);
		$objTemp->setVar('{$HTML_PAGFORMA_SELECT}', $HTML_PAGFORMA_SELECT);
		#$objTemp->setVar('{$HTML_PAGOPCAO_SELECT}', $HTML_PAGOPCAO_SELECT);
		$objTemp->setVar('{$YearNow-1}', ($ROW[0]['ano']-1));
		$objTemp->setVar('{$YearNow}', $ROW[0]['ano']);
		$objTemp->setVar('{$YearNow+1}', ($ROW[0]['ano']+1));
		$objTemp->setVar('{$HTML_ANOS_SELECT}', $HTML_ANOS_SELECT);
		$objTemp->setVar('{$SOCIO_INPUT_COD}', $SOCIO_INPUT_COD);
		$objTemp->setVar('{$SOCIO_INPUT_NOME}', $SOCIO_INPUT_NOME);
		$objTemp->setVar('{$ActividadeDisabled}', (($GET_IDM !== '') ? ' disabled ':''));
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('O Registo não existe!','Voltar','listagem.php');
		$objTemp->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $Msg;
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
	#############################################################################
?>