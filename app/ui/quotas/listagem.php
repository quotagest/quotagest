<?php 
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblQuotas.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplSocios.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	############################################################################
	$GET_ANO 	= (isset($_GET['ano'])) ? $_GET['ano'] : '';
	$GET_IDM 	= (isset($_GET['idm'])) ? $_GET['idm'] : '';
	$GET_IDS 	= (isset($_GET['ids'])) ? $_GET['ids'] : '';
	
	$CHECK_IDM_URL = (!empty($GET_IDM));
	$CHECK_IDS_URL = (!empty($GET_IDS));

	$URL		= ($CHECK_IDM_URL && $CHECK_IDS_URL) ? "?idm=".$GET_IDM."&ids=".$GET_IDS : '';
	$URL		= ($CHECK_IDM_URL || $CHECK_IDS_URL) ? '?' : '';
	$URL		.= ($CHECK_IDM_URL) ? "idm=".$GET_IDM : '';
	$URL		.= ($CHECK_IDS_URL) ? "&ids=".$GET_IDS : '';
	$ANO 		= (isset($_GET['ano'])) ? '&ano='.$_GET['ano'] : '';

	$GET_PAGE 	= (isset($_GET['page'])) ? $_GET['page'] : '0';

	$tblQuotas = new dbTblQuotas();
	#$tblQuotas->set_IDQUOTA($GET_IDQ);
	$tblQuotas->set_IDMODALIDADE($GET_IDM);
	$tblQuotas->set_IDSOCIO($GET_IDS);
	$tblQuotas->set_IDUSER($id_user);
	$tblQuotas->set_IDASSOC($id_assoc);
	$Result = $tblQuotas->getList($GET_ANO);
	$htmlSelectAnos = $tblQuotas->createSelect_DeAnos($GET_ANO);
	unset($tblQuotas);
	$ROW    = $Result['ROW'];
	$EXIST  = $Result['EXIST'];
	#############################################################################
	#############################################################################
	$tplSocios = new tplSocios();
	$ResultSociosTPL = $tplSocios->getTPL_QUOTASLISTAGEM($GET_IDM,$GET_IDS,$id_user,$id_assoc);
	unset($tplSocios);
	#############################################################################
	############################### MENU ESQUERDO ###############################
	$arrLeftMenu["1"] = array("titulo" => "Nova Quota/Outro",
							  "href" 	=> "/ui/quotas/novo-quota-outro.php".$URL.$ANO);
	$arrLeftMenu["2"] = array("titulo" => "Adicionar Quotas para Todos",
							  "href" 	=> "/ui/quotas/novo-quota-massa");
	foreach ($ResultSociosTPL['arrMENU'] as $key => $value)
	{
		$arrLeftMenu[$key]	= $value;
	}
	#############################################################################
	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_QUOTAS').'listagem.html');
	$objTemp->setVar('{$titulo}', 'Listagem de Quotas');
	$objTemp->setVar('{$ajuda}', 'Utilize a pesquisa para encontrar rápidamente a(s) Quota(s) que procura.');
	$objTemp->setVar('{$htmlSelectAnos}', $htmlSelectAnos);
	$objTemp->setVar('{$htmlSELECT_ACTIVIDADES}', $ResultSociosTPL['htmlSELECT_ACTIVIDADES']);
	$objTemp->setVar('{$SOCIO_NOME}', $ResultSociosTPL['SOCIO_NOME']);
	$objTemp->setVar('{$SOCIO_INPUT_COD}', $ResultSociosTPL['SOCIO_INPUT_COD']);
	#############################################################################
	if ($EXIST)
	{
		################################# LISTAGEM ##################################
		$objTemp->BuildRows('id=linhas','{field:','}',$ROW);
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('Não existem Registos!','Nova Quota','novo-quota-outro.php'.$URL.$ANO);
		$objTemp->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
	#############################################################################
?>