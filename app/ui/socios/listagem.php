<?php 
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlSocios.class.php');
	############################################################################
	$GET_PAGE 	= (isset($_GET['page'])) ? $_GET['page'] : '0';
	$orderby 	= (isset($_GET['ordby'])) ? $_GET['ordby'] : 'ASC';
	$fldord 	= (isset($_GET['fld'])) ? $_GET['fld'] : '';

	$objTplMdlSocios = new tplmdlSocios();

	$objTplMdlSocios->setTplFileName('listagem.html');
	$objTplMdlSocios->setObjTemplate($objTplMdlSocios->tplFolderPath.$objTplMdlSocios->tplFileName);
	$objTplMdlSocios->setTitulo('Listagem de Sócios');
	$objTplMdlSocios->setAjuda('Utilize a pesquisa para encontrar rápidamente o(s) Sócio(s) que procura.');

	$objTplMdlSocios->tblSocios->set_IDSOCIO(0);
	$objTplMdlSocios->tblSocios->set_IDMODALIDADE(0);
	$objTplMdlSocios->tblSocios->set_IDUSER($id_user);
	$objTplMdlSocios->tblSocios->set_IDASSOC($id_assoc);
	$Result 		= $objTplMdlSocios->tblSocios->getList($fldord,$orderby,$GET_PAGE);
	$HTML_PAGINACAO = $objTplMdlSocios->tblSocios->getPagination('assoc_socios WHERE id_assoc="'.$id_assoc.'" AND enabled="0"',$GET_PAGE);
	$ROW 	= $Result['ROW'];
	$EXIST 	= $Result['EXIST'];

	#############################################################################
	$orderby_codigo 	= 'ASC';
	$orderby_nome 		= 'ASC';
	$orderby_tipo 		= 'ASC';
	$orderby_estado 	= 'ASC';
	$orderby_saldo 		= 'ASC';
	if ($fldord !== '')
	{
		$orderby 	= ($orderby === 'ASC') ? 'DESC' : 'ASC';

		switch ($fldord)
		{
			case 'codigo'	: $orderby_codigo 	= $orderby; break;
			case 'nome'		: $orderby_nome 	= $orderby; break;
			case 'tipo'		: $orderby_tipo 	= $orderby; break;
			case 'estado'	: $orderby_estado 	= $orderby; break;
			case 'saldo'	: $orderby_saldo 	= $orderby; break;
			default: break;
		}
	}
	#############################################################################

	if ($EXIST)
	{
		$objTplMdlSocios->addLeftMenuItem(array("titulo" => "Novo Sócio", "href" => "/ui/socios/novo.php"));
		$objTplMdlSocios->addLeftMenuItem(array("titulo" => "Relatórios", "href" => "/ui/socios/relatorio"));

		$objTplMdlSocios->objTemplate->BuildRows('id=linhas','{field:','}',$ROW);
		$objTplMdlSocios->objTemplate->setVar('{$PATH_SELF_URL}',SETPATH('URL','PATH_APP_UI_SOCIOS').'listagem');
		$objTplMdlSocios->objTemplate->setVar('{$orderby-codigo}',$orderby_codigo);
		$objTplMdlSocios->objTemplate->setVar('{$orderby-nome}',$orderby_nome);
		$objTplMdlSocios->objTemplate->setVar('{$orderby-tipo}',$orderby_tipo);
		$objTplMdlSocios->objTemplate->setVar('{$orderby-estado}',$orderby_estado);
		$objTplMdlSocios->objTemplate->setVar('{$orderby-saldo}',$orderby_saldo);
		$objTplMdlSocios->objTemplate->setVar('{$HTML_PAGINACAO}',$HTML_PAGINACAO);
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('Não existem Registos!','Novo Sócio','novo.php');
		$objTplMdlSocios->objTemplate->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}
	$objTplMdlSocios->finishTemplate();

	$HTML 		 = $objTplMdlSocios->getSHTML();
	$arrLeftMenu = $objTplMdlSocios->getLeftMenuItem();
	unset($objTplMdlSocios);

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $HTML;
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
?>