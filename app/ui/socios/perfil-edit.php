<?php 
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlSocios.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjUploadFile.class.php');

	$GET_IDSOCIO = (isset($_GET['id'])) ? $_GET['id'] : '';
	$URL  		 = ($GET_IDSOCIO !== '') ? '?id='.$GET_IDSOCIO : '';

	$objTplMdlSocios = new tplmdlSocios();
	$objTplMdlSocios->tblSocios->set_IDSOCIO($GET_IDSOCIO);
	$objTplMdlSocios->tblSocios->set_IDMODALIDADE(0);
	$objTplMdlSocios->tblSocios->set_IDUSER($id_user);
	$objTplMdlSocios->tblSocios->set_IDASSOC($id_assoc);
	############################## GUARDAR REGISTO ##############################
	if (isset($_POST['Nome']) && isset($_POST['socio-sexo']))
	{
		$Result = $objTplMdlSocios->tblSocios->updateSocio($_POST);

		if ($Result)
		{
			if (isset($_POST['file-name']))
			{
				$objFileName = new TobjUploadFile();
				$objFileName->setFileName(SETPATH('ROOT','PATH_APP_IMG_SOCIOS').$GET_IDSOCIO.'.jpg');
				$objFileName->setTempFileName(SETPATH('ROOT','PATH_APP_TMP').$_POST['file-name']);
				$objFileName->setMaxSize(524288);
				$objFileName->setFileType('image');
				$objFileName->setInputFileName('upload-image');
				$objFileName->setMaxWidth('160');
				$objFileName->setMaxHeigth('125');
				$objFileName->setIsBase64((isset($_GET['base64'])) ? true : false);
				$objFileName->setIsBinary((isset($_GET['up'])) ? true : false);
				$objFileName->setEchoFileName((isset($_GET['printname'])) ? true : false);
				$Result = $objFileName->SaveFile();
				unset($objFileName);
			}

			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'/ui/socios/perfil.php?id='.$GET_IDSOCIO);
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
	}
	############################################################################
	$objTplMdlSocios->setTplFileName('perfil-edit.html');
	$objTplMdlSocios->setObjTemplate($objTplMdlSocios->tplFolderPath.$objTplMdlSocios->tplFileName);
	$objTplMdlSocios->setTitulo('Editar perfil do Sócio');
	$objTplMdlSocios->setAjuda('Edite as informações básicas do perfil do Sócio.');

	$Result = $objTplMdlSocios->getPerfilEditSocio();
	$ROW 	= $Result['ROW'];
	$EXIST 	= $Result['EXIST'];

	if ($EXIST)
	{
		$sKey = $objTplMdlSocios->tblSocios->encryptVar(date('h:i:s').'*'.$GET_IDSOCIO);
		$objTplMdlSocios->addLeftMenuItem(array("titulo" => "Guardar", 		"href" => "/", "javascript" => "ValidateForm_Socio('socio-perfil-edit');"));
		$objTplMdlSocios->addLeftMenuItem(array("titulo" => "Remover Foto", "href" => "/", "javascript" => "REMOVE_IMAGEFILE('socio','".$sKey."','unset');"));
		$objTplMdlSocios->addLeftMenuItem(array("titulo" => "Voltar", 		"href" => "/ui/socios/perfil.php".$URL));

		$objTplMdlSocios->objTemplate->setVarArray('{field:','}',$ROW[0]);
		$objTplMdlSocios->objTemplate->setVar('{$reg-update}', getLastDateEdit($ROW[0]['data_alterado']));
		$objTplMdlSocios->objTemplate->setVar('{$foto-url}', $ROW[0]['imgSocioSrc']);
		$objTplMdlSocios->objTemplate->setVar('{$imgsPath}', SETPATH('URL','PATH_APP_IMG_ICONS'));
		$objTplMdlSocios->objTemplate->setVar('{$FormAction}', htmlentities($_SERVER['PHP_SELF']).$URL);
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('O Registo não existe!','Voltar','listagem.php');
		$objTplMdlSocios->objTemplate->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}
	$objTplMdlSocios->finishTemplate();

	$HTML 		 = $objTplMdlSocios->getSHTML();
	$arrLeftMenu = $objTplMdlSocios->getLeftMenuItem();
	unset($objTplMdlSocios);

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo (isset($Msg)) ? $Msg : '';
	echo $HTML;
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
?>