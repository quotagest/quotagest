<?php 
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlSocios.class.php');

	############################################################################
	$GET_IDSOCIO = (isset($_GET['id'])) ? $_GET['id'] : '';
	$URL 		 = ($GET_IDSOCIO !== '') ? "?ids=".$GET_IDSOCIO : '';

	$objTplMdlSocios = new tplmdlSocios();

	$objTplMdlSocios->setTplFileName('perfil.html');
	$objTplMdlSocios->setObjTemplate($objTplMdlSocios->tplFolderPath.$objTplMdlSocios->tplFileName);
	$objTplMdlSocios->setTitulo('Perfil de Sócio');
	$objTplMdlSocios->setAjuda('Utilize o botão "Editar" para alterar os dados do Sócio.');

	$objTplMdlSocios->tblSocios->set_IDSOCIO($GET_IDSOCIO);
	$objTplMdlSocios->tblSocios->set_IDMODALIDADE(0);
	$objTplMdlSocios->tblSocios->set_IDUSER($id_user);
	$objTplMdlSocios->tblSocios->set_IDASSOC($id_assoc);
	$Result = $objTplMdlSocios->getPerfilSocio();
	$ROW 	= $Result['ROW'];
	$EXIST 	= $Result['EXIST'];

	if ($EXIST)
	{
		$objTplMdlSocios->addLeftMenuItem(array("titulo" => "Inscrever em modalidade", "href" => "/ui/modalidades/novo-inscricao.php".$URL));
		$objTplMdlSocios->addLeftMenuItem(array("titulo" => "Ver Quotas", "href" => "/ui/quotas/listagem.php".$URL));
		$objTplMdlSocios->addLeftMenuItem(array("titulo" => "Ver listagem de Sócios", "href" => "/ui/socios/listagem.php"));

		$objTplMdlSocios->objTemplate->setVarArray('{field:','}',$ROW[0]);
		$objTplMdlSocios->objTemplate->setVar('{$reg-update}',	getLastDateEdit($ROW[0]['data_alterado']));
		$objTplMdlSocios->objTemplate->setVar('{$edit-link}',	'perfil-edit.php?id='.$GET_IDSOCIO);
		$objTplMdlSocios->objTemplate->setVar('{$foto-url}',	$ROW[0]['imgSocioSrc']);

		#############################################################################
		############################ MENU QUOTAS EM FALTA ###########################
		$ResultQuotas = $objTplMdlSocios->tblSocios->getModalidadesTotal();
		$ROWQuotas    = $ResultQuotas['ROW'];
		$EXISTQuotas  = $ResultQuotas['EXIST'];

		$tDivida = 0.0;
		$tPago	 = 0.0;
		if ($EXISTQuotas)
		{
			$sPathQuotas = SETPATH('URL','PATH_APP_UI_QUOTAS');
			$ccI = count($ROWQuotas);
			for ($i=0; $i<$ccI; $i++)
			{
				$tDivida += $ROWQuotas[$i]['total_divida'];
				$tPago	 += $ROWQuotas[$i]['total_pago'];
				$ROWQuotas[$i]['goto-link'] = $sPathQuotas.'listagem.php?ids='.$GET_IDSOCIO.'&idm='.$ROWQuotas[$i]['id_modalidade'].'&ano='.date('Y');
			}
			$objTplMdlSocios->objTemplate->BuildRows('id=linhas','{field:','}',$ROWQuotas);
		}
		else
		{
			$objTplMdlSocios->objTemplate->setVar('{loop id=linhas}', '');
			$objTplMdlSocios->objTemplate->setVar('{/loop}', '');
			$objTplMdlSocios->objTemplate->setVar('por modalidade:', '');
			$objTplMdlSocios->objTemplate->replaceDOM('[class=modalidade]','outertext','');
		}
		$objTplMdlSocios->objTemplate->setVar('{$total_divida}',number_format($tDivida, 2, '.', ''));
		$objTplMdlSocios->objTemplate->setVar('{$total_link}',	'javascript:void(0);');
		#############################################################################
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('O Registo não existe!','Voltar','listagem.php');
		$objTplMdlSocios->objTemplate->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}
	$objTplMdlSocios->finishTemplate();

	$HTML 		 = $objTplMdlSocios->getSHTML();
	$arrLeftMenu = $objTplMdlSocios->getLeftMenuItem();
	unset($objTplMdlSocios);

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $HTML;
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
?>