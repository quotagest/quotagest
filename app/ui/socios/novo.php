<?php 
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlSocios.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjUploadFile.class.php');

	$objTplMdlSocios = new tplmdlSocios();
	$objTplMdlSocios->tblSocios->set_IDSOCIO(0);
	$objTplMdlSocios->tblSocios->set_IDMODALIDADE(0);
	$objTplMdlSocios->tblSocios->set_IDUSER($id_user);
	$objTplMdlSocios->tblSocios->set_IDASSOC($id_assoc);
	############################## GUARDAR REGISTO ##############################
	#if (isset($_POST['BICC']) && isset($_POST['Nome']))
	if (isset($_POST['Nome']))
	{
		$_POST['NIB'] 	= '';
		$_POST['Telemovel'] 	= '';
		$_POST['Fax'] 			= '';
		$_POST['Outro1'] 		= '';
		$_POST['Outro2'] 		= '';
		$_POST['data-entrada'] 	= date('Y-m-d');
		$_POST['data-saida'] 	= '';

		$resArr 	  = $objTplMdlSocios->tblSocios->addSocio($_POST);
		$Result 	  = $resArr['Result'];
		$NEW_SOCIO_ID = $resArr['NEW_ID'];
		
		if ($Result)
		{
			if (isset($_POST['file-name']))
			{
				$objFileName = new TobjUploadFile();
				$objFileName->setFileName(SETPATH('ROOT','PATH_APP_IMG_SOCIOS').$NEW_SOCIO_ID.'.jpg');
				$objFileName->setTempFileName(SETPATH('ROOT','PATH_APP_TMP').$_POST['file-name']);
				$objFileName->setMaxSize(524288);
				$objFileName->setFileType('image');
				$objFileName->setInputFileName('upload-image');
				$objFileName->setMaxWidth('160');
				$objFileName->setMaxHeigth('125');
				$objFileName->setIsBase64((isset($_GET['base64'])) ? true : false);
				$objFileName->setIsBinary((isset($_GET['up'])) ? true : false);
				$objFileName->setEchoFileName((isset($_GET['printname'])) ? true : false);
				$Result = $objFileName->SaveFile();
				unset($objFileName);
			}

			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'/ui/socios/perfil.php?id='.$NEW_SOCIO_ID);
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
	}
	############################################################################
	# RECEBER Nº SOCIO ultimo socio inserido
	$NumSocio = $objTplMdlSocios->getLastNumSocio($id_user,$id_assoc);
	$NumSocio++;
	############################################################################
	############################################################################
	$objTplMdlSocios->addLeftMenuItem(array("titulo" => "Guardar", "href" => "/", "javascript" => "ValidateForm_Socio('novo-socio');"));
	$objTplMdlSocios->addLeftMenuItem(array("titulo" => "Ver listagem de Sócios", "href" => "/ui/socios/listagem.php"));
	
	$objTplMdlSocios->setTplFileName('novo.html');
	$objTplMdlSocios->setObjTemplate($objTplMdlSocios->tplFolderPath.$objTplMdlSocios->tplFileName);
	$objTplMdlSocios->setTitulo('Adicionar Sócio');
	$objTplMdlSocios->setAjuda('Adicionar um novo registo de Sócio.');
	$objTplMdlSocios->objTemplate->setVar('{$NumSocio}', 		$NumSocio);
	$objTplMdlSocios->objTemplate->setVar('{$FORMAction}', 		htmlentities($_SERVER['PHP_SELF']));
	$objTplMdlSocios->objTemplate->setVar('{$imgsPath}', 		SETPATH('URL','PATH_APP_IMG_ICONS'));
	$objTplMdlSocios->objTemplate->setVar('{$dateNow}', 		date('Y-m-d'));
	$objTplMdlSocios->objTemplate->setVar('{$resHTML_ESTADO}', 	$objTplMdlSocios->buildSELECT_SocioEstado('0',$id_assoc));
	$objTplMdlSocios->objTemplate->setVar('{$resHTML_TIPO}', 	$objTplMdlSocios->buildSELECT_SocioTipo('0',$id_assoc));
	$objTplMdlSocios->finishTemplate();

	$HTML 		 = $objTplMdlSocios->getSHTML();
	$arrLeftMenu = $objTplMdlSocios->getLeftMenuItem();
	unset($objTplMdlSocios);

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo (isset($Msg)) ? $Msg : '';
	echo $HTML;
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
?>