<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjUploadFile.class.php');

	// GET dados do user "email_testes"
	$GET_IDUSER = 0;

	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_USER').'user-config.html');
	$objTemp->setVar('{$titulo}', 'Dados do Utilizador');
	$objTemp->setVar('{$ajuda}', 'Aqui pode alterar a sua password, email e o seu nome.');

	######################################################################################
	if (isset($_POST['nome']) && isset($_POST['apelido']) && isset($_POST['email']))
	{
		$Result = $objTplMdlSocios->tblSocios->updateSocio($_POST);

		if ($Result)
		{
			if (isset($_POST['file-name']))
			{
				$objFileName = new TobjUploadFile();
				$objFileName->setFileName(SETPATH('ROOT','PATH_APP_IMG_USER').$GET_IDUSER.'.jpg');
				$objFileName->setTempFileName(SETPATH('ROOT','PATH_APP_TMP').$_POST['file-name']);
				$objFileName->setMaxSize(524288);
				$objFileName->setFileType('image');
				$objFileName->setInputFileName('upload-image');
				$objFileName->setMaxWidth('160');
				$objFileName->setMaxHeigth('125');
				$objFileName->setIsBase64((isset($_GET['base64'])) ? true : false);
				$objFileName->setIsBinary((isset($_GET['up'])) ? true : false);
				$objFileName->setEchoFileName((isset($_GET['printname'])) ? true : false);
				$Result = $objFileName->SaveFile();
				unset($objFileName);
			}

			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'/ui/user/user-config.php');
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
	}
	######################################################################################

	$EXIST = true;
	if ($EXIST)
	{
		############################### MENU ESQUERDO ###############################
		#$sKey = $objTplMdlSocios->tblSocios->encryptVar(date('h:i:s').'*'.$GET_IDUSER);
		$arrLeftMenu = array('1' => array("titulo" => "Guardar", 	  "href" => "/", "javascript" => "ValidateForm('user-perfil-edit');"),
							 '2' => array("titulo" => "Remover Foto", "href" => "/", "javascript" => "REMOVE_IMAGEFILE('user','".$sKey."','unset');")
							);
		################################# LISTAGEM ##################################
		$objTemp->setVar('{$imgsPath}', SETPATH('URL','PATH_APP_IMG_ICONS'));
		$objTemp->setVar('{$FormAction}', htmlentities($_SERVER['PHP_SELF']));
		$objTemp->setVar('{$foto-url}', SETPATH('URL','PATH_APP_IMG_ICONS_USERS').'user-male-a-01.jpg'); #$ROW[0]['imgUserSrc']
		#$objTemp->setVarArray('{field:','}',$ROW[0]);
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('Não existem Registos!','Nova Entidade','novo.php');
		$objTemp->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo (isset($Msg)) ? $Msg : '';
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
?>