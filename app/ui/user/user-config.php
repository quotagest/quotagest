<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjUploadFile.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlUser.class.php');

	// GET dados do user "email_testes"
	$GET_IDUSER = $id_user;

	$objTplMdlUser = new tplmdlUser();
	$objTplMdlUser->setTplFileName('user-config.html');
	$objTplMdlUser->setObjTemplate($objTplMdlUser->tplFolderPath.$objTplMdlUser->tplFileName);
	$objTplMdlUser->setTitulo('Dados do Utilizador');
	$objTplMdlUser->setAjuda('Aqui pode alterar a sua password, email e o seu nome.');

	######################################################################################
	if (isset($_POST['nome']) && isset($_POST['apelido']) && isset($_POST['email']))
	{
		$A_POST = array('id'				=> false,
						'email'				=> $_POST['email'],
						'password'			=> false,
						'nome'				=> $_POST['nome'],
						'apelido'			=> $_POST['apelido'],
						'HTTP_USER_AGENT'	=> false,
						'REMOTE_ADDR'		=> false,
						'REMOTE_PORT'		=> false,
						'data_criacao'		=> false,
						'data_alterado'		=> date('Y-m-d H:i:s'),
						'enabled'			=> false);

		$Result = $objTplMdlUser->tblEmailsTeste->set_IDUSER($GET_IDUSER);
		$Result = $objTplMdlUser->tblEmailsTeste->updateExtra($A_POST);

		if ($Result)
		{
			if (isset($_POST['file-name']))
			{
				$objFileName = new TobjUploadFile();
				$objFileName->setFileName(SETPATH('ROOT','PATH_APP_IMG_USERS').$GET_IDUSER.'.jpg');
				$objFileName->setTempFileName(SETPATH('ROOT','PATH_APP_TMP').$_POST['file-name']);
				$objFileName->setMaxSize(524288);
				$objFileName->setFileType('image');
				$objFileName->setInputFileName('upload-image');
				$objFileName->setMaxWidth('160');
				$objFileName->setMaxHeigth('125');
				$objFileName->setIsBase64((isset($_GET['base64'])) ? true : false);
				$objFileName->setIsBinary((isset($_GET['up'])) ? true : false);
				$objFileName->setEchoFileName((isset($_GET['printname'])) ? true : false);
				$Result = $objFileName->SaveFile();
				unset($objFileName);
			}

			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'/ui/user/user-config.php');
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
		######################################################################
		######################################################################
		if (isset($_POST['password']) && (isset($_POST['password-confirm'])))
		{
			if ($_POST['password'] === $_POST['password-confirm'])
			{
				$ResultPass	 = $objTplMdlUser->tblEmailsTeste->updatePassword($_POST['email'],$_POST['password'],$_POST['password-confirm']);
			}
			else
			{
				$Msg2 = CREATE_POP_MSG('Erro: As passwords devem ser iguais.');
				$Result = false;
			}
		}
	}
	######################################################################################
	$Result = $objTplMdlUser->getPerfilUser($GET_IDUSER);
	$ROW 	= $Result['ROW'];
	$EXIST 	= $Result['EXIST'];

	if ($EXIST)
	{
		$sKey = $objTplMdlUser->tblEmailsTeste->encryptVar(date('h:i:s').'*'.$GET_IDUSER);
		$objTplMdlUser->addLeftMenuItem(array("titulo" => "Guardar", 	  "href" => "/", "javascript" => "ValidateForm('user-perfil-edit');"));
		$objTplMdlUser->addLeftMenuItem(array("titulo" => "Remover Foto", "href" => "/", "javascript" => "REMOVE_IMAGEFILE('user','".$sKey."','unset');"));

		$objTplMdlUser->objTemplate->setVar('{$imgsPath}', SETPATH('URL','PATH_APP_IMG_ICONS'));
		$objTplMdlUser->objTemplate->setVar('{$FormAction}', htmlentities($_SERVER['PHP_SELF']));
		$objTplMdlUser->objTemplate->setVar('{$foto-url}',	$ROW[0]['imgFotoSrc']);
		$objTplMdlUser->objTemplate->setVarArray('{field:','}',$ROW[0]);
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('Não existem Registos!','Nova Entidade','novo.php');
		$objTplMdlUser->objTemplate->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}

	$objTplMdlUser->finishTemplate();

	$HTML 		 = $objTplMdlUser->getSHTML();
	$arrLeftMenu = $objTplMdlUser->getLeftMenuItem();
	unset($objTplMdlUser);

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo (isset($Msg)) ? $Msg : '';
	echo (isset($Msg2)) ? $Msg2 : '';
	echo $HTML;
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
?>