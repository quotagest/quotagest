<?php 
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblEntidades.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	############################################################################
	$GET_PAGE 	= (isset($_GET['page'])) ? $_GET['page'] : '0';
	$orderby 	= (isset($_GET['ordby'])) ? $_GET['ordby'] : 'ASC';
	$fldord 	= (isset($_GET['fld'])) ? $_GET['fld'] : '';

	$tblEntidades = new dbTblEntidades();
	$tblEntidades->set_IDUSER($id_user);
	$tblEntidades->set_IDASSOC($id_assoc);
	$Result = $tblEntidades->getList($fldord,$orderby,$GET_PAGE);
	unset($tblEntidades);
	
	$ROW    = $Result['ROW'];
	$EXIST  = $Result['EXIST'];

	#############################################################################
	$orderby_nome 	= 'ASC';
	$orderby_abrev 	= 'ASC';
	$orderby_tipo 	= 'ASC';
	if ($fldord !== '')
	{
		$orderby 	= ($orderby === 'ASC') ? 'DESC' : 'ASC';

		switch ($fldord)
		{
			case 'nome'		: $orderby_nome 	= $orderby; break;
			case 'abrev'	: $orderby_abrev 	= $orderby; break;
			case 'tipo'		: $orderby_tipo 	= $orderby; break;
			default: break;
		}
	}
	#############################################################################
	#############################################################################
	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_ENTIDADES').'listagem.html');
	$objTemp->setVar('{$titulo}', 'Editar perfil da Entidade');
	$objTemp->setVar('{$ajuda}', 'Utilize a pesquisa para encontrar rápidamente a Entidade que procura.');

	if ($EXIST)
	{
		############################### MENU ESQUERDO ###############################
		$arrLeftMenu = array('1' => array("titulo"  => "Nova Entidade",
										  "href"    => "/ui/entidades/novo.php")
							);
		################################# LISTAGEM ##################################
		$objTemp->BuildRows('id=linhas','{field:','}',$ROW);
		$objTemp->setVar('{$PATH_SELF_URL}',SETPATH('URL','PATH_APP_UI_ENTIDADES').'listagem');
		$objTemp->setVar('{$orderby-nome}',$orderby_nome);
		$objTemp->setVar('{$orderby-abrev}',$orderby_abrev);
		$objTemp->setVar('{$orderby-tipo}',$orderby_tipo);
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('Não existem Registos!','Nova Entidade','novo.php');
		$objTemp->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
	#############################################################################
?>