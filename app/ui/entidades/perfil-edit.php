<?php 
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblEntidades.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	############################################################################
	$GET_ID = (isset($_GET['id'])) ? $_GET['id'] : '';
	$Msg 	= '';

	$tblEntidades = new dbTblEntidades();
	$tblEntidades->set_IDENTIDADE($GET_ID);
	$tblEntidades->set_IDUSER($id_user);
	$tblEntidades->set_IDASSOC($id_assoc);
	############################## GUARDAR REGISTO ##############################
	if (isset($_POST['Nome']))
	{
		$Result = $tblEntidades->updateEntidade($_POST);

		if ($Result)
		{
			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'/ui/entidades/listagem.php');
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
	}
	############################## RECEBER REGISTO ##############################
	$Result = $tblEntidades->getPerfilEditEntidade();
	unset($tblEntidades);
	
	$ROW 	= $Result['ROW'];
	$EXIST 	= $Result['EXIST'];
	#############################################################################
	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_ENTIDADES').'perfil-edit.html');
	$objTemp->setVar('{$titulo}', 'Editar perfil da Entidade');
	$objTemp->setVar('{$ajuda}', 'Edite as informações básicas do perfil da Entidade.');

	if ($EXIST)
	{
		############################### MENU ESQUERDO ###############################
		$arrLeftMenu = array('1' => array("titulo" 	=> "Guardar",
										  "href" => "/",
										  "javascript" => "ValidateForm('entidade-perfil-edit');"),
							 '2' => array("titulo" 	=> "Voltar",
										  "href" => "/ui/entidades/perfil.php".'?id='.$GET_ID)
							);
		################################ FORMULARIO ################################
		$objTemp->setVarArray('{field:','}',$ROW[0]);
		$objTemp->setVar('{$FormAction}', htmlentities($_SERVER['PHP_SELF']).'?id='.$GET_ID);
		$objTemp->setVar('{$reg-update}', getLastDateEdit($ROW[0]['data_alterado']));
		$objTemp->setVar('{$LINK_ANULAR}', 'perfil.php?id='.$GET_ID);
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('O Registo não existe!','Voltar','listagem.php');
		$objTemp->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $Msg;
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
	#############################################################################
?>