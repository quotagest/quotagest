<?php 
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblEntidades.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	############################################################################
	$GET_ID = (isset($_GET['id'])) ? $_GET['id'] : '';

	$tblEntidades = new dbTblEntidades();
	$tblEntidades->set_IDENTIDADE($GET_ID);
	$tblEntidades->set_IDUSER($id_user);
	$tblEntidades->set_IDASSOC($id_assoc);
	$Result = $tblEntidades->getPerfilEntidade();
	unset($tblEntidades);
	
	$ROW 	= $Result['ROW'];
	$EXIST 	= $Result['EXIST'];
	#############################################################################
	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_ENTIDADES').'perfil.html');
	$objTemp->setVar('{$titulo}', 'Informação da Entidade');
	$objTemp->setVar('{$ajuda}', 'Informação detalhada sobre a entidade.');
	
	if ($EXIST)
	{
		############################### MENU ESQUERDO ###############################
		$URL = (isset($GET_ID) && ($GET_ID !== '')) ? "?ids=".$GET_ID : '';

		$arrLeftMenu = array('1' => array("titulo"	=> "Voltar",
										  "href" 	=> "/ui/entidades/listagem.php")
							);
		################################ FORMULARIO ################################
		$objTemp->setVarArray('{field:','}',$ROW[0]);
		$objTemp->setVar('{$reg-update}', getLastDateEdit($ROW[0]['data_alterado']));
		$objTemp->setVar('{$edit-link}', 'perfil-edit.php?id='.$GET_ID);
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('O Registo não existe!','Voltar','listagem.php');
		$objTemp->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
	#############################################################################
?>