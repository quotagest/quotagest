<?php 
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblEntidades.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');
	
	############################## GUARDAR REGISTO ##############################
	$Msg = '';
	if (isset($_POST['Nome']))
	{
		$tblEntidades = new dbTblEntidades();
		#$tblEntidades->set_IDENTIDADE($GET_ID);
		$tblEntidades->set_IDUSER($id_user);
		$tblEntidades->set_IDASSOC($id_assoc);
		$Result = $tblEntidades->addEntidade($_POST);
		unset($tblEntidades);

		if ($Result)
		{
			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'/ui/entidades/listagem.php');
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
	}
	############################################################################
	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_ENTIDADES').'novo.html');
	$objTemp->setVar('{$titulo}', 'Nova Entidade');
	$objTemp->setVar('{$ajuda}', 'Insira um novo registo de uma Entidade.');
	
	############################### MENU ESQUERDO ###############################
	$arrLeftMenu = array('1' => array("titulo" 	=> "Voltar",
									  "href" => "/ui/entidades/listagem.php"),
						 '1' => array("titulo" 	=> "Guardar e Fechar",
									  "href" => "/",
									  "javascript" => "ValidateForm('nova-entidade');")
						);
	################################ FORMULARIO ################################

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $Msg;
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
	#############################################################################
?>