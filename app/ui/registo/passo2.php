<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
?>

<div id="corpo" class="blue">
	<section>
		<h2 class="titulo">Adicionar Modalidades</h2>
		<p class="ajuda">Adicione as suas principais modalidades.</p>
		<div class="coluna">
		
			<fieldset class="fieldset">
				<legend>Adicionar modalidade</legend>
				<form name="form-modalidade">
					<input class="input block" type="text" name="Nome" placeholder="Nome da modalidade" autocomplete="off" />
				</form>
				<span class="botoes">
					<a class="botao novo" href="#" onclick="InserirLI('form-modalidade','modalidade-new','Nome'); DONTSCROLL(); return false;">adicionar</a>
				</span>
			</fieldset>
			<ul class="lista" data-name="modalidade-new">
			<!--
				<li>
					<a href="#" data-fields="Nome" onclick="EditarLI(this,'rUH4KjEWNusf6nW_NvAAqr1cnpqwTLmBiBxgnXcDA7A'); DONTSCROLL(); return false;">Natação</a>
				</li>
				<li>
					<a href="#" data-fields="Nome" onclick="EditarLI(this,'gBEKQxC9ge3cgrEWkTP6cT3bzoNf-p7Unqsos5yr5Oc'); DONTSCROLL(); return false;">Paintball</a>
				</li>
			-->
			</ul>
			<span class="info block">
				Pode a qualquer altura editar as modalidades.
			</span>
			<span class="botoes center">
				<a class="botao navegar" href="<?php echo SETPATH('URL','PATH_APP_UI_CFG'); ?>cfg-editar-dados">continuar</a>
			</span>
		</div>
	</section>
</div>
	
<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>