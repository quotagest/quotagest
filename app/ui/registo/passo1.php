<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlAssociacao.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjUploadFile.class.php');

	############################################################################
	$DO_LOGIN       = (!isset($_SESSION['id_user']));
	$INLOGINPAGE    = false;
	$SHOWMENU       = true;
	$NEW_ACCOUNT    = true;
	############################################################################
	$Msg = '';

	$tplAssocicao = new tplmdlAssociacao();
	$tplAssocicao->setTplFileName('passo1.html');
	$tplAssocicao->setTplFolderPath(SETPATH('ROOT','PATH_APP_UI_REGISTO'));
	$tplAssocicao->setObjTemplate($tplAssocicao->tplFolderPath.$tplAssocicao->tplFileName);
	$tplAssocicao->setTitulo('Registo de Nova Associação');
	$tplAssocicao->setAjuda('Bem vindo ao QuotaGest, por favor indique os dados da sua associação.');

	$tplAssocicao->tblAssociacao->set_IDUSER($id_user);
	$tplAssocicao->tblAssociacao->set_IDASSOC(0);
	$HTML_TIPOACTIVIDADE = $tplAssocicao->getTipoActividade_SELECT(-1);

	$tplAssocicao->addLeftMenuItem(array("titulo" => "Continuar", "href" => "/", "javascript" => "ValidateForm('novo-registo-assoc');"));

	$tplAssocicao->objTemplate->setVar('{$FORMAction}', htmlentities($_SERVER['PHP_SELF']));
	$tplAssocicao->objTemplate->setVar('{$imgsPath}', SETPATH('URL','PATH_APP_IMG_ICONS'));
	$tplAssocicao->objTemplate->setVar('{$HTML_TIPOACTIVIDADE}', $HTML_TIPOACTIVIDADE);
	$tplAssocicao->objTemplate->setVar('{$foto-url}',	SETPATH('URL','PATH_APP_IMG_ASSOCS').'quotagest-logo.png');

	if (isset($_POST['Nome']))
	{
		#$_POST['Nome']				= '';
		$_POST['Abreviatura']		= false;
		#$_POST['NIF']				= '';
		$_POST['NIB']				= false;
		#$_POST['Telefone']			= '';
		$_POST['Telemovel']			= false;
		$_POST['Fax']				= false;
		#$_POST['Email']			= '';
		$_POST['Site']				= false;
		$_POST['Outro1']			= false;
		$_POST['Outro2']			= false;
		#$_POST['Morada']			= '';
		#$_POST['Codigo-Postal']	= '';
		$_POST['Observacoes']		= false;
		$_POST['Slogan']			= false;
		$_POST['DescricaoHistoria']	= false;
		$_POST['TipoActividade']	= false;


		$Result = $tplAssocicao->tblAssociacao->addAssociacao($_POST);

		if ($Result['Result'])
		{
			$_SESSION['id_assoc']   = $Result['NEW_ID'];
			$_SESSION['nome_assoc'] = $_POST['Nome'];

			$ID_ASSOC = $tplAssocicao->tblAssociacao->encryptVar($Result['NEW_ID']);

			if (isset($_POST['file-name']))
			{
				$objFileName = new TobjUploadFile();
				$objFileName->setFileName(SETPATH('ROOT','PATH_APP_IMG_ASSOCS').$ID_ASSOC.'.jpg');
				$objFileName->setTempFileName(SETPATH('ROOT','PATH_APP_TMP').$_POST['file-name']);
				$objFileName->setMaxSize(524288);
				$objFileName->setFileType('image');
				$objFileName->setInputFileName('upload-image');
				$objFileName->setMaxWidth('255');
				$objFileName->setMaxHeigth('255');
				$objFileName->setIsBase64((isset($_GET['base64'])) ? true : false);
				$objFileName->setIsBinary((isset($_GET['up'])) ? true : false);
				$objFileName->setEchoFileName((isset($_GET['printname'])) ? true : false);
				$Result = $objFileName->SaveFile();
				unset($objFileName);

				if (file_exists(SETPATH('ROOT','PATH_APP_IMG_ASSOCS').$ID_ASSOC.'.jpg'))
				{
					$_SESSION['img_assoc'] = SETPATH('URL','PATH_APP_IMG_ASSOCS').$ID_ASSOC.'.jpg';
					$img_assoc = $ID_ASSOC.'.jpg';
				}
			}

			$Msg = CREATE_POP_MSG('Associação criada com sucesso',500,'passo2.php');
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
	}

	$tplAssocicao->finishTemplate();
	$HTML        = $tplAssocicao->getSHTML();
	$arrLeftMenu = $tplAssocicao->getLeftMenuItem();
	unset($tplAssocicao);

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $Msg;
	echo $HTML;
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
?>