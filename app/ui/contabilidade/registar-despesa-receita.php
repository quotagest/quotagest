<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	$titulo = 'Gestão | QuotaGest';
	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
?>
<div id="corpo">
	<h2 class="titulo">Registar Receita/Despesa</h2>
    <p class="ajuda">Utilize a pesquisa para encontrar rápidamente os sócios que procura.</p>

	<form class="formulario" action="#" method="POST" name="novo-outro-despesareceita">
			
		<div class="tags">
			<input type="radio" name="tipo-conta" id="tipo-conta-1" value="Receita" checked="checked" required>
			<label for="tipo-conta-1">Receita</label>
	  
			<input type="radio" name="tipo-conta" id="tipo-conta-2" value="Despesa" required>
			<label for="tipo-conta-2">Despesa</label>
		</div>

		<div class="tags">
			<input type="radio" name="tipo-titular" id="tipo-titular-1" value="Entidade" checked="checked" required>
			<label for="tipo-titular-1">Entidade</label>
	  
			<input type="radio" name="tipo-titular" id="tipo-titular-2" value="Sócio" required>
			<label for="tipo-titular-2">Sócio</label>

			<input type="radio" name="tipo-titular" id="tipo-titular-3" value="Outro" required>
			<label for="tipo-titular-3">Outro</label>			
		</div>
		
		<div class="bot-plus">
			<input name="Descricao" placeholder="Descrição" type="text"  maxlength="80" required >
			<span class="bot-plus"></span>
			<div class="plus-menu">
				<span data-id="NumDocumento" 	onClick="ShowField(this,true);">Nº Documento</span>
				<span data-id="Observacoes" 	onClick="ShowField(this,true);">Observações</span>
			</div>
		</div>
		
		<div class="line">
			<label>Montante:</label>
			<input name="Montante" class="pequeno" placeholder="0.00 €" type="text"  maxlength="15" >
		</div>
		<div data-id="NumDocumento" class="bot-plus pequeno hidden">
			<input name="NumDocumento" placeholder="Nº de Documento" type="text" class="pequeno" maxlength="20" required >
            <span  class="bot-minus"></span>
		</div>
		<div class="line">
			<select name="Tipo-Documento" class="select" placeholder="Tipo Doc." >
				<option value="Tipo Doc." disabled selected>Tipo de Documento</option>		
				<option value="1">Factura</option>
				<option value="2">Guia de Remessa</option>
				<option value="3">Nota de Encomenda</option>
				<option value="4">Nota de Débito</option>
				<option value="5">Nota de Crédito</option>
				<option value="6">Recibo</option>
				<option value="7">Carta Comercial</option>
				<option value="8">Outro</option>
			</select>	
		
			<select name="Metodo-Pagamento" class="select" placeholder="Método de Pagamento" >
				<option value="Outro-Filtro" disabled selected>Método de Pagamento</option>		
				<option value="1">Dinheiro</option>
				<option value="2">Pontos</option>
				<option value="3">Transferência Bancária</option>
				<option value="4">Vale de Desconto</option>
			</select>
		</div>
				
		<div class="line bot-plus">
			<label for="data-pagamento">Data Pagamento:</label>
			<input type="date" name="data-pagamento">
            <span class="bot-plus"></span>
			<div class="plus-menu">
				<span data-id="data-emissao" onClick="ShowField(this,true);">Data Emissão</span>
				<span data-id="data-limite" onClick="ShowField(this,true);">Data Límite</span>
			</div>
		</div>
		<div data-date="data-emissao" class="line bot-plus hidden">
			<label  name="data-emissao" for="data-emissao">Data de Emissão:</label>
			<input type="date" name="data-emissao">
            <span  class="bot-minus"></span>
		</div>
		<div> <!-- Chrome Bug Fix --> </div>
		<div data-date="data-limite" class="line bot-plus hidden">
			<label  name="data-limite" for="data-limite">Data de Límite:</label>
			<input type="date" name="data-limite">
            <span  class="bot-minus"></span>
		</div>		
		
		<div data-id="Observacoes" class="bot-plus hidden">
			<textarea name="Observacoes" placeholder="Observações/Notas" ></textarea>
			<span  class="bot-minus"></span>
		</div>

		<div class="line botoes">
			<a href="#" data-name="clear" class="botao limpar" onClick="CleanFormElements('novo-outro-despesareceita');">Limpar</a>
			<a href="#" data-name="guardar" class="botao guardar" onClick="ValidateForm('novo-outro-despesareceita');">Guardar</a>
		</div>
	
	</form>
	
<?php
		if (isset($_POST['Nome']))
		{
			include(SETPATH('ROOT','PATH_APP_CORE').'inserir_novo-outro-despesareceita.php');
		}
?>
	  
</div>

<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>