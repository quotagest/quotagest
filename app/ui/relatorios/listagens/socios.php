<?php 
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocios.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	############################################################################
	$GET_PAGE = (isset($_GET['page'])) ? $_GET['page'] : '0';

	$tblSocios = new dbTblSocios();
	$tblSocios->set_IDUSER($id_user);
	$tblSocios->set_IDASSOC($id_assoc);

	if (isset($_POST))
	{
		
		$strEstadoSocio = 'all';
		$textEstadoSocio= 'Todos';
		if (isset($_POST['EstadoSocio']))
		{
			if (($_POST['EstadoSocio'] !== 'all') && ($_POST['EstadoSocio'] !== ''))
			{
				$strEstadoSocio = array();
				$Title = '';
				foreach ($_POST['EstadoSocio'] as $key => $value)
				{
					$arrAux = explode(':',$value);
					$Title .= $arrAux[1].', ';
					$strEstadoSocio[] = $arrAux[0]; 
				}
				$Title = substr($Title, 0, strlen($Title)-2);
				$textEstadoSocio= $Title;
			}
		}

		$strTipoSocio = 'all';
		$textTipoSocio= 'Todos';
		if (isset($_POST['TipoSocio']))
		{
			if (($_POST['TipoSocio'] !== 'all') && ($_POST['TipoSocio'] !== ''))
			{
				$strTipoSocio = array();
				$Title = '';
				foreach ($_POST['TipoSocio'] as $key => $value)
				{
					$arrAux = explode(':',$value);
					$Title .= $arrAux[1].', ';
					$strTipoSocio[] = $arrAux[0]; 
				}
				$Title = substr($Title, 0, strlen($Title)-2);
				$textTipoSocio= $Title;
			}
		}

		$strActividades = '';
		$textActividades= 'Todos';
		if (isset($_POST['Actividades']))
		{
			if ($_POST['Actividades'] !== '')
			{
				$strActividades = array();
				$Title = '';
				foreach ($_POST['Actividades'] as $key => $value)
				{
					$arrAux = explode(':',$value);
					$Title .= $arrAux[1].', ';
					$strActividades[] = $arrAux[0]; 
				}
				$Title = substr($Title, 0, strlen($Title)-2);
				$textActividades= $Title;
			}
		}

		$DividaMin 		= (isset($_POST['divida-min'])) 			? $_POST['divida-min'] 				: '';
		$DividaMax 		= (isset($_POST['divida-max'])) 			? $_POST['divida-max'] 				: '';
		$EstadoQuotas 	= (isset($_POST['estado-quotas'])) 			? $_POST['estado-quotas'] 			: '';
		$DataInscBefore = (isset($_POST['datainscricao-before'])) 	? $_POST['datainscricao-before'] 	: '';
		$DataInscAfter 	= (isset($_POST['datainscricao-after'])) 	? $_POST['datainscricao-after'] 	: '';
		$NumSocioMin 	= (isset($_POST['numsocio-min'])) 			? $_POST['numsocio-min'] 			: '';
		$NumSocioMax 	= (isset($_POST['numsocio-max'])) 			? $_POST['numsocio-max'] 			: '';
##echo '<!--';
##var_dump($_POST);
##echo '-->';
		$Result = $tblSocios->filterSocios($strEstadoSocio,$strTipoSocio,$strActividades,
											$DividaMin,$DividaMax,
											$EstadoQuotas,
											$DataInscBefore,
											$DataInscAfter,
											$NumSocioMin,
											$NumSocioMax
										);
		$ROW 	= $Result['ROW'];
		$EXIST  = $Result['EXIST'];
	}
	unset($tblSocios);
	#############################################################################
	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_RELATORIOS').'/listagens/socios.html');
	$objTemp->setVar('{$titulo}', 'Relatório de Sócios');
	$objTemp->setVar('{$ajuda}', '');

	if ($EXIST)
	{
		############################### MENU ESQUERDO ###############################
		$arrLeftMenu = array('1' => array("titulo"  => "Novo Relatório",
										  "href"    => "/ui/relatorios/novo.php")
							);
		################################# LISTAGEM ##################################
		$objTemp->BuildRows('id=linhas','{field:','}',$ROW);
		$objTemp->setVar('{$socio-tipo}',$textTipoSocio);
		$objTemp->setVar('{$socio-estado}',$textEstadoSocio);
		$objTemp->setVar('{$actividades}',$textActividades);
		$objTemp->setVar('{$numsocio-min}',($NumSocioMin === '') ? '-':$NumSocioMin) ;
		$objTemp->setVar('{$numsocio-max}',($NumSocioMax === '') ? '-':$NumSocioMax);
		$objTemp->setVar('{$divida-min}',($DividaMin === '') ? '-':$DividaMin);
		$objTemp->setVar('{$divida-max}',($DividaMax === '') ? '-':$DividaMax);
		$objTemp->setVar('{$datainsc-min}',($DataInscBefore === '') ? '-':$DataInscBefore);
		$objTemp->setVar('{$datainsc-max}',($DataInscAfter === '') ? '-':$DataInscAfter);
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('Não existem Registos!','Nova Entidade','socios.php');
		$objTemp->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
	#############################################################################
?>