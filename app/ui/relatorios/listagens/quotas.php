<?php 
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblSocios.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	############################################################################
	$GET_PAGE = (isset($_GET['page'])) ? $_GET['page'] : '0';

	$tblSocios = new dbTblSocios();
	$tblSocios->set_IDUSER($id_user);
	$tblSocios->set_IDASSOC($id_assoc);
	$Result = $tblSocios->getList('','',$GET_PAGE);
	unset($tblSocios);
	
	$ROW    = $Result['ROW'];
	$EXIST  = $Result['EXIST'];
	#############################################################################
	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_RELATORIOS').'/listagens/quotas.html');
	$objTemp->setVar('{$titulo}', 'Relatório de Quotas');
	$objTemp->setVar('{$ajuda}', '');

	if ($EXIST)
	{
		############################### MENU ESQUERDO ###############################
		$arrLeftMenu = array('1' => array("titulo"  => "Novo Relatório",
										  "href"    => "/ui/relatorios/novo.php")
							);
		################################# LISTAGEM ##################################
		$objTemp->BuildRows('id=linhas','{field:','}',$ROW);
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('Não existem Registos!','Nova Entidade','socios.php');
		$objTemp->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
	#############################################################################
?>