<?php 
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplSocios.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');
	
	############################## GUARDAR REGISTO ##############################
	$Msg = '';

	$tplSocios = new tplSocios();
	#$HTML_ESTADOSOCIO 	= $tplSocios->buildSelectSocioExtra('socio_estado',0,$id_user,true);
	$HTML_ESTADOSOCIO 	= $tplSocios->buildLABLESSocioExta('checkbox','EstadoSocio','socio_estado',0,$id_assoc,true);
	$HTML_TIPOSOCIO 	= $tplSocios->buildLABLESSocioExta('checkbox','TipoSocio','socio_tipo',0,$id_assoc,true);
	$HTML_ACTIVIDADES 	= $tplSocios->getHTML_SELECTActividades(0,$id_user,$id_assoc,true);
	unset($tplSocios);
	############################################################################
	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_RELATORIOS').'novo.html');
	$objTemp->setVar('{$titulo}', 'Novo Relatório');
	$objTemp->setVar('{$ajuda}', 'Deixe os campos em branco/por selecionar para ignorar o filtro.');
	$objTemp->setVar('{$FORMAction}', '/ui/relatorios/listagens/socios.php');
	$objTemp->setVar('{$HTML_TIPOSOCIO}', $HTML_TIPOSOCIO);
	$objTemp->setVar('{$HTML_ESTADOSOCIO}', $HTML_ESTADOSOCIO);
	$objTemp->setVar('{$HTML_ACTIVIDADES}', $HTML_ACTIVIDADES);
	
	############################### MENU ESQUERDO ###############################
	$arrLeftMenu = array('1' => array("titulo" 	=> "Voltar",
									  "href" => "/ui/relatorios/listagem"),
						 '1' => array("titulo" 	=> "Guardar e Fechar",
									  "href" => "/",
									  "javascript" => "ValidateForm('novo-relatorio');")
						);
	################################ FORMULARIO ################################

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $Msg;
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
	#############################################################################
?>