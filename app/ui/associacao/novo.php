<?php 
header("Location: http://app.quotagest.com/ui/registo/passo1");
exit();
	
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');

	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblAssociacao.class.php');
	#equire_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');

	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlAssociacao.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjUploadFile.class.php');

	############################################################################
	$Msg = '';

	$objTplMdlAssocicao = new tplmdlAssociacao();
	$objTplMdlAssocicao->forkTemplate_AssociacaoNovo($id_user,$id_assoc);
	if (isset($_POST['Nome']))
	{
		$Result = $objTplMdlAssocicao->tblAssociacao->addAssociacao($_POST);

		if ($Result['Result'])
		{
			$_SESSION['id_assoc']	= $Result['NEW_ID'];
			$_SESSION['nome_assoc']	= $_POST['Nome'];

			$ID_ASSOC = $objTplMdlAssocicao->tblAssociacao->encryptVar($Result['NEW_ID']);

			if (isset($_POST['file-name']))
			{
				$objFileName = new TobjUploadFile();
				$objFileName->setFileName(SETPATH('ROOT','PATH_APP_IMG_ASSOCS').$ID_ASSOC.'.jpg');
				$objFileName->setTempFileName(SETPATH('ROOT','PATH_APP_TMP').$_POST['file-name']);
				$objFileName->setMaxSize(524288);
				$objFileName->setFileType('image');
				$objFileName->setInputFileName('upload-image');
				$objFileName->setMaxWidth('255');
				$objFileName->setMaxHeigth('255');
				$objFileName->setIsBase64((isset($_GET['base64'])) ? true : false);
				$objFileName->setIsBinary((isset($_GET['up'])) ? true : false);
				$objFileName->setEchoFileName((isset($_GET['printname'])) ? true : false);
				$Result = $objFileName->SaveFile();
				unset($objFileName);
			}

			if (file_exists(SETPATH('ROOT','PATH_APP_IMG_ASSOCS').$ID_ASSOC.'.jpg'))
				$_SESSION['img_assoc'] = SETPATH('ROOT','PATH_APP_IMG_ASSOCS').$ID_ASSOC.'.jpg';

			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'perfil.php');
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
	}
	$HTML 		 = $objTplMdlAssocicao->getSHTML();
	$arrLeftMenu = $objTplMdlAssocicao->getLeftMenuItem();
	unset($objTplMdlAssocicao);

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $Msg;
	echo $HTML;
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');

/*
	############################## GUARDAR REGISTO ##############################
	#$tblAssociacao = new dbTblAssociacao();
	$Msg = '';
	if (isset($_POST['Nome']))
	{
		$Result = $tblAssociacao->INSERT($_POST,$id_user);

		if ($Result['Result'])
		{
			$_SESSION['id_assoc']	= $Result['NEW_ID'];
			$_SESSION['nome_assoc']	= $_POST['Nome'];

			$ID_ASSOC = encrypt($Result['NEW_ID'],'N5KyEXAnDdwGL4eV');
			include(SETPATH('ROOT','PATH_APP_CORE').'upload_associacaologo_temp.php');
			if (file_exists(SETPATH('ROOT','PATH_APP_IMG_ASSOCS').$ID_ASSOC.'.jpg'))
				$_SESSION['img_assoc'] = SETPATH('ROOT','PATH_APP_IMG_ASSOCS').$ID_ASSOC.'.jpg';

			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'perfil.php');
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
	}

	$HTML_TIPOACTIVIDADE = $tblAssociacao->getTipoActividade_SELECT('null');
	unset($tblAssociacao);
	############################################################################
	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_ASSOCIACAO').'novo.html');
	$objTemp->setVar('{$titulo}', 'Registar Associação');
	$objTemp->setVar('{$ajuda}', 'Indique os dados da sua associação.');
	$objTemp->setVar('{$imgsPath}', SETPATH('URL','PATH_APP_IMG_ICONS'));
	$objTemp->setVar('{$HTML_TIPOACTIVIDADE}', $HTML_TIPOACTIVIDADE);
	$objTemp->setVar('{$foto-url}',	SETPATH('URL','PATH_APP_IMG_ASSOCS').'quotagest-logo.png');
	
	############################### MENU ESQUERDO ###############################
		$arrLeftMenu = array('1' => array("titulo" 	=> "Guardar",
										  "href" => "/",
										  "javascript" => "ValidateForm('novo-associacao');")
							);
	################################ FORMULARIO ################################

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $Msg;
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
	#############################################################################
*/
?>