<?php 
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	
	#require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblAssociacao.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplAssociacao.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjTemplate.class.php');
	#require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'functions.main.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_TPL').'tplmdlAssociacao.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'TobjUploadFile.class.php');
	############################################################################
	$Msg = '';

	$objTplMdlAssocicao = new tplmdlAssociacao();
	$objTplMdlAssocicao->tblAssociacao->set_IDUSER($id_user);
	$objTplMdlAssocicao->tblAssociacao->set_IDASSOC($id_assoc);
	if (isset($_POST['Nome']))
	{
		$Result = $objTplMdlAssocicao->tblAssociacao->updateAssociacao($_POST);
		
		if ($Result)
		{
			$ID_ASSOC = $objTplMdlAssocicao->tblAssociacao->encryptVar($id_assoc);
			if (isset($_POST['file-name']))
			{
				$objFileName = new TobjUploadFile();
				$objFileName->setFileName(SETPATH('ROOT','PATH_APP_IMG_ASSOCS').$ID_ASSOC.'.jpg');
				$objFileName->setTempFileName(SETPATH('ROOT','PATH_APP_TMP').$_POST['file-name']);
				$objFileName->setMaxSize(524288);
				$objFileName->setFileType('image');
				$objFileName->setInputFileName('upload-image');
				$objFileName->setMaxWidth('255');
				$objFileName->setMaxHeigth('255');
				$objFileName->setIsBase64((isset($_GET['base64'])) ? true : false);
				$objFileName->setIsBinary((isset($_GET['up'])) ? true : false);
				$objFileName->setEchoFileName((isset($_GET['printname'])) ? true : false);
				$Result = $objFileName->SaveFile();
				unset($objFileName);

				if (file_exists(SETPATH('ROOT','PATH_APP_IMG_ASSOCS').$ID_ASSOC.'.jpg'))
				{
					$_SESSION['img_assoc'] = SETPATH('URL','PATH_APP_IMG_ASSOCS').$ID_ASSOC.'.jpg';
					$img_assoc = $ID_ASSOC.'.jpg';
				}
			}

			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'perfil.php');
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
	}
	$objTplMdlAssocicao->forkTemplate_AssociacaoPerfilEdit($id_user,$id_assoc);
	$HTML 		 = $objTplMdlAssocicao->getSHTML();
	$arrLeftMenu = $objTplMdlAssocicao->getLeftMenuItem();
	unset($objTplMdlAssocicao);

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $Msg;
	echo $HTML;
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');

/*

	$Msg = '';
	$tblAssociacao = new tplAssociacao();
	$tblAssociacao->set_IDUSER($id_user);
	$tblAssociacao->set_IDASSOC($id_assoc);
	############################## GUARDAR REGISTO ##############################
	if (isset($_POST['Nome']))
	{
		$Result = $tblAssociacao->updateAssociacao($_POST);
		
		if ($Result)
		{
			$ID_ASSOC = $tblAssociacao->encryptVar($id_assoc);
			include(SETPATH('ROOT','PATH_APP_CORE').'upload_associacaologo_temp.php');

			$Msg = CREATE_POP_MSG('O Registo foi salvo com sucesso!',1500,'perfil.php');
		}
		else
		{
			$Msg = CREATE_POP_MSG('Não foi possivel guardar o Registo.');
		}
	}
	############################## RECEBER REGISTO ##############################
	$Result = $tblAssociacao->getPerfilEditAssociacao($id_user,$id_assoc);
	unset($tblAssociacao);
	
	$ROW 	= $Result['ROW'];
	$EXIST 	= $Result['EXIST'];
	#############################################################################
	$objTemp = new TobjTemplate(SETPATH('ROOT','PATH_APP_UI_ASSOCIACAO').'perfil-edit.html');
	$objTemp->setVar('{$titulo}', 'Editar Perfil da Associação');
	$objTemp->setVar('{$ajuda}', 'Edite as informações básicas do perfil da Associação.');
	
	if ($EXIST)
	{
		############################### MENU ESQUERDO ###############################
		$id_assoc_encrypted = encrypt($id_assoc,'N5KyEXAnDdwGL4eV');
		$sKey = encrypt((date('h:i:s').'*'.$id_assoc_encrypted),'N5KyEXAnDdwGL4eV');
		$arrLeftMenu = array('1' => array("titulo" 	=> "Guardar",
										  "href" => "/",
										  "javascript" => "ValidateForm('associacao-perfil-edit');"),
							 '2' => array("titulo" 	=> "Remover Logotipo",
										  "href" => "/",
										  "javascript" => "REMOVE_IMAGEFILE('associacao','".$sKey."','unset');"),
							 '3' => array("titulo" 	=> "Voltar",
										  "href" => "/ui/associacao/perfil.php")
							);
		################################ FORMULARIO ################################
		$objTemp->setVarArray('{field:','}',$ROW[0]);
		$objTemp->setVar('{$imgsPath}', SETPATH('URL','PATH_APP_IMG_ICONS'));
		$objTemp->setVar('{$FormAction}', htmlentities($_SERVER['PHP_SELF']));
		$objTemp->setVar('{$reg-update}', getLastDateEdit($ROW[0]['data_alterado']));
		$objTemp->setVar('{$foto-url}',	$ROW[0]['imgLogoSrc']);
	}
	else
	{
		$sMessage = CREATE_NOTFOUND('O Registo não existe!','Criar Associação','novo.php');
		$objTemp->replaceDOM('[data-id=corpo]','innertext',$sMessage);
	}

	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
	echo $Msg;
	echo $objTemp->echohtml();
	unset($objTemp);
	include($_SERVER['DOCUMENT_ROOT'].'/footer.php');
	#############################################################################
*/
?>