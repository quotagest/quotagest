<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_MODULES').'TobjModule.class.php');

	$mdlAnimais = new TobjModule();
	$mdlAnimais->setModuleName('Animais');
	$mdlAnimais->setModulePath('PATH_APP_UI_MODULES');
	$mdlAnimais->setModuleFolder('animais/');
	$mdlAnimais->setModuleCSS('style.css');
	$mdlAnimais->setModuleJS('script.css');
	$mdlAnimais->setModuleTopMenu('Animais','listagem.php','','');
	$mdlAnimais->addModuleMenuItem('Lista de Animais','listagem.php');
	$mdlAnimais->addModuleMenuItem('Novo Animal','novo.php');
	$topmenu_modules[]  = $mdlAnimais->getModuleTopMenu();
	$CSS_files[] 		= $mdlAnimais->getModuleCSS();
	$JS_files[] 		= $mdlAnimais->getModuleJS();
	unset($mdlAnimais);
?>