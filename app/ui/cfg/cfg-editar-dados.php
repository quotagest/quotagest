<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
?>
<div id="corpo">
	<section id="configuracoes">
		<h2 class="titulo">Configurar Pagamentos</h2>
		<p class="ajuda">Aqui pode ver e editar as formas e opções de pagamento.</p>
		<!--
		<div class="ctrlz">
			Informação Variável! <a href="lol" accesskey="z">anular <em>(alt+Z)</em></a> 
		</div>
		-->
<?php
	echo '<!--';
		/* ************************************************************************************ */
		require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
		require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');

		function ColunasLI($sTitulo, $sULDataName, $tblName, $aFieldsGet, $aFieldsWhereName, $aFieldsWhereValue, $sFieldsName, $aFieldsType)
		{
			$tbl1 = new dbTable();
			#$sDataFields 	= implode(';',$sFieldsName);
			#$sFields 		= implode(',',$aFieldsGet);
			#$sWhere  		= $tbl1->SQLWHERE_FieldsBuilder($aFieldsWhereName,$aFieldsWhereValue);
			#$SQLstr  		= 'SELECT '.$sFields.' FROM '.$tblName.' WHERE '.$sWhere.' ORDER BY nome ASC;';	
			#$ROW = $tbl1->GET_VALUES_SELECT($SQLstr,$aFieldsGet);
			$ROW = $tbl1->SQL_SELECT($tblName, $aFieldsGet, $aFieldsWhereName, $aFieldsWhereValue,'','ORDER BY nome ASC');
			unset($tbl1);

			$html = '';
			if (isset($ROW[0]))
			{
				$sDataFields = implode(';',$sFieldsName);

				$ccI = count($ROW);
				$ccJ = count($aFieldsGet);
				for ($i=0; $i<$ccI; $i++)
				{
					$id_encrypted 	= encrypt($ROW[$i]['id'],'N5KyEXAnDdwGL4eV');
					for ($j=1; $j<$ccJ; $j++)
					{
						$fv = $ROW[$i][$aFieldsGet[$j]];
						switch ($aFieldsType[$j-1])
						{
							case "string" : $fv = utf8_encode($fv); 					break;
							case "float"  : $fv = number_format($fv, 2, ',', ' ').' €'; break;
						}

						$a[$j-1] = $fv;
					}
					$aStrA = implode(' | ',$a);
					unset($a); // This deletes the whole array

					$html .= '<li>';
					$html .= '<a href="#" data-fields="'.$sDataFields.'" onclick="EditarLI(this,'."'".$id_encrypted."'".'); DONTSCROLL(); return false;" >'.$aStrA.'</a>';
					$html .= '</li>';
				}
			}
#//FIXME: Colocar abaixo após correcção. (Campo Search)
#<input data-name="'.$sULDataName.'" class="cfg-search" type="text" name="Nome" placeholder="pesquisar" onkeyup="SearchLI(this,'."'".$sULDataName."'".');" >
#
			$htmlIni = '<div class="coluna">
							<h3>'.$sTitulo.'</h3>
							<fieldset class="fieldset">
								<legend>Adicionar '.$sTitulo.'</legend>
								<form name="form-'.$sULDataName.'">
								';
			$htmlinput = array();
			foreach ($sFieldsName as $key => $value)
			{
				$htmlinput[] = '<input class="input" type="text" name="'.$value.'" placeholder="'.$value.'" >';
			}
			$htmlIni .= implode('</br>', $htmlinput);
			$htmlIni .= '
									<!-- <input class="input" type="text" name="Nome" placeholder="Nome" > -->
									<a class="botao novo" href="#" onclick="InserirLI('."'form-".$sULDataName."'".','."'".$sULDataName."'".','."'".implode(';',$sFieldsName)."'".'); DONTSCROLL(); return false;">adicionar</a>
								</form>
							<span class="botoes">
								
							</span>
							</fieldset>   
							<ul class="lista" data-name="'.$sULDataName.'">
						';
			$htmlEnd = '
							</ul>
						</div>';
					
			return $htmlIni.$html.$htmlEnd;			
		}
		/* ************************************************************************************ */
echo '-->';
?>
<?php		
		echo ColunasLI('Métodos de Pagamento', 'pagforma', 'pag_forma', array('id','nome'), array('id_assoc','enabled'), array($id_assoc,'0'), array('Nome'), array('string'));
		#echo ColunasLI('Opções de Pagamento', 'pagopcao', 'pag_opcao', array('id','nome'), array('id_assoc','enabled'), array($id_assoc,'0'), array('Nome'), array('string'));
		echo ColunasLI('Estado de Sócio', 'socioestado', 'socio_estado', array('id','nome'), array('id_assoc','enabled'), array($id_assoc,'0'), array('Nome'), array('string'));
		echo ColunasLI('Tipo de Sócio', 'sociotipo', 'socio_tipo', array('id','nome','joia'), array('id_assoc','enabled'), array($id_assoc,'0'), array('Nome','Joia'), array('string','float'));
?>
	</section>
</div> 
<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>