<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	$titulo = 'Gestão | QuotaGest';
	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
?>
<div id="corpo">

	<form class="formulario" action="#" method="POST" name="novo-socio-tipo">
		<h2>Novo Tipo de Sócio <a href="#" class="close"></a></h2>
			
		<input name="Nome" placeholder="Nome Tipo de Sócio" type="text" maxlength="80" required >
		<div class="line">
		   <label>Jóia:</label>
		   <input name="Joia" type="text" class="pequeno" placeholder="0.0 €" maxlength="12" required >
		</div>
			
		<div class="line botoes">
			<a href="#" data-name="clear" class="botao clear" onClick="CleanFormElements('novo-socio-tipo');">Limpar</a>
			<a href="#" data-name="guardar" class="botao ok" onClick="ValidateForm('novo-socio-tipo');">Guardar</a>
		</div>
	</form>
<?php
	
	if (isset($_POST['Nome']))
	{
		include('PATH_APP_CORE.'inserir_socio-tipo.php');
	}
	
?>
	
</div>

<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>