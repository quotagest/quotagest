<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	$titulo = 'Gestão | QuotaGest';
	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
?>
<div id="corpo">

	<form class="formulario" action="#" method="POST" name="novo-grupo-orgaossociais">
		<h2>Adicionar Grupo (Órgãos Sociais) <a href="#" class="close"></a></h2>
		
		<input name="Nome" placeholder="Nome do Grupo" type="text"  maxlength="80" required >
		<select class="line" name="Contacto-OrgaoSocial" placeholder="Contacto (Órgão Social)" required >
			<option value="Contacto (Órgão Social)" disabled selected>Contacto (Órgão Social)</option>		
			<option value="1">Telefone</option>
			<option value="2">Telemóvel</option>
			<option value="2">Fax</option>
			<option value="3">E-mail</option>
		</select>	
		
		<div class="line botoes">
			<a href="#" data-name="clear" class="botao clear" onClick="CleanFormElements('novo-grupo-orgaossociais');">Limpar</a>
			<a href="#" data-name="guardar" class="botao ok" onClick="ValidateForm('novo-grupo-orgaossociais');">Guardar</a>
		</div>
	</form>
	
<?php
		if (isset($_POST['Nome']))
		{
			include(SETPATH('ROOT','PATH_APP_CORE').'inserir_grupo-orgaossociais.php');
		}
?>
	
<div class="darken"></div>
   
</div>

<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>