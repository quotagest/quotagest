<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	$titulo = 'Gestão | QuotaGest';
	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
?>
<div id="corpo">

	<form class="formulario" action="#" method="POST" name="novo-orgao-social">
		<h2>Adicionar Órgão Social <a href="#" class="close"></a></h2>
		
		<select class="line" name="Grupo-OrgaoSocial" placeholder="Grupo (Órgão Social)" required >
			<option value="Grupo (Órgão Social)" disabled selected>Grupo (Órgão Social)</option>		
			<option value="1">Mesa da Assembleia Geral</option>
			<option value="1">Contabilidade</option>
		</select>		
		<select class="line" name="Cargo-OrgaoSocial" placeholder="Cargo (Órgão Social)" required >
			<option value="Cargo (Órgão Social)" disabled selected>Cargo (Órgão Social)</option>		
			<option value="1">Presidente</option>
			<option value="2">Secretária</option>
			<option value="3">Tesoureiro</option>
		</select>	
		<select class="line" name="Membro-OrgaoSocial" placeholder="Membro (Órgão Social)" required >
			<option value="Membro (Órgão Social)" disabled selected>Membro (Órgão Social)</option>		
			<option value="1">Clarisse Silva</option>
			<option value="2">João Fernandes</option>
			<option value="3">Joana Seixas</option>
		</select>		
			
		<div class="line botoes">
			<a href="#" data-name="clear" class="botao clear" onClick="CleanFormElements('novo-orgao-social');">Limpar</a>
			<a href="#" data-name="guardar" class="botao ok" onClick="ValidateForm('novo-orgao-social');">Guardar</a>
		</div>
	</form>
	
<?php
		if (isset($_POST['Nome']))
		{
			include(SETPATH('ROOT','PATH_APP_CORE').'inserir_orgao-social.php');
		}
?>
	
<div class="darken"></div>
   
</div>

<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>