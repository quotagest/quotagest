<?php
	include($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	$titulo = 'Gestão | QuotaGest';
	include($_SERVER['DOCUMENT_ROOT'].'/header.php');
?>
<div id="corpo">

	<form class="formulario" action="#" method="POST" name="novo-departamento">
		<h2>Novo Departamento (contacto) <a href="#" class="close"></a></h2>
			<input name="Nome" placeholder="Nome do Departamento" type="text" maxlength="80" required >

		<div class="line botoes">
			<a href="#" data-name="clear" class="botao clear" onClick="CleanFormElements('novo-departamento');">Limpar</a>
			<a href="#" data-name="guardar" class="botao ok" onClick="ValidateForm('novo-departamento');">Guardar</a>
		</div>
	</form>

<?php
	
	if (isset($_POST['Nome']))
	{
		include('PATH_APP_CORE.'inserir_departamento.php');
	}
	
?>
	
</div>

<?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>