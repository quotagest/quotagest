	// http://www.tutorialspoint.com/ajax/ajax_in_action.htm
	var xmlhttp2;
	function XMLHTTP_CREATE()
	{ 			
		var A;
		
		var axmlhttp = new Array(
			'Msxml2.XMLHTTP.5.0',
			'Msxml2.XMLHTTP.4.0',
			'Msxml2.XMLHTTP.3.0',
			'Msxml2.XMLHTTP',
			'Microsoft.XMLHTTP');
		for (var i = 0; i < axmlhttp.length; i++)
		{
			try
			{	//Internet Explorer
				A = new ActiveXObject(axmlhttp[i]);
			}
			catch (e)
			{
				A = null;
			}
		}
		if((!A) && (typeof XMLHttpRequest !== "undefined"))
			A = new XMLHttpRequest(); // Firefox, Opera 8.0+, Safari
		if (!A)
			alert("Could not create connection object.");
		return A;
	}
	//LINK: http://www.impressivewebs.com/callback-functions-javascript/
	function AJAX_REQUEST_CALLBACK(sMethod,sUrl,sParams,fCallback,async)
	{
		async = (async === false) ? false : true;
		xmlhttp2 = XMLHTTP_CREATE();
		xmlhttp2.onreadystatechange=function()
		{
			if (xmlhttp2.readyState==4 && xmlhttp2.status==200)
			{
				if (fCallback && typeof(fCallback) === "function")
					fCallback(Trim(xmlhttp2.responseText));
				else
					alert('fCallback Error: '+fCallback);
			}
		}
		
		XMLHTTP_SENDREQUEST(xmlhttp2,sMethod,sUrl,async,sParams); 	
	}
	
	function XMLHTTP_SENDREQUEST(object, method,url,async,params)
	{
		switch(method)
		{
			case 'GET' : {
							url += ((url.indexOf('?') === -1) && (params !== '')) ? '?' : '';
							console.log(url);
							//url += (url.indexOf('&') === -1) ? '&' : '';
							object.open('GET',url+params,async);
							object.send(null);
						 } break;
			case 'POST': {
							object.open("POST",url,async);
							object.setRequestHeader("Content-type","application/x-www-form-urlencoded");
							object.send(params);
						 } break;
		}
	}
	
	/*
	function XMLHTTP_CREATE()
	{
		try // Firefox, Opera 8.0+, Safari
		{
			xmlhttp=new XMLHttpRequest();
		}
		catch (e)
		{
			try //Internet Explorer
			{
				xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch (e)
			{
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
		}
		return xmlhttp;
	}
	*/