	// EXPRESSÕES REGULARES
	var arrTimeRecord = new Array();
	var SCROLL_POSY = 0;
	var SCROLL_POSX = 0;
	var TIMER;
	var INTERVAL;
	function GET_SCROLL_POS()
	{
		var doc = document.body; 
		SCROLL_POSY = doc.scrollTop;
		SCROLL_POSX = doc.scrollLeft;
		//alert(doc.scrollHeight+' - '+doc.clientHeight);
		//percentageScrolled = Math.floor((scrollPosition / pageSize) * 100); 
	}
	function DONTSCROLL()
	{
		window.scrollTo(SCROLL_POSX,SCROLL_POSY); 
	}
	
	function eventFire(el, etype)
	{
		if (el.fireEvent)
		{
			(el.fireEvent('on' + etype));
		}
		else
		{
			var evObj = document.createEvent('Events');
				evObj.initEvent(etype, true, false);
			el.dispatchEvent(evObj);
		}
	}
	function addEventHandler(elem,eventType,handler)
	{
		if (elem.addEventListener)
			elem.addEventListener(eventType,handler,false);
		else
		if (elem.attachEvent)
			elem.attachEvent('on'+eventType,handler); 
	}	

	function isEmpty(str)
	{
		return (!str || 0 === str.length);
	}
	function isBlank(str)
	{
		return (!str || /^\s*$/.test(str));
	}
	function Trim(str)
	{
		return str.replace(/^\s+|\s+$/g,'');
	}
	function isNumeric(string)
	{
		var numExpr = /^[0-9]+$/;
		if (string.match(numExpr))
			return true;
		else
			return false;
	}
	function isFloat(nValue)
	{
		nValue = parseFloat(nValue);
		if (nValue == 0)
			return true;
		else
			return (nValue != "") && (!isNaN(nValue)) && ((Math.round(nValue) == nValue) || (Math.round(nValue) != nValue));
	}
	function Trim2(aStr)
	{
		aStr = aStr.replace(/^\s+|\s+$/g,"");
		var p = 0;
		var ss = '';
		for (var i=0, cc = aStr.length; i<cc; i++)
		{			
			if (aStr[i] === " ")
			{
				p++;
			}
			else
			{
				if ((p>=1) && (i>1)) ss += ' ';
				p=0;
				ss += aStr[i];
			}
		}
		return ss;
	}
	function FloatToString(sString)
	{
		sString = sString.replace("€","");
		sString = sString.replace(",",".");
		sString = sString.replace(" ","");
		sString = Trim2(sString);
		return sString;		
	}
	function FloatToCurrency(fNum)
	{
		fNum = parseFloat(fNum);
		fNum = fNum.toFixed(2);
		fNum = "€"+numberWithSpaces(fNum);
		return fNum;
	}
	function FloatToCurrencyExtra(fNum,iFixedNum)
	{
		fNum = parseFloat(fNum);
		fNum = fNum.toFixed(iFixedNum);
		fNum = numberWithSpaces(fNum);
		return fNum;
	}
	function isArray(object)
	{
		if (object.constructor === Array) return true;
		else return false;
	}
	function numberWithSpaces(x)
	{
		var parts = x.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		return parts.join(".");
	}
	function FormatFloatDecimal(fNumber, aCasasDecimais)
	{
		return fNumber.toFixed(aCasasDecimais);	
	}
	
	function ShowMessage(sMessage,bEncode)
	{
		if (bEncode == true)
			sMessage = encodeToUnicode(sMessage);
		alert(sMessage);
	}
	if(!Array.prototype.indexOf)
	{
		Array.prototype.indexOf = function(needle)
		{
			var ilen = this.length;
			for(var i=0; i<ilen; i++)
			{
				if(this[i] === needle)
				{
					return i;
				}
			}
			return -1;
		};
	}
	
	String.prototype.replaceArray = function(find, replace)
	{
		// LINK: http://stackoverflow.com/questions/5069464/replace-multiple-strings-at-once
		var repStr = this;
		var ilen = find.length;
		for (var i = 0; i < ilen; i++)
		{
			repStr = repStr.replace(find[i], replace[i]);
		}
		return repStr;
	}
	function encodeToUnicode(sString)
	{
		// LINK: http://programandosemcafeina.blogspot.pt/2007/04/caracteres-especiais-representados-em.html
		var aChars = ["á","à","â","ã","ä","Á","À","Â","Ã","Ä",
						"é","è","ê","ê","É","È","Ê","Ë",
						"í","ì","î","ï","Í","Ì","Î","Ï",
						"ó","ò","ô","õ","ö","Ó","Ò","Ô","Õ","Ö",
						"ú","ù","û","ü","Ú","Ù","Û",
						"ç","Ç","ñ","Ñ","&","'"];
						
		var aUnics = ["\u00e1","\u00e0","\u00e2","\u00e3","\u00e4","\u00c1","\u00c0","\u00c2","\u00c3","\u00c4",
						"\u00e9","\u00e8","\u00ea","\u00ea","\u00c9","\u00c8","\u00ca","\u00cb",
						"\u00ed","\u00ec","\u00ee","\u00ef","\u00cd","\u00cc","\u00ce","\u00cf",
						"\u00f3","\u00f2","\u00f4","\u00f5","\u00f6","\u00d3","\u00d2","\u00d4","\u00d5","\u00d6",
						"\u00fa","\u00f9","\u00fb","\u00fc","\u00da","\u00d9","\u00db",
						"\u00e7","\u00c7","\u00f1","\u00d1","\u0026","\u0027"];
		
		return sString.replaceArray(aChars, aUnics);
	}
	function getCurrentTime(AsParams)
	{
		var currentTime = new Date()
		var day 	= currentTime.getDate();
		var month 	= currentTime.getMonth()+1;
		var year 	= currentTime.getFullYear();

		if (month<10)
			month = '0'+month;
		if (day<10)
			day   = '0'+day;

		AsParams = AsParams.replace("Y",year);
		AsParams = AsParams.replace("m",month);
		AsParams = AsParams.replace("d",day);
		return AsParams;
	}
	
	// EVENTOS DE TECLAS
/*
			function DetectPressedKeys(e)
			{
				var evtobj=window.event? event : e;
				if (evtobj.altKey)
					alert('Premido tecla: "ALT"');
				if (evtobj.ctrlKey)
					alert('Premido tecla: "CTRL"');
				if (evtobj.shiftKey)
					alert('Premido tecla: "SHIFT"');
					
				alert("Tecla: "+e.charCode+"\n"+String.fromCharCode(e.charCode));
			}
			function KeyPressed(e,fCallback)
			{
				var evtobj 		= (window.event) ? event : e;
				var CharCode	= (evtobj.charCode) ? evtobj.charCode : evtobj.keyCode;
				var ASCIIKey	= String.fromCharCode(CharCode);
				
				alert('['+CharCode+'] '+ASCIIKey);
				if ((evtobj.altKey) || (CharCode === 18))
					alert('ALT');
				else
				if ((evtobj.ctrlKey) || (CharCode === 17))
					alert('CTRL');
				else
				if ((evtobj.shiftKey) || (CharCode === 16))
					alert('SHIFT');
					
				if (fCallback && typeof(fCallback) === "function")
					fCallback(Trim(xmlhttp2.responseText));
				else
					alert('fCallback Error: '+fCallback);
			}
			document.onkeyup = KeyPressed;	// COLOCAR NO DOCUMENTO EM QUESTAO		
*/
	// ----- FUNÇÕES PROCURAR ELEMENTOS
			function AddToClass(eElem,sString)
			{
				eElem.className = eElem.className+" "+sString;
			}
			function RemoveFromClass(eElem,sString)
			{
				ReplaceFromClass(eElem,sString,"");
			}
			function ReplaceFromClass(eElem,sString,sStrToReplace)
			{
				var regexSpace = new RegExp(" "+sString, "gi");
				var regexNoSpace = new RegExp(sString, "gi");
				var sClassName = eElem.className;
					sClassName = eElem.className.replace(regexSpace,sStrToReplace);
					sClassName = eElem.className.replace(regexNoSpace,sStrToReplace);
				eElem.className = Trim(sClassName);
			}

			function GetObjectByID(AName)
			{
				return document.getElementById(AName);
			}
			function GetObjectsByName(AName)
			{
				return document.getElementsByName(AName);
			}
			function GetObjectByName(AName,AIndex)
			{
				return document.getElementsByName(AName)[AIndex];
			}
			function GetObjectByClassName(AName,AIndex)
			{
				return document.getElementsByClassName(AName)[AIndex];
			}
			function GetObjectsByTagName(aTagName)
			{
				var arrAux = document.getElementsByTagName(aTagName);
				return arrAux;
			}
			function FindParentFORMName(eObject)
			{
				// LINK: http://www.randomsnippets.com/2008/06/26/how-to-find-and-access-parent-nodes-via-javascript/
				if (eObject)
				{
					var testObj = eObject.parentNode;
					while (testObj != undefined)
					{
						if (testObj.tagName != 'FORM')
							testObj = testObj.parentNode;
						else
							return testObj.getAttribute('name');
					}
				}
				
				return '';
			}
			function GetObjectsByTagNameData(aTagName, aDataName, aName)
			{
				var arrObjs = document.getElementsByTagName(aTagName);
				var ilen	= arrObjs.length;
				for (i=0; i<ilen; i++)
				{
					var eObj = arrObjs[i];
					
					if (eObj.getAttribute(aDataName) === aName)
					{
						return eObj;
					}
				}
				return false;
			}
			function SetAttElementForm(form_name,input_name,att,attvalue)
			{
				var Field = document.forms[form_name].elements[input_name];
				Field.setAttribute(att,attvalue);
			}
			function SetAttElementByID(id_element,att,attvalue)
			{
				var Elem = GetObjectByID(id_element);
				Elem.setAttribute(att,attvalue);
			}
			function SetAttElementObject(eObject,att,attvalue)
			{
				eObject.setAttribute(att,attvalue);
			}
			function RemoveAttElementObject(eObject,att)
			{
				eObject.removeAttribute(att);
			}
			function createObjElement(sType,arrAttName,arrAttValue,oParentElem)
			{
				if (oParentElem == undefined) oParentElem = document;
				
				var oElem = oParentElem.createElement(sType);
				for (var i=0, ccI=arrAttName.length; i<ccI; i++)
				{
					if (arrAttName[i] !== "")
						oElem.setAttribute(arrAttName[i],arrAttValue[i]);
				}
				
				return oElem;
			}
			function CleanElement(objElement)
			{
				while(objElement.firstChild)
				{
					objElement.removeChild(objElement.firstChild);
				}
			}
	// EXPRESSÕES PERSONALIZADAS
			function CREATE_POP_MSG(sDescription,tTimeLength,bRefreshAtEnd)
			{
				tTimeLength 	= (tTimeLength != undefined) ? tTimeLength : 8000;
				bRefreshAtEnd 	= (bRefreshAtEnd != undefined) ? bRefreshAtEnd : false;

				var objMSG = document.getElementsByClassName("ctrlz")[0];
				if (objMSG == undefined)
				{
					objMSG = document.createElement("div");
					objMSG.setAttribute('class','ctrlz');
					var spanMessage = document.createElement("span");
						spanMessage.setAttribute("data-id",'message');
						spanMessage.innerHTML = sDescription+" ";
					var spanTimeOut	= document.createElement("span");
						spanTimeOut.setAttribute("data-id",'timeout');
						spanTimeOut.innerHTML = "("+(tTimeLength/1000)+")";

					objMSG.appendChild(spanMessage);
					objMSG.appendChild(spanTimeOut);
				}
				else
				{
					var spanTimeOut = GetObjectsByTagNameData('span','data-id','timeout');
					if (spanTimeOut == undefined)
					{
						spanTimeOut	= document.createElement("span");
						spanTimeOut.setAttribute("data-id",'timeout');
						spanTimeOut.innerHTML = "("+(tTimeLength/1000)+")";
						objMSG.appendChild(spanTimeOut);
					}
					else
					{
						spanTimeOut.innerHTML = "("+(tTimeLength/1000)+")";
					}

					objMSG.innerHTML += '<br/>'+sDescription;
				}

				var eBody = document.getElementsByTagName('BODY')[0];
					eBody.appendChild(objMSG);

				clearTimeout(TIMER);
				TIMER = setTimeout(function(){DESTROY_POP_MSG(objMSG);},tTimeLength);
				clearInterval(INTERVAL);
				INTERVAL = setInterval(function(){ 	var spanTimeOut = GetObjectsByTagNameData('span','data-id','timeout');
													var str = spanTimeOut.innerHTML;
														str = str.substr(1,str.length-2);
													spanTimeOut.innerHTML = "("+(str-1)+")";
												},1000);

				if (bRefreshAtEnd !== false)
				{
					var tt1 = setTimeout(function(){window.location.href = bRefreshAtEnd;},(tTimeLength+100));
				}
			}
			function DESTROY_POP_MSG(eElement)
			{
				clearTimeout(TIMER);
				clearInterval(INTERVAL);
				var	eParent	= eElement.parentNode;
				eParent.removeChild(eElement);
				//var eScript = document.getElementById('ctrlzscript'); //ctrlzscript
				//	eParent.removeChild(eScript);
			}
	// ----- FUNÇÕES PARA TRABALHAR COM IMAGENS
			function ChangeImgSrc(vElemID,vPath)
			{
				SetAttElementByID(vElemID,'src',vPath);
			}
			function ChangeUserTumb(vElemID,vOption)
			{
				var strAux = (vOption === "Feminino") ? 'user-female-a-01.jpg' : 'user-male-a-01.jpg';
				ChangeImgSrc(vElemID,"/img/icons/users/"+strAux);
			}

	// ----- FUNÇÕES VALIDAÇÃO DE FORMS
			function GetFormElementsArray(aFormName)
			{
				var arrInputs = ''; /* CRIA ARRAY DE INPUTS*/
				var arrTitles = ''; /* CRIA ARRAY DE TITULOS*/
				var aForm	  = document.forms[aFormName];
				var aElements = aForm.elements;
				var ilen 	  = aElements.length;
				var add		  = true;
				
				for (var i=0; i<ilen; i++)
				{
					var eObj = aElements[i];
					if ((eObj.tagName === 'INPUT') ||
						(eObj.tagName === 'TEXTAREA') ||
						(eObj.tagName === 'SELECT'))
					{
						if (eObj.getAttribute("required") != undefined)
						{
							add = true;
							if (eObj.getAttribute("type") === "radio")
							{
								add = false;
								if (arrInputs.search(eObj.getAttribute("name")) == -1)
								{
									add = true;
								}
							}
							if (add)
							{
								arrInputs += eObj.getAttribute("name")+';';
								arrTitles += eObj.getAttribute("placeholder")+';';
							}
						}
					}
				}
				arrInputs = arrInputs.substring(0,arrInputs.length-1);
				arrTitles = arrTitles.substring(0,arrTitles.length-1);
				return [arrInputs,arrTitles];
			}
			function GetFormElementsArray_Normal(aFormName)
			{
				var arrInputs = ''; /* CRIA ARRAY DE INPUTS*/
				var arrTitles = ''; /* CRIA ARRAY DE TITULOS*/
				var aForm	  = document.forms[aFormName];
				var aElements = aForm.elements;
				var ilen 	  = aElements.length;
				var add		  = true;
				
				for (var i=0; i<ilen; i++)
				{
					var eObj = aElements[i];
					if ((eObj.tagName === 'INPUT') ||
						(eObj.tagName === 'TEXTAREA') ||
						(eObj.tagName === 'SELECT'))
					{
						arrInputs += eObj.getAttribute("name")+';';
						arrTitles += eObj.getAttribute("placeholder")+';';
					}
				}
				arrInputs = arrInputs.substring(0,arrInputs.length-1);
				arrTitles = arrTitles.substring(0,arrTitles.length-1);
				return [arrInputs,arrTitles];
			}
			function GetFormElementsArray_Objects(aFormName)
			{
				var aObjInput = new Array(); /* CRIA ARRAY DE INPUTS*/
				var iIndex	  = 0;
				var aForm	  = document.forms[aFormName];
				var aElements = aForm.elements;
				var ilen 	  = aElements.length;

				for (var i=0; i<ilen; i++)
				{
					var eObj = aElements[i];
					if ((eObj.tagName === 'INPUT') ||
						(eObj.tagName === 'TEXTAREA') ||
						(eObj.tagName === 'SELECT'))
					{
						aObjInput[iIndex] = eObj;
						iIndex++;
					}
				}
				return aObjInput;
			}
/*
			function CheckIfEmptyField(eFieldElem,bAlert,sFieldTitle,sFieldType)
			{
				var Result = false;
				if (Trim(eFieldElem.value) === '') 
				{
					if (bAlert)
						CREATE_POP_MSG("Por favor preencha o campo '"+sFieldTitle+"'.");
					if ((sFieldType === "INPUT") || (sFieldType === "TEXTAREA"))
						eFieldElem.value = '';
					
					eFieldElem.focus();
					
					Result = true;
				}
				return Result;
			}
			function ValidarCampos(form_name,arrFieldsNames, arrFieldsTitles)
			{
				var	Result 	= true;
				var isize 	= arrFieldsNames.length;

				for(var i=0; i< isize; i++)
				{
					var vField = document.forms[form_name].elements[arrFieldsNames[i]];
					if (CheckIfEmptyField(vField,true,arrFieldsTitles[i],"INPUT"))
						break;
				}
				return Result;
			}
*/		
	// ----- FUNÇÕES TRABALHAR COM FORM-INPUT ELEMENTOS			
			function ValidateForm(aFormName,aSubmit)
			{
				/* DEVOLVE TODOS OS CAMPOS "required" DO FORM  */
				var aux 			= GetFormElementsArray(aFormName);
				var arrFieldsNames 	= aux[0].split(";");
				var arrFieldsTitles = aux[1].split(";");
				var isize 			= arrFieldsNames.length;
				var bValid 			= true;
				var msgop			= '';

				aSubmit = (aSubmit == undefined) ? true : aSubmit;
				console.log(aSubmit);
				for(var i=0; i<isize; i++)
				{
					var vField 	= document.forms[aFormName].elements[arrFieldsNames[i]];
					bValid 		= true;
					
					if ((vField.length > 0) && (vField.tagName === undefined))
					{
						var bValid  = false;
						var jlen 	= vField.length;
						for (var j=0; j<jlen; j++)
						{
							if (vField[j].checked == true)
							{
								bValid = true;
								break;
							}
						}
						if (!bValid)
						{
							msgop = "seleccione uma das opções de";
							vField = vField[0];
						}
					}
					else
					if ((vField.length > 0) && (vField.tagName === "SELECT"))
					{
						if (vField.selectedIndex == 0)
						{
							msgop = "seleccione uma opção da lista ";
							bValid = false;
						}						
					}
					else
					if ((vField.tagName === 'INPUT') || (vField.tagName === 'TEXTAREA'))
					{
						if (Trim(vField.value) === '')
						{
							vField.value = '';
							msgop = "preencha o campo";
							bValid = false;
						}
					}
					
					if (!bValid)
					{
						CREATE_POP_MSG("Por favor "+msgop+" '"+arrFieldsTitles[i]+"'.");
						vField.focus();
						break;
					}
				}

				if ((bValid) && (aSubmit))
				{
					document.forms[aFormName].submit();
				}
			}
			function CleanFormElements(aFormName) /* ResetFormElements(aFormName); */
			{
				var arrElem	 = document.forms[aFormName].elements;
				var ilen	 = arrElem.length;
				var bRad	 = false;
				
				for (var i=0; i<ilen; i++)
				{
					var eObj = arrElem[i];
					
					/* SE O ELEMENTO FOR "RADIO" */
					if (eObj.getAttribute("type") === "radio")
					{
						if (!bRad)
						{
							eObj.checked = true;
							var eField = GetObjectsByTagNameData("LABEL","data-name",eObj.name);
							//if (eField.getAttribute("onClick"))
							if (eField.onclick != undefined)
								eField.click();
						}
						else
							eObj.checked = false;
					}
					else
					if ((eObj.tagName === "INPUT") || (eObj.tagName === "TEXTAREA"))
					{
						eObj.value = '';
					}
					else
					if (eObj.tagName === "SELECT")
						eObj.selectedIndex = 0;
				}
			}
			/* *********************************************** */
			function ChangeAttByTagData(aPEelem,aElemName,aElemDataName,aAttLocation,aAttDestName,aAttDestValue, aMsgYes,aMsgNo)
			{
				var sAtt = aPEelem.getAttribute(aAttLocation);
				var eObj = GetObjectsByTagNameData(aElemName,aElemDataName,sAtt);
				if (eObj.getAttribute(aAttDestName) !== aAttDestValue)
				{
					SetAttElementObject(eObj,aAttDestName,aAttDestValue);
					aPEelem.innerHTML = aMsgYes;
				}
				else
				{
					SetAttElementObject(eObj,aAttDestName,'');
					aPEelem.innerHTML = aMsgNo;
				}
			}
	// ----- FUNÇÕES MOSTRAR-ESCONDER ELEMENTOS
			//LINK: http://stackoverflow.com/questions/8834126/how-to-efficiently-check-if-variable-is-array-or-object-in-nodejs-v8
			function ChangeHiddenElements(aObject, aNameObj, aDataAtt, aClassValue,aRepClass, aClick)
			{
				if (aObject.getAttribute(aDataAtt) === aNameObj)
				{
					var straux = aObject.getAttribute("class");
					if (aClassValue === '')
					{
						if (straux.indexOf(aRepClass))
						{
							straux = straux.replace(aRepClass,"");
							straux = Trim2(straux);
						}
					}
					else
					{
						if (straux.indexOf(aRepClass) == -1)
						{
							straux = Trim2(straux);
							straux += " "+aRepClass;
						}					
					}
					SetAttElementObject(aObject,"class",straux);
					
					var arrChildren = aObject.children;
					var ilen = arrChildren.length;
					for (var j=0; j<ilen; j++)
					{
						var eObj2 = arrChildren[j];
						if ((eObj2.tagName === "INPUT") ||
							(eObj2.tagName === "TEXTAREA") || 
							(eObj2.tagName === "SELECT"))
							eObj2.focus();
							
						if ((eObj2.tagName === "SPAN") && (eObj2.className === "bot-minus"))
							SetAttElementObject(eObj2,"onClick",aClick);
					}
				}					
			}
			function ShowField(eObject,bValue)
			{	
				var vName 	= (eObject === Object(eObject)) ? eObject.getAttribute("data-id") : eObject;
				var clValue = (bValue == true) ? "" : "hidden";
				var boolVal	= (bValue == true) ? false : true;
				
				var opMenu 	= (bValue == true) ? "active" : "";
				var btnAtt 	= (bValue == true) ? 'ShowField("'+vName+'",'+boolVal+');' : "";
				
				var ObjMenu = GetObjectsByTagNameData("SPAN","data-id",vName);
				SetAttElementObject(ObjMenu,"class",opMenu);
				SetAttElementObject(ObjMenu,"onClick",'ShowField(this,'+boolVal+');');
				
				var arrObjsDIV = GetObjectsByTagName("DIV");
				var lenObjsDIV = arrObjsDIV.length;
				for (var i=0; i<lenObjsDIV; i++)
				{
					var eObj = arrObjsDIV[i];
					ChangeHiddenElements(eObj, vName, "data-id", clValue, "hidden", btnAtt);
					ChangeHiddenElements(eObj, vName, "data-date", clValue, "hidden", btnAtt);
				}
			}
	// ----- ----- ----- ----- ----- -----
		function InitInputsForm(aFormName)
		{
			var arrObjs = GetFormElementsArray_Objects(aFormName);
			var ccI = arrObjs.length;
			for (var i=0; i<ccI; i++)
			{
				if ( (arrObjs[i].getAttribute("data-magicinput") == undefined) ||
					 (arrObjs[i].getAttribute("data-magicinput")) === "true")
				{
					addEventHandler(arrObjs[i],"blur",function(event){ MagicInput_Blur(event); });
					if (arrObjs[i].tagName === "SELECT")
						addEventHandler(arrObjs[i],"change",function(event){ MagicInput_SelectChange(event); });
					else
						addEventHandler(arrObjs[i],"keyup",function(event){ MagicInput_KeyPress(event); });
				}
			}
		}
		function getMagicInputLabel(aElemParent,aInputName)
		{
			var aList = aElemParent.getElementsByTagName("label");
			for (var i=0, ccI=aList.length; i<ccI; i++)
			{
				if (aList[i].getAttribute("for") === aInputName)
				{
					return aList[i];
				}
			}
			return undefined;
		}
		function findMagicInput_Label(event)
		{
			var eInput  = event.target;
			var eParent = eInput.parentNode;
			var bExist	= true;
			var eLabel  = getMagicInputLabel(eParent,eInput.name);

			if (eLabel == undefined)
			{
				eLabel = document.createElement("label");
				eLabel.innerHTML = eInput.getAttribute("placeholder");
				eLabel.setAttribute("for",eInput.name);
				bExist = false;
			}

			return {"parent":eParent, "input":eInput, "label":eLabel, "exist":bExist};			
		}
		function MagicInput_KeyPress(event)
		{
			var result = findMagicInput_Label(event);

			if (result["input"].value.length > 0)
			{
				if (!result["exist"])
					result["parent"].insertBefore(result["label"],result["input"]);
			}
			else
			{
				if (result["exist"])
					result["parent"].removeChild(result["label"]);
			}
		}
		function MagicInput_SelectChange(event)
		{
			var result = findMagicInput_Label(event);

			if (result["input"].selectedIndex > 0)
			{
				if (!result["exist"])
					result["parent"].insertBefore(result["label"],result["input"]);
			}
			else
			{
				if (result["exist"])
					result["parent"].removeChild(result["label"]);
			}
		}
		function MagicInput_Blur(event)
		{

		}
	// ----- ----- ----- ----- ----- -----
			/* JAVASCRIPT --- LOGIN --- */
			function CheckLogin()
			{
				var fLogin = document.forms['login']['login'];
				var fPassw = document.forms['login']['password'];

				if (sLogin === "")
				{
					CREATE_POP_MSG("Introduza o seu Login.");
					fLogin.focus();
				}
				if (fPassw === "")
				{
					CREATE_POP_MSG("Introduza o seu Login.");
					fLogin.focus();
				}

				//var url	 	= '/core/scripts/logincheck.php';
				var url	 	= '/core/scripts/logincheck-testers.php';
				var params 	= "login="+sLogin;
					params += "password="+sPassw;
				
				var func = function(sResult)
				{
					if (sResult.indexOf("false") !== -1)
					{
						return false;
					}
					else
					{
						window.location.href = 'index.php';
						return true;
					}
				}
				AJAX_REQUEST_CALLBACK("POST",url,params,func);		
			}
				
			/* JAVASCRIPT --- QUOTAS --- */
			function UpdateFloatFields(sFormName)
			{
				var eToolTip 	= GetObjectByClassName("tooltip alert ",0);

				var fMontante 	= document.forms[sFormName].elements["Montante"].value;
				var fDesconto 	= document.forms[sFormName].elements["Desconto"].value;
				var fTotalPago 	= document.forms[sFormName].elements["TotalPago"].value;
				var eSpanTotalDivida = GetObjectsByTagNameData('span','data-id',"TotalDivida");
				var eSpanTotalPagar  = GetObjectsByTagNameData('span','data-id',"TotalPagar");

				var fMontante = fMontante.replace("","0");
				var fDesconto = fDesconto.replace("%","");
				var fDesconto = fDesconto.replace("","0");
				var fTotalPago= fTotalPago.replace("","0");

				var validPercent = (fDesconto.indexOf("€") !== -1);

				var fMontante = FloatToString(fMontante);
				var fDesconto = FloatToString(fDesconto);
				var fTotalPago= FloatToString(fTotalPago);

				var eFDesconto= document.forms[sFormName].elements["Desconto"];
				var eFMontante= document.forms[sFormName].elements["Montante"];
				var eFTotalPago= document.forms[sFormName].elements["TotalPago"];
				
				if (fTotalPago == 0.0)
					eFTotalPago.value = "0 €";
				if (fMontante > 0.0)
				{
					if (validPercent)
					{
						//(25/483)*100 = 5,175
						eFDesconto.value = FloatToCurrencyExtra( ((fDesconto/fMontante)*100) ,2)+"%";
					}
					else
					{
						if (isFloat(fMontante) && isFloat(fDesconto) && isFloat(fTotalPago))
						{
							var fTotalTotal = (fMontante - (fMontante * (fDesconto / 100)));
							var fTotalDivida = (fTotalTotal - fTotalPago);

							eSpanTotalDivida.innerHTML = FloatToCurrency(fTotalDivida);
							eSpanTotalPagar.innerHTML  = FloatToCurrency(fTotalTotal);
						}
						else
						{
							// Valor invalido!
						}
					}
					if (eToolTip.className.indexOf("hidden") === -1)
						AddToClass(eToolTip,"hidden");
				}
				else
				{
					RemoveFromClass(eToolTip,"hidden");
				}
			}
			function ReplaceParamsHREF(sParam,sReplace)
			{
				var sHREF = window.location.href;
				var sMark = (sHREF.indexOf("php?") !== -1) ? '&' : '?';

				var strAux = '';
				var iAuxPos= 0;
				var iNext  = 0;

				var bQFound = sHREF.indexOf('?'+sParam); 
				var bSFound = sHREF.indexOf('&'+sParam); 
				if ((bQFound !== -1) || (bSFound !== -1))
				{
					if (bQFound !== -1)
					{
						strAux = sHREF.substr(bQFound+1);
						iAuxPos= bQFound;
					}
					else
					if (bSFound !== -1)
					{
						strAux = sHREF.substr(bSFound+1);
						iAuxPos= bSFound;
					}

					iNext = strAux.indexOf('&');
					if (iNext !== -1)
					{
						strAux = strAux.substr(0,iNext);
					}
					else
					{
						strAux = sHREF.substr(iAuxPos+1);
					}
					sHREF = sHREF.replace(strAux,sReplace);
				}
				else
				{
					sHREF += sMark+sReplace;
				}

				return sHREF;
			}
			function SelectOption(eSelect,sTag)
			{
				var sOptionValue = eSelect.options[eSelect.selectedIndex].value;

				window.location.href=ReplaceParamsHREF(sTag+'=',sTag+'='+sOptionValue);
			}

		var liSelected;
		var liIndex;
		function KeysUpDownListagem(event,sListName)
		{
			event = event || window.event;
			var keycode = event.charCode || event.keyCode;

			//var eList 		= GetObjectsByTagNameData("span","data-id",sListName);
			var eList 		= GetObjectsByTagNameData("div","data-cod",sListName);
			if (eList)
			{
				var arrLinhas 	= eList.getElementsByClassName("linha");
				var ccLinhas	= (arrLinhas.length !== 0) ? arrLinhas.length : 0;

				if (liSelected)
				{
					liSelected = arrLinhas[liIndex];
					SetAttElementObject(liSelected,"class","linha");
				}

				if(event.which === 40)	// BAIXO
				{
					if(liSelected)
					{
						liIndex++;

						if(liIndex > ccLinhas-1)
						{
							liIndex = 1;
						}
					}
					else
					{
						liIndex	= 1;
					}
				}
				else
				if(event.which === 38)	// CIMA
				{
					if(liSelected)
					{
						liIndex--;

						if(liIndex < 1)
						{
							liIndex	= ccLinhas-1;
						}
					}
					else
					{
						liIndex	= ccLinhas-1;
					}
				}
				liSelected = arrLinhas[liIndex];
				if (liSelected)
					SetAttElementObject(liSelected,"class","linha cabecalho");
			}

			return false;
		}
		function KeysUpDownSubmitForm(event,AsFormName)
		{
			event = event || window.event;
			var keycode = event.charCode || event.keyCode;

			//console.log(event.which);

			if ((event.which == 13) && (event.shiftKey))
			{
				//console.log('SHIFT+ENTER');
				document.forms[AsFormName].submit();
			}
		}


		//##################################################################
		//##################################################################
		function checkALL(ASelf,AsPlace)
		{
			var doc = GetObjectsByTagNameData("div","data-cod",AsPlace);

			var cList = doc.getElementsByTagName('input');
			for (var i=0, ccI=cList.length; i<ccI; i++)
			{
				if (cList[i].type == 'checkbox')
				{
					cList[i].checked = ASelf.checked;
				}
			}
		}
		function ANULAR_unsetRecord(ASelf,AJSON,AsPlace)
		{
			var url 	= '';
			var params 	= '';
			var eListParent = GetObjectsByTagNameData("div","data-cod",AsPlace);
				AJSON 	= decode64(AJSON);
			var arrJSON = JSON.parse(AJSON);

			/* #################################################################### */
			for (var i=0, ccI=arrJSON['objects'].length; i<ccI; i++)
			{
				params += arrJSON['objects'][i]['value']+';';
			}
			params = params.substr(0,params.length-1);
			/* #################################################################### */
			url = '/core/scripts/unset-record.php';
			params = 'array='+params;
			params += '&place='+AsPlace;
			params += '&opera=debunk';
			params += arrJSON['extraparams']; 

			var func = function(sResult)
			{
				if (sResult.indexOf("false") !== -1)
				{
					return false;
				}
				else
				{
					//arrResults = sResult.split(';');
					var spanLinha;
					var index = 0;
					var boxes = document.getElementsByName('checkAction');
					var ccI = boxes.length;
					if (ccI>0)
					{
						for (var i=0, ccI=arrJSON['objects'].length; i<ccI; i++)
						{
							index = arrJSON['objects'][i]['index'];

							spanLinha = boxes[index];				// <checkbox>
							spanLinha = spanLinha.parentNode;	// <span>(checkbox)</span> 	.. col-short
							spanLinha = spanLinha.parentNode;	// <div>(span)</div>		.. linha
							spanLinha = spanLinha.parentNode;	// <label>(div)</label>		.. for="line1"
							spanLinha.removeAttribute("style");
						}
						DESTROY_POP_MSG(GetObjectByClassName('ctrlz',0));
					}
					return true;
				}
			}
			AJAX_REQUEST_CALLBACK("POST",url,params,func);
		}
		function unsetRecord(AsPlace)
		{
			var mrResult = confirm("Deseja remover os Registos selecionados?");

			if (mrResult == true)
			{
				var url		= '';
				var params 	= '';
				
				var boxes = document.getElementsByName('checkAction');
				var ccI = boxes.length;
				if (ccI>0)
				{
					var strIDValue = '';
					var arrLinhas 		= new Array();
					var strJSONAux = '';
					var arrStrJSON = '';
					var spanLinha;
					var k=0;

					for (var i=0, ccI=boxes.length; i<ccI; i++)
					{
						if (boxes[i].checked == true)
						{
							strIDValue = boxes[i].getAttribute('data-id');
							
							spanLinha = boxes[i];				// <checkbox>
							spanLinha = spanLinha.parentNode;	// <span>(checkbox)</span> 	.. col-short
							spanLinha = spanLinha.parentNode;	// <div>(span)</div>		.. linha
							spanLinha = spanLinha.parentNode;	// <label>(div)</label>		.. for="line1"
							spanLinha.setAttribute("style","display:none;");

							strJSONAux  = '"index":"'+(i)+'",';
							strJSONAux += '"value":"'+(strIDValue)+'"';
							arrStrJSON += '{'+strJSONAux+'},'; 

							params += strIDValue+';';
							k++;
						}
					}

					switch (AsPlace)
					{
						case 'quotas-listagem' 	: 	{
														var sSocio	= document.getElementsByName('CodSocio')[0].value;
														var eMod	= document.getElementsByName('lista-modalidade')[0];
														var sMod	= eMod.options[eMod.selectedIndex].value;

														extraparams = "&ids="+sSocio+"&idm="+sMod;
													} break;
						case 'socios-listagem'	: 	{
														extraparams = '';
													} break;
						case 'entidades-listagem':  {
														extraparams = '';
													} break;
						default 				: 	{
														url = '';
														extraparams = '';
													} break;
					}

					arrStrJSON = arrStrJSON.substr(0,arrStrJSON.length-1);	//JSON sem a ultima ','
					arrStrJSON = '{"objects":['+arrStrJSON+'],"extraparams":"'+extraparams+'"}';
					var encodedJSON = encode64(arrStrJSON);
					params = params.substr(0,params.length-1);				//params sem a ultima ';'

					url = '/core/scripts/unset-record.php';
					if (params !== '')
					{
						params = 'array='+params+'&place='+AsPlace;
						params += '&ids='+sSocio;
						params += '&idm='+sMod;
						params += '&opera=bunk';

						var func = function(sResult)
						{
		console.log('['+AsPlace+'] '+sResult);
							if (sResult.indexOf("false") !== -1)
							{
								//alert('[ERRO]: '+sResult);
								return false;
							}
							else
							{
								var objA = document.createElement("a");
									objA.setAttribute('href','javascript:void(0);');
									objA.setAttribute('accesskey','z');
									objA.setAttribute('onclick',"ANULAR_unsetRecord(this,'"+encodedJSON+"','"+AsPlace+"');");
									objA.innerHTML = "Anular";
										CREATE_POP_MSG("Registo removido. "+objA.outerHTML,20000);
									objA = null;

								return true;
							}
						}
						AJAX_REQUEST_CALLBACK("POST",url,params,func);
					}
				}
			}
		}
		//##################################################################
		//##################################################################
		//##################################################################
		//##################################################################
		//##################################################################
		function ANULAR_RegistarPagamento(ASelf,AJSON,AsPlace,AOpera)
		{
			var url 	= '';
			var params 	= '';
			var eListParent = GetObjectsByTagNameData("div","data-cod",AsPlace);
				sAJSON 	= decode64(AJSON);
			var arrJSON = JSON.parse(sAJSON);

			/* #################################################################### */
			url = '/core/scripts/registar-quotas.php';
			params = 'place='+AsPlace;
			params += '&opera='+AOpera;
			params += arrJSON['extraparams'];
			params += '&json64='+AJSON; 

			var func = function(sResult)
			{
console.log('['+AsPlace+']{'+AOpera+'} '+sResult);
				if (sResult.indexOf("false") !== -1)
				{
					return false;
				}
				else
				{
					var spanLinha;
					var divLinha;
					var arrSpanChild;
					var index = 0;
					var boxes = document.getElementsByName('checkAction');
					var ccI = boxes.length;
					if (ccI>0)
					{
						for (var i=0, ccI=arrJSON['objects'].length; i<ccI; i++)
						{
							index = arrJSON['objects'][i]['index'];

							spanLinha = boxes[index];				// <checkbox>
							spanLinha = spanLinha.parentNode;	// <span>(checkbox)</span> 	.. col-short
							spanLinha = spanLinha.parentNode;	// <div>(span)</div>		.. linha
							divLinha = spanLinha;
							//spanLinha = spanLinha.parentNode;	// <label>(div)</label>		.. for="line1"
							var arrSpanChild = divLinha.getElementsByTagName("span");

							var sData = arrJSON['objects'][i]['q_datapag'];
								sData = (sData === '') ? getCurrentTime('d-m-Y') : sData;

							var eDescricao 	= arrSpanChild[1];
							var eTotal 		= arrSpanChild[2];
							var eDivida 	= arrSpanChild[3];
							var eData 		= arrSpanChild[4];
							var eData 		= (arrSpanChild[4].innerHTML.indexOf('Pago') === -1) ? arrSpanChild[4] : arrSpanChild[5];

							if (AOpera === 'paycheck')
							{
								eDivida.setAttribute("class","col center");
								eDivida.innerHTML = '<span class="etiqueta verde">Pago</span>';
								//<span class="col center"><span class="etiqueta verde">Pago</span></span>
								//<span class="col center numero red">€20.00</span>
								eData.setAttribute("class","col data numero green");
								eData.innerHTML = sData;
								//getCurrentTime('d-m-Y');
								//<span class="col data numero ">26-08-2013</span>
								//<span class="col data numero red">03-09-2013</span>
							}
							else
							if (AOpera === 'payuncheck')
							{
								var sDivida = (arrSpanChild[4].innerHTML.indexOf('Pago') === -1) ? arrJSON['objects'][i]['q_total'] : arrJSON['objects'][i]['q_divida'];
								eDivida.setAttribute("class","col center numero red");
								eDivida.innerHTML = '€'+sDivida;
								eData.setAttribute("class","col data numero red");
								eData.innerHTML = arrJSON['objects'][i]['q_datapraz'];
							}
						}
						DESTROY_POP_MSG(GetObjectByClassName('ctrlz',0));
					}
					return true;
				}
			}
			AJAX_REQUEST_CALLBACK("POST",url,params,func);
		}
		function RegistarPagamento(AsPlace,AOpera)
		{
			var boxes = document.getElementsByName('checkAction');
			var ccI = boxes.length;
			if (ccI>0)
			{
				var mrResult = confirm("Deseja liquidar as Quotas selecionadas?");
				if (mrResult == true)
				{
					var url		= '';
					var params 	= '';
					var extraparams = '';

					var strIDValue = '';
					var arrStrJSON = '';
					var spanLinha;
					var ccK=0;

					var divLinha;
					var arrSpanChild;
					for (var i=0, ccI=boxes.length; i<ccI; i++)
					{
						if (boxes[i].checked == true)
						{
							strIDValue = boxes[i].getAttribute('data-id');
							
							spanLinha = boxes[i];				// <checkbox>
							spanLinha = spanLinha.parentNode;	// <span>(checkbox)</span> 	.. col-short
							spanLinha = spanLinha.parentNode;	// <div>(span)</div>		.. linha
							divLinha = spanLinha;
							//spanLinha = spanLinha.parentNode;	// <label>(div)</label>		.. for="line1"
							arrSpanChild = divLinha.getElementsByTagName("span");
							
							sAJSON 	= decode64(boxes[i].getAttribute("data-json"));
							var arrJSON = JSON.parse(sAJSON);

							var AddRow = false;
							var ElemPayed = (arrSpanChild[4].innerHTML.indexOf('Pago') !== -1);

							var eDescricao 	= arrSpanChild[1];
							var eTotal 		= arrSpanChild[2];
							var eDivida 	= arrSpanChild[3];
							var eData 		= (!ElemPayed) ? arrSpanChild[4] : arrSpanChild[5];

							var sData = arrJSON['q_datapag'];
								sData = (sData === '') ? getCurrentTime('d-m-Y') : sData;
							if (AOpera === 'paycheck')
							{
								if (!ElemPayed)
								{
									eDivida.setAttribute("class","col center");
									eDivida.innerHTML = '<span class="etiqueta verde">Pago</span>';
									//<span class="col center"><span class="etiqueta verde">Pago</span></span>
									//<span class="col center numero red">€20.00</span>
									eData.setAttribute("class","col data numero green");
									eData.innerHTML = sData;
									//<span class="col data numero ">26-08-2013</span>
									//<span class="col data numero red">03-09-2013</span>
									AddRow = true;
								}
							}
							else
							if (AOpera === 'payuncheck')
							{
								if (ElemPayed)
								{
									eDivida.setAttribute("class","col center numero red");
									eDivida.innerHTML = '€'+arrJSON['q_total'];
									eData.setAttribute("class","col data numero red");
									eData.innerHTML = arrJSON['q_datapraz'];
									AddRow = true;
								}
							}
							if (AddRow)
							{
								arrStrJSON = sAJSON+',';
								ccK++;
							}
						}
					}

					var sSocio	= document.getElementsByName('CodSocio')[0].value;
					var eMod	= document.getElementsByName('lista-modalidade')[0];
					var sMod	= eMod.options[eMod.selectedIndex].value;

					extraparams = "&ids="+sSocio+"&idm="+sMod;

					arrStrJSON = arrStrJSON.substr(0,arrStrJSON.length-1);	//JSON sem a ultima ','
					arrStrJSON = '{"objects":['+arrStrJSON+'],"extraparams":"'+extraparams+'"}';
					var encodedJSON = encode64(arrStrJSON);

					url = '/core/scripts/registar-quotas.php';
					if (ccK !== 0)
					{
						params = 'place='+AsPlace;
						params += '&ids='+sSocio;
						params += '&idm='+sMod;
						params += '&opera='+AOpera;
						params += '&json64='+encodedJSON;
						NotOpera = (AOpera === 'paycheck') ? 'payuncheck' : 'paycheck';

						var func = function(sResult)
						{
console.log('['+AsPlace+']{'+AOpera+'} '+sResult);
							if (sResult.indexOf("false") !== -1)
							{
								//alert('[ERRO]: '+sResult);
								return false;
							}
							else
							{
								var objA = document.createElement("a");
									objA.setAttribute('href','javascript:void(0);');
									objA.setAttribute('accesskey','z');
									objA.setAttribute('onclick',"ANULAR_RegistarPagamento(this,'"+encodedJSON+"','"+AsPlace+"','"+NotOpera+"');");
									objA.innerHTML = "Anular";
										CREATE_POP_MSG(ccK+" Quotas liquidadas. "+objA.outerHTML,20000);
									objA = null;

								return true;
							}
						}
						AJAX_REQUEST_CALLBACK("POST",url,params,func);
					}
				}
			}
		}

		//##################################################################
		//##################################################################
		/* CODIGO PARA BOTAOS (recuperar) */
/*
				var boxes = document.getElementsByName('checkAction');
				var ccI = boxes.length;
				var arrCheckboxs 	= new Array();
				//var arrIDValue 		= new Array();
				var strIDValue = '';
				var arrLinhas 		= new Array();
				//var arrBtns 		= new Array();
				//var arrBtnsLinks 	= new Array();
				//var strBtnHREF = '';
				//var arrBtnsText		= new Array();
				//var strBtnText = '';

							//arrBtns[k].innerHTML = "Recuperar (10s)";
							//arrBtns[k].removeAttribute("href");

							//strJSONAux = '"href":"'+(strBtnHREF)+'",';
							//strJSONAux += '"btntext":"'+(strBtnText)+'"';

							arrBtns[k] = spanLinha.getElementsByTagName("A")[0];
							strBtnText = arrBtns[k].innerHTML;
							strBtnHREF = arrBtns[k].getAttribute("href");

							var encodedJSON = encode64('{"objects":['+strJSON+'],"extraparams":"&ids='+sSocio+'&idm='+sMod+'"}');
							arrBtns[k].setAttribute("onclick","ANULAR_unsetRecord(this,'"+encodedJSON+"','"+AsPlace+"');");


							var eListParent = GetObjectsByTagNameData("div","data-cod",AsPlace);

							var tLeft = 19000; // 10s
							var iInterval = 1000;
							var iIntEnd	  = 20100;
							var ccI=arrCheckboxs.length;

							var tInterval = setInterval(function()
							{
								for (var i=0; i<ccI; i++)
								{
									arrBtns[i].innerHTML = "Recuperar ("+(tLeft/iInterval)+"s)";
								}
								tLeft -= iInterval;
							},iInterval);

							var tStoper = setTimeout(function()
								{
									clearInterval(tInterval);
									for (var i=0; i<ccI; i++)
									{
										eListParent.removeChild(arrLinhas[i]);
									}
									if (ccI>1)
										CREATE_POP_MSG('Registo(s) removido(s) com sucesso!',4000);
									else
										CREATE_POP_MSG('Registo removido com sucesso!',4000);
								},iIntEnd);
*/
		//##################################################################
		//##################################################################