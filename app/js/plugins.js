	// Avoid `console` errors in browsers that lack a console.
	(function() {
		var method;
		var noop = function () {};
		var methods = [
			'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
			'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
			'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
			'timeStamp', 'trace', 'warn'
		];
		var length = methods.length;
		var console = (window.console = window.console || {});

		while (length--) {
			method = methods[length];

			// Only stub undefined methods.
			if (!console[method]) {
				console[method] = noop;
			}
		}
	}());
	// Apoio Directo
	(function ()
	{
		var done    = false;
		var script  = document.createElement("script");
		script.async= true;
		script.type = "text/javascript";
		script.src  = "https://www.purechat.com/VisitorWidget/WidgetScript";
		document.getElementsByTagName('HEAD').item(0).appendChild(script);
		script.onreadystatechange = script.onload = function (e)
		{
			if (!done && (!this.readyState || this.readyState == "loaded" || this.readyState == "complete"))
			{
				var w = new PCWidget({ c: '25d2abd4-944d-4e9e-8b32-76c092e0de96', f: true });
				done = true;
			}
		};
	})();

// Place any jQuery/helper plugins in here.
