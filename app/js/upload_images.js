	var funcUploadImgFile = function(sResult)
	{  
		var aMaxFileSize = 524288;
		var aImgShow 	 = 'imgUserTumb';
		var aTargetPHP   = '/core/upload_temp_file.php';
		var aStatus		 = 'status';
		var sSize		 = 0;

		if (fFile.size <= aMaxFileSize)
		{
			var funcAfter = function(sResult)
			{
				if (sResult.indexOf("done") !== -1)
				{
					var objINPUT = document.createElement("input");
						objINPUT.setAttribute('type','hidden');
						objINPUT.setAttribute('name','file-name');
						objINPUT.setAttribute('value',sResult.replace("done",""));

					var fForm = undefined;
					if ((document.forms['novo-socio']))
						fForm = document.forms['novo-socio'];
					else
					if ((document.forms['socio-perfil-edit']))
						fForm = document.forms['socio-perfil-edit'];
					else
					if ((document.forms['novo-associacao']))
						fForm = document.forms['novo-associacao'];
					else
					if ((document.forms['associacao-perfil-edit']))
						fForm = document.forms['associacao-perfil-edit'];
					else
					if ((document.forms['novo-registo-assoc']))
						fForm = document.forms['novo-registo-assoc'];
					else
					if ((document.forms['user-perfil-edit']))
						fForm = document.forms['user-perfil-edit'];
					if (fForm != undefined)
					{
						fForm.appendChild(objINPUT);
					
						var eImgNew = document.getElementById(aImgShow);
							eImgNew.setAttribute("src","/tmp/"+sResult.replace("done",""));
						//ObjProgressBar.innerHTML = '<span style="height:100%"></span>';
						//alert(fFile.name+' - '+fFile.size+' - '+fFile.type);						
					}
				}
				else
				{
					ShowMessage('Ocorreu um erro!: '+sResult,true);
				}
			}
			AfterFileUpload(sResult,aTargetPHP,funcAfter);
		}
		else
		{
			sSize = FormatFloatDecimal(aMaxFileSize/1024, 0);
			alert("Ficheiro deve ter "+sSize+"KB no máximo!");
		}
	}
	function INI_FILEUPLOAD(aInputFileName, aPlaceDragDrop, aProgressBarName)
	{
		INI_FILEUPLOAD_MASTER(aInputFileName, aPlaceDragDrop, aProgressBarName, funcUploadImgFile);
	}

	function INI_FILEUPLOAD_MASTER(aInputFileName, aPlaceDragDrop, aProgressBarName, aExtendedFunction)
	{
		var eObjInput = GetObjectByName(aInputFileName,0);
		var eObjDrop  = GetObjectsByTagNameData('DIV', 'data-id', aPlaceDragDrop);
		//var eObjProg  = GetObjectsByTagNameData('SPAN', 'data-id', aProgressBarName);
		//var eObjDivProg = eObjProg.parentNode; // PASSADO "<SPAN>" DEVOLVE "<DIV>"	

		if (window.FileReader) // Firefox 3.6, Chrome 6, WebKit
		{
			var func = function(event)
			{
				/*
				if (eObjDivProg != undefined)
				{
					var classAtt = eObjDivProg.getAttribute("class");
					if (classAtt.indexOf("hidden") !== -1)
					{
						classAtt = classAtt.replace("hidden","");
						eObjDivProg.setAttribute("class",classAtt);
					}
				}
				*/
				var funcProgress = function(event)
				{
					if (event.lengthComputable === true)
					{
						var loaded = (event.loaded / event.total);
						if (loaded < 1)
						{
							//var percent = Math.round((event.loaded * 100) / event.total);
							//eObjProg.style.height = percent+'%';
						}
					}
					var percent = Math.round((event.loaded * 100) / event.total);
					//if ((eObjProg != undefined) || (eObjProg != false))
					//{
					//	eObjProg.style.height = percent+'%';
					//}
					
					console.info("[Status] "+percent+"% done.");
					console.info("[Status] Reading CHUNK. {"+event.loaded+"/"+event.total+"} {"+fFile.name+"}");
					console.info("[Status] Reading DATA. {'"+fFile.name+"'}");
				}
				FILE_ReadStreamToString(event,aExtendedFunction,funcProgress);
			}
			Ini_UploadFile(eObjInput,func);
			DragDropUploader(eObjDrop,func,true);
		}
		else // Safari 5 does not support FileReader
		{
			Ini_UploadFile(eObjInput,aExtendedFunction);	
			DragDropUploader(eObjDrop,aExtendedFunction,false);
		}
	}

	function REMOVE_IMAGEFILE(ASectionOption, AKey, AAction)
	{
		var url 	= '';
		var params 	= '';

		url = '/core/scripts/unset-image.php';
		params += '&section='+ASectionOption;
		params += '&key='+AKey;
		params += '&action='+AAction;

		FMessage = '';
		FTime	 = 0;

		var func = function(sResult)
		{
console.log(sResult);
			if (sResult.indexOf("false") !== -1)
			{
				if (AAction === 'unset')
				{
					FMessage = "Erro ao remover imagem.";
					FTime	 = 3000;
				}
				else
				if (AAction === 'recover')
				{
					FMessage = "Erro ao recuperar imagem.";
					FTime	 = 3000;
				}
			}
			else
			{
				if (AAction === 'unset')
				{
					var objA = document.createElement("a");
						objA.setAttribute('href','javascript:void(0);');
						objA.setAttribute('accesskey','z');
						objA.setAttribute('onclick',"REMOVE_IMAGEFILE('"+ASectionOption+"','"+AKey+"','recover');");
						objA.innerHTML = "Anular";
							FMessage = "Imagem removida. "+objA.outerHTML;
							FTime	 = 20000;
						objA = null;				
				}
				else
				if (AAction === 'recover')
				{
					FMessage = "Operação anulada com sucesso!";
					FTime	 = 3000;
				}
			}

			CREATE_POP_MSG(FMessage,FTime);
		}
		AJAX_REQUEST_CALLBACK("POST",url,params,func);
	}