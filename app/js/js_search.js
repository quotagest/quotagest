/* ************************************************************************************ */
/* ****************************** SEARCH CTT INFORMATION ****************************** */
/* ************************************************************************************ */
	//js_xmlhttp.js
	//main.js
	function CTTError()
	{
		var objInfoBox = GetObjectByClassName('info-box',0);
		SetAttElementObject(objInfoBox,"class","info-box erro");
		objInfoBox.innerHTML = decodeURI("Código Postal incorrecto!");
		
		var objMorada = GetObjectByName("Morada",0);
		objMorada.value = "";	
	}
	function CTTINFO()
	{
		var COD = GetObjectByName("Codigo-Postal",0);
		var value = COD.value;
		var auxcod = value.replace(/-/gi,"");

		var objInfoBox 	= GetObjectByClassName('info-box',0);
		var objMorada 	= GetObjectByName("Morada",0);

		if (auxcod.length == 0)
		{
			SetAttElementObject(objInfoBox,"class","info-box");
			objInfoBox.innerHTML = "Introduza um código postal<span>(ex: 3000-123)</span>";
			objMorada.value = "";
			return false;
		}
		else
		if (auxcod.length === 4)
		{
			if ((auxcod+'-') !== value)
				COD.value = auxcod+'-';
			return false;
		}
		else
		if (auxcod.length === 7)
		{
			value = (auxcod.substr(0,4))+'-'+(auxcod.substr(4,3));
			COD.value = value;
		}
		if (auxcod.length < 7)
			return false;

		if (isNumeric(auxcod))
		{
			auxcod = value.split("-");
			var objCOD1 = auxcod[0];
			var objCOD2 = auxcod[1];
			var url 	= "/core/scripts/get_cttinfo.php"+"?cod1="+objCOD1+"&cod2="+objCOD2+"&up=1";
			var params 	= "";
			
			var func = function(sResult)
			{  
				if (sResult.indexOf("false") !== -1)
				{
					CTTError();
				}
				else
				{
					/*
					var aArr 	  = sResult.split(";");
					var str  = (aArr[0] === aArr[11]) ? aArr[0] : aArr[0]+", "+aArr[11];
						str += ", "+aArr[12];
					objInfoBox.innerHTML = str;
					SetAttElementObject(objInfoBox,"class","info-box");
					
					aArr[0]=''; aArr[10]=''; aArr[11]=''; aArr[12]='';
					objMorada.value = Trim2(aArr.join(" "));
					objMorada.focus();
					*/

					var arrJSON	= JSON.parse(sResult);

					objInfoBox.innerHTML = arrJSON['concelho']+', '+arrJSON['distrito'];
					SetAttElementObject(objInfoBox,"class","info-box");

					objMorada.value = arrJSON['rua'];
					objMorada.focus();
				}
			}

			AJAX_REQUEST_CALLBACK("POST",url,params,func);
		}
		else
			CTTError();
	}
/* ************************************************************************************ */
	function SearchSocioCodigo(sFormName,sFieldName,fCallBack)
	{
		var objSearch 	= document.forms[sFormName].elements[sFieldName];
		var url			= "/core/scripts/find_sociocodigo.php";
		var value 		= objSearch.value;
		var params		= "pesquisa="+value;
			params		+= "&ids="+window.location.href;

		if (value !== "")
		{
			var func = function(sResult)
			{
				console.log('[CodigoSocio] Ajax-Result: '+sResult);
				if (sResult.indexOf("false") !== -1) // IF DIDN'T FIND RECORD, GOOD
				{
					console.log("[CodigoSocio] Exist-Result: Nao Existe.");
					if (fCallBack && typeof(fCallBack) === "function")
						fCallBack();
					return true;
				}
				else
				{
					console.log("[CodigoSocio] Exist-Result: Existe.");
					objSearch.focus();
					CREATE_POP_MSG("O código: <b>'"+value+"'</b> já se encontra atribuido.",5000);
					return false;
				} 
			}

			AJAX_REQUEST_CALLBACK("POST",url,params,func);
		}
		else
		{
			console.log(true);
			if (fCallBack && typeof(fCallBack) === "function")
				fCallBack();
			return true;
		}
	}
/* ************************************************************************************ */
	function SmartSearch(sFormName,sFieldName,vOutput)
	{
		var objOutput 	= GetObjectsByTagNameData('DIV','data-id',vOutput);
		var objSearch 	= document.forms[sFormName].elements[sFieldName];
		var url			= "/core/scripts/search_api.php";
		var params		= "pesquisa="+objSearch.value;
		
		if (objSearch.value !== "")
		{
			var func = function(sResult)
			{  
				if (sResult.indexOf("false") !== -1)
				{

				}
				else
				{
					objOutput.innerHTML = sResult;
					objOutput.setAttribute("class","");
				} 
			}

			AJAX_REQUEST_CALLBACK("POST",url,params,func);
		}
		else
		{
			objOutput.setAttribute("class","hidden");
			objOutput.innerHTML = '';
		}
	}

/* ************************************************************************************ */
	function GetValuesByBI(aFieldBICC)
	{			
		var sBI 	= aFieldBICC.value;
		var url	 	= '/core/scripts/findbi.php?bi='+sBI;
		var params 	= "";
		
		if (sBI.length <= 7)
			return false;
		
		if (isNumeric(sBI))
		{
			var func = function(sResult)
			{  
				if (sResult.indexOf("false") !== -1)
				{
					//Sócio não existe.
				}
				else
				{
					var MR_OPTION = confirm("É este o Sócio que pertende introduzir?\n\nSócio: "+sResult);
					if (MR_OPTION === false)
					{
						return false;
					}
					else
					{
						alert('Desabilita todos os campos desnecessesários.');
						eField = GetObjectByName("Nome",0);
						eField.value = sResult;
						return false;							
					}
					//var arrValues = xmlhttp.responseText;
					//alert('O BI: "'+sBI+'" já se encontra Inserido!');
				} 
			}
			
			AJAX_REQUEST_CALLBACK("GET",url,params,func);
		}
	}
	function GetValuesByNIF(aFieldNIF)
	{			
		//var eField = GetObjectByName("Nome",0);
		
		var sNIF 	= aFieldNIF;
		var url	 	= '/core/scripts/findnif.php?nif='+sNIF;
		var params 	= "";
		if (sNIF.length <= 7)
			return false;
		
		if (isNumeric(sNIF))
		{
			var func = function(sResult)
			{  
				if (sResult.indexOf("false") !== -1)
				{
					//NIF não existe.
					alert('O N.I.F: "'+sNIF+'" ainda não foi Inserido!');
					return false;
				}
				else
				{
					//var arrValues = xmlhttp.responseText;
					//var eField = GetObjectByName("Nome",0);
					//eField.value = xmlhttp.responseText;
					//alert('O BI: "'+sNIF+'" já se encontra Inserido!');
					return true;
				} 
			}
			
			AJAX_REQUEST_CALLBACK("GET",url,params,func);		
		}
	}
/* ************************************************************************************ */
	// <div class="hidden" data-id="SRSocioInsc"></div>
	function SearchSocioForm(eInputSelf,sResultsOutput)
	{
		//var eForm  = document.getElementsByName(sFormName);
		var eResults = GetObjectsByTagNameData('div','data-id',sResultsOutput);
		var sValue = Trim(eInputSelf.value);
		if (sValue !== "")
		{
			var url	 	 = '/core/scripts/getSociosList.php';
			var params	 = "q="+sValue;

			var func = function(sResult)
			{
				if (sResult.indexOf("false") !== -1)
				{
					sResult = sResult.replace("false#","");
					eResults.innerHTML = sResult;
					eResults.setAttribute("class","hidden");
					//return false;
				}
				else
				if (sResult.indexOf("true") !== -1)
				{
					sResult = sResult.replace("true#","");

					var eUL	= createObjElement('ul',['id'],['socios-list']);

					var arrJSON		= JSON.parse(sResult);
					for (var i=0, ccI=arrJSON.length; i<ccI; i++)
					{
						var eLI = createObjElement('li',['data-id','onclick'],
														[arrJSON[i]['id_socio'],"SearchSocioFormLI(this,'"+eInputSelf.name+"');"]);
							eLI.innerHTML = arrJSON[i]['nome'];
							eUL.appendChild(eLI);
					}
					eResults.innerHTML = "";
					eResults.appendChild(eUL);
					eResults.setAttribute("class","");
					//return false;
				}
				else
				{
					eResults.innerHTML = sResult;
				}
			}

			AJAX_REQUEST_CALLBACK("POST",url,params,func);
		}
		else
		{
			eResults.innerHTML = "";
			eResults.setAttribute("class","hidden");
		}
	}
	function SearchSocioOnBlur(sResultsOutput)
	{
		var tt = setTimeout(
							function(){
										var eResults = GetObjectsByTagNameData('div','data-id',sResultsOutput);
											eResults.setAttribute("class","hidden");
									},500);
	}
	function SearchSocioOnFocus(sResultsOutput)
	{
		var eResults = GetObjectsByTagNameData('div','data-id',sResultsOutput);
			eResults.setAttribute("class","");
	}
	function SearchSocioFormLI(eLI,sFieldName)
	{
		var eField 		= document.getElementsByName(sFieldName)[0];
			eField.value= eLI.innerHTML;
		var sFormName  	= FindParentFORMName(eField);

		var eUL = eLI.parentNode;
		var eDIVResult = eUL.parentNode;
			eDIVResult.setAttribute("class","hidden");

		
		eLI.setAttribute("class","selected");
		eUL.getElementsByClassName('selected')[0].removeAttribute('class');
		/*
		var arrLI = eUL.getElementsByTagName("LI");
		for (var i=0, ccI=arrLI.length; i<ccI; i++)
		{
			if (eLI !== arrLI[i])
				arrLI[i].removeAttribute("class");
			else
				eLI.setAttribute("class","selected");
		}
		*/
		var sIDSOC = eLI.getAttribute("data-id");

		if (sFormName !== '')
		{
			var eInputHidden = document.forms[sFormName].elements['CodSocio'];
			if (!eInputHidden)
				eInputHidden = createObjElement('input',['type','name','value'],['hidden','CodSocio',sIDSOC]);
			else
				eInputHidden.value = sIDSOC;

			document.forms[sFormName].appendChild(eInputHidden);
			window.location.href = ReplaceParamsHREF('ids=','ids='+sIDSOC);
		}
		else
		{
			window.location.href = ReplaceParamsHREF('ids=','ids='+sIDSOC);
		}
	}
	/* ************** LIMPA LINHS EXISTENTES ************** */
	function RemoveLines(eElemResult)
	{
		var arrLinhas = eElemResult.getElementsByClassName("linha");
		for (var i=arrLinhas.length-1; i>0; i--)
		{
			if (arrLinhas[i].className === "linha")
			{
				eElemResult.removeChild(arrLinhas[i].parentNode);
			}
		}
	}
	/* **************************************************** */
	function SearchListagem(sFormName,sFieldName,sResultsOutput)
	{
		//var eForm  = document.getElementsByName(sFormName);
		var eResults = GetObjectsByTagNameData('div','data-cod',sResultsOutput);
		//var eResults = GetObjectsByTagNameData('span','data-id','linhas');
		var eField = document.forms[sFormName].elements[sFieldName];
		var sValue = Trim(eField.value);
			sValue = (sValue !== "") ? sValue : '*';

		switch (sResultsOutput)
		{
			case 'socios-listagem'		: var url	= '/core/scripts/getSociosList.php'; break;
			case 'entidades-listagem'	: var url	= '/core/scripts/getEntidadesList.php'; break;
		}
		var params	 = "q="+sValue;
			params	+= "&limit="+"0,10";

		var func = function(sResult)
		{
			if (sResult.indexOf("false") !== -1)
			{
				RemoveLines(eResults);
			}
			else
			if (sResult.indexOf("true") !== -1)
			{
				sResult = sResult.replace("true#","");
				switch (sResultsOutput)
				{
					case 'socios-listagem'		: SearchSocioListagem(eResults,sResult); break;
					case 'entidades-listagem'	: SearchEntidadeListagem(eResults,sResult);  break;
				}
			}
			else
			{

			}
		}

		AJAX_REQUEST_CALLBACK("POST",url,params,func);
	}
	function SearchSocioListagem(eElemResult,sJSONResult)
	{
		/* ************** LIMPA LINHAS EXISTENTES ************** */
		RemoveLines(eElemResult);
		/* **************************************************** */
		var ElemMulti = eElemResult.getElementsByClassName("multi")[0];
		
		var eItem 	= new Array();
		var arrJSON = JSON.parse(sJSONResult);

		for (var i=0, ccI=arrJSON.length; i<ccI; i++)
		{
			var sColor = '';
			if (arrJSON[i]['divida'] > 0.0)
				sColor = ' green';
			else
			if (arrJSON[i]['divida'] == 0.0)
				sColor = '';
			else
				sColor = ' red';
			
			eItem[i] = new Array();
			eItem[i][0] = "<a href='perfil.php?id="+arrJSON[i]['id_socio']+"'>";
			eItem[i][1] = "<div class='linha'>";
			// COLUNA: CHECKBOX
			eItem[i][2] = "<span class='col-short'>";
			eItem[i][3] = "<input type='checkbox' name='checkAction' data-id='"+arrJSON[i]['id_socio']+"'>";
			eItem[i][4] = "</span>";
			// COLUNA: CODIGO
			eItem[i][5] = "<span class='col-short'>"+arrJSON[i]['codigo']+"</span>";
			// COLUNA: NOME
			eItem[i][6] = "<span class='col-long'>"+arrJSON[i]['nome']+"</span>";
			// COLUNA: TIPO SÓCIO
			eItem[i][7] = "<span class='col'>";
			eItem[i][8] = "<span class='etiqueta verde'>"+arrJSON[i]['socio_nome_tipo']+"</span>";
			eItem[i][9] = "</span>";
			// COLUNA: ESTADO SÓCIO
			eItem[i][10] = "<span class='col'>";
			eItem[i][11] = "<span class='etiqueta azul'>"+arrJSON[i]['socio_nome_estado']+"</span>";
			eItem[i][12] = "</span>";
			// COLUNA: SALDO
			eItem[i][13] = "<span class='col right numero"+sColor+"'>"+"€"+arrJSON[i]['divida']+"</span>";
			eItem[i][14] = "</div>";
			
			eItem[i] = eItem[i].join("\n");
		}
		ElemMulti.insertAdjacentHTML('beforebegin',eItem.join("\n"));
	}
	function SearchEntidadeListagem(eElemResult,sJSONResult)
	{
		/* ************** LIMPA LINHS EXISTENTES ************** */
		RemoveLines(eElemResult);
		/* **************************************************** */
		var ElemMulti = eElemResult.getElementsByClassName("multi")[0];

		var arrJSON	= JSON.parse(sJSONResult);
		for (var i=0, ccI=arrJSON.length; i<ccI; i++)
		{
			var eA 		= createObjElement('a',['href'],['perfil.php?id='+arrJSON[i]['id_entidade']]);
			var eLinha 	= createObjElement('div',['class'],['linha']);
			/* COLUNA: CHECKBOX */
			var eLinha_ColCheck 			= createObjElement('span',['class'],['col-short']);
				eLinha_ColCheck.innerHTML 	= createObjElement('input',['type','name','data-id'],['checkbox','checkAction',arrJSON[i]['id_entidade']]).outerHTML;
			/* COLUNA: NOME */
			var eLinha_A_ColLong  			= createObjElement('span',['class'],['col-long']);
				eLinha_A_ColLong.innerHTML = arrJSON[i]['nome'];
			/* COLUNA: ABREVIATURA */
			var eLinha_A_Col1  				= createObjElement('span',['class'],['col']);
				eLinha_A_Col1.innerHTML 	= arrJSON[i]['abreviatura'];
			/* COLUNA: TIPO ENTIDADE */
			var sEntidadeTipo = arrJSON[i]['tipo'];

			var eLinha_A_ColRight  			= createObjElement('span',['class'],['col right']);
			var eLinha_A_ColRight_Eti		= createObjElement('span',['class'],['etiqueta verde']);
				eLinha_A_ColRight_Eti.innerHTML = sEntidadeTipo;

			/* COLUNA: NOME */
				eLinha.appendChild(eLinha_ColCheck);
			/* COLUNA: NOME */
				eLinha.appendChild(eLinha_A_ColLong);
			/* COLUNA: ABREVIATURA */
				eLinha.appendChild(eLinha_A_Col1);
			/* COLUNA: SALDO */
				eLinha_A_ColRight.appendChild(eLinha_A_ColRight_Eti);
				eLinha.appendChild(eLinha_A_ColRight);

				eA.appendChild(eLinha);
				//eElemResult.appendChild(eA);
				eElemResult.insertBefore(eA, ElemMulti);
		}
	}