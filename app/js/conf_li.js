			/* *********************************************** */
			/* ***** TRABALHAR COM LI "cfg-editar-dados" ***** */
			/* *********************************************** */
			function KeysUpDownInputSubmit(event,ASelf,sExtra)
			{
				event = event || window.event;
				var keycode = event.charCode || event.keyCode;

				//console.log(event.which);
				//if ((event.which == 13) && (event.shiftKey))
				if (event.which == 13)
				{
					//console.log('SHIFT+ENTER');
					//console.log(sExtra);
					GuardarLI(ASelf,sExtra);
				}
			}
			function LIgetObj_ByTagName(aObj,sTagName)
			{
				var i=0;
				sTagName = sTagName.toUpperCase();
				do
				{
					aObj = aObj.parentNode;
					if (aObj.tagName === sTagName)
						return aObj;
				}
				while (i<5);
			}
			function SearchLI(eInputElem,sULDataName)
			{
				var objUL = GetObjectsByTagNameData("UL","data-name",sULDataName);

				if (!objUL || objUL.length == 0) 											// IF THERE IS NO LIs INSIDE <UL></UL>
					return;

				var sInputValue = eInputElem.value;
					sInputValue = sInputValue.toLowerCase();
				var arrLI = objUL.getElementsByTagName("LI");
				for (var i=0, ccI=arrLI.length; i<ccI; i++)
				{
					var bFound 	= true;
					var eA 		= arrLI[i].children[0];
					//alert(arrLI[i].children[0].innerHTML);
					//alert(arrLI[i].childNodes[0].innerHTML);
					//alert(arrLI[i].firstChild.innerHTML);

					var sAValue = eA.innerHTML;
						sAValue = sAValue.toLowerCase();
					if (sInputValue !== '')
					{
						bFound = false;
						if (sAValue.indexOf(sInputValue) !== -1)
						{
							// ENCONTROU LI {filtro}
							bFound = true;
						}
					}
					if (bFound)
						arrLI[i].setAttribute("class","");
					else
						arrLI[i].setAttribute("class","hidden");
				};
			}
			function InserirLI(sFormName,sULDataName,sFields)
			{
				GET_SCROLL_POS();

				var objUL 		= GetObjectsByTagNameData("UL","data-name",sULDataName);
				var aFields 	= sFields.split(";");
				var value 		= '';
				var key 		= '';
				var paramsAux 	= '';
				var sValuesTitle= new Array();
				var error		= false;
				var eFormElem;
				var errorInput;
				for (var i=0, cc = aFields.length; i<cc; i++)
				{
					eFormElem = document.forms[sFormName].elements[aFields[i]];
					switch (eFormElem.tagName)
					{
						case 'INPUT':
							{
								key 	= eFormElem.getAttribute("Name");
								value 	= eFormElem.value;
							} break;
					}
					if (value === "")
					{
						error  = true;
						CREATE_POP_MSG("O campo encontra-se vazio!");
						eFormElem.focus();
						break;
					}
					else
					{
						if (key === "Joia")
						{
							var vStr = FloatToString(value);
							if (isFloat(vStr))
							{
								value = FloatToCurrency(vStr);
								value = value.replace('€','');
							}
							else
							{
								error = true;
								CREATE_POP_MSG("O valor introduzido não é válido!");
								eFormElem.focus();
								break;
							}
						}
					}

					paramsAux 	 += (i==0) ? '' : '&';
					paramsAux 	 += key+'='+value;
					sValuesTitle[i] = value;
				}
				var eSelect = objUL.parentNode;
					eSelect = eSelect.getElementsByTagName('select')[0];
				if (eSelect != undefined)
				{
					key 	= eSelect.getAttribute("data-name");
					value 	= eSelect.options[eSelect.selectedIndex].value;
					paramsAux += (i==0) ? '' : '&';
					paramsAux += key+'='+value;			
				}

				if (!error)
				{
					sValuesTitle = sValuesTitle.join(' | ');
					AjaxGuardarInserirLI(objUL,paramsAux,sValuesTitle,sFields);
					for (var i=aFields.length-1; i>-1; i--)
					{
						eInputElem = document.forms[sFormName].elements[aFields[i]];
						eInputElem.value = '';
						eInputElem.focus();
					}
				}
			}
/*
			function InserirLI(sULDataName,sFields)
			{
				GET_SCROLL_POS();
				
				var objUL = GetObjectsByTagNameData("UL","data-name",sULDataName);
				
				var nodeLI = document.createElement("li");
				var objDIV = document.createElement("div");
					objDIV.setAttribute('class','edicao');

				var aFields = sFields.split(";");
				for (var i=0, cc = aFields.length; i<cc; i++)
				{
					
					var objINPUT = document.createElement("input");
						objINPUT.setAttribute('type','text');
						objINPUT.setAttribute('name',aFields[i]);
						objINPUT.setAttribute('placeholder',aFields[i]);
						objINPUT.setAttribute('value','');
						objINPUT.setAttribute('onkeypress',"KeysUpDownInputSubmit(event,this,null);");
						
					objDIV.appendChild(objINPUT);
				}

				var objA	= document.createElement("a");
					objA.setAttribute('class','hidden');
					objA.setAttribute('href','#');
					objA.setAttribute('data-fields',sFields);
					objA.setAttribute('onclick',"EditarLI(this); DONTSCROLL(); return false;");
					objA.innerHTML = "";

				var objCANC = document.createElement("span");
					objCANC.setAttribute('class','botao cancelar');
					objCANC.setAttribute('onclick',"RemoverLI(this);");
					objCANC.innerHTML = "cancelar"; 
				var objGUAR = document.createElement("span");
					objGUAR.setAttribute('class','botao guardar');
					objGUAR.setAttribute('onclick',"GuardarLI(this,null);");
					objGUAR.innerHTML = "Guardar"; 

				objDIV.appendChild(objINPUT);
				objDIV.appendChild(objCANC);
				objDIV.appendChild(objGUAR);
				nodeLI.appendChild(objA);
				nodeLI.appendChild(objDIV);
				objUL.insertBefore(nodeLI,objUL.firstChild);

				var objINPUT = objDIV.getElementsByTagName('input')[0];
				objINPUT.focus();
			}
*/
			function EditarLI(aObj,aStr)
			{
				GET_SCROLL_POS();

				var nodeLI = aObj.parentNode; 	//Objecto passado é <A></A>, e o seu Parent é o <LI></LI>
				var objUL = nodeLI.parentNode;	//Objecto passado é <LI></LI>, e o seu Parent é o <UL></UL>

				
				var objDIV = document.createElement("div");
					objDIV.setAttribute('class','edicao');
				
				aObj.setAttribute('class','hidden');
				var aFields = aObj.getAttribute("data-fields");
				var aValues	= aObj.innerHTML;
				if (aFields != null)
				{
					aFields = aFields.split(";");
					aValues = aValues.split("|");
					for (var i=0, cc = aFields.length; i<cc; i++)
					{
						var objINPUT = document.createElement("input");
							objINPUT.setAttribute('type','text');
							objINPUT.setAttribute('name',aFields[i]);
							objINPUT.setAttribute('placeholder',aFields[i]);
							objINPUT.setAttribute('value',Trim2(aValues[i]));
							objINPUT.setAttribute('class','input block');
							objINPUT.setAttribute('onkeypress',"KeysUpDownInputSubmit(event,this,'"+aStr+"');");

						objDIV.appendChild(objINPUT);
					}
				}
				
				var objDEL = document.createElement("span");
					objDEL.setAttribute('class','botao delete');
					objDEL.setAttribute('onclick',"ApagarLI(this,'"+aStr+"');");
					objDEL.innerHTML = "apagar";
				var objCANC = document.createElement("span");
					objCANC.setAttribute('class','botao cancelar');
					objCANC.setAttribute('onclick',"CancelarLI(this);");
					objCANC.innerHTML = "cancelar";
				var objGUAR = document.createElement("span");
					objGUAR.setAttribute('class','botao guardar');
					objGUAR.setAttribute('onclick',"GuardarLI(this,'"+aStr+"');");
					objGUAR.innerHTML = "Guardar"; 

				//objDIV.appendChild(objINPUT);
				objDIV.appendChild(objDEL);
				objDIV.appendChild(objCANC);
				objDIV.appendChild(objGUAR);
				nodeLI.appendChild(objDIV);
				
				var objINPUT = objDIV.getElementsByTagName('input')[0];
				objINPUT.focus();
			}
			function GuardarLI(aObj,aStr)
			{
				var objDIV = aObj.parentNode; 	//Objecto passado é <SPAN></SPAN>, e o seu Parent é o <DIV></DIV>
				var nodeLI = objDIV.parentNode;	//Objecto passado é <DIV></DIV>, e o seu Parent é o <LI></LI>
				
				var nodeUL = nodeLI.parentNode; //Objecto passado é <LI></LI>, e o seu Parent é o <UL></UL>
				var nodeDIV = nodeUL.parentNode; //Objecto passado é <UL></UL>, e o seu Parent é o <DIV></DIV>
				
				var error	= false;
				var aINPUT 	= objDIV.getElementsByTagName('input');
				var aSELECT = nodeDIV.getElementsByTagName('select')[0];
				var Values 	= "";
				var valTitle= new Array();
				var InputP	= 0;
				for (var i=0, cc = aINPUT.length; i<cc; i++)
				{
					var sValue 	= Trim2(aINPUT[i].value);
					if (sValue === "")
					{
						error	= true;
						InputP = i;
						break;
					}	
					var sName 	= aINPUT[i].getAttribute("Name");
					Values += sName+"="+sValue;
					Values += ((cc>1) && (i<cc-1)) ? "&" : "";
					valTitle[i] = sValue;
				}
				/* http://stackoverflow.com/questions/1085801/how-to-get-the-selected-value-of-dropdownlist-using-javascript/1085813#1085813 */
				if (aSELECT)
				{
					Values += "&"+aSELECT.getAttribute("data-name")+"="+aSELECT.options[aSELECT.selectedIndex].value;
				}

				if (!error)
				{
					valTitle = valTitle.join(' | ');
					AjaxGuardarLI(aObj,valTitle,Values,aStr,nodeLI,objDIV);
				}
				else
				{
					CREATE_POP_MSG("O campo encontra-se vazio!");
					aINPUT[InputP].focus();
				}
			}
			function CancelarLI(aObj)
			{
				var objDIV = aObj.parentNode; 	//Objecto passado é <SPAN></SPAN>, e o seu Parent é o <DIV></DIV>
				var nodeLI = objDIV.parentNode;	//Objecto passado é <DIV></DIV>, e o seu Parent é o <LI></LI>
				
				var aObjA = nodeLI.getElementsByTagName('a')[0];
					RemoveAttElementObject(aObjA,'class');

				nodeLI.removeChild(objDIV);
			}
			function RemoverLI(aObj)
			{
				var objDIV = aObj.parentNode; 	//Objecto passado é <SPAN></SPAN>, e o seu Parent é o <DIV></DIV>
				var nodeLI = objDIV.parentNode;	//Objecto passado é <DIV></DIV>, e o seu Parent é o <LI></LI>
				var objUL  = nodeLI.parentNode;	//Objecto passado é <DIV></DIV>, e o seu Parent é o <LI></LI>
				
				objUL.removeChild(nodeLI);
			}
			function ApagarLI(aObj,aStr)
			{
				if (aStr !== "")
				{
					var sOption = LIgetObj_ByTagName(aObj,"UL");
						sOption = sOption.getAttribute("data-name");
					
					var sObj = aObj.parentNode; //Objecto passado é <SPAN></SPAN>, e o seu Parent é o <DIV></DIV>
						sObj = sObj.parentNode; //Objecto passado é <DIV></DIV>, e o seu Parent é o <LI></LI>
						sObj = sObj.getElementsByTagName('a')[0].innerHTML;
					
					MESSAGE_AnularApagar('Apagou o registo "'+sObj+'".',aStr,sOption);
					AjaxApagarLI(aObj,aStr);
				}
				else
				{
					CREATE_POP_MSG("Impossivel Apagar!");
				}			
			}
			function AjaxApagarLI(aObj,sID)
			{
				if (sID !== "")
				{
					var sOption = LIgetObj_ByTagName(aObj,"UL");
						sOption = sOption.getAttribute("data-name");
					
					var sInfo 	= sID+"#"+sOption;
					var url 	= '/core/plus_conf_delete.php';
					var params 	= "encrypt="+sInfo;
					
					var func = function(sResult)
					{  
						if (sResult.indexOf("true") !== -1)
						{
							var nodeLI = aObj.parentNode; 	//Objecto passado é <SPAN></SPAN>, e o seu Parent é o <LI></LI>
							var objUL  = nodeLI.parentNode;	//Objecto passado é <LI></LI>, e o seu Parent é o <UL></UL>
							
							objUL.removeChild(nodeLI);
						}
						else
						{
							CREATE_POP_MSG("Erro!\n\n'"+sResult+"'");
						} 
					}
					
					AJAX_REQUEST_CALLBACK("POST",url,params,func);
				}
				else
				{
					CREATE_POP_MSG("Impossivel Apagar!");
				}
			}
			function AjaxGuardarInserirLI(AobjUL,AParamsAux,AsValuesTitle,AsFields)
			{
				var sOption = AobjUL.getAttribute("data-name");
				var sInfo 	= ''+"#"+sOption;
				var url 	= '/core/plus_conf_both.php';
				var params 	= "encrypt="+sInfo;
					params += "&"+AParamsAux;

				var func = function(sResult)
				{
			console.log(sResult);
					if (sResult.indexOf("true") !== -1)
					{
						sResult = Trim2(sResult.replace("true",""));

						var objLI = document.createElement("li");
						var objA = document.createElement("a");
							objA.setAttribute('href','#');
							objA.setAttribute('data-fields',AsFields);
							objA.setAttribute('onclick',"EditarLI(this,"+"'"+sResult+"'"+");");
							objA.innerHTML = AsValuesTitle;
						objLI.appendChild(objA);
						AobjUL.insertBefore(objLI,AobjUL.firstChild);

						AfterInsertActividade(sOption,AsValuesTitle,sResult);
					}
					else
					{
						CREATE_POP_MSG("Erro!\n\n'"+sResult+"'");
					} 
				}
				console.log(params);
				AJAX_REQUEST_CALLBACK("POST",url,params,func);
			}
			function AjaxGuardarLI(aObj,sValuesTitle,sFieldValue,sID,oLI,oDIV)
			{
				var sID   = (sID != undefined) ? sID : '';
				var error = false;
				
				if (aObj.tagName !== "UL")
				{
					var sOption = LIgetObj_ByTagName(aObj,"UL");
						sOption = sOption.getAttribute("data-name");
				}
				else
				{
					sOption = aObj.getAttribute("data-name");
				}
				
				var sInfo 	= sID+"#"+sOption;
				var url 	= '/core/plus_conf_both.php';
				var params 	= "encrypt="+sInfo;
					params += "&"+sFieldValue;
				
				/* ********** VERIFICAR CAMPO joia ********** */
/*
				var aParams = sFieldValue.split("&");
				var asFV 	= "";
				var InputP	= 0;
				for (var i=0, cc=iNFields; i<cc; i++)
				{
					sStr = aParams[i].split("=");
					if (sStr[0] === "Joia")
					{
						var vStr = FloatToString(sStr[1]);
						if (isFloat(vStr))
						{
							asFV += FloatToCurrency(vStr);
						}
						else
						{
							var aINPUT 	= oDIV.getElementsByTagName('input');
							error = true;
							CREATE_POP_MSG("O valor introduzido não é válido!");
							aINPUT[i].focus();
						}
					}
					else
						asFV += sStr[1];
						
					asFV += ((cc>1) && (i<cc-1)) ? " | " : "";
				}
*/
				/* ****************************************** */
	//			if (!error)
	//			{
					var func = function(sResult)
					{  
						if (sResult.indexOf("true") !== -1)
						{
							var aObjA = oLI.getElementsByTagName('a')[0];
								aObjA.innerHTML = sValuesTitle;
								RemoveAttElementObject(aObjA,'class');	
						
							sResult = Trim2(sResult.replace("true",""));
							if (sResult.length != 0)
								aObjA.setAttribute('onclick',"EditarLI(this,"+"'"+sResult+"'"+");");

							oLI.removeChild(oDIV);

							if (sResult === '')
								sResult = sID;
							AfterInsertActividade(sOption,sValuesTitle,sResult);
						}
						else
						{
							CREATE_POP_MSG("Erro!\n\n'"+sResult+"'");
						} 
					}

					AJAX_REQUEST_CALLBACK("POST",url,params,func);
	//			}
			}
			function SELECTChangeLI(eObj)
			{
				var eDIV	= eObj.parentNode; //Objecto passado é <SELECT></SELECT>, e o seu Parent é o <DIV></DIV>
				var sInfo 	= eObj.options[eObj.selectedIndex].value+"#"+eObj.getAttribute("data-name");
				var url 	= '/core/plus_selectli.php';
				var params 	= "encrypt="+sInfo;
				
				var func = function(sResult)
				{  	
					var objSPAN = eDIV.getElementsByTagName("SPAN")[0];
					var objUL = eDIV.getElementsByTagName("UL")[0];
					if (objUL)
						eDIV.removeChild(objUL);
					
					if (!objSPAN)
					{
						var objSPAN	= document.createElement("span");
							objSPAN.setAttribute('class','botoes');
							objSPAN.innerHTML = "";
							
						var objASpan = document.createElement("a");
							objASpan.setAttribute('class','botao novo');
							objASpan.setAttribute('href','#');
							objASpan.setAttribute('onclick',"InserirLI('"+eObj.getAttribute("data-name")+"','Nome'); DONTSCROLL(); return false;");
							objASpan.innerHTML = "adicionar";
							
						objSPAN.appendChild(objASpan);
						eDIV.appendChild(objSPAN);
					}

					var objUL	= document.createElement("ul");
						objUL.setAttribute('class','lista');
						objUL.setAttribute('data-name',eObj.getAttribute("data-name"));

					eDIV.appendChild(objUL);
						
					if (sResult.indexOf("true") !== -1)
					{
						sResult = sResult.replace("true","");
						aStr 	= sResult.split(";");
						
						for (var i=0, cc=aStr.length; i<cc; i++)
						{
							aaStr = aStr[i].split("#");
							
							var objA	= document.createElement("a");
								objA.setAttribute('href','#');
								objA.setAttribute('data-fields','Nome');
								objA.setAttribute('onclick',"EditarLI(this,'"+aaStr[0]+"'); DONTSCROLL(); return false;");
								objA.innerHTML = aaStr[1];
								
							var objLI	= document.createElement("li");
								objLI.appendChild(objA);
								objUL.appendChild(objLI);
						}
					}
					else
					{
						// NAO ENCONTROU DADOS!
						//alert("Erro!\n\n'"+sResult+"'");
					} 

				}
				AJAX_REQUEST_CALLBACK("POST",url,params,func);			
			}
			function MESSAGE_AnularApagar(sDescription, sEncrypt, sOption)
			{
				var objSection = GetObjectsByTagName("SECTION")[1];
				var objMSG = GetObjectByClassName('ctrlz',0);
				var error = true;
				if (objMSG == undefined)
				{
					objMSG = document.createElement("div");
					objMSG.setAttribute('class','ctrlz');
				}
				else
				{
					error = false;
				}
				var objA = document.createElement("a");
					objA.setAttribute('href','#');
					objA.setAttribute('accesskey','z');
					objA.setAttribute('onclick',"MSG_Anular(this);");
					objA.setAttribute('data-info',sEncrypt+'#'+sOption);
					objA.innerHTML = "anular <em>(alt+Z)</em>";
				
				objMSG.innerHTML = sDescription+" ";		
				objMSG.appendChild(objA);	
				
				if (objMSG && error)
				{		
					objSection.appendChild(objMSG);
				}
				/*
					// addEventListener
					// LINK: http://www.asnippet.com/one_snippet.php?command=getSnippet&snippetId=1757
				*/
				TIMER = setTimeout(function(){MSG_Apagar(objA);},10000);
			}

			function MSG_Anular(aAObj)
			{
				clearTimeout(TIMER);
				
				var eDIV	= aAObj.parentNode;
				var	eSEC	= eDIV.parentNode;
				var sInfo   = aAObj.getAttribute('data-info');

				var url 	= '/core/plus_undelete.php';
				var params 	= "encrypt="+sInfo;
				
				var func = function(sResult)
				{  
					if (sResult.indexOf("true") !== -1)
					{
						window.location.reload();
						//eSEC.removeChild(eDIV);
					}
					else
					{
						CREATE_POP_MSG("Erro!\n\n'"+sResult+"'");
					} 
				}
				
				AJAX_REQUEST_CALLBACK("POST",url,params,func);	
			}
			function MSG_Apagar(aAObj)
			{
				var eDIV	= aAObj.parentNode;
				var	eSEC	= eDIV.parentNode;
				var sInfo   = aAObj.getAttribute('data-info');

				var url 	= '/core/plus_delete.php';
				var params 	= "encrypt="+sInfo;
				
				var func = function(sResult)
				{  
					if (sResult.indexOf("true") !== -1)
					{
						eSEC.removeChild(eDIV);

						AfterDeleteActividade(sInfo);
					}
					else
					{
						CREATE_POP_MSG("Erro!\n\n'"+sResult+"'");
					} 
				}
				
				AJAX_REQUEST_CALLBACK("POST",url,params,func);
			}

			/* *** COPIAR Modalidade para SELECTS *** */
			function AfterInsertActividade(AsOption,AText,AID)
			{
				if (AsOption === 'modalidade')
				{
					var eSelectNiveis = GetObjectsByTagNameData('SELECT','data-name','modsocnivel');
					var eSelectTurmas = GetObjectsByTagNameData('SELECT','data-name','modturma');

					/* PROCURA NA SELECT A VER SE JA EXISTE */
					var bFound = false;
					var arrChilds = eSelectNiveis.getElementsByTagName("OPTION");
					for (var i=0, ccI=arrChilds.length; i<ccI; i++)
					{
						if (arrChilds[i].getAttribute("value") === AID)
						{
							arrChilds[i].innerHTML = AText;
							bFound = true;
							break;
						}
					}
					if (bFound)
					{
						var arrChilds = eSelectTurmas.getElementsByTagName("OPTION");
						for (var i=0, ccI=arrChilds.length; i<ccI; i++)
						{
							if (arrChilds[i].getAttribute("value") === AID)
							{
								arrChilds[i].innerHTML = AText;
								bFound = true;
								break;
							}
						}	
					}
					/* ******************************** */
					if (!bFound)
					{
						var eOpt2Niveis = eSelectNiveis.childNodes[1];
						var eOpt2Turmas = eSelectTurmas.childNodes[1];

						var elemOption = document.createElement('option');
							elemOption.setAttribute("value",AID);
							elemOption.innerHTML = AText;

						var elemOption2 =  elemOption.cloneNode(true);

						eSelectTurmas.insertBefore(elemOption,eOpt2Turmas);
						eSelectNiveis.insertBefore(elemOption2,eOpt2Niveis);
					}
				}
			}
			function AfterDeleteActividade(AsInformation)
			{
				var arrInfor = AsInformation.split('#');
				var AsID	 = arrInfor[0];
				var AsOption = arrInfor[1];

				if (AsOption === 'modalidade')
				{
					var eSelectNiveis = GetObjectsByTagNameData('SELECT','data-name','modsocnivel');
					var eSelectTurmas = GetObjectsByTagNameData('SELECT','data-name','modturma');

					if (eSelectNiveis != undefined)
					{
						var arrChilds = eSelectNiveis.getElementsByTagName("OPTION");
						for (var i=0, ccI=arrChilds.length; i<ccI; i++)
						{
							if (arrChilds[i].getAttribute("value") === AsID)
							{
								//console.log(arrChilds[i].innerHTML);
								eSelectNiveis.removeChild(arrChilds[i]);
								break;
							}
						}
						SELECTChangeLI(eSelectNiveis);						
					}

					if (eSelectTurmas != undefined)
					{
						var arrChilds = eSelectTurmas.getElementsByTagName("OPTION");
						for (var i=0, ccI=arrChilds.length; i<ccI; i++)
						{
							if (arrChilds[i].getAttribute("value") === AsID)
							{
								//console.log(arrChilds[i].innerHTML);
								eSelectTurmas.removeChild(arrChilds[i]);
								break;
							}
						}
						SELECTChangeLI(eSelectTurmas);
					}
				}
			}